# idev_jfx

#### 介绍
>javafx开发客户端项目
>基于jfeonix模板开发

#### 软件架构
>客户端
jdk1.8
javafx
datafx8.0.1
jfoenix8.0.10
tilesfx
openfeign
controlsfx
kotlin
spring

>服务端
springboot
mybatis
websocket

#### 图片演示

![输入图片说明](image.png)
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/103155_f1ca251e_1549247.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/103212_f517f275_1549247.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/103236_063c960e_1549247.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/103325_3188ed3c_1549247.png "屏幕截图.png")
#### 实现功能

1.  权限管理
2.  图片上传（头像展示）
3.  国际化
4.  主题切换
5.  websocket消息
6.  自定义事件捕捉
7.  其他示例

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
