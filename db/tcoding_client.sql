/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 80019
Source Host           : localhost:3306
Source Database       : tcoding_client

Target Server Type    : MYSQL
Target Server Version : 80019
File Encoding         : 65001

Date: 2022-07-20 11:42:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_client_service
-- ----------------------------
DROP TABLE IF EXISTS `auth_client_service`;
CREATE TABLE `auth_client_service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178295930045` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of auth_client_service
-- ----------------------------
INSERT INTO `auth_client_service` VALUES ('1', '3', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('2', '2', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('3', '2', '3', null, '2017-12-31 08:58:03', 'null', 'null', 'null', null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('16', '1', '7', null, '2018-11-15 19:50:50', '9', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('17', '2', '7', null, '2018-11-15 19:50:50', '9', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('18', '3', '7', null, '2018-11-15 19:50:50', '9', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('19', '7', '7', null, '2018-11-15 19:50:50', '9', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_element
-- ----------------------------
DROP TABLE IF EXISTS `base_element`;
CREATE TABLE `base_element` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `menu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `path` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178345917581` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_element
-- ----------------------------
INSERT INTO `base_element` VALUES ('4', 'menuManager:element', 'uri', '按钮页面', '/admin/element', '6', null, null, 'GET', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('5', 'menuManager:btn_add', 'button', '新增', '/menu/{*}', '6', null, null, 'POST', '', '2020-12-03 13:25:44', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('6', 'menuManager:btn_edit', 'button', '编辑', '/menu', '6', '', '', 'PUT', '', '2020-12-03 13:26:03', '1', 'admin', '127.0.0.1', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES ('7', 'menuManager:btn_del', 'button', '删除', '/menu/{*}', '6', '', '', 'DELETE', '', '2020-12-03 13:26:12', '1', 'admin', '127.0.0.1', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES ('8', 'menuManager:btn_element_add', 'button', '新增元素', '/element', '6', null, null, 'POST', '', '2020-12-03 13:26:26', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('9', 'menuManager:btn_element_edit', 'button', '编辑元素', '/element', '6', null, null, 'PUT', '', '2020-12-03 13:26:37', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('10', 'menuManager:btn_element_del', 'button', '删除元素', '/element/{*}', '6', null, null, 'DELETE', '', '2020-12-03 13:26:43', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('12', 'groupManager:btn_edit', 'button', '编辑', '/group', '7', null, null, 'PUT', '', '2020-12-03 13:46:59', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('14', 'groupManager:btn_userManager', 'button', '分配用户', '/group/{*}/user', '7', null, null, 'PUT', '', '2020-12-03 13:26:58', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('15', 'groupManager:btn_resourceManager', 'button', '分配权限', '/group/{*}/authority/menu', '7', null, null, 'PUT', '', '2020-12-03 13:48:31', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('19', 'menuManager:view', 'uri', '查看', '/admin/menu/{*}', '6', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES ('20', 'menuManager:element_view', 'uri', '查看', '/admin/element/{*}', '6', null, null, 'GET', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('22', 'groupTypeManager:view', 'uri', '查看', '/admin/groupType/{*}', '8', null, null, 'GET', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('23', 'groupTypeManager:btn_add', 'button', '新增', '/groupType', '8', null, null, 'POST', null, '2020-12-03 13:17:51', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('24', 'groupTypeManager:btn_edit', 'button', '编辑', '/groupType', '8', null, null, 'PUT', null, '2020-12-03 13:22:14', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('25', 'groupTypeManager:btn_del', 'button', '删除', '/groupType/{*}', '8', null, null, 'DELETE', null, '2020-12-03 13:18:12', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('27', 'serviceManager:view', 'URI', '查看', '/auth/service/{*}', '10', null, null, 'GET', null, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('28', 'serviceManager:btn_add', 'button', '新增', '/auth/service', '10', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('29', 'serviceManager:btn_edit', 'button', '编辑', '/auth/service/{*}', '10', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('30', 'serviceManager:btn_del', 'button', '删除', '/auth/service/{*}', '10', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('31', 'serviceManager:btn_clientManager', 'button', '服务授权', '/auth/service/{*}/client', '10', null, null, 'POST', null, '2017-12-30 16:32:48', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('34', 'home', 'uri', '查看', 'home/', '16', null, null, 'GET', null, '2018-11-08 17:50:05', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('37', 'userManager:btn_add', 'button', '添加', '/user', '1', null, null, 'POST', '', '2020-12-03 13:23:40', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('38', 'userManager:btn_del', 'button', '删除', '/user/{*}', '1', null, null, 'DELETE', '', '2020-12-03 13:24:35', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('39', 'userManager:btn_edit', 'button', '修改', '/user/{*}', '1', null, null, 'PUT', '', '2020-12-03 13:35:36', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('42', 'groupManager:btn_del', 'button', '删除', '/group', '7', null, null, 'DELETE', '', '2020-12-03 13:48:03', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('41', 'groupManager:btn_add', 'button', '新增', '/group', '7', null, null, 'POST', '', '2020-12-03 13:47:29', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('40', 'userManage:rest', 'button', '重置密码', '/user/password/{*}', '1', null, null, 'PUT', '', '2020-12-03 13:42:38', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('43', 'userManager:btn_add', 'button', '新增', '', '74', null, null, 'GET', '', '2022-06-08 14:29:53', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_group
-- ----------------------------
DROP TABLE IF EXISTS `base_group`;
CREATE TABLE `base_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent_id` int NOT NULL,
  `path` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `group_type` int NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` timestamp NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178352287356` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_group
-- ----------------------------
INSERT INTO `base_group` VALUES ('7', 'financeDepart', '财务部', '6', '/company/financeDepart', null, '2', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group` VALUES ('8', 'hrDepart', '人力资源部', '6', '/company/hrDepart', null, '2', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group` VALUES ('9', 'admin', '管理员', '-1', null, null, '1', '管理员2', '2022-06-01 11:49:28', '1', 'admin', '127.0.0.1', '2022-06-01 11:49:28', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_group` VALUES ('10', 'youke', '游客', '-1', null, null, '1', 'aaa', '2020-10-26 16:10:37', '1', 'admin', '127.0.0.1', '2020-10-26 16:10:37', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_group_leader
-- ----------------------------
DROP TABLE IF EXISTS `base_group_leader`;
CREATE TABLE `base_group_leader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` timestamp NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178357041319` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_group_leader
-- ----------------------------
INSERT INTO `base_group_leader` VALUES ('32', '4', '29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_leader` VALUES ('35', '3', '29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_leader` VALUES ('36', '1', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_leader` VALUES ('38', '9', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_group_member
-- ----------------------------
DROP TABLE IF EXISTS `base_group_member`;
CREATE TABLE `base_group_member` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` timestamp NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178361150862` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_group_member
-- ----------------------------
INSERT INTO `base_group_member` VALUES ('54', '4', '29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_member` VALUES ('57', '3', '29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_member` VALUES ('58', '1', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_member` VALUES ('59', '1', '4', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_member` VALUES ('60', '1', '29', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_member` VALUES ('62', '9', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_group_type
-- ----------------------------
DROP TABLE IF EXISTS `base_group_type`;
CREATE TABLE `base_group_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` timestamp NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178366074910` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_group_type
-- ----------------------------
INSERT INTO `base_group_type` VALUES ('1', 'role', '角色类型', 'role', null, null, null, null, '2017-08-25 17:52:37', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_group_type` VALUES ('2', 'depart', '部门类型', '部门类型', null, null, null, null, '2020-08-01 19:25:34', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_group_type` VALUES ('3', 'freeaa', '自定义类型', 'sadf', null, null, null, null, '2020-12-03 19:57:40', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent_id` int NOT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `open_type` int DEFAULT '0' COMMENT '开启方式0:tab，1：新窗口，2:web',
  `type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_num` int NOT NULL DEFAULT '0',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `enabled` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` timestamp NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178377753991` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES ('13', 'adminSys', '系统菜单', '-1', '/base', '', '0', 'dirt', '0', '', '/adminSys', null, '2020-08-02 18:39:35', '1', 'admin', '127.0.0.1', '2020-08-02 18:39:35', '1', 'admin', '127.0.0.1', 'Layout', null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('44', 'home', '首页', '13', 'com.tcoding.client.gui.uicomponents.HomeController', 'fas-home', '0', null, '0', '', 'com.epri.fx.client.gui.uicomponents.home.HomeController', null, '2022-07-18 10:41:00', '1', 'admin', '127.0.0.1', '2022-07-18 10:41:00', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('87', 'onlineUserPage', '在线用户', '73', 'com.tcoding.client.privilege.user.OnlineUserController', '', '1', null, '4', '', null, null, '2022-07-14 15:29:39', '1', 'admin', '127.0.0.1', '2022-07-14 15:29:39', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('86', 'animationPage', '3D箱子展示', '72', 'com.tcoding.client.gui.animation.Yard3dController', '', '0', null, '7', '', null, null, '2022-06-07 15:19:54', '1', 'admin', '127.0.0.1', '2022-06-07 15:19:54', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('85', 'userManage2', '用户管理2', '73', 'com.tcoding.client.privilege.user.UserManagementPhonixController', '', '0', null, '4', '', null, null, '2022-06-02 16:05:18', '1', 'admin', '127.0.0.1', '2022-06-02 16:05:18', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('56', 'datashow', '常用数据展示', '13', '', 'fas-database', '0', null, '100', '', null, null, '2022-06-02 13:34:02', '1', 'admin', '127.0.0.1', '2022-06-02 13:34:02', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('57', 'heaScrollPage', '头部滚动页面', '56', 'com.tcoding.client.gui.uicomponents.ScrollPaneController', '', '0', null, '1', '', null, null, '2022-06-02 13:36:03', '1', 'admin', '127.0.0.1', '2022-06-02 13:36:03', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('58', 'listViewPage', '列表数据', '56', 'com.tcoding.client.gui.uicomponents.ListViewController', '', '0', null, '2', '', null, null, '2022-06-02 13:39:40', '1', 'admin', '127.0.0.1', '2022-06-02 13:39:40', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('59', 'tableViewPage', '表格数据', '56', 'com.tcoding.client.gui.uicomponents.TreeTableViewController', '', '0', null, '3', '', null, null, '2022-06-02 13:40:11', '1', 'admin', '127.0.0.1', '2022-06-02 13:40:11', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('60', 'progressBarPage', '过程条', '56', 'com.tcoding.client.gui.uicomponents.ProgressBarController', '', '0', null, '4', '', null, null, '2022-06-02 13:40:49', '1', 'admin', '127.0.0.1', '2022-06-02 13:40:49', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('61', 'highlightTextPage', '文字高亮', '56', 'com.tcoding.client.gui.uicomponents.HighlighterController', '', '0', null, '5', '', null, null, '2022-06-02 13:41:44', '1', 'admin', '127.0.0.1', '2022-06-02 13:41:44', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('62', 'formUi', '常用表单元素', '13', '', 'far-check-square', '0', null, '101', '', null, null, '2022-06-02 13:44:48', '1', 'admin', '127.0.0.1', '2022-06-02 13:44:48', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('63', 'buttonPage', '按钮', '62', 'com.tcoding.client.gui.uicomponents.ButtonController', 'far-check-square', '0', null, '1', '', null, null, '2022-06-02 13:45:47', '1', 'admin', '127.0.0.1', '2022-06-02 13:45:47', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('64', 'checkboxPage', '勾选器', '62', 'com.tcoding.client.gui.uicomponents.CheckboxController', '', '0', null, '2', '', null, null, '2022-06-02 13:46:23', '1', 'admin', '127.0.0.1', '2022-06-02 13:46:23', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('65', 'selectPage', '下拉选择', '62', 'com.tcoding.client.gui.uicomponents.ComboBoxController', '', '0', null, '3', '', null, null, '2022-06-08 10:45:58', '1', 'admin', '127.0.0.1', '2022-06-08 10:45:58', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('66', 'radioPage', '单选开关', '62', 'com.tcoding.client.gui.uicomponents.RadioButtonController', '', '0', null, '3', '', null, null, '2022-06-02 13:47:41', '1', 'admin', '127.0.0.1', '2022-06-02 13:47:41', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('67', 'sliderPage', '滑动器', '62', 'com.tcoding.client.gui.uicomponents.SliderController', '', '0', null, '5', '', null, null, '2022-06-02 13:48:16', '1', 'admin', '127.0.0.1', '2022-06-02 13:48:16', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('68', 'spinnerPage', '调节器', '62', 'com.tcoding.client.gui.uicomponents.SpinnerController', '', '0', null, '6', '', null, null, '2022-06-02 13:48:53', '1', 'admin', '127.0.0.1', '2022-06-02 13:48:53', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('69', 'inputPage', '输入框', '62', 'com.tcoding.client.gui.uicomponents.TextFieldController', '', '0', null, '7', '', null, null, '2022-06-02 13:49:27', '1', 'admin', '127.0.0.1', '2022-06-02 13:49:27', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('70', 'pickersPage', '选择器', '62', 'com.tcoding.client.gui.uicomponents.PickersController', '', '0', null, '8', '', null, null, '2022-06-02 13:50:45', '1', 'admin', '127.0.0.1', '2022-06-02 13:50:45', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('71', 'alertUi', '交互元素', '13', '', 'far-comments', '0', null, '103', '', null, null, '2022-06-02 13:51:38', '1', 'admin', '127.0.0.1', '2022-06-02 13:51:38', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('72', 'otherUi', '其他元素', '13', '', 'far-keyboard', '0', null, '105', '', null, null, '2022-06-02 13:52:44', '1', 'admin', '127.0.0.1', '2022-06-02 13:52:44', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('73', 'systemUi', '系统管理', '13', '', 'fas-cog', '0', null, '108', '', null, null, '2022-06-02 14:00:14', '1', 'admin', '127.0.0.1', '2022-06-02 14:00:14', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('74', 'userManagePage', '用户管理', '73', 'com.tcoding.client.privilege.user.UserManagementController', '', '0', null, '1', '', null, null, '2022-07-12 13:42:00', '1', 'admin', '127.0.0.1', '2022-07-12 13:42:00', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('75', 'menuPage', '菜单管理', '73', 'com.tcoding.client.privilege.menu.MenuManagementController', '', '0', null, '2', '', null, null, '2022-06-02 14:01:21', '1', 'admin', '127.0.0.1', '2022-06-02 14:01:21', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('76', 'orgPage', '组织管理', '73', 'com.tcoding.client.privilege.group.GroupManagementController', '', '0', null, '3', '', null, null, '2022-06-02 14:02:05', '1', 'admin', '127.0.0.1', '2022-06-02 14:02:05', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('77', 'alertPage', '弹窗', '71', 'com.tcoding.client.gui.uicomponents.DialogController', '', '0', null, '1', '', null, null, '2022-06-02 14:02:52', '1', 'admin', '127.0.0.1', '2022-06-02 14:02:52', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('78', 'popupPage', '浮层', '71', 'com.tcoding.client.gui.uicomponents.PopupController', '', '0', null, '2', '', null, null, '2022-06-02 14:03:31', '1', 'admin', '127.0.0.1', '2022-06-02 14:03:31', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('79', 'iconPage', '图标', '72', 'com.tcoding.client.gui.uicomponents.IconsController', '', '0', null, '1', '', null, null, '2022-06-02 14:04:15', '1', 'admin', '127.0.0.1', '2022-06-02 14:04:15', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('80', 'chipPage', '切割文子', '72', 'com.tcoding.client.gui.uicomponents.ChipViewController', '', '0', null, '2', '', null, null, '2022-06-02 14:04:52', '1', 'admin', '127.0.0.1', '2022-06-02 14:04:52', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('81', 'toggleButtonPage', '状态切换', '72', 'com.tcoding.client.gui.uicomponents.ToggleButtonController', '', '0', null, '3', '', null, null, '2022-06-02 14:05:36', '1', 'admin', '127.0.0.1', '2022-06-02 14:05:36', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('82', 'svgloaderPage', 'SVG加载', '72', 'com.tcoding.client.gui.uicomponents.SVGLoaderController', '', '0', null, '4', '', null, null, '2022-06-02 14:06:08', '1', 'admin', '127.0.0.1', '2022-06-02 14:06:08', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('83', 'masonryPage', '瀑布流', '72', 'com.tcoding.client.gui.uicomponents.MasonryPaneController', '', '0', null, '5', '', null, null, '2022-06-02 14:06:55', '1', 'admin', '127.0.0.1', '2022-06-02 14:06:55', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('84', 'floatbtnPage', '浮动按钮', '72', 'com.tcoding.client.gui.uicomponents.NodesListController', '', '0', null, '6', '', null, null, '2022-06-02 14:07:42', '1', 'admin', '127.0.0.1', '2022-06-02 14:07:42', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('88', 'httpTest', 'JNF后台', '73', 'http://127.0.0.1:3000', '', '0', null, '7', '', null, null, '2022-07-13 14:51:49', '1', 'admin', '127.0.0.1', '2022-07-13 14:51:49', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_message
-- ----------------------------
DROP TABLE IF EXISTS `base_message`;
CREATE TABLE `base_message` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `publisher` varchar(30) DEFAULT NULL,
  `message_type` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_message
-- ----------------------------

-- ----------------------------
-- Table structure for base_resource_authority
-- ----------------------------
DROP TABLE IF EXISTS `base_resource_authority`;
CREATE TABLE `base_resource_authority` (
  `id` int NOT NULL AUTO_INCREMENT,
  `authority_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `authority_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `resource_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `resource_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `path` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178383042404` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2939 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_resource_authority
-- ----------------------------
INSERT INTO `base_resource_authority` VALUES ('2165', '1', 'group', '1', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2937', '9', 'group', '84', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2167', '1', 'group', '4', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2168', '1', 'group', '5', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2169', '1', 'group', '6', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2170', '1', 'group', '7', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2171', '1', 'group', '8', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2172', '1', 'group', '9', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2173', '1', 'group', '10', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2174', '1', 'group', '19', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2175', '1', 'group', '20', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2176', '1', 'group', '6', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2177', '1', 'group', '11', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2178', '1', 'group', '12', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2179', '1', 'group', '13', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2180', '1', 'group', '14', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2181', '1', 'group', '15', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2182', '1', 'group', '16', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2183', '1', 'group', '17', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2184', '1', 'group', '21', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2185', '1', 'group', '26', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2186', '1', 'group', '7', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2187', '1', 'group', '22', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2188', '1', 'group', '23', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2189', '1', 'group', '24', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2190', '1', 'group', '25', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2191', '1', 'group', '8', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2193', '1', 'group', '44', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2938', '9', 'group', '88', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2936', '9', 'group', '83', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2935', '9', 'group', '82', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2934', '9', 'group', '81', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2933', '9', 'group', '80', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2932', '9', 'group', '79', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2931', '9', 'group', '78', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2930', '9', 'group', '77', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2929', '9', 'group', '76', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2928', '9', 'group', '75', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2927', '9', 'group', '74', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2926', '9', 'group', '43', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2925', '9', 'group', '73', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2924', '9', 'group', '72', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2923', '9', 'group', '71', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2922', '9', 'group', '70', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2921', '9', 'group', '69', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2920', '9', 'group', '68', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2919', '9', 'group', '67', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2918', '9', 'group', '66', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2917', '9', 'group', '65', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2916', '9', 'group', '64', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2915', '9', 'group', '63', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2914', '9', 'group', '62', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2913', '9', 'group', '61', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2912', '9', 'group', '60', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2911', '9', 'group', '59', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2910', '9', 'group', '58', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2909', '9', 'group', '57', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2908', '9', 'group', '56', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2907', '9', 'group', '86', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2906', '9', 'group', '87', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_resource_authority` VALUES ('2905', '9', 'group', '44', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_rsa_key
-- ----------------------------
DROP TABLE IF EXISTS `base_rsa_key`;
CREATE TABLE `base_rsa_key` (
  `rsa_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `key_value` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`rsa_key`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_rsa_key
-- ----------------------------
INSERT INTO `base_rsa_key` VALUES ('IPSM:AUTH:JWT:PRI', 'MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAIaPvusxk+x8KcVONjFUo+ursfLx\nSwOIvci5q0xPa+nUm3C0uu3jX1RQP+s3hWqCb7KgR67HDcZUS8bn2pNzJJWJ352lrpVrb1wftOot\nmrWPhnrdr9sdcgE2pLV8HAy18pq+agXmjWnkf+T558zo5k3I8VBVcTfzsL6HLc5mFt/dAgMBAAEC\ngYAYkZpsWwsFPM5rphIjA69CUoEVOJVXudndEnmCX2X2tckD1RvkX4WCYXyqN+L2RYabeMRd1x5e\nvflwAdlY3sco+otA0Ld9vP9yadlbfjuxzA8XyuvomaZPeJjMxL3dZnEvy29SMkNx3358ILp3BwGg\ne16GYPNZAjbB9fF+9YcggQJBAMs67m65cu2/h9Fe8+kevU32VVF6WZ3+506tYJFmsEbNILp+HFMK\nyp/OXX5DIMJIVA5f9ZYzG9GIgyaNMaKaFlECQQCpgEkflrwhwMkyOqJ0bJDB6GDk6iql2onR4bzF\n2sExbwYh5FjKdSQay+UIvrxLDvJOHyiySKL3rHxQggrgtbHNAkBVfQSvLvNZbygCup5/gZ7AHM7m\n50CrZX6HagclIr0kC0WpLUNc6z0w2+4YHsS49RPmuBbmh7k7+uCwZtRqQccBAkAeFunkM6v+Ce0t\nf38rT8J6ybe5q2AaxSrGGV6bePPmxKDAD3s7pYPdRejInqNqZ6LX/2wICiDz+qiu2gb4UQjZAkAx\nRtRw7QzNu/5aLnU5pxdD33lE+fubQSkq17y3JfeLO2/HsIl0BnKUQGg4TaKpYOaEoHZpgyd2pwlm\nUuNuRalU\n');
INSERT INTO `base_rsa_key` VALUES ('IPSM:AUTH:JWT:PUB', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGj77rMZPsfCnFTjYxVKPrq7Hy8UsDiL3IuatM\nT2vp1JtwtLrt419UUD/rN4Vqgm+yoEeuxw3GVEvG59qTcySVid+dpa6Va29cH7TqLZq1j4Z63a/b\nHXIBNqS1fBwMtfKavmoF5o1p5H/k+efM6OZNyPFQVXE387C+hy3OZhbf3QIDAQAB\n');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `birthday` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mobile_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tel_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES ('1', 'admin', '$2a$12$7csvqL0ofdK2NQzThrWomOUBPACdJDt21IlSOlJTjuz8wR4kl9YJS', 'liwen', '', null, '', null, '', 'iVBORw0KGgoAAAANSUhEUgAAAFoAAABWCAIAAADACBYuAAA7WklEQVR42r28B3Bk933niapz1d3eeW3XrlZ7W7e2ZFk+nWUFilpSzGlmOBETkBqdc0Aj59w5dyOHQc5AN9BodM45A5gZUsG2JGtlUYGSSFEiRYoSKZLCfV8/EBoOOUqWtupXXa8fGo33//y/v/R//4eyyoYzvsMNX3YtlN2MZG3xjD2U2g5kdrzZbWfaupex+nLbwdxOLGNLpbYyKWsgv+FOLXuzy+HDrdDBpiO9sJdZ9N+w+g63dpIrvv3dyDPevdSOK70XOgi60u6N4rbjGafzlt2RXw0VV4oHq8XoRHJLtimn7QzQRliPqSrvWx9gZ9Z1Bedo0Tftja+Fs7bkgSux7wxldvxJazC9Hc07QpltWDizGc2sRzOrscxKLLMEC2Q3fy8LZay/wco+CIeNxOHK2Fy5beAIZK2R1FY8vp6Ob0Tym6HsauLAWvgnd/YrzmBx3ZNdDu5v+Asbzsxa9BlP5sshb8buTjnCxYAn5dwsWJ3P7O0Wt+zp+XB+ORwZ313q3h4STbdcXG69stlR5VTwtlXC0daqBa3omehivLgbTNs8sXVvfCOYtUcKe6Hcrq90SSUi1hKR1eifDof/4INxePN2b9Huz+/4M1uRxDpwpGLruYPtSHLZG5rxhK87A5O7gQlvcj55aNsOTu5GZsOFndiB0xXbckatgbTLE9/dyW8FnnXbs6t7+FhxZXdHPaaiz8koa301bjU3bJCEjPV2jXCsrXqoi7I+2gJpeJKbztiaJ7EFEJF9ZyDncCe3cEmBLIED1xkuCaREZOWPjKO6/q44fIVd//6uLwe5biUy1mxmK5/c/OINx0HBms+swzKZlVR6uVDcvPUlVzA+H02tZAr2VGE3krRFktuxtCOUdOwmV2O3HK7kEqhli2tri53a9osrKkZgWBodqvdrBLuD7F2tyGqomxxkqpqv7AQXgSO6vxc7dAfzu570DsyXxWXs+LMnRNbD2VWSyJ8Cx9YJjmjWDq0e4yjaCctuRbJb2cJ2MWvLJzZcq5r1qZ5pQ/2QnGdWcCaNdZsL/Z5dUzaxdJDbyqc34rGVeHQjEbdFwpvBwJo7spg52Akll8LxuWx6fnmi0dB2cW9YErTUeZQcr4oXMtW5jNIVJW9igGHprYUuMObogTN64PLn7a7Mjie3G9h34bVEZNufIwRCEMkQUP5X4MBfdeds8BRvwebLbkRz1lzOloou+e2jy1rpjIw/1EnVNl3RNF42d1FmtEKIPOkcvhFb3I8sJr2z+ehaIWHNRDYzsa2wfyabWo2FZ+Lh6YRvdHFYPNp9xTtW79ULIkZJdqwlNtzo0AltJsmciiOTns3cdIdAIb7ujG9gSgDCf+DyFvfcUEoORhBBLDsh8kfGUSM9G9z/ABzOzJansAMc/txmPLeZTKy4rKZZc4u57vJsN8NubAxM9vgmOmx6yXR3ramxPLQo/1Jo9kuR+ZxzMuedTTqnw9ujSdfs3rLKsayyL8lca/LdhZ7rStpI68UNGdXaS/EqOVGjZE/B3higukcbN0zibu4jmRuuaG4bcdSd2CDkUHR6Co5dXE9ht0RkB6EdAiEGViLyJ8KxcYwDfyxD4HCkNtwFq7ewFcxvJLLr0dDsylSvsq3aY2j2m9tCltaAqdmrl3p0dU6daFcr2FBwcqvK/IbWMdq2opUMd1CVdVf0bZTrMs5oN3W0lzqr4lyX001NZzW8R0y8xyb4jxsqPzcvenpvkLHaVXW9/dqSirms5/pDi7HUZvbAmT4gZOJMbtrjm/bkFnCUiOx48kh2Jzg2/8g4aE0Xvbk1X3o9WrAFU6VP5+2YgdANx3poNnpz15NaDscXkpGFcY3Y0s/0aescMp5Lxo+ZGxPmBreMbe2o3mivsLAeHReeXuulIk2syjircv6Wvn5dW7ejlewa653wiJH6HYvIqudalYztwdq9AepuV9Vmc/mc8Iyu9n4V44GFgZr4al8xbU3FVoP+Ba9/0RNZw9wEi47AoRPm3t/1FIhwFihAIJvB3Fa0sOXPbMAwTryFhfLWcMEGI8+/334LDkYjcGyQOALHOAj8wZsOa2w++cU9VBnR2EIsMD2u5A/10OOmRp9S4FdwE/q6tKk+quYF5ayIirvWcGG96dLeAN2vFzk1AqdO4jE32HV1QYPUb6hzGYS7Ot62jrNn4PuGRLFhadIkCSOU9tOcvZTV5ssTkjNTzReWBykZz0whtFSMr+USG0jtwfg6Eo0nY0XWd+e3XTmbO7flzW55M+ve9EogvUoOHhTAgpz/u4H4HXGUw1MIHPkdEkewsO3JW30HO9uJxcSzjnAeOOYi7tFJJRelQWaoKawRBuQcgEhqBDEFK6VkFfSCQF+Np6sipGDGDaKwXhQ2SyNDDR69OGmoixjEfg3fqWbtalkeAy8yXJedaE6b66IKTqifkVDzYzqBU05f66lY6r66aWx1TPSFNsxZ/3wRdTDUEV/fDS96MpsAgQuD//oLWyiO4cWYqpNxkgIBF1IgfxwcCNokDld+wxqfRbkZzq4kk4sxz8icVjTcWYOxBVWcoJIdVXMSKlZcTkvJ6HkNKyWnRvspaTUrY+BHVOyIlh8ziQNaQVzJTqi5UT0vZOB5dGy3luXWMP1qVkzDDQ3Qwn3UlJKT0QvjRkHIIAgYBDMtlJl26opC7J1XFgILN5DRUhvO0LwnuQoVA4Qvvw4Wkf3NcHEjXHJz2Ptx/IGxg9lwVxy2xJw7sxTJLRdzq7nglHW0ZaqHtt5+Zbu7yiejxbWctJaTUTEKKuahlpWVUTIyyr6eW9Rzw4O1wcHamI4HS8jokE9Kx40beQEdy6VmOOVU1wAlqeFl1Lycmp9RcuMyZmiQHtFw0kMS14Bgu5e32c+36RpdM7KoYzwdW04jUqRWQ4XNYGET7RIsiA4ot+JLLZCeAiPdBAPxptZgfzQcgRIOT3FrL7fizi4mCqv7+bVcYGJvumN2gGFi3j8jecrefTWkpCU1jIyaVlQzbmmZRTllX1H7jJF308CNDdSEBigpHSet5+aVrJyKnVSzIlqmT033qukBDTuq5UbkzLxGcNMkPdSKkoPMUG9NWE5L6nlJpTShaQiopOu9nOs9jEVjU3Bv7NZNR7Kwmb65E79h8+dX/NnlyMFabH8tkFkgVJNae79AyMj6fvvtOEpQN9+LYxM40KT6i6vJ4loqMeO3aqwjjdd7qGPCx5bbzjtlNUEVLaqojcuqs4qaAyX1UFm7L695Rse6ZWCn5JSknJrXc1Ia9r6KBfmk1Yywih5Q0fwaRtjASZlE4UF6Vsk90AgP1cKCkpNWsjJqyI0b7+VlVHUg4pAJ5jpp0wNcFMEux3A0uRQrrIcKa+7kvDczH7+5mX3Wmjhcc8WX3YkVQAERYvJLnhIpbv+RcThz64GbW6H99Xh+JeAZ2pxqX9Tyx9oqnRpa2MLPjEnSFl5cVRsbrMjKqveVlBsqamGw6kBVe6hl5BTUrIYOHHEVrQguSkZWzYxpWSENAzhCGibiTlYvzKv5eRknN8guqrj7OkFex0+q2dl+fl4pLRhaU5YOj6llGdl9kCXvo9l2zXb/uD04sekbskcmYvsruS/a0rc2Ezcc8cNdcvyQCeg4Igu74fk/EAdFfMG/v+M/tDv3bdu5dfezdt+X95wHG5uBSXQE6NaSrgmbqWW+nbbRwfLIJAgESQXzxPA2rWJjYmFZDRdGTLKKTZ7MaXkZ5GOdBDkIGQQWV/GiClZYxoirODElGw5C+IiWkzeL4FkRBW1fxQGdeD/V210d0fDSo80OLW+o4eKaSRLfMX715u6/fjVweMMeiC84glP2wKQ/MO31TXq8k77gDHrI7P5O4aZr/xmPJ7rii6+hHY1mdxNZBwwtZTS1Gym1YKTFMseG43CGKOrKaNJyX3HbU7S5D7Z38muuGzaYPbvsTi9G08sh78T2TN+KSmQdFHiV0rC6noiL7xIhj09wnNgJDtCJKngJtTCtE2X0YrymtEIEUVhKy/f31bq7KyIKOpIREWvVTFhGRi9q+RkVe7vx4ijtgWHmI5t9tZHr3VP9tGVLvWtdjQoomVhOZFZT+c3cjZ1nbzpuHuwUC9ZCfitfsGYL1hTGmdoEEfTWSNK+8FowthlL25O5PdjtOG63YxyMhisInEQ9fsO+W1h3FDf2CmuO9ELqpi0Yml6b7ZlScJcGOF59Q8LcEtc23IHj/SzusJiSn9SISBCQRkTOhjpIaey2XrG3XooqGTmTMKZiQhqIvgUdP6fh4ps93dXzwlNm2oOTdWc3lWyt5Jy64dKInLWzOBj2TkQDMx73uHNvxGM1Orf0hG0bfc6xkH86Ep6PxhYzeVscZVRsNRRbj6atcXTJqe1QfCv0bnUfzm3dbuTJMnr9VW/WhlYtdGsP8QLZxJdfDRfXM/trO1vKCS1vso9iU3JCenGc0LwI4yeJ3C4N0kdIXdzhNZAGfot0lpKncJBT4CxgsdNc7uq6BhCQBl5jKgb8JWcQIIKAV9ooSlukHiVntumSnvP4bA8FjaK26fLoIHNSLVR31tZznmJXPdDCfLSN/Xi3+Gl1R/WUQbK9pIh5J3PJVTRZifhyNgu9bCfRiyU3gvFVf3SZTEAndhJTyLdl1Lor/vxOaN8RvbHnzqwhpccPNvM3Nt1Ow4xFNNJbsaXjhIekYQ0PY8hpBaQjnLC4HcftIYOIGmoOFBRXAYQQ0sgZ6/ImKbj4eilgsch7wtlRUaIAXdARPjIGHtQRlNMSOm7CIEDlFjdLkmPNPr1opZsy01E1JL1klJZP9zDWjPVbw23WkXbreKfd0rhpkCwqOOPdVEPrNWVD+UDDxd6GS+4tHdxqP7+1X4RMVgPR+VB8MZ5b92TXiaYku4GuBxbIb5FGvi2jiq+GCo7I/l6kaPenViK51WR2JR6dnBuTDvdXTvdeC4xI8lONyAsxGfVAf4zjxE60ACMx4QxyBAwHCTmj5CmSrIGwmJK71145z3lspOreZcFTwQEa6SYhGRUCyZkECQ0roGImTMKwnuccpHkhk5HGgFGy0V2z2Fax0F691E1b6WdtKIVbavGmWrKhEttVPLTUblOd01y/rRMtK7jTA/SxPpq+u3rWInXb9On4XC63ls6sxFKLkdQCcJBGQMlvwvylqp84AA6GtDJ64AnnHcH0Vji1mi9sRYPj63Pt44ramYGqLTU1MsTPjyIQMrMq2r6GeaKC23FAC9mSw0MOOIMWBoaDuIwOFkgucBNPN2Wj7vws67Ep2gPT9AcRRxFQs0ZBHBWagp7QsOEpSS07ahainPdpWD4NJ2QSBY3ize7qUd6T1+vObfXSfHpp0NQYsbQlx7pjwx1ebb1bzgqiFbDUJ0abo8ONPnO9wyi2mSQqyRlV08VhBXNroScenigUVlKZeV94DB4Ac2XX3bkNorzKb6IJgnlKaMo4DbXxA284vROMriUSazeL1uCOfmSganawekfPDFq4UQMzqaPvG1lFHSMjr/pdcBQNQhgRRwdpcBZEU7BYFT09SX0QOOAjWYPocKQRr6AAHDAEVMQOOIvPwPXouH6jIDpUFzQKt/upK82XFxsu2nuoEb0E/bR3kOvqY0U0kqi23jvIPw7MSGFafkjNRa8YQK80XB++3jY7WDsofkrdUb4x35GOTeazc7H4hCu9ChxQB0nEXSAKTsJKaMrELWxffMcf2chm7YX0RiE865zvGWm/tCqriY2JC1NIrhR314VI/5W8ipJT1py4xh2GwZM4TqIspBEdoN4a74C/AIe/j4YIAnN3VcFTjJc/Zb76GeOVT2ou/L3h8j/Msh/a67gcVdLdqD5GpSCSGG8EjjHuo9dFZ9A3ZoYaE3pJRMkPK/hxpTChEiVVorhCkJDRUNGSUwK5ocBDmAvoeOFhyERoMwnMnZcbWfcNq2rzicl/enYzdmC3BWcQLAh/KWyFbti3onPBm3aCC3AIGxiJjDsatx3k9g7i655l5byStdhf7VDT4xZ+dpiX0NbGFVVZTe2+gZHXUm+vuG43MmqcxFTSZQ7NEkdblburdrf12jT9Yfnpj/c9/hHLtc+uik4jmro6K5FZdtvKrY3nVkRPTtHvM1395HzrxfXeys3eqvCQZE/BMNO/MC140jNId/ZSPH3U0CAzoYQShbCUnBsfYGVVDPTTBQ03jwtTEdEdUBCJs2NNtgHKQm+lzcxf1DFVzWcmtbTD+EQoTSz9BtHpo68pWKERXxF1xuYxDr64Np3zZpK7N3POjPv6okY01nTZreOj+wxraHENNa2jFU2sfTO7YGTm9PQTHzmBQuYR8jzeIogCBBlKwWin+doS/8wU7aEl/ilohCzD0L8WLdK8SYKEglBKhg9He/mS4LHp+nMzDefXuiojFqmtp0pP+Txw2NquLghObdWfd3dWxgYZKSUnq+LBcipeXsMoqlkwtAI5uKqCibIlq+d7+2sjhtIii47rHpOO915V1j+1YGRv747l9h2R1EbqYBdE9uJLiS+5HamVYxw8QU0kas+nHIcJ2+60bLqbvtpHjVkkYTUzMFAVGqzMahn7Ji5CaVKDapp5R349wUEOHgYQCBzwmr3mSyvcx8YpD85znrI1lsNZkFlKlRgTOaVgrkNhigo9OFgLKPvDdcUhMaLpVN3Tw7zH5xoueLX8lZZLZur9G83lez1V262XoaZgP4WIRzJ6ihg2Z98gyCipOTUdUNAo5hR0EEEPnVdznS3l+6ON2eGGXRltT8/ZNQkmu68ohY+OWtqD/oVsdhtQUImizYnfdADHceyQ1NGD/q1CfDflnB/v4S70MhNjbVGdAJMW7K+JySjoxHJGopsIKqgxPft2HLd7DRlECQdWMAPdVTv151Z5jy8wH4KzoO64Nd52Y7QFlRjUntELn51si5WWhRBKEUQhDeRad/c1a+NZC+sRRcU9Bsr9a61XZ0SnRlkPuwdoMR0vDuIGPhJzzsBPq1lJJQ2GRjmhoKBdLOpYN/ScA2BFA62AUlg3TZJQby3QZ4bqfTqeQ8W2qZhz3dcGWumzY32FlC2X2UZtlijuoCH2lVgQOJoa2PGIPRu0OheNRmn1lkxwY27AJ2MkVPiTjIyGnTcKMGkBBQ0VQdQouKMxOTFMGsnF11kBUcwzHrTVPR3uo4RlHH8fw9tTG5axUHoULQ1EQlGjl2FBHSg3wMLXV+3svGprOrcmeWpeemGE8ai++n/Mip+eEZ2ZYD1i764IKpnO7gq/jILCJGPgZA2clBYFXm1UURNX1aR1hH5vWQS3jIIbWu6Bgr0vZ319vM3XVhHuox4MNwWUbLeSGRmu9+gF8iaavofv35mMB5eTyc3sgcMemAkVbb7jzCKsPcj6w7sLC/qOiXaGQy1NWprDSmK0WbTbpSWvuJYT1nKCej5C/R1t60kLQxJxt13ZFJ1a5jxqlZxBWrlhQcUhyBqk+0ONMHRx8BdUtzCyi4MGAwMUfz+x9oNEi8ziVXC3uyjjnMeX6s6vN10GjjnBE+uN57fbyx1dVzz9FWEVNaVnpA3MhJYWU9dGVdWoA3JG9r6Rc6jjQh1gATtQcPaV/IJGEJOj0mUljOLkcCMccEbdou/kTuvbfLtT+ewOmj13ZJHEQdQdjNpLX7oRd66Nj/SI1lT1Xn2Ta5CDajqt4qKthEBiCgayV9QoiphEfr2QjBS3Z1aUnjAkEU/71Tn6A7O0L+w2XsCPEEHwSXgK6o5Sq8IGCxTpcJYcLg7Nu06AqIE4WqrBeKjBwnJqSC3wytjT3KeAw95VM4USlnrfNOeh7JA4ZeRFNXTgiGqpcT09pquNaGqCykpAQVzLqGlZOS07SM0P0IsDjGRH9bMGaVHFR/aBJON6oaOfFtIJ965rxwelA/UU58bwjeKeP7SIdhY9yzEOwbUnvp5zW4d7dJLLTlOjQ8VBgRxS02MqeCYtp2NmNMy4EvNGQEno+BBLQsXEK3w4qWYF+qojMmrRLMKZBc7Dc6wHvejZZVR8plDy87CaDYuqWSjz4zomhlEaCS2iqY1o6XEDJz0kSo80hPUiWzdlXnppXHBqSnxGefWz6mv3JE0SdHGTrEcWxWdmuE+tN5Tv9dLc/Vx7N9vWxXYppFFzp8/QuKcSu9V1IXOTT1u328d2ybkQeFQnjmpEMUwGMR/wTQ4ydEYrWhvkZZe1g+wzY73sW/H1qG8mFlsJJdZCGas/Zy1rZp7POGYm+vmmhgrPUMu2jBFE8tOzoUD4JHDkdOyUho1pBIskepaS+5AGHCAFw+BtDWeX+Y/Zmy/gw6CAMzB8gFAWylNUnzp2Qs9KGBgkkdwwL2niRLRMv5LmlTFQa7tlbNhaR6W1q1pW/qkR5iMpcx2Cuquvdq+nZqjmC0M1DwxRHjZRHjFSHjPTT08Jryw0Ujd7Ucg252cVhwuq7FR/2NTkUQidA2yoLKziR1ETa/iQZErDy+gEOYPIZWgKTvaMt1PU9Vd25hTpyFI8vprMbwdztkDeViaTVuxMDmjrr0521gYnWq39NX4NI2XmwjlTWnpGAyO0ADHD1eNaAQaZ0XFxJoY2VM3CyKNyGihcp9+/03QexxDFvkUMRkhMQIPyGQaaKMBTOhZhegYpEEIsBk4MXayK41dw/Cp+UCP0qrhuBVtx+dOL9efg8EgrxbFmSD031IQJt/fQF6TXRjnn9dSnFNce7b/8sJp6ysy9OCy4NC68vNHDRiOTneyJmRtDWlFEK4yWrhleiUoko+OlNJzoeIdVJXSYm5WSC8YeRiZK4EgX7cfqkEvLFzQSY9PVdRU3MtlsG6gO6phJEwvqSKgpcTlCNw2jQgpAOkCIDQ/WggXO4ABosnqeu/PqNO2+dfFTYIHzAEH6UWiAQhwgZOq4MKSntB4xgg0csKCCElGDCDtp5CeNYhTgUR0GIPIqOdbOKpQb9q4q9CCQBnDgp+4BplfBD2qlYWNbdKgrOtzvNXTvKlvmOrhTzQwj53J/xWMa2umlVppHIw3rG2LGhoSxjvhmOLge0kDJQwv3V6WmOleR5qa7Jnpo8oZL3h1zPLYUR3efWvOkN8rqKz831U+b6avZMfBCY5I9JSVqwiWibaMihyGTxZRUjI2IeSpOSMEm7hho2Ph24mTJWbYbz41WfRbnoYtEqVoBBaRnyKfkU5ASFzODX8kAR0kgKOdyJh6iSUBe6x+sjai4CYMoZZTCNlsuT7IfXZaegweFVJyIhgdpxPTipKk+PdIRH+7YU9TNt9LNvKv9Vaeaz31BQSs38msmmlgTDcRJPfPsKO/CfEOFXyOKGyRJozCBS9Uy4fioUAK95bnpDusA1TvSuG2uUzdfnjJJY+G5BFr++LIzsVpW+YW/Gu+4vKll2A2swBC6SWrMyEga6cCR1NQm1HhlEOrQEvMMPzzxEXgERuvpurZZd3pF8DhGCwQkIERW/JS4tyKjHpdqWg6Zs1EvJDQMGMQS1TCDcloYzZ6W+Gb/AGuvk3Kd98QE82F0KBEIW8vPWKR4xTxD/AFN3Wo7XV7xKPv+j135//7rub/7z6f+5i/u/9D/+ej//ZcVn/lYx+UnR+tqZ1sYU5Jrw5ynN9qrEY+iWi6gIyVn9PSMtjYmryhOtfj1/F0NxzPeYOm4Kmspd9nN6fyGL76Eeqzsyj1/NtN9yTnE2TPQQ0Mcv67Wr6oEjriGAhwIHxgAMUgVB85Sikls0l9ujtZj5PCRDckpYthQI8o2HRcsyOBCFCzELQWifkFBnS25GPFVpSXiiBqpikVMnV6Abz5ZDZkXPomSPKRgEud1/KialzIRfpQebvWrJZh59n0fefzDf3bfX5U98KE/e/C//O/3f+j/+syf/2+f+Q9lZz7yofZLjy60c619wsXm6kn+GWsn6jciMcfUNVkjvWCkZXRVCSM3bhF49RzvqHR6gNInfXpusiUSnY1mVkuLg1/4DxvyitA432umJcb5fm21o+9i0kiLKKvwLcCRUNOjpeV/1JGosjEkpFKcORyug4PMML4AIkCDwZPB4saIlLgp+a7LZEt34UAk8y4LlFtgkR+SZM0i4PANUO0tl9HjLrCfmGM+ttV08dZUWwQljx5JgevorMgNNwTkHKjD1kVXVj587RN/+eBflT384T97+mP/6dwnPnz/f/6Pj//1hx76L3/+mf+j7NR//4/9FU/YZKKIpc1Cf3itpRzFdEhVg7Gk9ZSimZoz1Hh6riKjpUYlwdG6NQ1L3XzBIKO598zp4maiaCvrvPQ3kXFualrk0lT79TWQRtxIyw1z0gY6NIZCGOrIGvlZOIKG6L4gBIwKYQJc5tkPAQfcB4rw91YBB47JyHKShtGAF7TstJKeUjFBB1V2CJ8xCRBB02YxPAK92SLviWXBU872axEZA0UaqngYDpARSumMB38hoqxKgKDQdeFz1Z/68Nm//YszH/2LMx/7S4A4/ZEPP/2xD1/4+H+Vnrp3voXpN7R6tJKdXuqc5NRS3ZN+eWV+CJVeVVx5pWCqDfRcKVgEYS3To2MHp5onB2rkrZdW5nriiYVYerWs49x/8xtp8WGOW10RNdMj+tqIjhJWV6PUC8mr/P2VwUEKphTBmSgcNTwMEqPCOJ3tlydrP+9ovQQ0yC9QBCRDVmhE1EQ2KckB7ea7OOgZAy+iYvrl1JiBnx2R7nRdmxc/Be9YFp/ea78Wg/pUnJO7MGltKTWirykly7hOHDfVB7Ti5VaKnnm69+pDTec+Lz71aern/1/h4/f2VZ4eE1O2+iVBU3tqvCc72YWEbeu4Mid81Np6BlObMdRGFFfT+urYYMWhhY8KyI+SZ0SyqKTpOq4Oa7iR4GQKOLrP/jefpiaip/mUVZkRXtzA8Mkq7J3nAcLVddneen6n5YKz8wraCjSdxBqvhv3l6VYgAI4Jyr2QA44BAiPHj05YkIa3ROutZ4EF8lzSwA2pGC4ZNajn+rXcCf7jE9xHdnsq/XJ6rJTL/b3VxGIfml0VAs1xVoLhR/b2q2gsI3oJaoqwpdlvbNrT1G/LxQ5N27as3qVti4/2hyzt26hZB1hRkxRdRVDNmBU8MsX+PLy+MIwO4Bo0klNSDs085LUABGLgbRv5o33VXZIzmfBMJr5YNnD2r4Oq2pieidfssABNgbPnirXl7CTzf4xRPmep+JTxyj+Yrn4SB8NVnxmp/AzyCLQABPAOeAoogAgZRE8QkEa+zWqoeR0zpaLG1fRk6SLcSjpaQYecPiF8cr39Sma0PjNcV0qHHF9fVVzBJTSihrjek5KS6lJlbBAkTdLEUGNsqDlkbvAb6tMT/QFDk1dbHzI2ulUCVPpIKPmJlqiehzZnQfy4qfKT/oFrxWFuSF6B/LCvZuzr2QdmUVjN3FMxvWMNCyq2pObeYmQGRMpkp/8moiDmLQwxmwUhJc3TXwVDtCPWOAYpuERvb6WnpwLlFhQxdO1T6E2s9U8joawKn8CA4Sakv5AsSB8hcjMpFnVtTo94TI1pGekhYdjI9+iI1VCnhrvYXr6JiGPgoUuK67n46+jiUmoBDJ5yvLYEKGqibyoaeUQprGKElQxEnLCGF9QKAloBBu9RsAMafsQgjpnEcYMoYRIiKvlktYVR6Ubj08aKf3B0lefN/IiiFrXlgZJeUDEPjELktT10p1OtNpOoiXLf3tJA1jdRNvDEfw8PUiFINGmIl8QtMiUrbRTAMujBCBMglBK1k4YBwaM3Ga/5HKIGuIDOwZAEwRU4SEWQLEgjiaTUNWkdscgIAaZGRGGL0GvkR8abVnurt2S0bTnVoaDGTMLCeH3KzA8qqAQLDb6NT7TO7+LIaugJeU1ssComoxCbSgw8dLelYCxMGAgVZCwi9FlEQ2gRoNBw91ViRnNDou22i8OUe6wt5xN6DlGA6DkHckZezizq+cjuXiUrMt68NyTVSM+qmy9lXSNliic/Sqw+ovhTstF3w3VjSnbWLAEUhD203sgFCKVRJTWqqI3IKbfGGuAvkIb2/McRMpBW8UqC+EBLaygogdCSR/SMxIgwYOK7jLzgWMN0y8XMQnd+qdutZcUsIuBAOxfAmJV8+EtSxSf2Bmk4OQ0L0Qfx+KaZe2Bg7RtZB2Ze1sDC9QQHq1HpY8JR8iPwB2WVURUla+QicaSMHIgdkgeOcdp9q3WnMZakngdFHMhYRRmziFCtEwQ0XL9J4hySLinZqM4TO/oy/blPJBXsrEaIiygYpRE5OzTIRI1YakO5cWI9AizoUFqUsJpnJ5r22sqRU3UX/h4SQPVJdi53C6WoBU9wxIb4XiPXoWP7hutWZbUv5pafT1x3adj2/kqfmhZS0/cn6kKD7MggKybnEOvDana+hKOooWUVNQlZRUpendPSsjoavI9YBBsRpHS1MAiQqDv1dHJNCHEKXCAH6GKCfv+i8Am4WMYkRnV3KGcfKNgFDQ/dLXo8t4a/ByJjzdKrnw2vKcrkp/4G04tYSG6vIGpnJR0uiuaKNAiMMCOXNHwYtsR7FCEDhZavpxJFB47vpg6EUjQLGWJdQ+IzCJ2Guh1z87K2yTY++L1nY2//4F/yztVFXc+ytmfN0O8Y04bMHQ5FnUslRYAMGqQxc33CUo8wkTRJEETTlsaAjB9QiNLm9pi2JaxqDOmJDXk+FTugZKM5Tpv4cCU0AQUECznF23ttkf+w4don4fhwLhSpKTXUysT1YGgItzGLJDrcGBxuGmssH5JeOMZB3j0ncRAsUEe/FweEd4IDwQJBFK/AgfwCHGQv+4GGUIq0guvzKei7cpZ7qHFvpH1GJg6tDT3/xeTRT771xvNf/f4Xs7vTxqHOOmOr0KFv29M2+4c6/OY2u1ywK+cFjNKwWepT890KTtggTVpaE6a2sLbZp4B/tQe0wrBBjLQa1QkiGhb6IHg30SIauWFZDWqFee6D1zkPInPhp7C0ppZY0zIQoSSs5YSNwoBR4tKLr7dck9EeKpOf+QgonOBI6Th34Pi1QEoGCjCED7zCR+A1wAFx/QYcpT/P8yoZLjUvOt3rmewb7+PtXtf85Ju3jl797uvPf+2NH34j7Vx3L0/NGWQLg/WLMum2sWPX0rmlqd/RNfhHO4Oj7cHhth2lcEcm9Olb3JrmjW6BfbAxPixzydlBnTBhqSP27aqZCMa4eHgKWmeow9F+cYL6OUQQxA4U7IR8SjiyJRzIUEEtz6Ph2xUcq4zTdeWeX+MAiLCcSuIgwudtOG7nAr+A7bZchCIQHcj1nl/nkfdZUkFJo0NBCrCIQ2ZpZKrHMdI50svzrQ0f/ez5o7d/8tPnv/7zl77zpVz0le9+46VvfTXnXtu5bpiUN6kbaAOia5Z25pyibmZAYGqo0omuqNgXRxpqxxvpRkHVeAN7rk203VPrlDFDOj5SbFTLRqGAi0daRaQDEZSRuksfR5ZBmQdYwHE8PXoiRRAbGLV86A6VW8DY0HXh02WKpz+KYAEjV25/Mw5CYIO10AVCBtmeEF2JnvebMouKGhmoJorO4XqfsW5bJ1nRNEzKxLcS9qOffe/ojR8dvfnyWz97KZ8OvfSDbx/96o1fvvqDF7/z1W9+pfjlg8gzGc8/7we/dhA4DFvj9utfy7qeOwi//o2bR9/96tv/9pVf/s8vP5cJ7fbT7X1Ul4yOSgSjJRplYizMQH/l/pBwq+G0/Mxf+wZrUDcgdhDOoqLktIyslpg/YrnTKEZz6FbwwqamrrP/WCY7+1HkJBhGG5DXEu6gJuIo6JwY6JwYGUrJ9oSsL04qjg+0m6gI4F8qZtwscWoEK4PcRWXdgr7t64fBo5//4J1f/OiN11/69re//pUv3/rRi99/5acvPfftb7z4o+++9fbPjo5+/vYvf/Krt19++80XX37hGzcy3jde+re3fvzc0WsvHL3x8tGbrxy98ep3buQROxwDdEd/bVDFQS2HgaB0DMlq/H0VRYtggfcQ1OGXUXIWMYqUMFHCUIEDdR1x5TpB1ixNGqRBpTBhbhm88Nky+bm/JXGQa1NEvNUwb8dBUiAXKYj2/N2SHF8HIgTj0uvd7NaQOI3WFgnbLHFrhFaVcFVdNzEo+nLW9Q6c5Vev/vy1F2/eKv78jdd+dfTO67/42ZtH7/z86Jevv/36C6+88NwPvvXSqz98461XX/3ZC88+mz9659W3X//RWz/94Rs/+u4bL3zn6PWfPHeriPbEJWcBR0jNPcGBsiA0WI2icbjqU3OcBzCirJlwJaijoKUTOFSMJJIpat8SjpBCmLG0qcs/XyY7/7dI+LCojgWKH4iDHDlp5Lr5ycIXuYb8G3CkFIwEPqDjwrd9etGesWFxUCAXXwlZJ75SDL78wjeP3vlZLpt46cc/fOXVl99865evvvPmK2/94mdHb/3i6O03jt558+it149+8Y3v/Gs45n31NUjpJ0dv/vSNl3/40+/929GrP/7WFw/CBglih3OQFiZu3POAA86S1rNjSqq356r6/N8ifKD8h7MQnbSOs69nFrRMdNjEVWlQ0UmSurqgXJAyNCvP34MW7iMkDlS4ASWVdBYo5QTHbxrq72B5LTfcX4Nin1gHVnF3lHyL9LK5mTIpl65NKJ7N+l/70XM395M/fP65N37x2osv/OD1t9588bVXfvrLX7z61s+hlG9+/9sg8uy/PGN3bf/yrdde+fH3Xv7hc0dvvfrOKz986d/+ObqzNsk/tdNbmxySZobqMflkZiHyqJK6InxUe/Hv0Jrfmm72DVBQdAAHoQ41HerIEEUjcc8hrOD7BriF4c7BM58q6z/7kaCaHnwXB15vx/HrfPmu/b44ctCFnNgDhjCORmt7gD4sPjfbzUCy2J6QWSe1Idv8rVTw6M3Xjt55462fvnwEXbzxGuzHL7/4yqs//to3/ulXR7/8zvP/0+neOjpCQMHHXv3Vq9//2n40617fGFOvt1VAHag7EkYxRgtdk3cw4CljNZ8Zqf40Sp6sRegfrCVcSQ1p0NEBneCIawVhlQClXW64o+/0J3+NI3Z3HJl/Bw605/itrB5/mIMqcKO9YoT3pE3JWxng+adkYx08dR1tc0T1lWTg+S/fAJQ3X/r+2y+/UHKRV371i5e//c1/AoXvf+eruYTnrde+//oLX3/+n/N5z+qErN7UylSIK/0aAeoOJFqkTKKdswjRcAKBq7Nce+Fjq+LHcyYeJINYnjIJQioGUeNr6Mj95Ho4cETUQoTS9HB739lPlfWe+4hfc4zD/0E4TkCQi8C/Lw7E0VxpZYxI4Rr2kvTMBPexgEG8q+BnZuTBsT7v8IBzWD7aIlTzqPYh7fcPkz/9l1tHL33n6OXvHb3y/Iv/euvtl7713Fey+yHbP+c8riXDtKJurJt1vZcdnpPd3BlJDjfFzFLggDMCR35YTNQ4GgakYbn2D0gx6F9Qd2QIeRKhFJklW7qXRlQJWuRmAbFGrxVHzM2Dl+89xgE7wRHWHOO4XRokiz8AR1JJI25rqugoigIDNXOch9BQJY1CtE+2bupGJ90hl3h17dv9DQ5lR2xUq6Zc0NAu6dhX5fRyFbeih35RV0/r5VwSlX/B2Fw73c9ZUQp2jfW+oYbAcP2OnEEsBZmlYb0AOMiepVRPUjof+k8bdU/uD4s8PVeRIoojUs9ADSEfDTWjKd0e0KLH4aG0J5/0RjegpT5S1nP+GEfEeFccJyxyqt8/mqqIqQCUvJ7jbD03S793t/U88bUGfljG2h9pQwzLW3rSxp6kvmeeX5M2dCd0nQFlq72vbk/Z5NC07Jk6bNrm+V7Wnrk5ONHuMgjtSrrfwEV/HNQx0cKhZ4kYwJeP+Uda9PdXI5t0P/IhFOmHoxJnZzmZaJ29lcQSiaYWODA3RA5FmAdHU11oqPF601UENQKHT/s74QCLPwDHsTTUtKKRY5U8Pke/JyGvQeVeQL2sYh8a6z1tFF87Mzog/cqkOq/rLqiaD3TtBX1nXN2cNLTHTZ17CsmuQmyT87dlrIBJHLYIfWqaX1UdUFQEFNcQO9DRkrGDuN2roFobz07RPz/Luj9TugeKGgTpMqSgo2eJEqttFEIdJRwRHTdkEKB1CI00G3mnFnpq78QRey+OzL8bR9HIgzSyWsYNC3+N/8AC/XMHBlZqsLKoZmRk9IJGEO1h3jS0hjs5sV4JbH9QclPTtK9pTsokMVVD0tjqHBT4tHWZqa7tfqpbyUyPigtjwrSZEVNVxNXX0HH41Dy/mkOGUvjFIp/Ir0i0JfesKg4J0Y7vdV3DTxFKgYO4l3YbjrClPjLWqmQ+atUIgOOvfVqqX0ONGFl+FSVmJDYMxrQMYlecllikhBHLc2pmrmSZ9yaa32qo2dDyurquIsJpLnx8WfJkysgLqmiEDLXMkIYBwWMmImZu0MDyamhRPRtGrPSVSkFyMjAN+zr+gV6A1yKxaMg5niEd16Wv31WLnWpJZqonM9o5xTujvfq5reZrMa0IYbL0PB6xSYD82rCBndcIUkpe3lDvG2RFDXXR4db1Pkbkei//9N/v28134ogb3oODJPLvwYHKFSUsgihc2nT1k7bWC5gr4sExNOA6FkFEz8JVwoDDr2PcDUdBwyU3j2aVrDQ0omRmVCz8FDj8lpaguSloatztZ5prHzbXfMHVSyNuzRDGi5e2q8RKXwtLKzhpFTerE4dV/IheEjI3IOsjNjdf/dyha4TAARa/C47sH4SDWEnVE4/uQMNT9PvIPgBFMFwSOMhLBJFSUGTBSBYkjpPIRfopAULOSMnoSTkdtX+KeMSO5dLXJSe6wkMtW93UCd5pQ/X9c/zTURXBgriDpeUQew+OO3I0H8yEjE48O6DiJAyikF7kNUgcxrrJ9iql4PSBe+T3TrR3GzZ5Q+T2M7jWNLF/lgYW+DbztX9ckzwFaRDrLlomKZC4kRsreU1AywALeE3sNmncjoNkkRykESanowEjzW2QhIebHAruhPC0mfbQnPDMXk9N6ZFnYnNLyUoDKU0t5jijpMPXQgO1abM4bBQiTzmHpL2Mh2YUrKzT8nuXYb8ZxwkRkgWMuDMip4ZktfryTzg7r6CbIltE4AAUYvcP/qKOeGowQtK5Cw5YRsEkpQFPgTTAAtLb0yHjMGYbLpqZD18XnvYrWGmjJDhAO972SlzzsbRLO5mIHagFPQuNTMLED5kFLpNw2yBsqLrHvdiXhjr+WEX6ydL5cfX1bhOMosjbW7nTcmG89l5wITtmIvMp0YMyIJBjxyYPDBySxd1wQCBgQUIHi2B/jW2QNtN4YUz4xHXxKVtXZX6k/sZoU6CvJqvmEL/1bshD20YaOtqcnh5R1sArg2aBw8RbUFDb2Q9lfCO5yPQfrYW7G46cSQBRoBjdkJ4mns8YrCG+3MAl7vKVjNhRaCBW6vEa1bHez+I96lAwyIRCbjNxd19bbC8Hi/mms7sDNT4FHY38wVAdmnci+hLGhBVVdFihZBnUHVpq2swO6OkeM8dmYA91Xzb2VWbC49nkXNnA0x9FNobFS1uLiRVnFTNW2ipI2rseyHmPK/7OOIpDYkd7+WjNPbh0NFd73Vcx/tIOUWZYRYdGcAA6OAPJ3L7UcgcOMnaQ0siXbusgVeGbp+qeGuE/YuuvSg6L0MKjcyVuIet4JRzMorJkJRykRQavxlU1+xNir47hMHFW9CxN24W5MUnIN5xMzpZJ7v1zpOX8SB2IWJsvhBGf9DxAKVjECQ2bfG4PFCIyWlzJLJhEty8akotDiA7khmHyKXNiZ09pLZpcRkNmXZWe2mw6i3qRbDfJHS93fM8ddnsIJ6WBciOv5eItGTKCgxTgQG92XfqkgX6vY7B6f7KR2Nwiox5axOHeamKbjYKZk9HycjqIELuRie3qjKiiqjgmjJrYHgPLbuGYOi4aBqpCfks4OhmITZf1PPH/kDfsgMDZXeEboARkVOTqrFFAPrQHKDhGi0HcQNccrxUSV6wj90ZybkcDKIia5EI0xkz0bPxHlsRPbLddBA5CBToO+et3DP7XLd9dmgMMj3hcRcmEI5R2I1GJbXYyirX7yijngbXW87nRugCxvZdVMPCj/RRUKHAWgkjJUAEn+qqj3RU3pxriYKElpLGkZVgGq2Ym6v3B0Xh+KVJYKdNd/kf/IA2WMYmDcoazu8rWUo63xA5pcruPHCmKnzdJMnohjjG3JzhKeyOP7d1buceL8jBE0M36M9PsByANhAxijfv4ntBd19kymjuzO8niOI6WEgqxQIk/pCGMSNg61qzkiUnOg2kzscaTNwqJbfJE0CUsq2Tn1dyiFrUsH8EV5xNDwtWO8tCY1DVa18l+wITeMrsYTc8nDrdiN61lpiuf9vfVerqrMdqoguXuqloTn7E2XDjZrBaQwSFpxFZZvYDYh30XdUC9OE/e7seBr68KLGZYBAvM4XG5oWaQv0jc1tF8QNrKaD64UToWyLuPDBAS03MQegkz8taazlpq7oHAQ4P0w+GGSD9tXy9G9ZmSEzVoTs0vGsT7RknBJMkZRFs9NW4d32WRDHdcG2g4t7ncf3BrJ36wGT7YDBxayzTnPxGWMfBFkABwYP7BYpH3xGrd06RkUOfiAMLBAYjcfs+FmJ/Sv15ARMSYUVPsj0lxobudlxEvFkWPw8ibFYiU5CdJKZ2sNmfuXubdwQLiP9l+SAQs/HUdUc6F1Wxra7m58p6Ykg0DjkB3zTMjTQW9iHhATMmLK7gxJWFhJbEvdlvByy/JzQ3l7YxHIo6hw0PbjnskVNzyFNbdhc0yxZm/I7djwTtipT0Nvl7KpvTccM3nNxrOh5UsUhQ4IAVyOw6MECBIH8ZbYr+KgQsWiBezvIe3ms8Rt7/0pZtVKjqxQqdmHHtW6V7E+1mc4PgAaZRiR6q0lE98m4qGsgB1I8Tr6KzA1bo6q1Nqwb65wd9FeXa0Na8TpzXChFqIgj2qEcLCGkFQxdvVN0w0V6slF1eGmjPhhUR6zRNf9BWtzuKms2gtU5z+WJLYMErggDoQI+IqDqq6We5j8/wnQMTdWwMWxE57LTeu579fHSBCPAilZeEALKCIGe5D6w1nEC8IH9ETBSiQkXezSF8j83HmfWtLRIx435k7cJD7MNH1BPC1Khrm3NdLnah9YIV/Go1Z0VQf6K69aWmJK/hxdC4aUdJQlzIjfNaHDZKAVrgmEw0wTm1Z2r+Y2vI4RnfdE5ln3b6DHee+DVamPPN3UTkzVnr2CF5D7l0EFG9/7aLwqTHq/UuiUwQRNbF2RPTLWvJRjPdAIdagZBR7RzlYXOc8CE/xDlSTPgL3Jho2FR3HRLlRirXkvsIPFMLdcPx6c6q29Hff7VBTWmFokDlJfXCS8kBOLykYG4LddPTvwEE8/q+rSxnrY0apXyNwDDA2uqrne3jBWWXWPlEILd062ItnrTvhBU8BOHb2DnbK5Kc/HlVwYPjesAxQiKc/EmriBkSgn25tuFR6Lpp4ztHZUUU8n6Nl355NUWLYWy8uCR5DS4KGdavhaSQUohhXE+U5Iuvtt3g/MLne2f6oeSUjex9ywYV+m5FnyCfxiE9mB/lpmWBFcMFQ/VDE0uYfarfrGvwjPcHhvsBQr3eoZ8/UsaSQTPSzpzSilYl2m2fUHZuLFjYSB9ZIft2TWnYnlzzpFX+u9HjP3XBkDRLyDHwSUFaEZxZ5Ty1wn5xlP0jaPPdh9OwLvEdwDBAggsYEIJBZChYRXslK5E+NY1/XkFAKp1mnus7+40o3fb6bOdlGXewTrCukc5280UaqubF6uIM+pyP+90cuOgcWgfQycET2N4P5NX92zZ9bDxY2fwsOqCOtEwFKzliHY38fzdZYDiiggJGviJ5YFj5OHkAREAj0Aq/OGHhggcIcuoA67tgn8qfAkVCJQgrhYv1VFf2xHUPDilYy0csa72HrGqotbfQFVf3ejCKyPRJzT0T8U+Hw9UBu1Z9d8aaX3OlFGHThy64F8hu/BQd5Mq5CiBXlTVISCvHPSJT04z32pW2WoHBSbpC+A4HgM0S81HNB50+Nw6vgu1Wi1T7mWFt1bFMX3bH4Ng17Sxr3qsG3YY7ax9P++Vx8NZtZTxU2UsXN4P6GJ7u8l5x3JOacxD9sWCXt+B9W/AZ14GRwgAHDASkW4t9wlEaOoZ4UYGQPQhZXZCsBRqV921yiQ/kT4whZmh26urkehq75qmtVlQ7NHuatxczmzcLuzby9mLYmoyuB8II/uQjvSDxrD9zc8uyvOfPL7vyKb38dpZe/AECrx/+w4m44iP99p+RCDidPOOJHxL/wMfDIoIDxY7R4CwpAQKrmpHInYf0viB3B0fYdff3MIEfbVmVbVoQD1zOZrVBoYW9vMhJZKRR2C4fO5L4dhZYrt7qdXnQUV3YLy47CsutgzX9j03/DigLMlV4mcfz/xTBU5tsD9qYAAAAASUVORK5CYII=', '男', null, null, '1233333123123菜市场', '2022-07-12 13:55:35', '1', 'admin', '127.0.0.1', '2022-07-12 13:55:35', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_user` VALUES ('4', 'blog', '$2a$12$qWkIM1ZwBhzfs6eHgu6yFOeZB0Rtklt/MlzTg/YkagS19k8EmeV2C', 'Mr.Liwen(博主)', '', null, '', null, '', null, '女', null, null, '12', '2020-12-03 19:56:04', '1', 'admin', '127.0.0.1', '2020-12-03 19:56:36', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_user` VALUES ('30', 'liwen', '$2a$12$TsY.B4VLwtuIfGSTIpWjHu3tiuwjmVoFoqYTbJv7f/193kdCCRIXO', 'liwen', null, null, null, null, null, null, '男', null, null, 'admin', '2020-11-25 02:37:33', '1', 'admin', '127.0.0.1', '2020-12-03 13:42:11', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);
INSERT INTO `base_user` VALUES ('33', '123546', '$2a$12$1GAcGCcSkDpCvh8O2qyr8er9rLXcB.MxZfY7VaYF0CA2wHHkKzqke', '3333', null, null, null, null, null, 'iVBORw0KGgoAAAANSUhEUgAAAnYAAAFmCAIAAABvCECoAACAAElEQVR42uy9BXxc15n/rd232zTvNts2u+1226RJGyiEGnZiZmaQmTExyGK2wCTLwhGDZYEtZhoxjthiHJQ00mg0wpE0DPf/3DnS1Vi2k7iRHY18n8/P1+eeOXNhdO753uegnlwuNzY2XrFixZYtW3bv3r1q1arFixdDeP0ztg0bNqxbt+7o0aPHjx8/dOjQqVOnjh07dpQ00kgjjTTSdNOAZQCyCxcubN++3cnJCcMwvc7OzrfeemvJkiWffPIJbIGvsF22bNniZ2wA9VdfffXNN99cuHDhokWLvvrqq48//vif//znF6SRRhpppJGmg7ZgwQIgKWLca6+9VlNTo8fj8YBtw8PDY2Nj2JSpVCrsGRuczt3dPTQ0dGBgQDteTRpppJFGGmk6aIAwiUQyoTHwHgUCgV5PTw/4jgTh5HK5UmPYszdHR0cfHx+4FIw00kgjjTTSdNwAnWKxGIXfe+898GBxxH722WdDQ0Pj4+PP+Wpu3Lhx9+5dhUIBYZlMhgBPvgeRRhpppJGmo14sYR988EFvb69eV1fX2rVrpVIp8YFIJIJd1TO2kZERGxuboKAgYD7sAlzJNyDSSCONNNJ011AbK/iyQqFwwYIFOGI5HM6XX36JPp6YmNBm7bM2FxeXsLAw5L+CLwuXNeMtgDTSSCONNNJ0xRDOENGWLVuGt8WCN/mXv/yFiCUCz9qLhbNcv37dy8sLaI+wT1wcaaSRRhpppOmiF4voBtuPP/64p6dHDzzZTz75BNN05dV2dZ+DXbly5e7du0Tj8HM7709YgYBeJohIBoPh4OBw9OjRnTt37t69e+vWrbt27Tpw4MC2bdu2k0YaaTpr69evP3PmzI0bN0QiESpdn39nF9J+EiNI+tFHH/H5fBKxz/t3R/XhqO25qqrK3Nz8zp07JSUlEC4uLi4tLS0vL4dAOWmkkaazVllZCYWbkZFRXV2dRCLBptrCSAKRiCUR+6x+d/QLo8cMECuTyfLz8wGxDQ0NMyrqSSONNF03eFG2sLCApxt1cCE7mpCIJRH7bH93BFfiNiGmqKjI1taWxWJhmr5mCLEkaEkjTdcNXqBLSkocHBy4XC56qMnnmkQsidhna4T/SsTQaDQbG5uOjg5tsopEIvKFlzTSdN3KysoMDQ3b29vRsz/vyzfSSMTOiZ+eqDUCg/dcQCx6CAlDLTcq0kgjTWcN0/S0uHTpUltb24tQuJFGInau/PTIi0VvtcXFxfAjtLa2Qgz8DgBXADBCLGmkkaa7Bk9xdXW1ubl5Z2cnRrb+kIglEfusjagp0q4EptFotra2CLHa8TKZjJyKjDTSdHoivbq6usuXLyMvFjUSkfOxk4h9ARCrVsAZIb8L5ZgIm5QMtmIpfISppZhsHFNK1Sq8e4IcfhK1fJalkqGAZGy4uqzYwcaivrp8xkfTgR8thVLzZ4WwVIjJRjHVBNyjQi6VqTBwluGJ75dh45ofYUKFzYO5P9QY/NEUak1oWriBGyHHMCm+hRjIa0pNHB7zNFJpvjt9ZLSvmCE1pkIiS5yf1kRjQ5q/Oy4VPN0P/Zk0fzsodpRTmQGPU82OZLKasjJLY2N2ezsml8/aYbUF2AZp7aokEni00X2p4ZVeppXVVTNyrErzmJC+NYnY2f8B5HKZBG5VrMBEamxIDDTTZDzgq1KEKccx0SC7ubqmJLeuqqymglZdRJ0VVRZkVOSnQwC25XlpD0qyi6mJ9wI9Tc+fiAvzrynOQgkgHqWZtfPSiitLCysKqLVF1L62KmyiD79HvKzBJEpsQKQGuEo09JBpYuYvYlUPIZYoeh6h4/dIjc3UYyirIhE7ZwyHq0Q8ppCLIW8oVFKlWqbWEEmt1szYiv5QSkQctdaf7sdJjT2orLIyM2e2d6BdTZabzeOD8FdoqQy2RG7U3JQS+DqdPx//UkgilkTss/oBpErJuCaHYRNShUimoYpKJhvhZUUH7V/92edv/mrbwn/sWPbJ+q/e379p+d7VX8yWdq/4dP/aBaBdyz/RX/U5xGxZ+P7if/wRdg+s+wo+hRgIzOIZQTtWL1r71Ye7V35+atvSrV/8ZdE7/215Zm9rRZ5UiC/TC78APGTjUqVUic2PTsxqTDpZcHwHYjHVNGLVsyFsuuTSKsJIxP70JhkfnXzpwbGDO314DZZoQqn1x5/MLejvNntWVlZhaWnd0tL2bMrxh3ah7FQoVHI5jtbpglSNySRS6bhEIZaTiCUR+7zOqxLjVcFqvJzVNJBAnpRyGitvmpy+em5XKsWytyRKUBE/0pAlbCloz4vqr0yaFQ3WpPbS4gRVycO16fyKRBAEhh6kQbywgTpQncIri4ctJECB2TpvZ2Um/wG1vzq1jxbDK4lsSva5Y3f27NaFKWHeyvFBVG2u6YuhkovRz6LrhiD6wxCrenqazvQJSJvTppRPUkSpVsnVKoWmtkZbqNqBwK1ilgSZLL+k3NbxRm1Tm3z2DktIJFc/9rBwI3I1JpHKpTKF+vGvg9qvgCRiScTOPmIncMqqJFOtldLulpqgG6YUs2NtKd6iqqjx8gh5bexgUUhPTqC6jSosi5gt9eUGDBTeGa+4P1IaNlR8F2IgMEoLh5jhklCIGSu/B1tBQTAEZuuknLwwSWO6ojGlPy+An+0jrYkeLcdB63Bud2ygq0LIx5TiyZ9CIcLba+cJYlWPQyx8JNZsH3FzZ8eRJW0uVmvg4FTi7BGrMTa/v6ato6qlrYbOqKGzQLUduOraOUho98erpo0ReD/mvJlVVGomhEGzdWRtVbfSK5raqlo6GlldHMFw/4SUaG2Wq6cddK2aZLKWhUTsc/BiQcAVpRTv+CPkx/o7O13cJ27KBLhi1eGyEt/+1BuKsiCsIZqX7qIoD5kVqSpDJwr9JCWB6qowFKOsuCsq8gdBQF52B2KwmgjYSkuDUGBWJK+L5+f692dR4LxYbYS4yHe8JFjZlJoXfNX61M7itGj8nUOO94GCn0U1MTR/EQvFjlSNTajxklaK2mtnh60ka+ewiWVqJd4Uoi6tbgi+H3s7INjWxf1bG/tz1nYaOYC+sZrUeUtHIvwjddnBaefJb79ct3XvOYOLV67jB7e5+q31rB0fjnnB9hoEzlrYgeDgho63zG66+YZFVDQ2y6Y8WpFIIp4QTSFWRSKWROxzQKyms65KjinxALej3uHS0Ux/e1aalxSYVOwty3XBqgKxmjvKEh9VeaCskDIrgiNLCzzhmGqaHwRgFwKiXDeQqtQXfYqV+UMYEqPtrAiAqqqJUFeGiPPcRTkucAGSYr8eqresNTvkuqGdwQlMLlSLhtUSIaLsPChUH0WserIbFMB1XI0JNVsx6hilaZx7Cv0g1pI2ZwxIIxDKMgrKTO1vHL1gYnLd5WZg6PWAUOewqFvhSDEg57BJod0fL8/YFKew6Ct+IbB1j06CrWtkgsv9+Nk6Puj2vTg4pltUImzh+A6BYdbeQYfOX7K+5ZJeWDwwLsZ9WYVq2n8lEUsi9rmcVjV9OrWiIj/95PYVnKJoSW2CuDhAXuiJlVKwEnes2ENZ7CXJ98BovrMiVYk3HqgKwsr8pPnu6lIfCMMpQFhFAGzFua54mjI/RREFNFvnhfcGYKq8yAuj+cCtKQvc5aV+qrrY3uL7temhu1cvGO/vRMOEFJKJedEWOxOxU/+D5zquxEaU2JBGEIDdCcVTSonJNVIoJ/FMUnau12mExqafM73iERJV1MygdXTmt7ErewbyGV35DG7elPLpvZOC8Gwoj95dzOkr7eqHbSGrN7uNU8TmQWC2jg+HglPktHfmdnQVMHvg4LCFcDm987pf0CVbx3hq7pgc72yCj9KVy3C4qh6irBp1qCeNROzsmhJ1elFNNvS7Ozm6Wl9sy7k3UnZfWhqEQ67YQyMKhHFHtsRbpyWmBcN94VjFb40CgvA4LXSgIq6BGmFwXL+iKAf36edVofooYlUO1ywt7M9/Y7TX3P74WaOdhjZHDK2PGVge+dZ8/1Pp9KWDplcuOtyymlCOINaOS8STrX1T3aDUmu6dKhXJ2udqaN40NEEpUdjVMvvW7TkWl1de1MR60DOY/qCN1i3IZ/bkMHuzp8XLYUwKwjotGrM7o6rhTnLmOXOrwqoaxeQPguCq0Giy5zSJWBKxz6oAloCno8KLYbhhl5tXXWwMWnPuD5dHAY0IpkIAUCQrDYCtTktEC4H7ghtBiIUthMfKwgUV8XXU+5eO7ysrysNXAcImx9/PE8TOrCVWvPP+a5v3LNx24Ku9Jxdt3v+R/umFO44t2Lj/0+3HP38q7Tu9bvvhFa++8R/fGh8ZlvAUmFgDWtVDXiym1fGYtOdTN6VSyWSyRxF7xuKatWtAUUtnUQc3t60zo5FV0jOU3tZJZfGmxeQjZU8FdFfg1xYxuek1jdaunjcpvhMyfOoZfD6KSb5qRCKWROwzR6wmADd8+4ajs41hW27kYHkM0AiYBHAFAYfA+YMYCOi0gKZwF3AvxNvDDMTSivJR5300hmFeIFYxo5ZYiUl//+Z/GtkednA7ecXtsLnTzqveR2zc95k777J03/FUMrm+397zrN7/r3fcYKv5tW97hxlTtceqh2qIyRrj516uoXmAEWhRaTY8PPzHjxel1bSWcgTF7P6MZja1ozu/sz+lrTOTxdOIT0iDKIF2jC4KbrC0W0Bj9wYkpumfOsvg9sIPIZOKH0WskkQsidhnYQgkU2MnMZfrdq62l+n5UeDFIqAi5w/xFVAEAZ0W3MI4LVQbsURFMeHFIsQq5gli5Y8gVqXExL/4tZ7l9aN2HofMb20xdlpn6bbJ3GWDjedWU5fVTyXD6xvtvQ79/h96Bva7tx9ZaH7tHJ1bq9RQFu88pVbJ5XK8rCcR+5NWF0NholQqa2pq3luzvax7KKutK4/dl8vuy2T2pHZ0Z7B5GvGRNHwVIOk6YnM6+zPbu/Lo3Qm06j2nzuaVV8iVaFYyxdQwRQXREEsilkTs7BsxHanG6VC4XrVyt7nIKogcKbsvpgUjvgJoIQwcEpZFQECnhbxYuCNALGqLhQDEwytFY2aY4fHdeFusWjmPHjnF9MwS04gd/81relc9Ttm677Fy32jtudbEZamJ62KHwA2W3queSsbOq228tun9l56tx36zmwc2H/jSwvFsR3edDO+lrJm7Ua1UyOSTM9iRPTaf/59/ak0bqVQaGxu787IVOHbZjJ5M8F+5A+C5Jnd0ZXX1p3F4BF8JxGYzdB6xud1Dya0ceKWIKir71sY+PpM6Mj6mQaxmGAWJWBKxz6HVZmr6b8hzYncHM2/rs92F95DDiiqKEWIhBhCr614svCigVwcNX900osANwq21ZIYYH9tRVZiJD2Saap6Zj4hVKDHhr/+oZ+Ny0OzWBnvfTfYBa41cvjBx+8LSd6Gp15KnksOdDU6hO/V+pecZdcb4xjYjxz0Hzq2xvn6ByW0SSYfUkz60UqWEskxJIvZ5GhQgwFe0pg1ibUBAgL6FQ+yD1rKB8ZSOznQmN4PTm97Zl8ruBcSmcQgXlo/4Og8Qm9E5CNs8Dt8vNdvwmlNMRsaEVKJUSEjEkoh9fnMUT0oz6T/F7rKv1aneQrw2FfUJQlWpBGJ1vS2WaIjFu0kXuWBFt4GyqK64PSPQ7OiWBwVp+E8x9ezpfgZXTM90ODUiVomN/AoQ67rX1HmNY8A6u4Dlph6fWAcsMKF8bEL56qlk6Pb5Gcf3f/uhnsGNpWbOW8yc9G/4XNI/tsYz4FppRfaEZHRypR0VDlrNoB7SnochshLLoSPz8/PbYmSR1MIq7h9NYXQl0ztT2T0ZXfxHEZvN5OcwBCBd7+6UzhlKY/ILugbuZBWetboSlZquxNtiJyYRqyIRSyL2mb/riqcmeBrHlEKvKxf9rU70FU66evMPsWjQjqY7sRvO16JbAFrYBfTS0/0sj2yoz0+aXHsHNdXofAZXPRaxv35Nz85jv7nrmiu+K808Pzf2/Oe18KVGnu+Z+yx6Kln6fu0QsvLld/SsKOsd/fQdvY+dNdtic/ObjTuXuHtd72A2Ta2YppocKUHa83Jhtcs4RFx/f/9tZtY53X2Jraw0FjeN05vC4iaDL9vF1yCWN/8Qm8kdjW5gUVm8+Kr6QwbGoXEJSlS18zjEkjMU/5SIRXkUttptGygSYoCLkKy1tTU0NJT4IupogAwtPiwUCucaYqeX1wAnQy31dDDxsTrNzQ8F5Gi3VhLdnXQdsbLy23IaRV4cLC+6j6s4WFrmLarwGKvy7aB6WxzZUZtTgGnW88OH0GFS3c/hM7s7KTGpArzY1/XMb+2xdN9qRVlrRllq7rPMzHsxyNxr2VPJynOTrffml9/QM3VZbO21zMJjoQ1libXnMiv31bvO/NXYYWtHb4EY61NgYqFkXIlNdy2WTq0UqMnn2iJtNss1YkV0MF9f331GxkUMZgGDncvozGZ0Z3Nwpqaz+jPYAhDOVxYvm9mbw+TmMbtAUz2Nn5WIwbhovNCMXs2ZU+OIiGRPe/xEzkBa50AWozemsPq8hUNsQvrkInqaSf/RfCmTkzqpydz3kyIWbQm+oq7wM4xCoVy4cIFIho4wNDSk/UUSsSRi5xNiTV3WW3psfOl1PZPbS6wpyy08Ftt4LwXEmrussHLddNF67YlLa3uGW8TqEQWmGBoXKuSTV0Lk7qlHg0Tsc0Is8DWfwflOxPJypij7fBBLZRGIfWTUzSRlScS+ABXF4JWiMHyEAhwOB7aDg4P4nFxyeVpa2smTJ1HisbGxGWeCZCRiScTOM8SauW4AR/YXb+qZuy2/4rPKirLU1mcFINbYaYkdZftZs2WnjdbpH1/VM0QXjIEvqwDnFa+61upINlXZQyL2eSBW38h0ErH07ixGzwzEznBkQVPjeZ6VtKe8eOLYVq00T3t8ErG6hFgUQ9QVQ9FgZmZGp9NRZE5Ozp49ezw9Pf/3f//3f/7nf95+++1XXnmFwWBoz67y3b4siVgSsTqHWHPXzdaUrb94Q8/UdYWN1ypzt8WWnkvMXBfZea+zcFnnFHjM2H7nJSv9vcfXdw+0K7CJqbw97cUSXV5Jew6I3WNkBnwF9xRcWByxOFMFOGJZgwRiCcqCiMGyz0hP3UP4KY9PIlaXEIsASZQIo6OjixYt+s1vflNRUQFp8vLybG1tnZ2dr1+/TlQmr127dnx8HMMXURJ9b2lCIpZErO55sS6brDy2vfQnPROXlVaU1QBXS4+lANprAZvNb6+2ctl6xf3gZdudhjYHzhrq19OLicua8eZK2vNB7G5jC+AruKfA10xmbxYHxyogNo09CJpBWVAaW/BMBYB/Kj3t8UnE6hJiJyYmtIsD2N2wYQP4qeCwwvbDDz+ET318fGxsbFACgGV5efny5ctRVdjAwABZUUwidp4h1spju63Xzpff1DNzXWPjvd6SssLOd7Wt92oL9xU3Aneev7L0ioe+vcfRCxbbTewPnTPe0dbWIhZPaNcJPxmxZL3x7CN2p7EV8BXcU6DOo4jVouwkaJ81YnHvGWn6vA9psgZ7KhmJ2PmMWERKFK/QLB2ye/fuysrKo0ePRkdHg/MqFApdXFzc3NwwTX9jSHDp0iUIgyOL/Ffk0ZKIJRE7bxBr7b3LxmfXf/xJ7/LNZaauKwxvLTJ1W2Lqstjeb8M3dl+6hR+2cdtu6LjpitthK+fD+84udnC0qayiqdT4Itma1Raw7+QrWebNMmJ3GNtM9hsCMfoIxGY8QjuktGeuYZzrmvNOYVW7ZlhD2Um4DmoSk4idv4gl6oeJj1asWFFfXw80/d3vftfX1+ft7Q3Q3bNnT0NDA1EtTGAVjdshEUsidj4h1txjm5n7Fr3/0Ttu/ekpm08Pm/7tmOXfj1r87ajZ389d+Uz/wjvHjD/5xnrJMaOv9M9+fMxw0RdffhQU7D0qHMDXu1PKn0xZErHPDrGavkXMSS8Wb+Bk9mtjVQtpz0FAzeEM1jBC7GMGtk4idhilfNrjk4jVGcRq91QCdlKpVAsLCyArINZeY1ZWVs7Ozps2bVq1ahXA8tq1a3Q6HX0LGPzYQT4kYknE6jpi7fz1j1l+sVj/1++v0ft4478t3PPS4t0vLdiu9/XOf1+442cLt/984faXF277z0Vbf7Voy69BH3/6rqn5hVFhvxr/lb9j/iwVNrkWA2mzidgtRtb53MHU9q4sdl9Bz1CGpkUWSJbbPZTU2lXIn0ho7U5q78nsGikakCW082AXReZwR6idg+Dvxrd0ZXIGUui8fN5YYhsXVDGqTGX0xTZxsrqG4FP4qGpMHdPIhgB8K7qBBfFpTD5sc3tG4QgQiQ4IieNb+cmMgSK+LLWdn8MZSm3pyu8UUNu5mW3deZz+HHCv6fws9nAmeySFOZjKHM7rFcK5YAuvAnCdcEw4IAiFs7uH4XrQKSAGUgJiM7nDOay+qPzKc6ZX4hIz8KdAqSIROxe9WCAr8A/Bsq2tLSQkZNmyZeC2gvPK4/EgDH6ql5eXi4tLQUEBBGxsbOAr8HV0KPgu2aOYROw8Q6yN33YD51Xgwh4y++Co1YcnbD8+YvneAeO3cEfW/B/HQGbvHzP78JjZR7hMP166/FNbOyPhOF+lFmsoO5XJZ67DQyL2mSB2q7FNAb5AbFdmRzcouYmZxejJYfHSOrpzuwRFfWMp7dzSQSmA6n49G6/CZfYnt/WkM/iFvHHc2WX2Q7igdyyN3gcB+KhaqA4qrsvtHoFIiCnmi+7XdKR28KjsgaRWLnyrfEgO8XGNnOzOIfgIvghfgU8hXNIvzuwcqxJikbUsfOEBOi+roye9iVXSNVDcKchu7y7qGiwBprb1pjEEeT2iZPogHBmOltLeG1PPyuOOFvVNRNcxIRLCcA1weXB82EIMKL9HCIjN6B4Cxx0Q+42ZHYnYOY1YtAQjapEF0I6MjBQWFr711lvDw8PAxdDQUBMTE3d3dyAuSl9UVLRly5Yf3nOSRCyJWJ1DrBllvbnXegvvDVa+G2z8Nlr5rDXzXGnstsSSsgLJynPVpDzWgLZsX3btpplwnKdQjWnmVpx8AX1ktTuEWDlZQs0uYtd/a5zL7qPSufmd/XnsXmpHZz6HB9uSngFgLcQnNDHBowX/EjzC0mFlMW8suaULtknNnYlNnMohWV7XUEwtvZQ/AYEczgBsc8G77eiFZKltXCIGEkOaqJp2iIdP09p7qoblBdyRTEYf7PrnVdaNYxAZXd8FHmpulzCjvaeMJ8xn8lrG5DmtrIyGdlq3IKutK6mekd8JbjcfUgJl8apjRl+ZQIwOlc0WwNEgHFfPTGnthgvI7x4u7BlFZ4FTx4OP2zVIIlYHEKs93kYbli0tLStXroQAeK6nTp0SCoV9fX2jo6NoOGxHR8fSpUvRNBRAYhKxJGLnGWJNPVebUdaYe601915tSlkJfAWZA1m9VhKyphBavmffqpvO5qNjPQrVqObaNPPUqx9FrIJE7LNA7F5Lx8LugXRgWBs7s5VRwObW9A/lMzipjW2FnJ5cVk9SI4MmGM/uHKB2DoZXt6W1dgKfyvhjEWX1BV0D1UOSmJrW8v7xe+UNEF/YPQgJkhqZ8FEWo7eIi/vHEJPR3g1pEhsYEAMf5XcKclh9sJvL5kMyCFcNiiEMh6INqkLLWgt7xvJY/JR6enJNU3VPf1p1Qw23P6OuuZDRXdo9GP+gPb2jr2JIReWMwrfg4Jkd3Pi6DjgvhJObWKW8UTgdRCbU0+H4cDoQRMLZ45j8tM4BuNTogioCsSqFkkTsnEMscmEnn35NAOIRNdEUiWCBgYGWlpYEI4HKKH9D4HtH7JCIJRGri4i18ltj7r3S2GOpodtiI/clJp7LNJRdbj7lxVpSloGsKEuRdu1dfu2m0bCwU6kW4j8w3ulJqVI+FrFyErGzjthNF4yzGd1A1mrBCK2bl9NGDy8ovuTsCkpvbL0VnXDA2jGcVpvcykln8Er6xUHZpevPXI4ub6Bxh751ojhHpxZx+tMamWU9w42j8oxmdkJN63vrdyY+aIN4JIBiLr1nzcmL8K3FB079f39+T++/X//52x/+Y90O9wQqOKbw9eS6jn9u2Utt7QT31DwoxtgnrLx3JLets3Fg1ODaLXMX9/SqBynlNbfCoxzvRKY2MIp7RhMaO+MauuCMd3LL7EOibQLvwfHzGL0xFY1wzOAc2sdb9/3qgy8/3LTn3VVb9F5799/f/MdX+sdi6LxUjgC4SyJWN9piUUC7b7BcLkcpR0ZGMjMzL168iBg8Pj6OZlUkvgsxEomERCyJ2HlVUey33MRnqYnnElPKUgufVZa+qwGuJu5LYWs+xVdLylJLr8UgK8riHXuWOly/NDTKUmGTiMVfWNUkYp8TYncYWQJfS7r7yrh9UaXl9oFBKw8d1nvp5Zf+/Jefv/HWz9/6q95rf/7lB58dtL1ZNSKNbWC6xaW5xKREl9XeK666EhSRUtdWxOZV8oazWtl382ju8elRtAcbzlyCBMHZxSG5pRBIb6Rfuu1l5O5n5hUUWVrjmZiZ094JX4TEkCCxprl+SHQtLMbYwx+OvPKM2QXX4KsRiZEltem1LfHF5av0D1y4Yv/HDz76cPnqbae+2XLm4rKDZwzcA5Mbu/K7xzKbmYZuvjfvxQdlFcE2uba1aVQKFwPntfQNcY1NdQiJvB4ee7+k+lZkIlxwdEdvCrsfIfZbc/v4pEzIY0q5gkTsXEQsEUAVvwQ+EXG121yJ78pksry8PERWsi2WROz8Q6wh5Wsjr4VAWTiChc8K8GhNKctNPZY9DrELQbv2LnW8YTAsZKsxeIikakz+nYiVkiXU7CJ2l5F5Zisjs7kdX2yntY3GYlHr6y08PKwolJCsbBMPL3Nv/2KuILWFndLWmdbRff6GazC1wCE44sJNt12XTL0SM7yTMu8XVdwrLN9z2fyL7Xv/8Pkivd+/8fqCpa99ueT9tVtuRyZU9gxcvk3xT8sx9fDb9q3hZ1v37LhgfDen2Pl+fFRJVSmHl9XMgC8Ws3p2G5j910dLfvnh17/7bOl7a7b6JqS5h0caXbux6eBBew8Pe4r34csmm06ccwi+n8/sK+WNZbb3RRSUGbl6J9c0lbB7rX3vUBLSq3oHc1pZ0aXVby5cAXr1g89e+svf4WJ+8/6nB0xtotp7kll8Kr2HRKwOIFYkEhF9nWBbWVmZmJgYFxcXExMDaQQCAewmJSWVlZURlckJCQlo7R1MM3SHRCyJ2HmGWGPvr019l5j5LTXzXmrkvgjVFVt4rUSy8lpu5bUMl/cSK+9FIP0DK67fMhoZYyHEKlVSErHPE7G7jS2yO9gFzM6y7l5AbAmDQa2tMb1966XX/6j30kv//trrBi4e4OZS6dyygQnnpKwP128763hr5eFTm89e+r/PFv591cYl+46G5pYAaO0Cw1IeNCdWNRyxtE+ra81tY7vHpgBEMxraLzq5J1U3esSlQmIA7V4jy7jy2mt3IwMz8sq7++HTD9Zt9U3JcoqITazn2N2Nd4pMKe8eCKXmf7pmwz+XLdP7j5/hevllvf94We+lV95dvuFqWHxSPSu0uMHCKzAkuwiOFpSZDxS/EhAKxE2orIfLAOICv+GFwDM+rYIruBEWfT00KrKNSyJWxyqKiRGuhw4d2rhxo4HGvLy8qqur33777cuXL5uZmVEoFDabHRkZaW5uTiAT+77ZJ+YlYqUlHspyb2WFn7zMd6zAvTvlarLDHmbi9RFacG2kXWuScz8ttL88oo8WNlgdI6iM4hdTRqsCx2qChRUBA4WU4RIfdeM9YbGPrCJYXOIHUpYHYxUhqrJgaZGfKIcyW4iV1AW1ZVBMDmx5kJ2Pyec/Yl/5I45Ya8/tgFhTzyUASJyvPkueeuqJp9Qu/RUO1wxHx3rhqlSqqatSaWN1arDszGE8pM1GRbGBVXIdvYQjoHX2Uxvaq7v5WQ+ajW66bDh8atWBY/qXLI7bOhWxB3NZg7F1nOsx2aZuPkfMr7y7dI1fMtU+IPQOgC0lKzy3xCMm+aCJ9VELu3MOTn9ZuOLbq876BmawDc2pKe8aO2PvHpFTfT049ovNBxbtOLL1lGFQSp5fQlZYdql/cp5zeOLJK7du3UvaeMrwhO219Se/3Xjy28s33R18Qg4bWK7WP7br1KWdpy59vn7Hzm8MN506/7X+IY+k9NTmjmMON27ejcuopd8KS7D3v5fdyL4WHL3trHFwetG9vMple08eNb+618AaYvZdtjls6nDxumd0R28ig5fZwY0prL5g6YimniDbYucoYrVbUiHy5MmTJSUlELa1tQ0LC8vPz9+xYwcAGLxYNzc3Ozu7GzduEAvYwdG0WfsCebEl3ljz/bF8t5FsV6wmtNjj7JGPXgq8uHaoKMjxwJdh1nsFZRFDldETDcndBaHdRWEYM4mb78kr8pa3RA/SAkTVoROVIf35XtLqsP4sj9ECX0VV2Fi+LwirvofVRc8WYofLKO2ZXtbHdzcX0fBCHh+zqZrAJlQ6bmqVVK2SqZUqFZQlSkypVskxsQwbIhH7YiL2sPWNhhF5AYOXVN2cUt3odj9u25kLX27Z9Y39je3fXD5l57TLwGqH4ZVT171DaS13ihvNPfwgmYWnf2xptbV3EAB1zyXTwLScq8ER646dPWxmC2T92/J1wNp9hhZ7L5t7JOQXMPovOfuHFzywC4j8fNuB5QfPHDRziC2rc76fGJCeH1vWkFbPsPC+6x6XucvQesclk0+37P5q14GLN1xvh8fae98xv+VlTwnef970oIHFPgNzADCkAcSmtdApGdmA1YCUfEBsSGYJsDa3uTO/tbuUyb+TUbz1jNEhE/vt50wgsN/QFrZn7W4DYpOYfQixxOxOJGLnImIJ5xV82dHRUQjs2rUrNzcXPg0ICIDdvLy8P//5z4cPH96/f/+RI0dcXV0hjGlmnCAI/d2UnZeIHcvxxGrCx/IokuIArCWhNsjs5Ge/9jy5oj7cIcR4F9XtckeKt83exSeWvW29d0mqm0lJuKX9sQWG2/8Wef1QqO0eu0OfH1/02zSX0/x8v6q7llZb/3Fp+etlfsbSigg+1YuX4TmLiGXm+AFiKzOy5aMKKPRJL5ZE7PxD7MZvTeOqWqitnTX9wpxWlsv92DN2105ZO2w9e+HCdZcLtzy3XTTXN7tqH5YcU8vO6Ry9djca6GjlE3I3p9Ta9+6tewk3w+MgppjFiy59UEDnwvas4+2sZlZuW2d8RUNURUsBZ/CSW1BQbrlLPPWAheNxu1sGrn4xFY3XwuOjyuoz27uy6NyLbn4pTcw8dl9aQ8ft6GSf5OyiDm5xOzcwOdfohueeb0zO2zmbufit2H9yxaHTVv6hATnFqa3MbBacrs7xTnRgRlEMrf7a3Vj/tIIK7nBuW7eVT6h3Uk7Kg3avxGy/1HyIcYtJd76fHEPnJbP4MxBLjoudu22xCK5ERXFWVhaRpqKiYvXq1eDLJiYmAm4nJiZqamq2bNmCTXWP+t45FOclYpWlIaI8P2VZGFZ1fyDDqzbIynn/1/7fbLp9ZIX7yfW+F3Z1UkPyvGyTnY1DLE5eP77p7tUjtscXJ1IuUgNMj696M8z+cOS1416GW4vvWN8x20M5vzH99vn2RLeBwruiqhhVY8osVhQ3p7rvWPCe0dGTVy2vmZvZGJganzU+a6LjZmpiYGpy2dTYxMTYAmRsamJofvGy5SkSsS8mYveaO+az+wvZ/JyOrjx6dwm7NyAj+5CFza17cZnN9NsxKbtN7I7Yu4SVNafTBQU9EzcjEsMLqhxDYiJLau/mloP7m1jdGlVad6+oppjdX84dht2VR77Jp/eWcAQ5bfgUUSltnceuu1GoRWkdXbH17QlNjPvVTSltHLt78XdpDxKbmbld/RcpgTF1bXndgqTadkCvZ0JWfkdPWddgMZ2X1cC6l1Nh4uIPvimg9Ob9pLQWdknvcGHvEHxd38j2s20H3li45rcfL3ptwcp9JnYRhdVwJdZ+4UUsfkXPCBzqWmgcXLOhq799cFQck5/C7qfSe0jEznXECoXCGV84efLk66+//r//+79vvPEGWjgWwu+///6CBQvefvvtt95664MPPnj11VeBu4Db7513Yr4iFquMGqX6KssisLqkvhRKpY9Z6MXt1f62wRd3nV/ybqztmcHiOOONXy7//c8++aWe/sd/OrP1g/RAy6GmlAQvowvb/smvjm3PCnD6ZmOE48lkVwOjbZ8cX/xmQYCNpD6VVxDGL4qYLcSOVng3JrvuW/rJTROLyOCoyPtx0YnxkamRUTpu0VHh0VER0XAfkXGgyOioe7GhEfF+JGJfTMRuOG+a0d6V2swq6RlIbejYZ26z7NAJ37Tsku6+Kv5oeivHMy3/sIOrb14VtXMwsbXXyj8ipqJpv7njn5duePWfC//740W/ePfjJQfPBOeU5dJ784C4D9p3XrZOa2RRW7tgN5criG9mGPrf9c4tyeD0ZnXzQfGtzOXfGGw0tgwsrUpoYyW0MM+6+YRW1JUIhAm1Hc6xGb7phXCEjEZ2ThuXxhnMbu6Kq2g+aXf7d58tPX3dI79TENdAT2xhZ7J4e03tHENjQwsqvVJyzX3uXnbzv3EvMayw6npEQuKDtqy2rswWTnx1i0ts+vozl409g+JZ/akcwQzEkssAzFEvFoXlcjmaR4LFYo2MjEAyLpfb1dXF4/HodDp8BIGenp7+/n7YhTAxMQWwU9sJfkEQq6JFyIruTuQEYzXx6sqYGh/LgDMbpGWx3qc3faCnF3/lbEOE27UDq9vjA5JuGNnpr/SxOXzf5WInLZIafOXk+vdaswL7quKsDy2jhd/oLozg5N6Nc7pwx+pIS5K3si13uDJhthArehDQXXTH7vS+amqubEQul2FipVyoEip13FRKiUopVSmUSgUGUqiUMkwkxQZJxL6YiN1leoXWN5rH4ae1cvCF2eldsTVNcQ+aE+vb0ts6Mzu6S/jC6Dp6Yhs3obWb2j0aWdlSO6rMZQsggBSUX5XUyM5m8mEL8VQ67155U0Z7DwSKuCMZrO4C3kBsY1synZ3C6Ipp6sCZ2sZKpnfeKX8A0M3pEdRKVElt7Owufiqdk8Xoi6vpoLb3VvaLS7tGCpiCIvZgdlsvCCLhdHCiol5hJotf0Dsa08hMbmLF1bbnsvm0PiFsS3pHinuGIyubSnmjEEht4aQ0syGQw+q7W1xTPSTRXmnnW3N7ErFzvS1WG3sHDhz4/e9///LLL4Pn+vOf/xy5rT/72c/+8Ic//OlPfwIH97333vu///s/5P5+90qx89qLvY+VR4jzg6XFd0X5IQ1B1iHnNw9QA/kZgWcWvFbgYfYg5Maef/xm9e//bdu7/+VzfleMy4XVf/vFV6/pUYx3O5/f/OXv9T7/Hz1fU/0HMbcNNn/0jp7e9vd/leNj0V98ryfv7kBp5Gwhlptzsy7B2fTg1vq8ItTdCbXFkoglETufELvD7EpEZUMOi1fKG4l90AqsrR9TUOncnE4+TTCW0MxKo/eWDUvzeWORDaziQWkxbyK2jpXU3J3e0ZdB59P6JWUCaXwDp4ArTG7hQgzEVw4pslgDkCavayS9nZnH5YGywYXt7AWOFvFHivqFGWxeGp2b2z2Q3M5JaGLE1LWl0zsLuf3F3LFc1mA2Q0Dt4Kc2c1OauiFc2juRzxnO4QxRmYIMZn9Ke28KnYfW+cnp7E9u5WSx+zKZvfeqmhKbWcV9o7ldAhSZzx0Efxd2C3uHU9u7Yus7kuBbXYMEYmPi06byFYnYOenFokhwRsGXvXjxYnJyMor87W9/i4a9uru7Ozo6omSQpqamZteuXXAEROgXcNCOojR4PNcbq45QlYcOpLthdTHC3ABVddRowZ2xolBR+f3x0vvDRWEjxfdEFbEDBWHsXH81M2u0NpZbEDxWGzdSHT1Udm+QFjFSETlaHimpTYBtX27QeEWUoj5pqDhsthCrbA5lZPuaH95el1uIBu3IMBXZ3YlE7HzzYi3sgaZZbF4Gg5vJ7IFwOr0b+JTB7AXl9Y5ksPlR9fSMzsGCflFsSxcQLr9HWNQ3kUbvK+SNwzaukVPQO5bY0l0qkMAuBKjsAfgopp6V0zVcxOlKeFAXlJPvkpAamFsSUVbnn192LSY1qKgqsqYFUFfYJUhv5SQ+aKEkZzlHJfnlVkXV0NPaebmdw0W944DqlNaeTEY/nCWTJYBTozXp8MXp6Lw0tiCd0ZPHHYQtALukfwzCSZqLz+LwU9q7aAMTmSwehOFdAXAOnyZ3DZGI1RnEIrdAoVCgj06ePJmdnS2RSAClr732GnJVKRSKi4sL4BZNPXHkyBH0XdQWq72WwAuCWHmpH6HJkbKlQSAxLRgExwSN00IJCcunNVY2Le00Ii2RU0/8a4idtSXZScTqlheLL8neS2X1Al9BQCNgUgazD8iqJXxh9jSNMpmDT6WCto7kyuqNJ8++v27T5m8MzLyDP9956LNdR1aeuBhR/IDa2pVWz8hp7dpvaP3x2h0LtuqvOmt+LToroRG85P68LmEWezi5lZfOHEDLwj+6ODxc7VOJXJJdlxCrPQMihPX19S9duuTv7w+e6yuvvAJbLy+vjRs3rlq1KigoyNvbm81mz/Bcv7u6eH4iFnhW6k1IRvMBSWl+IDEtAElUFkRITJsphORHJZsUiVgSsaT9YMSaWGWzuEBZDWJx5w+Yms7qQ1h9WNO4/eGqZrGCEpJOmVsfN7M+buVg7XvXKykntrzpeljSrftpua3cBr7YJzrzsqObpbN3JLX4vEvQvbKWwp6xDDoffGLwXDM5A3m9wrQpxhN6+Kp+qEjE6gxitacdRoHQ0FDIslevXr1+/bqzs7OTk5O1tTVsg4ODbW1tb9y4AR9BDPoK+LXf2xw7TwfteMhpHjjVpiSjAWi9AWxSnLU+4jKQHyF5qc9TiUQsiVjSfjhidxpb5TBxxGYxEGJxbqWz+gk3EZEMRx0LHNynRmxVe/udhPhjRiZ7zl04bWlv6urnHptxycnP2DU4u5mbWcv2i83+xvpWXG7lLb/wQ+dMTHzuAWILOkcyGfhq6sDXjM5BavfwE/hKInb+IvbROZ4UCoVMY2gXeasQiUbBjo6OFhYWgo8Lnu4PvJR5ilgKaBqxk74sko90Wn5IcK6nEolYErGk/XDE7jayymNwc3DE9mYy+rI4eE1sOnNgkq+ailnEVyoTlyb8FCpuqE3Iz9lw8ODyXXtPWTucsLq28ZThf3+0eNXRi27R1LzmHmotM77gwY7jl1bvPLxp3/FVJ41OOHreiMqMqGjNYPZTOweT6Lz4Nm4qRwDS5uvUKfhPJRKxuoRY7Rpj7ckUgalod8bkEhAJxC0oKCAO8gIO2lGWeiMWPuJ9+j0iTfofDFfSiyURS9rTInaPkUU+g5vH6M2mA2X7stl4A2o6cyiDNYyEN6myBADXbI2eFmmlLXWJxbnHTIxsvL0NnFwOWzketLx22Nb59HVvx7tJue28r7cf+XDVdr2f/defPvryL//86uV3P3vlHwu+2HPC+k5MdB0dr7LuEuTwRlI5fFCaRuCMaiGfROz8bYuVakzbl0VMRSlFGpPL5SEhIXfu3HkIY5q12V9QL7Z4WvISQkGEZKUPSVnih6TdT0qrw9TjwUwilkQsaT8EsfqGFgV0bj69N3cSscDUYRyxzBFcGsRSmQKAaw4DxHva7kWlHfVxJTmBaQl59PYz129ecPdyScm5FpvhSaX5ZFekt3F90gru5pRFFZQfMbL0ikzwTS2wDrh3/X5yUiMznzuYwexNpHeBUjt5aRxcGWz8sFSNspm8p/WqScTqEmIBn4h/2rwEx3Tz5s0CgYBAr7W1dXh4OKIvHPHrr7+GMFAWvvsCLsmuLA4mJJ9UyHfpcdyVPa67E+oMRSKWRCxpPxyx+7QQm02fQixjGCEWwlQmPkpVw1d+HmOSbT9cBR01ITmJ90tzE+uqjl27dsnX3/Z+wm47J+v7KSl0XmpHTylvNKOZHV1abeDkWtDKDMwscYvNDCuszmX2ZbH7ALE53AEqV4D4qo1Y4CsI8P9UIhGrM4jVxiryZYkl2V999VVMq7fwiRMn4uLiINDZ2Ql8JY7w3YNi5y1ii0I0CkWSF3+PZCXBhKSlDwmN85khErEkYkl7KsQWdXALfgBi8+j/EmIZD27c83cMC7gRE2Ho73f4htNuh5v6jrcP3PC8mpRD7RT45ZX5UoveWbEumlbpHh1v5R12C+hbzyjtGUmnc1Pau6id/ISOThKxL2JbrPZ3uru7y8rKCgsLKysrf/7zn1dVVdFotNLS0vr6+t27d4eGhtbU1Bw6dAi8W+TCoi/OwZV2JksxPHvhiHV3MKHYnO4qDB0rC5HRfFQlOGJVxT7gWUpLQ0Sl5JLsOmdCeP2DrIfopcakSmxcgQl+9Sc9c+edlh4brbxWmlGWmPssMvNeqNHSZyoSsT9xW6yhTR6jN4fZm82YHhebzuoFkhG9cDUNn5OIetq2z6gHrferm1PaOg/a33JJybG4EwmBhCZmUFHVtZjUyJqWA3ZOq84ZxjXQIfJmfMbaC+YOUakZzL58QCwDH6GbxRGk4xXU/FkRiVidQaxCoSCaVPv6+sLDw8+cObNr1y59ff1f/vKXmzdv3rBhw+HDh9evX/+3v/0tNjbWwcGBqD3GpiadmINTT5CIne+InSD4OlWsTCiwQQ1id1l6bLairNZ4sUuQF2vmteyZikTs/EZsTmc/8PXBmCK8oiGveyCD0RNR2VgjlEM4tKyufFAEn4aV11ePyvK5g/GNjIiqljTwpzsHUjt6klq7qOx+QGxKO5dE7IvYFjsjjBpWgZpvvPEGSozaX83NzYOCglB9MqochvjR0dHv5iuJWBKxz8ak09DCHltRvN7Mc7m59/JJR9NrxTMVidj5jdgsdh84sgU9Q0W8EcBtYe9wNpyC2VvCF8bUtcOnEJ/bJQDiptO58FGpQFTIE+LDczX+a273EBwkoZlDIvaFQywa7QqYFIlEMw7xyiuvEPiET4GU4OMODg4S7bVoSyKWROxPYSqiIXaqLValwMY1iN1v6b7DynODmedKc++VU47mqmcqErEvgheb1MIGpiY0MaksHsSAtwpMBbJCPAjgmtrehU98we5LbOkE5xX4CnAFgRcLYdiSiH1BvVhtAxYCO8FVff/99wkuwi54sX5+fqhuGXYBunAoNCKWWNiORCyJ2OeVwTUFx5QgUytxiE386jU9c6eDlm67rDw3mXmu1iAWOZokYknE/uuIzWD0lPaPAVPBf03r6M7nDgJcUbhsYAKIixxZ8F9BEAlfSaP3goCsqR09yW14TDF/nETsC4dY2KJhrxAGDxUgunv37vXr169YseLXv/71okWLlmoMdt95552oqKhz587V19ejI/yQlexIxJKIfY6IFT8OsbgXa05Z8UxFInZ+IxbgShOMg6sKNAU/FXmu4MgCegGuya0cICtwF7mzEJnfMwLfArICX2cLqyRiddWLhQBR2cvj8YCgdDq9trb29ddfb29vZ7FYFRUVAwMDR48eDQkJmZiY+OqrryBlW1sb+gpayJ1ELInYueLFTlcUT7fFmns9W5GInfdtsai1FVAKlAX/NaWtM53OhUgAKgihF/xXFJnSzgW4gheLVzJ3DeZxh3EutnSSiH3hEEtMjiiRSLSXBADovv322wBU1EYLDq6BgUFMTAx4rgqF4t1330UpEZtHRkZIxJKIfd6IRWXHdFusQoEJHxkXuwQN2jH3WvxMRSJ2fiMWPNSEJibwFQiK1kuHGARUYC3sAmIhUNAzBIHU9i5AbG73EPiyGcw+8GVR72LU6YlE7IveFktQdsmSJdoNtNevX/f394evI6wuXLgQIXbGDMZzDrFQzmkQ62l96gVErLDSpz3TCxBLLMk+X7o7KbR7PE31KB76zz/o2bjvM3VZa+Oz2shtgaH7F45hq408PzelfPlYWfsvNvf+ysTzCxAELH0Xgix8vn5S+idpz75Vzq5WQyPdmgvDFHLNqx2J2PmC2KedquJZVA6TiJ1XiMWmpm0iuhwDX9HU/wQs5XL59/KVRCyJ2Gdj0iluob8zlCliBTb4yz/qWbnuMXZebR+w3jZgpTHlC+vAJbC18l70WJl7fmXptdDaZ7Gt31Ib3yUQhhgzjwVPSv8kbdm+yP7q5S5uq0Q6DhlcKlFpoZRELIlYErEkYh82+JTAJwpADEBXocALNalUinxZBOBHB/yQiCUR+4xtYoqyKu2pJ155Tc/i9m6DGyutKKuvBK657PblZbfPTShfmXssfqxM3RZaUpba+KwAQQB2QWbui56U/knae2CNm+cV4TiP8GKVCkwuU5OIJRFLIpZE7BMNdWVqa2tLT08nIom+UQi9ZEUxidifwsRTs0+gv7OKmHrC1v2wmctmM9c1lt5rjD2WGrottvZfa+m56rGy8V4LAh5beKw0d18BW2uvNbY+656U/knauOWrazdN+gfYakwzWFwxeU0kYknEkoglEfvk9i7N8uwJCQnnz5+H3eHhYYKv4MgiuAI75+B6sSRiX4yK4slCZKq70/gvfqtn7nTIxnO/6e1Nxi7rrXw3m1HWW/pssvTY/CSZu200c90AggA+86LnFtB3pH+swIt19bAVDHIk0nG5XCmTqsmKYhKxJGJJxD7RUA0wpOFyuQYGBrW1tdperFAoRMfp7+//3kshEUsi9hmYVnen6UE78lf/9IsLNvus3Y5Zue81vLXF1m+fuedOI5ctli57Hit8QmOXPdZue23c94EgYOWqD3pS+idp284lLu42qEcxwVGpREkilkQsiVgSsU+0rq6ukydPtrW1gf+q0hhE7ty5c+/evZhWEyzpxZKIfe4Z/KGhsfBH1iAW+8M7v121Y8GGg1/sOPXl8r1vbzz5wfrj760+9PeNBz96krYe/XT78c83HPhw+Y53lm1/e92+93ec+OI70j9WCxa+a2X7bW8fXaGUEJenkJNeLIlYErEkYh9nEolELpevWrWqo6ODqDS2sLB48803f/e73xUXFxN8JdaXJRFLIva5IpaYekIzPEapuTULR3Ozaxe/td5v5nTkqOnaM1e2nr+6y+Dm3ktXHq+LtvqGDgdge+jCmo0HPtt86IsTRpssnI4/Kf2TZGP3bVLqXU0nLHhQVHIZRlYUk4glEUsiFiPwiRCoVCoHBgaAmmvWrPnrX/86ODiIGlwhwc2bNyMiIsCd/fvf/87lcgkSz82K4jGJYqqviRQTjxQk3798cCM9P2qoKmG4JHS0MEBa7KMu8sSKXLFiV6zcEy3S/uOlLHDHaD5Yma+q0EOe54qfotQbx3mhB4Rhq8h3gy2eptQbTzxL58VK3LASd7gddaGboogCdycqDRqlhQ9XJxaEu5zdvWaUS8fUcvjby9SYWAcfOaITAFpLEbKx9rsdMfkJxBNzaMMWMtvY2Bjk4bGJPjUGeVUhHBWhQmdoQIy/cChUwrEhQODAIM/p1jV/f184yOAAXisDjw3q7jc+Pg6HgtNBQCgUwtGIjggQA48AnEUqFSuVcgL7sAtbmUwCkSqVAoTixeIJuVxKlnk/tpFAM64BZQD48f39/bddtqTxRpLqGcU9w6ntXbldgtQ2bm7nIJXJzwbhi7EL8uiC/A5BQYegqA1fnl2nlUrvT2fwcxi8sLzSM9ZXYrOyJ1Ry9eTLnEbqqZYUjHyl+0kRqz2Df39/v5OTEyQDV7WtrQ3KEdQiiwwtXQfZGo4IBY12AYdG0M4pxEL+Gp2Q4LcKWU0t5dPrTE/sKrzn3lUQMVIRrXgQjT24h9WGYrUhWClFnHlVTfObLWHlAaDpcJm/ssRHWuCpKPaGACHYlRVSZuuk0mynSXKX+clKfEVF/uKyu6KauNHaVGrwzeNbl2GqCUwpFUtk8MvIdC03QwZD7CSyIsFd1EiBAIxYSMD44Zw2oVSJxJIxooZ5XKgcEwIdMU3Vrlww0Ovh6XL79q3xcZzBxHe1HwHtxwdN0I3WWtZcGE5QiUQ0MTGGAlMonRRQlogB0JIl1I8xlAGIxaqDg4N3GFnnc/hVg6KoBy0FPUPFfGEGs5eYH5j6MGVBhDuro0rr6C/qHae2c8PzaUdNzAGx8IvIFGISsXPUi4WCaQYvFRrDNOu0U6nUpqYmIsHbb78NvixC7HfD9adF7LhErrlVvKIYmxgIdLK6ZXSsuzRuvD4N2MPP9ulLdRYXemEVgRgNZ9KsSFISOF7gO5bvA2FpaRDsThT6QQzOvOIAiFGUh8howRBGu7N1Xqw6BKsMVpbhlcYTJYGjxXdGSsMEtKjGtKCr3+pnRwVgokF46lSaF10dQqz2KyBQDZWqqBWDqEFBnCOcG23HF+VSTUp8HK1ILITiZmJcoVZOTb6oySwqtbSnl+NJcY2MvIeKJJRd4bBEFwTidBqfVao92yjxWGlfAOFjac9R+sPrfkj7jnKN+G1RTHx8/C7TKzE1LUW84Sw2L6uzL6m9s2RgPL6dk8bhadUS85ELCKzNYOu2Ujt4Jb3CjGZ2fNmDY8bmCbm5JGLnOmIHBwehSIIyoqGhITo6GjIxlDKQhsVi6evr29rahoeHMxgMSPPpp5+iAgi+hdLMQcRKNf1fZHJNUaqWY1Jhb1uN/cUj8V52dcn+IzVJkvpkcVXkRFkYeHtiYF519GxplBYOElVFSWpiJiojQSggLIuA+LHyeygAW4icrZOOlwQP5/sN5PmPlYUr6hOkdYl8WnQ7NcTP5qyr1XlMMaqYGEZtNfD8iZQ6lpuFQiFkPPQimJubm5CQkJmZ2draSqxzDPmWgDHEAIwR2Ih3R5FEQCBWrcKXlhWNq6US4J9CJocjyMcnhq2szXx9vXHPVShGINReC5lYmQoMZWYEVKIxRfMVPDA8PDo4OIx3claqNaRXV1RUDQwMoQQokrR/zYi/gnbbQU1NzT8278zl9CY20/N6+pPpnfGtzDz+cDKbm9rJAwFo0ziToEVK5Qh0WlQ6L5vem93aFUerOmFqmV9VJVUryYriuYjYGTP4j46OQvl14cIFNze3W7duERVi+fn5J06ccHFxqaur++KLL9hstnYdmrYTPEcQK5LjjopcoTmXUoY7sqqJCmqso8ExiuXpJC/bptSAvtKo/tLIQdr9scro/vK4WVEfLaa3JAoEAV5pNAoMViUOVSfBVlARjyL5ZbGzdUYkUX3qeG3yaHWCoDyWnRtaEeUW72EVYH+BYntB1NsuH+2D9wypRIS63Y5KdKYtEHmiYCjbAGu3bNly+vTpU6dOGRkZoRwIkUSLLFHsonze1NTE4XA0+RP3YsWSMXzGJU1F8cQYXgDJ5UqVWiaWCIdH+j08XcCLVWtqkonjaA9LI1AK1wMJtOuQ4bAKOSaTPrRQ/OjIxMS4VCJWXLF1zM0pxJ1jyJcqssj7EbVTWvUExJ8D3q7+66PPqB3s4t7+qNqGXC6/eGAksqmV2itI7eydEgItT9MBipfK4eu08tn9ufSeUg6fEplwzMikjcudQqziMYgl7Sf3YmfUsIGFhITY29vb2Nhot0jFxsZ6eHj827/9282bN1Epg33f7Ik/YUUxf3AEFZNKhQy1yAJle1qr08K8rl8+an5sq8Xh9Ub6yy/vXmq+f8Xl/etmRQb71hod3GB4YD0EQBBzae+ab3YuP7Hpa+NDG61P7TQ9shkiYQsXACln67xnNn5usGuJ6YE1xvtWXd67yuL4NjfLcyEuV+B+4a6VohHNI4f/LGIFptDBIpVokjAzM4PMLBAIINDc3AwIhDe/kydPBgYGQkYPCwuDlAMDA/BSWFRUBO+FAOPg4GCFajQ1Lc7M3MjczDonu7icVu/rfdfVhTI0BK+YioFBXnZO+jffnra1tc7KymltoScmJkIJDh5zfHw8HBNc1YqKChaLBbn9wYMHjY2NmKbjFSTLy8uztLS8bGAS4H+nrZWRlJh288btC+cvX3W8GRZ6382VQiutPHnibEJ8ilKBM1itIpYHIO1f9GKJN3uiNDtpd/WojX1V30Auu5PGH8zj8uKaW3N6eGmd3ElxekEZU9J1xJZwBEXM3qz6drNb7hY3nUdkchyt4EsgyqoVJGLnCmLRkuyoIIOPtGdDhI+io6Pt7OygfCEiY2JiPvjgg9u3byNkAmjnJmIBrv1Do6iuGO+QIpdKJ4QayoqHexjVeSn5CaF5scFZkf5poZ4pIe5JYb6zpfTIoNR7AfEhXsnhfhlRwVEBbh6O5sd2rnOzN4UYiL/nezvuDiUx1CfS33W2Tpp13zc3OiAnOjAjwifjvn9VdryA2YDJRuF+Mdk4PG9jwhHNFEh4LzCFzmZocFi/+OKLM2fOHD16NCkpqaenB14E4bUvIiLCwMDA399/z5494NN0dXUBVsvKygwNDYHENBqti9vq4nr9fmSYj3eAy22vy5csrS2vpaZQ+XwBPqOwQtRBb964ae3GjesjI6PZrG4fH5/79+87ODgAZS9cuADHh5dOVHlTWlp66dKl7u5uOF1aWhqcCx6KkDsRFua2YaGRO3fs9fUJ8qL4b9q43d3N+7KBKcXT79zZiznZhaMjIjRYVqUkC6hZKNq0q/Gb+4a2njznHZ9c0M4sZncl1TWW8vjZLA6V3ZXJmRKbS2VNKhMfV6PDKqb3xJfWOAWF23n6FNU1ijTtYlK5ZBqxGInYueTFzvAYCGoCcb28vK5du0bUGINByUVUl6GvEPicO4gVy/FyDG51fEI8dVKVGrKgUoo3zWpYiynH8X62IMWYJnI2hI6vkinEk8ccG+qvKS9xumpXV1WGYtQy0aydjpBciAMVBAG4NRDcI1yMQiqXSSbG8apUpQqToSZq3fGiUMcW1BKBegkBMsFhhS1QsLOzMyAgIDMzE0rb5OTk8+fPr169GtIAeoG7XC4X4AckhpiGprK/vPXHRYsXLPx66fp12w4fPBMdmSKXYWlpGcuWL9q3f1ddfdXefbv27t3T1NQC+aapqWnTpk137tyBI8OL5qFDh4DT6BmBSwKQwwO1atWqlpYW8GjBh16+bM17//gngPbggWNDg2OgY0dP9/YIKitqbzt7nDh+NjuroJ8/jFdBK0jE/ti2A+1yDP4iEB5XY+mFZfvPXLjpG1TSSi9qZdTxB7NbO/KZ7Dwmh1A+owspj9Gr0ypo7HALi959+sLdhGR4NobFUvhRlMhzxab4SiJ2riH2sbgdGRmBgoZCoRCTJj6t/SSIVT8s7aomTc6bqkshEDXbdVlExQDslpSU2Nratra2PvYFfJZumMDtVB2R1uBL9Xf9IHPdiI7EiLLffPPNwMAAIBAoCw4lZM579+7Brw15DDxa8G4hq9PpdHgvZDKZgFjwNSHrltCoR47u7aC3VFY84LB5vt53Q4KjwKEUCsebmuv6+N31DdXgxS5ZsigkJBR+IHBb4U92+vTpvr4+IDe4s7dv3yYejaioqIsXL544cQIuLDIyEjxmJqPL3MwmLTXrzOnz42PSMaHk4gUjiVjZ2NAGHi3gFjxmOB2akoJE7Ow3KCjxN0haRd15I4uNu/ZdtL7iFR7lGx3vF5voGwdKnlZsil9sim9smk7IJyYVtgEJmUFJWYGJVI97CY6+d82cvT5ftdHSybWihQHvFqh2WK6G9/qHxolNU5YE7RxErDZoJyYmUHfNf22wwRxD7BMrlmdLcs18ufBWqZzM+lhhabnVFYemNrpiKg3yJmdR2kwl7pr4VEfhOuPFBfLh2NjY1atXwXmF7OTm5paTkxMXF7d9+/a//vWv69atY7FYZ86c+fLLL//whz8AiYGsfn5+a9euNTU1HR7lHj9xYMFXn/3h//60Z/chMxO79NR8peY1UiIdHxkVJCRGr1i55NSpE0FBd/x8gywtLQHtwO933303LCyso6MDfFkgq7GxMZVKBf8YzlJdXQ2nAB8anrE333jn/fc+DvAPcXP1mhiXdXJ6wXnt4fYXFtDCw6Ic7G/k5RYLR8VAX62JFUmbPZNOjsLq5w9Ts/LtrjntPnR02fota3fsWb1zz+ode5HWEtq+Xye0aov+ik27l67fAYLw1n3HD5y6ePKiWUltY/fAqFQzAE8+9YxPuxCPEWlzz4uFlDNmSSTqiuc+Yn8geh+lkc4idua9KKakfOgJxHTrkXs023R1dWEP9xvQnomCeAvU7sGn+S7eo3hUOIjaDxSyh8bFwl+J28N2c3fOy8vBsMnuSKiDFRoFix4EdA1wura2NuA6cXl4i4l88tcfGhQiiE6MS4k/yYBgRKX1pqNUkAXUrLuxmnnL5JN/VtGETKYZUyBVPSSZUsc0XaqoMJEUE8umdqeebokSm5DK5AqVQqWcehBIxOoCYhFTiYHeM8b26AJin5jD5jdiZ/CVoOz0S+5kTbJumDZHZ+Qc2CWA2t/fT2RReBEcHh4msjGio2axdCk+kZPW+gGiCbmmjhGf3WlUOHDL+bqPjxfeAUr90PxBxNQT8DigM5aWlmZkZKB4NL0UlOmaSS2k+Oo66mncojFCaKAObMUiORq6Q5ZQs0zY0QlMLMeUU4+7Gl98CcAzoyJn+q+v1g1NjjGbEmQhTXODWiRTyhWQddVyfDqZqc+nG/JIxOoIYr93iv+5i1i14uFWyR8E2lmRUtP8gbI+/IPd4hKaja1dS2u7aup5gWdjdk9K3JQ2YmVaiNXi6yy3PT+3imJiJm3gHLiYKCNpd7WDvE3AFXt4OJlcOYL3mpJNgENJIBYvsPA/k1ypkvTyOgGx0dGRk7+jxotFU15gWl1sUEUOajohHiViXDiaVkIkkigUaG6EySRqsjHsWRs8ZipcMnjZkUiJZ2Gy548al1pr3YjJqLkv2CiUcik+Q4paqXq0vQdy2hMmM9Eu98hqk7laUSzVGAHdf62Tzk+EWLlWn6bvZ+2T2zCeWmrNnO9KVBepVsJuaUnRFVvrttZmbHLiPpVKa8r42RM2o2R5nAsrn+xQrTsGDuV3ZDzIn8i/JCagwDRje+BbqKZ3akDthKa9TiER467NxJhKNK7WrigeHOpzcXUKDw8Vi6VQShNtIij/a2d+7SpoYhlHicZmPIHEPH9o5mRUuQ3JvnuqFtL+1ZriyWdc059MJVEpJuTSmQ0lhBc7+0/fs5SmGJme71o5jVsIoF7ukEPRi90jiFWQiJ2LiJ1RikHRoP5XX8V/OsQ+3Mn2O0E7k8c/RvhkUgp8gBCEVXLYLSsusLex7GhpxHdRGoV0Ns84fWuPoaxW+aKYHrCkQ/7J1FTDaJontOgNZEgUqU1f4KJ2i8aMZQCGR/rU2NTkSpq/v1yGe5xKlVSuEInEoxQvN39/Xzg8+r2g5NI+GtAUzZ5IIHZgYEDLk1ahpXXG8fFRKjWUiZrXKU1yeHaUKpWCrLh7ho87XjEkEytEErVEqpaL4GUG/hy4JutyZlKWmGhwbksiHYPM+Wi8XCxSy2Xf2YmRROzcRiwxEesM11Y3EfswaB/r/83iEFU0NFYuRgNkYbe8ON/BxoLe0oDvojQKyWwPjZ2m7BMQq9JRxBJr7Gj3D0BbopV0RnckFCCaOYgJFFVqvFSSSTG05g2UUWiAFRRkffxu59s37t69g1c3Kie9ZwKu2jkfzqXdgIIyNlpgByQSjaPF7BBT4dJQPLwboDDgVqGQkSXU7Bo8b/DeqsD7sSnE+F9Yhc/lpgkgyiq1YYTXX8h1TnKVWAqeuUqswmSoVhxTyNVyyE1y0CPVxSRidceL/ZE2p3oUPwf7acbFkvbj/mTDw8O3bt2CjDrv8ydppJH2/9g7D/i2quuPH0/Jlvd29k6ABBIgYYYQINPZibem5S1rS17ytjMJgVJWGX9WoLRAoWzoIMyywswg24kd7z1k7fe/9z1ZcUKgBUqzzu9zEC/Ss7be9/3uPfccRCwiFoWIRaFQiFhELCIWEYtCoRCxiFhELAoRi0KhELGIWBQiFoVCIWIRsYhYRCwKhULEImIRsShELAqFQsQiYlGIWBQKhYhFxCJiEbEoFAoRi4hFxKIQsSgUChGLiEUhYlEoFCIWEYuIRcSiUChELCIWEYtCxKJQKEQsIhaFiEWhUIhYRCwiFhGLQqEQsYhYRCwKEYtCof4LiD1+/PjcuXPPyeGjqqqKHLk8Da5HtrO++DSSndwrJax97733TCbTgQMHuKM5eSt+WXN71H9dnq8lQeyOHTuefPJJ/GhQKNR/KHKQv/766yli29vbyZbFYiHHFIKB/yXnamtrn332WeJiyeNyro67vCjFIZZskCP10NAQh9j333+fnGccOnTIc/rD7UY+DifqnMrziXR2dm7ZsuXxxx/nrsR3BoVC/fRxw+NiT5w4AeS/a6+9lvCVs7fcTh6r+5sOwVVXVz/33HOEN56x4kth4JGjLPfPjz/+uLKycv/+/dzpxQ+HGlDnXH19fQSxTzzxBHMJzGWgUKhfI+KOBgcHOad0xRVXtLa2QmNj4w033DCSAR479ZuKPEppaelTTz3FIZZ7XMfFKw6i5AMgfOWGCshpza5du0wm07fffkveBHKTjZVnYBl1DkU+Du5rSRBbV1f3yCOPkG1yJb4zKBTqJ7g20sV2d3dDQ0PDddddx41kkiMIOaD8z07Va2trd+7cecme7xDWfvjhh+RN8AwUo086D0XOSbl0J3wrUCjUT4tg1DMkPHPmzJMnT9J0p3nz5p11199UxK5VVlY+/PDDXV1dnG/mZigv1rMb8nrJJTdETC4JSolJ+uCDD4qKir766itPcg3nZclbgeeD58PZKPk4yDno5s2bH3300YGBgf/B6A4Khbpw5ZkEJIdx4l3r6+uhs7Nzzpw5LazIdltbW0dHx4kTJ7p/YzU1NZEj10MPPfT11183Nja2trYePXq0vb39xEUqcipDXibZaG5uJu87OXCT7TfeeIMgdteuXYcPHyY7kPeEXElOfMj2CdQ5FfmMyM/h4MGDu3fvJi6WnAseOnQIPxcUCvUTIsdwcmwnh3pinG655RayDYRtwcHB5AhSXl5eVlZWXV1NDih1dXWVv7HIQ9x000233nprUlJSRkZGbm5ucnKyTCYTX6SSSCTk1ZFL8mLT0tKkUmlWVhb5DGbMmEFulcvl5EpuT7JNdhOjzqkSExMLCgqEQmF6evqsWbPICSn38eE7g0Khfkw5OTnkiEEO79nZ2ZMmTaKIJYbyd7/7HfFS27dvN5lMpaWld955J0FgFeq/Ku7Eora2tqSkpILVli1byHH8mmuu0el05MyGnOKQHchHQG4il/iOnVuVsyKf1+bNm+fOnbts2bJ77rmHnIPiO4NCoX5M5OhNjhjkeE6O4U888cTRo0eBYZiOjg5uHtRqtXLrZy6dVTT/M51Rv4lLa/riiy+2bdt24MABLGtwfqYtkB9CZ2cngetzzz1HPjLuZ4JCoVA/fbQfHBzklocA2TprfSUX6r8qLsXJA1fyVnOlJwoLC7/++mtPIjH5LLjlPahzK88PgZyAktPSxx9/HH8XKBTqp0UO7FzhIM/xHDyHEnLCfnGXMDxPXKxncQ7xSVx1p4MHD45cuYzv1XniYrmNnp6ebdu2EcRiPRAUCvVvNXLhJdkGDqsjix5gpdzfCLEj2wBwVvXDDz/k2gB4nCuuiz1PxI3ukA+ru7t7+/btTzzxxMjfCAqFQv3E0Z6zs9TFnnHU4IY0cUDst5Cn/rDH1H788ccmk2nPnj1clWbPpzA0NIRv13kyUNzb27tjxw5PjRR8Z1Ao1E8MFHsQyx1GTg0UE7KazWZycEcj9b8ZgeQQO7INgGeQAdPNzp8xn46Ojk2bNj366KM4tINCof5zI8ttwFkHJ7nqrKj/bgcGTx1mz+F7165dZWVlXDM7TwUoz62oc94ZiWHnYrds2fLYY495UsFRKBTqx47znmEwjq2Apxv/42lwbMl+YX1k2JIdhUL9YiFiEbEoRCwKhULEImJRiFgUCoWIRSFiEbEoFAoRi0LEohCxKBQKEYuIRSFiUSgUIhaFiEXEolAoRCwKEYtCxKJQKEQsIhaFiEWhUIhYFCIWEYtCoRCxKEQsChGLQqEQsYhYFCIWhUIhYlGIWEQsCoVCxKIQsShELAqFQsQiYlGIWBQKhYhFIWIRsSgUChGLQsSiELEoFAoRi4hFIWJRKBQiFoWIRcSiUChELAoRi0LEolAoRCwiFoWIRaFQiFgUIhYRi0KhELEoRCwKEYtCoRCxiFgUIhaFQiFi8XiNiEXEolAoRCwKEYtCxKJQKEQsIhaFiEWhUIhYRCwiFhGLQqEQsShELH5kiFgUCoWIRcSiELEoFAoRi4hFxCJiUSgUIhaFiEXEImJRKBQi9vyQg7E7KEHpFmNjgyDVSf7XwTANDHOcYVr6h1rJjXYn8+77x0tq7/7q+2NWulc/w3Q6HS3WwTb6JxaGMbMxxDDkZnp472fvBIWIRaFQiNhLFLFOiliGhaJjOFw2xtnFOFsYppVhugk/yQ5DVubtd77SFld8f6SegLl3oNVq72KYAbKztXuIYtXChpWDtJPeRP8WhYhFoVCI2EsUsZSvXFC5YWtnnBwwbYzLardZuD3e3/VRUYlq91cfDt/kZDj7ax8RTu7uPMYWhYhFoVCI2EtSw3x1uqhVHWCDbLDsdDL2QcbWb2ccLsZl6Wo68MQfNq1LuPLVF3/vGGimg8kerJ4ywjYWqyyAuTtBIWJRKBQi9pJlLItYm4PpdzDddhpmOlpsZ6xmxtrLzc7aG/Z99NT9etGaKasXBCnFc198rG7gZD03dzvYy7gcnJslf9hNwsX0u6jHZRCxiFgUCoWIvdRtLIdYwlcr02tlbFaGGldqT1mINnz7yUbdWumKWMmKwEJxXNKtvtLlE5+6u6Jp335mkKKV3dduZfqtTKedBqG10+Xikp5QiFgUCoWIvUSPyhxnnQ5KVouFsXEZSw4WkH0tPZ++8+q2otQNCwLlCT7VuVGlaX6b8sYakseuvym6JGf9oW8+YVx2i23QTv92wEIJTVBt47KUXXiQR8SiUChE7CXvYt1JS9bhjGC7w+Ww9r36/ENZydcm3haoTQ3doowpEcImCZSnelWIw/Vpo0QJ8fr829//59MM0+mgFpZQlnhZp3tyFhGLiEWhUIjYSxuxdgJYyxAzZGYcTjZY4nb3nHjwXt2G5bHClYHF8ohikXepELbl+t6d5VOZChXpUJMTohIGJq/0V+bOuf9+NaGsjbH0DdlsLnoPdidjt1PKcpUrztBZr0QhYlEoFCL2IpOlr7ebWxTb18U4LHR8+ND+L01FG7Ikk6WJfsYs/1olrzwDTGlQI4EtQqhMgUoRbFRAlQIKpCBJ4YtSJpQWpfT3tBI493VTYJM7cZ5e98nhcNhsNnLpqQyFQsSiUChE7EUNWEsXuwqW6e9wOftpctOX77+vyVyx6vZgyXovnRzK86A8GwhiqyRQK4VNQtgohkoxmCRQngsVStBkQvragKSEcUUq0cFv9tDEp0HGOuSgNaJsfWcAgPhXRCwiFoVCIWIvEZnttkFzXz+dgB10vLrzyaz189cvjNTL4k05/PIcKM6AUilUZUBdJtRlQHUabM6k2yUiKBRBRS7UKL3L8yL10qmr58fnpS779G9v0RU/LpttiMC7nwD1DKzioR8Ri0KhELGXio/t6mpkmAHXQMujdxUn3jZRuizWKIqtygohzrVMCjWZsDEHauRQKYEyMZSmUju7ORc25UKVHEpFUCykl5WZserk0cIlY7I2zHv24Tpz92GG6XY62sgD2Gw2i8XiOeI7HA6ci0XEolAoROylIEK7gfpDH22vEa9dGJy9LnhzwegKiW+lCMoITUWwJcuLmNfiNChKI0zlVcj5JYSpQtiY6bM127tGCOXJUJ4GdXLf4jTfipzRWeujl98Scv92RX/3Pk8bAOJirVYrkhURi0KhELGXkIaGBnftek2jSNiQEKHLiCrP9i9Kh61ZsE0GWyWwVey9UeRfnupTmAQlYp/q3KCa/LASqX9hindZKm+jMHCbmLdd6rUjC8qS4C4lmDJAne6rEI9au2xUsT6xoX53T0+Ph7KIWEQsCoVCxF5CenrnsxnyxHWrZigyxqulXkUZsDkPaiWwWQTbhLA13b82JbBaGFKbEV4hDyqW+BilfuVZodWZUdWiyKqkoI1J/C2psFUEm8VQLYHKLCjP99HIoyRJ48WpN6jyEr/88ksPZT2ItVgs+M4jYlEoFCL2/DqEutvguDu7ni1cfYxryNOWzuWkS1QtTquV6XAxjQxzzO44zLhaGZel+XDjzgf+qFoDug1QlErnWStlXlUZ3hVSKJcAufxZQf6W+3MSZIPcW0k6FKZA2vJr/vbC/c6eI4yrm3H1k8d1sot3XCOa/DgdjMPhrpVsZ2xcOE4PFw07267AicWPEbEoTuQn3cww7QzTx3Zqtg/XkBlie0n2MkwPG73sP0mQH+AFHjb2gEFfYN/wq/O8diq60J8N99EShYj92ZT1NLixjeiiPuJKl93FwtXlom1bbYSytFC/mfzQBoeOM0wbw3Q0HfliW4UuJeE2YzKdZzWJTmHylyGW+/ORiC0V0nvWZdyy+tbxT/6+2NZ9gKWs2WWzEkI6KFbp0/Pw0kWfNIErsbp2hzs8iLW73OF0ndF9D4WIvYQ1cIoubANKh5kNGy0i4/I0+WCGW2ANWIcruF2gwTjNLGXp9iAL1z52w+p+mc4RfMVDBCL2VxlZ+w/atLLhpAUM2cOrcySTyH8DfYP0y2fpbDz8Ubbw+sU3BWenTeNoysGV4+svQ+wZ91POpR8LQbIqxiC/avXCqGrDuqZDH1Ej63TazfR50gqLXLBFkp0ui9Xew7aI58ID2pF8PQ2x+BNCxCJkGVcv4+xk7G2MtYWxNjEWstHJ2HoZ+8AwcfsZB7uDvWmIRfKFG/S1OMlLtrlYrHJNra2sdx/JV09FWBQi9hdT1nnWkRAn6wFZ/ND+rzZnn52WDWa/ceTUtsf2xbtvLrw2KjkhtKhglCrTz+M7f4FtHRkEqCMp6+FuSXasMj28KGtqwk38vLQbDn/1T8Y8QE+pHewvgq2z6KSUJQQ1O5i+4RFudwxj1XlWviJiEbGXvPpZxHYzjg7GxkU3haudNbL0COGkrZ0dBLfk+s4fHD0utCAvwdHLelkKUIfnIEDhanMHO5Js5ZCMQsT+B0D9GRqGE6GRze7qt9oGnAS53Deul/nzg4/MmxKmSL1MK43QZ0FRLnhGhgkjR2LyF7jYM4wsF2XZXmVZPEWSjyl3WtrSuDULp/zjpZ2MeYiefLKUtdkYs41zqwSxAz+E6E8GUgQRe0nL7jFwDDcHSceHHay3Gxyef3WPJLsTHwYu7HAjdoCdlx0evaNktVDuDg8je2ajUYjY/9yznq4fYY6dHSe2Mw6Lw86ND9M/tTGHd3+9tVi78oaJevGc/KTQ8rxAvQSKM0/B9Vci9oepT+4R4wwoFkFNPr9QGmyQTEhdEr9mweQXn7ins/GYy2LzzJ/YqJulWU6eF/Jv4coF/iQQsZeyuOFT6/B0kXU4D6iLzYEi0ckmBA2w0HVP2V7QQfhKx4pZlHKIpScWFgpd9xiyhR3LQ8QiYn/Geerp2XE/SR4r02dnLDaX02Zz+9mexq5/vf2GRpIgWz3dIJ2sFQabMvz1abBZAaVit/v8YfxcrHKjzSPZzP2zLguqyD9lsFERok7yV6fFl+fNWzgneOdDFbs/fs0y0MomYjjpE6ZD2z+K1rPxlU7Q4k8CEXspy+XJzHBn0rotLJdO3Ducbcu1f6bjrBc4Yz1DwcypeSLnKcRy9B1OOUbEImL/c8Ta/z1i3Xv3OugsLNtancC2y/rGc39UpN8hXjVaLQrXCqEsC6qzoVoGxUlQJ/X2LLYZOWLMJRj/rKiW+/xwTpf8c6OMrrKtFFPQbs4PMsmDcxP5xdlTFl3vs6l8w1efvWjuP+F00EkTF9sF77RX5PwxyjqHE6AQsYjYS/wjH2BTCHtZe9dL51ydFu5H4mDJ6nJ5FvrZGJv5QnexP+xszY7VjTSyZk8yFM7FImL/E9lOQ6wHMmzSrQeuDoe7QiGxgg7uN2Zn+lo7nn5gh3TNvPSEWH1GVJEcSjOhQk5r+tdIoE7oW5ce8CuznP5tbBTRzjx1EqgmTlcGJjmUyMEg5xWIgjYs4ynkV7/6wt1dLYcpNB2M3cIu2xk+46ZYtY9YP+s6DbHcqh78SSBiL2VZhtj2WBxgLN3MYCdhjG2wz/1j8fyU6DyMhbEO/NSS+vMqyEHsbNdzWcR9DvdMM3nxfdZhytK52AHPWLELM4oRsf8xYm0/RKzV4gYOAStbLMk9XtTT1c9uWrqaDpXpkhOXjlNKJukzIork3oSv5RxfpVAnho1C343pvN8cseRRhN7k4ciDEutckQmmLCjJAn0WGHNCU1cHitZNfebhup6mRvdCX5fdZTUPdnc7rTbu6OCwUvSegVjPqln8SSBiL2X1MkyrlZ2OpWedlKj19fV0EMvJDLloWJ10ATr747KTg8UgmwZ1/odnlY6ZQ6mLRp/TbWHJ8aDVTMfAB4YXxTLcGLI748mTCYVCxP5SxLqRQ35Xduvwt85ssXWzi8VcJw/sTl87a9Vt/gWSsJK8AI2I+lfa81VKyxzWEFsporFRCL81YuvSBXVCHnks4ptrZFBFbHQWmLKhWgXKdNBIgnNTxqYsnbK1JO/Et9+SF2rta6M/K/ZHYh8cdFmdHkeLiEXEos7QETstJdPDMC2DLgvNLXaeecSgBw12sbzDRs5Ve4YrIp3n0cm2EOEytjrY7C3uekLQwZ6Ovr4+8vIau4Y4HneancPrYm2nikAhZRGxPxOxzBmIpQvenJz1M9sc3U6mm2EoYr9592+3XRMjXh1VlBdeofHTSqlxpP6V8FXsWyPk1Yj8KGXZ+K0RW5MeVCMMqBF5U8TSKVs3ZQtFUJVHWCsw5YxTp09bdWOsXrLk+49eIy/BYW6xD7QxLjZpkJh0s80zL8u9dDaLCxGLiEVRF9tlo56VcMXWeuCR6twa2W3zR8HVoTAnDK5iY04o/ec1ITA3BKZE+lwQcVkcn4vL4wNmjhZcNS5kzoSwayZFlAlv/+ylh5mhlsGOk8ywtxh0DU/KuilrQ8QiYn8WYu0/RCxBD/tvcml2uHpszrYhe+OA5djOHVWL50TlJ00pSAvZZAgzSMFI2JZD50GpfyVwJcAT8qrF3tXs/OhvjdgqUUCV2P1wHGWJl62WwXbiYtdAoRA2KmJ1aVGalDGa1KmJC0I+/eDFvvbv6bkC0+8a6uaSJJ0Wx0gXO4xYnItFxF7qcvX10UIT3ceOv/FAya2RW+8QVF4Ne+pu2m2a83nZnM/Krv6s7NrPTPO4+Nw0b2/xFRdEfGOYzsXX+mlfaqd8oZ70acH4f+WP3ZIwRj8/fKvoxqED7zH9TQSl3T199uH1wW7K/nhlHhQi9scQ+4N0YvbL5GKG7M4eFkhtn+5+pXZL/sJZUJYzTScMr1XGZq6gyU3bNT7FyZSmxLmSoLQTe1dJSNDm6r81Ysul3hUS2lqAPBY3Rl3DXlakwY4CQVk6aNbCJkVUXX507mrIWAZLFk5/8Y93M9YGxtFh6W1m8zicjiH7yIwnrraGg03hwJ8EIvaSlrmFad+79+ma1Gh4O3vS7vzYk0VxDWr+SbVPo8anQeN/XMOv1wTVa0KO0gi15cIFEa4CYJTAqIBR00ungl5pyYY9xVd9XjzvvpXRWbP997/+MK1E4bJ1dHaPpCxWf0PE/mrEumW3OXptzo7ewaMff/ZSSYX4+lvGFGVBkRyq8gQ5q+AudTwxkZoVsCMvxM02KZvZS4FHyOdXIf7NM4pNGTTKZezCHrEvsdF0albIq033r0713pEXtkMRXJgEukSoy4NNKliXcOXy26eXGoQHv9tFh8EcA+berjOW7nCItSNiEbEoS2f7x68qboh7Uzt/b/ncro2XH8uFThX062j06qBb79Wp9+0w+LcZ+G2GQLsKLogYyIHBXDDngYXAVUmvcahpNKojjunGfmOc+Zpy7p1pVx//4Hm6VIldXTFycY8DaxQjYn89Ym32QXISO2g5+cobj6VLb7tt6bR02U2VKtBJQJcOm/PCS1IE5UnhmyWxFSl+NL9J4h6krWKBVy4WlIsifmvElmbShToUscQ3i3k1QkFdWsjG1LC6lOBtkrCaNJ+SRPqstiqgKhuKRKDKXZy0+uqVS2YV64Tv//0Fp5nw1UazixGxiFjUD9XdpU9esVV0xz8q1n1pumF3XmRbYXi7GrpIaKBDC61aaNb6nNTxGnT8Bl3gMW3cBRENxjHH9aPIxlFNLAmyUa+LJ9dYjMFHMrxbq2btUl9Zu2z081sKmJ4TTsuA6/SGPPbTZtdQiNgf1/B3xd5j6WRrSti7O/opWdiqLUP1TU/fVZy9Oj5rDZTmQJWKdag/DAnUyb1JVIqhNA1M6fTKWhk1teWyWGNqqOQ2cmVISUqAKU1QJQramBFeIwnJuQOKk/l18qjCpABdYoA2UVAqjipXxGat8xEv97vjSlh0JdwyHRKuhfkzYNlckK6C4uwwozzUIIvO2xCeviioSDpTuSEqd6V/7mow0jKKsLEAKjOhVMKWfJJS7pIoZ9fLmrKgNBv06VCn5BlFfhvmgyptxgcv3cd0N3MpX4NdbAdcFzNotpntQ+TdGLB14E8CEXsp65uvv1DfHP5NVrBV69OSA12mcSd10e0Ffn0q73a14LiOkqlFE9ql9u9UBzRrw9s0wRd0nDCMrs/2sul5A4VhL63zuzP9moaD31jc63bYhgeuYf9KcyTxFBwR++/UNTDA5sYRx2Z1cY7WxXS3dJPNQ59/cU+5OiPhsszVYRXZQZV5oE47C1y5KBeyJZaokfXelOW7MdOnJAUKEqA6b2LBOv6SWZC30sckjtiaN6YmI7o0NXBLTlzqDZC9GMqlUbqkIPkyyFhKMBmtSPUTLoOVN8Cq62H19d5LZgNB6Zqb4I7ZsHo+KNN5BLFacYR8lSBxgZ9iw4TEmyBjmb9RGFSRxS/NAGM65Wttjvu5VUh8SRDElmeAiV0yS24tkUJNXmhlztiMlTGihKlP3G3qb6q3dvWRn87QgHPI7GAXlTsw3QkRi3rzjVc08yP25UcQxLbmQnvx6OOq8C41/yJGbEOer0Xr12cIeS05cGPirH27P0TEImJ/jYt1DjmtQw6Lg3EODg2caDjKpqn37/n0TWPu0tvm+OUlxm1ST9amgGIdbFX5VEtOYXU42OQmsS+JShHZwa82M6Bc7JW7DDbMhVnxsGgOLL4K1t8Mc+JhViSsuRaIWzWkhEjvAMVaXvYqf/ESL2VSRF5imCTBvyDNX77GZzUB54qw9DtCkhYI5CujhEv8xcu95Gu8THnRWeu9rpkAY/lweRSsvdH3ymiYFQ03TYH024EgdqvKb2M+VGawT4w+JR6dEiZGNoMuKyKU3ZzvbUwFbRJUZEUXZYzPXD82K+XKooI1g12HHZZOYmZ7e7s7u7vIz2fI7jDbcCgIEXtJ68EH7jUsjDmsiTOroD3fu9kQeyQ/uFcnuIgR26oOHFB6dWsFf5dEVK6e/sE7fx1CxCJif7FsjJXYNZvL6XAfMO0Oa9tf/nSncMNUaWJ0bkqgVsovlftXZPKqM/i1Mn41zRnmgs0cZslaJfbbKA+skvHKhH6laT7lIn+TyE+zDjLugPy0MStvgWlRsHQuiJf5a4SxquTIrFX8266AOC+4ehykL/ZZtwCunQSTo+CaySBNgMQFxL9C6kKfZbMhbWHAymthxbUgWQbJt4FWKDBIQrLX+OWuDREtClh3g5d4Sbg8ITR/fXChSFAi9TUKgRC0KJUFv5hHl/SIAihl2VYBBLRFidTg1mTxyuQBpfLI4uxxWcmjVtwWmrph5rFD/2RXotMy3909A0NDbH04FCL2ElZdbWX58vFHdaN686CzwLdJH3MoV9CnD7pYEXtcP6pDF9ybD12awA9zRpWtmPLmX55BF4uI/eWyMoN0RNTJ9PYN0Tbrgz0P/b58w5rJiav4+vzQCm2ARgyaVNiYHbA1O6I0kUeLPLBRLfblgvCVRIWYVynhl4n5JhExjkE12RHVWeHlsmDxupiVC/1mT4JVt0DKMn5uUoxaOEovHacVj9mwEBZfC9NiYVQg3Ho1pK2KkCXGK5L80heBYkOIdCl/zfVQLJmWOB9Sb4PcdQHZa73LcmKM0nBVSnBl7hR1cvy6G6FQNlObPlGbFlssi6zKDavJFRB81mby2GflRizdYBf2EMrWUHcLNcTmZvoUSX0K0nxVsohC5YyktbEJS0f/8x+PM86uIUs/zXtiO7qjELGXsmprKjaum05cbGcWdKv8L3oXe0wb16kP6SHnE+qATwvGl6+c+voLTyNiEbG/XA7G0jfQ7e786mJef+X5tSuvTlk3waAcr8oCbSZU5ENFNpSkgCnZZ7M00oNYD2U5F1ua7lUu4VXKBGWyQJNEQKJQyNckeS+Z752w0Hf+HEheFrR6gVfiYj9Fany1epZRPpHwdc1CyEmOT18Ztmy+1+KbIW1NdOZKWHMDZK7wXX8jEBebszJs+RxYcQ1suBkyEgjsBUvnwGQBzAiDKyLgymiYNxGmRcDkEJg/HbJWQClx0hIfYxJwiK0WBbDBo0+SXaq7I8+rUghFSWASk9flW5ojMGSFqbNH52dN3bBmglR0857v3qMlmIfYut/4C0LEXtrasrl2a9IVB5TRXdnQo+YRF3tCHdGtCbhYEXtUE0tcLIfYf+WPrVw9/e2X/4iIRcT+cpmtfa4R1YmH+jqeeXyzKGlGyhq+PjekQsMrlEGREDZl+2zLCqpI8TmF2OERY3YJLI3KDP/yDF6pxL9I5K9NBvkyysW4YJgcB2ND6SDw5HCYFAbXT4X1t0LWupA180G4nC9KEKQsDcxKjktZGbroRlCnBAoXgfAOkCf4Za8MFN5ON8SLIXkBRaxRHGYQhRZJo4yiaBHZ7XZyD0HZ68P1krjSzLgSaVChyIc8jTsLQqs4ky3yo8GdCrBzxkVrYWsm7FD6VmdCkQQKMwQqcZgkMWzl4tAM0TWffvQcWzPOTn5GtFufA38RiNhLWtu2bty0fsZ+ReSgkiKW8JVQ9iJOdyKIbdcG9SmgQ8XfJY8hiP3n6y8gYhGxv1zcctChIavD4bBTqpgZpuOTd/9PnjojY0NkfqqgJFNQkelnEkGlCDZnuks4Ub6y1ZS4vOIKGVSyi1OLxZRbpTLfIrF39ipInA9FuVPzkqMTboT85Ei9ZFRZ1vhi2ehCcYwyMXj+NLomZ/k8IKxNX+6XdIc3NbUrIeUWWDwTlOv5FZmj1t8A1Tlji0QRsiVQsM6vXB5TlhFdIoki/F49l2ZLJd4BqUsIsP0MkqCK7LAyOa9UBCXp7mJPNIZnjjnEbs+lS4lK02i7+LLMEGNGDLHRaStGGxVL6g+8SysYW7oZh7vYkwOznRCxl7yLrV41mSDWpvPtVvkfVYS0GOMuYsQe08a1aQT9BRSx74jCCGLfe+ulQRciFhH76xDLyt1OhzZJdHV3N+yu0yeuuiFEmRhflR1vTIeidNiYwxYBZtvpEOKa0mmUSWgGL10VI6eFlkrlNLOXDe+SDN/ynHF56wOFiyF7lS9BY2VmbIk43JAckHQD5KyAwvQQ1YYAQuJFV8Hya2miU8IcSLoZclaBURhkyggrlgRrknl5a2D5HMhf41MoDBXfActnQ9J8UG7wLZGG5KfykhZDwg2Qugg0KYSadImOSQzlIloEo1JMn2ed3Lta6lWWTp65f4UYNudCBXl6UkFpxpjstaMy183caBQyljbG2ke7hZyqjYY1SBGxiNjamtVTvi+IMqvcLrZBE9mjDbyIEcu52C5N4PtZcRWrpv391T+ji0XE/grEujy1rW0sX9n+Mw6nvaO3v/7EE9sqk2+dmL0mpiI3sjiDlm4oF7IGkW3OWiWH6iyKK2Je6ZIYT4wAbUVWtC6Zr1gDuiSfcnFQlTSYVlVM55Um+5CNSkmQbr1XOiHrbFg6i3JUdBtkLgN1olexOKAsQ1AqDTCk+WiSvMS3A7mfEnFo3iog28TsFgqJqQ0yZPrlJIN0JeSsI1Qm4IQyWrgYSmn9KdhWAGUiMCZSX1sh8S0T+m3X+hvSQZfmTZy0bEVs1trZrzz5O8Zmoy/dMrLaN3fCMYA/CUQsIhYRi4hFxP4qxLpoxX/OwrI2zs5+p/oY84mut3Y+krn2MnGCv0kh0MsJq7zKpb5lEq8SEZTJaI+d6hwwyShiufBQlotKabBJxCsVEvvIq5b4c9OiG6V+m2T+dTJeNV1K61su8i8T02zkcrHAkMwrTOOViHkmqV9Zho9J5l0i8S4W+RpT/UqEQSXCEGMKX5/sS64pl/tXZvmX5nkVykEvgUIpFEvp8C/hKwF/uYRGbRbU5UJttvcWRfCW/KhiYYAimZwohBqkscJl4XrZ/E/feo7p63d0OWl3Zgt7muFGrM3F9LP9+1CIWEQsIhYRi4j9tYi1seF0V+nlkEO+Wb29n/1tpz7r2tW3Q740oFQaVJ0TUZ0dXCQCXQoUS2i+cWXOKcTSYMeNK2RsSP0rpf7VGbxaGa9W6svN426SwNYMH64fTnkaVEv8auT8KhmvXOJXKQ+ryAipkPPL5d5lcijjaC3zLs8IKBEFFKUFlooF5RmCsgw/cj25tSiDRgndx21hyYNWZ8KdSihMh2IRPQkolYEhlQRfmyhQSWPLVVckLw3XZc5vO/oJY7eY2wYoWa3DfHW6Eetk+hCxiFhELCIWEYuI/RWIHe4awbZvc3LbLi7TZ/gGxtW798u/1patW7koqmAdv1gcWZ0bV5kdUSzxMwihUMz2uuGwOkxWT23FCpkfiUqpT7XUp0bmXZfhRchaJYRaCb0sT4cyNkrToZBQMBmqZCFVGYGVGb6VmbRTXkUme8/0Pv2IFS5NDyC2uFLGM0loKUSC8yIZNa+E9Ca2XGKVzL9S5lUpoX9Cr8mkGDakg1EYWJU1bmPB5dLE6bdeF3TXRtlAx17G2sVYrZ6X6XKwa2FdXIN2m9NldtJKzShELCIWEYuIRcT+Inmw6ukd4ekjYbZZXfT/Npd9gHFZOpuObqkukS7ly5bzFBtCSjPiKnPjSjMCC0XsXKyMA6Ebrp48XpPMlzjOMomXSQwEfjTzSAZlaWBKZTOSpFCT6UuLRWTza3IFtXlBdLhY4kfviuNrJuVoGfkTiVeZ2L9cLKiUCShuxdS2VmTRW8me5VI651op4ddlCKol/qY0d/e6ymx6BlCriN6snmoQjstMiFx0yxWvPP8Ha38DO89qcTrtXd29Q7ZTpxMOt5UltHU4sUYxIhYRi4hFxCJifwVibWzjNjdcrezwsIWxd1parEz3ENPa0d/gYnsDDPYwtgHm+fvElXmzJcuC89aFmuTxpsyoEpnAlOFvknlz3eVYvnrXiHzZ8CuS+pTK/UwZfgTDxFZWyr2rMqBMzO6ZQf9JaFokokZTlw564mslAWUSH5qyJGf5mkk5SiBKLamUOFRBuYRHcEsdMwdgGZ15rc3wqhT5Vgr5tdKQGomgnNbBgDvVghIpqFOgLCuaPNu1N/pmLB/zze4vPdnChKBm+6DZabYzdisbdvdb4bJjy2VELAoRi4hFxP5qxJodFC0j+UrCbGE6epkjVqbRznSa7UODAzTRmE7Qur746GWTOnWCaGmAOiWiUBJZTJAmDzJJ/eg4rcSbrV18qjW6TgSmTF5ZFq9Y4k1QSqxnVaYXt4i2nJ09LZV6lUh9SjJ8S+X+pVn8SlkAMamEoGXsVGt5FnWiVdk0j4kgtkIaWJLuQ7arsqCKnWQtFUJ1BkVshdCvPIVfIwqllJUGFKfCpnwevXNZQLEsOmmhd2Xe9Zbjb3IWlTj3IQsx6TY7Y7My5h57u5UZIBtWxmJjyJUuLucJaxQjYhGxiFhELCL2fyduFfa+778rMoqXLYzMTIyqzI8l4CScqxLTudXteX5lyVAtCqhII6iL46wtB1Q6P+oJdobVPX2bQR2tO7geeTL39e4s5Uw25MN/JfUlXpYu/hELKjNjC1MDqqTBlek+myTedUIoWQv35AWVpniZpJEa8bjUtRNS067feHfRF4e/6sN1rohY1M/R3RvLNq2ccFARMajy6dIEHdXE12vjejQCcwH0qvxbNOHNmohOtaBbxW9ThzZo48g/STRpo0g0amPOGtyt3J7typg2VRS9H21wk07QqA9u0Ice10cQeDdoRzdqxjapRzer41vUUW3q8HZ1cJM2whOE6D8MwvtToQ2md0UjqkFHnk9UkyaGRPNwkO1G+jzJrdxuEYhYROw5ltvm2gaPHf70qYeN2ckT0peAIZ1Xk8UrSoatOT6FG2CHIrhS5LtDEWtMdI8en6Ks1JuG7BR0K1iaEqbSyDjViZa7/sxEZfed+NLmOSxiVRugLi+iOMVrS3ZQlRAqU+D/ikapE6A2M8ooihUmRIoTL3/ssequoaYhxt48hOtcEbGo3xCx5BouOIJ6cDsSqyQ8u3UqI9pV4eRvW7SCZl1Akz6AULZRF86SL65JE8/yNaZNHdGuDiUPdHaUnh6cHyUmm0SDPpwEuUM3lclDq6OGw/MM6Q4kyJ6IWETsuXaxDq5wLzm89nee/PLVZ2qKs+el3s4zpEZsUYxWrqY9eWozvDZl++QuhbvVgTSpWDoiJN5uyrKgdV8pO0uPd/eaH5k7V9mdV+y+Ew6xvCoxzyTzNqTBXdowQxKY0uBuZaR2LWzOGWVMi11/i1+B6OqPdz3BMJ0OxtZrtw3h54eIRf2WiCVG0xPkmrPGyH26laFdqmByD+0afpuW36LjE9ASR0s5qqEgJB63XRXRqSK7kUcJ8ECUI2inOqBdE8BtewZ7uetJdKn5Tbpg9t6CiaNlH/rMYOnu3ofsjIhFxJ5rF+vgJild1sE+xmVmrO2fvfN0lXJF0oJIfdrELcppuiTvEhHF4cZ8KBWd2b+9YrhnwNnDjU9PnGWfKk+wTX5Mctii8del0pypTYqwEolAnxqqWB+9fr7g7vLkA1++wri66LN2MmY7M4Rzq4hY1G+J2C5VwA+jkzJPcNabepU0yJ/T2VyNL4l2jX+bNqBNQ6gZTMjKwpWQWMDuyWchKuDw2a3y54JsU9ZSyrq5y93aq/Kld8UGITG9nn1Qcm803M+NhbTGvRsiFhF77g+z5D+HzUnLVLALXGxdXQ17v7izNHvlTdGZq+Mq88YVy3wKxXBPMejThmvxcz0DToWbkW4/yoWYd9aococfG1xGFbsiiLvPPJozXCqHOw1RikTITwqtUMxJuDHkke2KpkMfMa5+p2Wwu7OH9n91MYNoYxGxqN8SsWTjZ0Wf0p8EYSGJbrVvl5pQ1p8GS00OhxyG+woC+pR81sJSiLIE9e5TedM/VNFkK0JfalVZF0sQS66nOVkaPglyb11skEcknD4VbkL7cw9K6I6IRcSe86OsjS3QwKJ2xErStuOHn3lo4/KbIjLXRelEgVX5ftmrYav2NMSeFmwXPJayHFwDysUCGhJeuSSAm2clUSWiUS0UcG1f2fqL3u57kNLQC6EyDzZpAnM3gEYanbw0dPGNYbveeIRxdFj6u+1DFq6Yv91KK/xjthMiFvU/cLFncLSX4tD/jCvdLpa9qfeUHx0G3ikoknsjiBXQKKDpToSy7fQR/fuGEUt2I2Rt1ronXOkOmoCRLtZtYT2IHX7Q3jMeFBGLiD3nclnNdEGpk7EMMlYLS1snYx7g6g4OvPfmo4uu46uEo/RiQZHUu0h8apL1rKB1U1ZMsToCsQWj0vUAAD5ASURBVG7KnuKrG7Fc59fhJnpshtRGtXdBEihSYFvprA1LgxJXTP7m81fNAy3cslaXa7hfjpNd7mrFDxARi/oNETtyqvWH48PcNSMnazmOslOngk4Vd72AG7kdNqAB5Ho6XKyM6CqIqtfFN+hiCE05I8saX8rXRi296Zh+NAluB87LNrnTo0I5g+t5PqdI734CfG7EGBGLiD3XoiWPHAxbmsHhoitJbU53fxqH3czYW/Z9/qI8ccaGhQFG0djyzIlnJDRx1rNGQsMzelwh8aUVnSQBJmmASebLrslxZzOxZA2oEZJwW9gqlq8Vw6t9lClQrYxViiJumwcVxuUnjn7EMGby5R+0MnYXDbrQ1cqVgbQzDmQsIhb1GyJ25MqcM5KHPWnGI9fzEPIR08kmN5GbotjgVuMEN+sENMgGvSmmRTW6RTn2e+OUw4bxx3VxHES58WHC1yN6etNe44x9xmkHDRMIbjk7e0wfT7bpEiAdXZ/DPh8uW8qdMHVGqhQiFhF7zhE7bArZr5qnWgXt/WYfYKsSdtbv+eedpVLxkhnSJTO0KbBZGVabF1gspM3vajNp3m+5iG06y0YVt6SH5hj7mqR+puG1PRWsweU689SIfCvSvSqFtJqxSQRVmbQeRRm7WLYsb0r6sgDp2lFPPqRuqv/Y5eob+aw4trKysc+tFz/AX6OhoaH29vbf/e5399xzD74bl4Luqi3dtnbKUXVMbz60KnjHtKOO6+I7CngWlVdnvneTKrSzMK5HF9KS43UyP7CtePxB7bhDuvFHDRM+TPYmOzcYRh9WRhH0ntDEniwcuy8n5Bt50FHtmGPGiXsL4j+RBDUWRh/TRjQXj28pntJUOOXd1dBUOGm32PewKvi4Pry9In5/Hv9beUBz0dQjikkfrgv609qAXfKY+rLph1SRBzL92o3RhJ1fZAR9mjdmb8V17+RM/kf+tHckMQeLpjUWTziYF/yv7NhvVGO/VY4m3P1UEnBIHX1UHXlC/aPLahGxiNhzbmQ8vWhofWMrDbuFsVkYq8VptjtoNWPGYWn+fu/Oe+7OXb9OkeSnExJHyyui9YqhRAjlYtheQNvN0uCqTJwqTOFtGl786q7FODxxW5YKdXLfuix+RaafSe5TJPU2yrwL5b6r5odqM6576amq1uOfce1dybd/wH4GYu0c+xmmAz/AXyaz2Uxsq8vlIi6WIPYPf/gDudJqxVGBix+xNcvG7M8NHdL49RvD63Wjj2liiYu1qr371DyC2EZlaIcqsIeOsoYRlH5nvGJ/yVVfqiY9djO8tcH/a8XoQ8bJTeWXnyiZtjsn+pWVsEsYstcw/bP8cW+mhj6/0udIyegjxeO+U8Yf1Ez5Xjn1d9Phc1nkzhvhn4nwVR7ve11Y1+bL22pmfZE16k+3gsILNFPg3gVw1xz4Mjeqt/byRuPoE8WT7p0Dr8tGP7gyrPYWv+KroWI27JgHf0/lN1VcVn05fKaf+X35vJfW8++bC1/kxTRXXt5YOqleH3dcF8PVo+CCW7mLiEXEnj+ItTtorUGznRJtYIgh9pG6WQe5zkFNY/uRrjeffTsnMTI3OUIjjNSLQw1C/4pMXl2OT5loGLEyN2IrPLWfMobrUchOm8HdnO1XmAzU5soFBSnepbkxJfnxqxeAMW/1Wy88YOutZ1x0QtjFNnodcp7y1g63he1nEduGH+AvHLxwuIcDiIvdtm3bww8/TN9Wm82Fuqh1/501BLHEfdr0/MGiyCPquCOq6EFj2KACBnWBrdqI+jxBuzLAbAwliP1Oxr9vITx4Gzy53O+ua+GPCd5P3AYPXQ87b4d9xhmf5I75U4LX66lhu3Im/jUt+uX0uJdFo9/PDPxaN/qTnLjvtJfXl16/cTzs08x44ibYnRf+VUHIG4mwVx/3lWrsSyv4/0gb//hNvJ2yy59MHvViauSXynFH9GO/zQ55fjEUjgbjZZA5FTaMgtkANwOox8GryUGf58betzTkz+kTHlzCv+tG2DQbHlsILyTA1wUxhw2jj7kpe1rlKUQsIvY8EduBh7G4mIHhMLsoyZxOO/vFs1HEEeq999qWwvwbU5dH6qSTKnLHl0hDysS+5ZJT87JVMnctp1PlnzgLO2LulmwYk2GrItyQzlMkBhTnTJSsjUheGbG9etn+Lz+y9LbSAsoup3nQZrHQGWJP1yB3e3X6bMwsZbH/669VW1vbli1b7rvvPnwrLgX9bnPF5lUTDyujiIvtVAsOKKKOqmMIYvtyoV/Db9NFnlSFdWmC+nVBjXkBu9O9f3e738broPIqyIuBTXNg+zVwz1x4Zhl8UjD5T6t4mliougKeS4x8en3U3Qt98kbDA/PhxfXwN0nEW6nR32mv3TYNjpRet/NW+LIg+l2Z11/WwNf6mM+UY59L4L+yfuwfF8eX3RpSu5D3du60b40zjhRNPqgbu3MhFE6CzQv9t66dUHh7XP7cgOrbw+5dGvCaOPrJhbCcB5U3+lTfAKVXgn4SbLkaHl0I32jHHjSOPaaP9xhZduaYBiIWEXuO5XJnEXEUtQ1PepINu9VitpitnlRe2raGOp+2rz564c4KoXj1lOQ7ggolY7aqxpek+1SNWHhzGmIlfmx9RO8zdjCJQZPkVZk3rlI5a92i8GXzIx+8W9HT+oXL4XT3lHe6FxO5WDPrOq1VjnOYufiT+OUudnBwkGz09vYSvtbW1n7zzTffoy526XIlFYvi6rVxA0rvlnx/4mIbDKN7tUHmAuhR+jWrw1q0kd3a4C4lr7kg6GhB5HslC17NvfKR1ZGaCfDA7V6vS8d+pr3iYPVN7+dNfl0YUzoVjJNAORaSAqF4Fmy51f+R2+HFRPhMNXnnIp+/pYy9+wrYo5391C3wVhL8cRn8TQQHyyfsLZ7xenLs/df5bp4O0hlQtSCgdCa8mhSwXzv22/zojzKjsyOheC7cK5qzYTqkzADpNKi9Cd6SjXrgBhCOgx2rxj2UOOGptAlPJ0a+mBr+Skrg0YrLDxqpiyV85SZlWzThbWoaiFhE7Dl3r2Y7LfFkY79mwwtk7Sxn2Uwoh8PlcLfGc/a7LOwKH1vHiX0PbtEm3j427fbQovRRZZJIuiZH4n0qqdhTW1EcRBfFSvxO3crmQ21ShCk3+OYnRqUnxKUkXPbnp+5ibF2Mw+ygaVbDNafYnwHx0S6n9dTTOyNQv07d3d33339/SkpKVlaWTCaToC5qJa+8Q3u94IgqmiC2XRnQWDi+uXh8RwHPrvUliG0sCObmYgliO3URHaUT3zfd/njy2HuXBG+5EZ5LjnlyRcDOlfx/qWYc23TrvzRX3DkPHk0IfHxtpGk2PLg6+o/SGa8K4b28sO/Lrnpmse+Ly0PvmQlf5k57+hb4Rxr8XQjfGILfzfC+dx7oYyANIBVgUSisDoNVAIZYeO5W+Ewa+L4kpGgybF8WUbtm6rV8WDEeVsWAjp2Cbds2v3bxqMqFEbnTQD8TDJdB4RRQx8Bds+CwMf64nvA1vEXrXl/UpaKBiEXEnmNZmX4rM8BOuzrdcOXa4JlZi+ikKTDd5q4BpmeQ6R1kusmX0D7I0new490X75clTFk7z2tTzpQKUViVKGB4HY73qTJPwogKUQgFMLlpBGK1SXB30ay0JcEbFo3b/f5fyJfbZaU956ynesi77E6LwzHAOPoZ16D7CXmqZHAe14Ef4C9Uf3+/J7mJGFnibw6zqkdd1FLK04oXRBwqiOQW7RDENhrHdCr5xMVy6U4nFMEEvX1aAXGxe+WBpddAciis8oGlALIwWAOQCFA9A55bHfBBwfQ/rgt9J2/Gm3lX3LNI8GTq+HsTIp9OgL/LAz7KHf1mYtQrqyK3ToB/Scf933XwyDz4OAsaa0cdKo//hzjw1Q0RbyZPffaOsQ9J59yzOv5l2fi3UgSfywTH9PHPLwbTdLhnZcyiCJgGkH4lT34FfXTjePgkJ650QfT/ya59RHjZnXcEFc2EJ1b4viUK+iQ79IgxpkFPlwa1a2hJil4lv48NRCwi9hzLwvQSyp5CLDdUPDTiknY1HzAzHT1MYxdzgpbe576SdjszePKbdx6uypq3YR5UCKNoWQkRr1rElkUUD9dQFEYRylaJgsg1nioTxOCWSgWrr4dNugUdR/7O2Lut/UPkbgkyB4czh61Oh9015J5zdfTQeWC3h2a4PUk4ELG/8gTLajWbzadNHKAuaj14V92d66YeVkYRF0us3hF1HJdR3JEBA9qADkM0cbEEvYOGkIMSePkWuHtp2Mu5V/5NM2/7LfBsYtRHmlmfai7/IHfi18Yr95jmFk+GtAAonAH3Lw97JX824fFDt8J72cHvZ0QfKb1uj/Lqu6fBt3nTd94Ib62FY2VRe/T8g6UxB0om7y+8+jvdgs3T4KWipa9ob3piNV2Nc0gd+4088Ov8mAcWwNuFCx7LX/CoJkF4Fb/sjui/G274svDK3QVjk0bDqnCQT4GdoqkvyCa8kRH/XnZ0Q+X0Y4aoRl1omzagk5Za9O9T+g4ofQcLfBGxiNgLbe7WOsi6SbvDPsQBed/+TzdvVC1bNCllzXhD9tRK5WiDGAwpUC2HTTmwOR22SGFbNmzJ8S+ThytTR8mTLstIvXHBzZc9+OBdra3NThdjtTm4QV+7A0d+UajfUPfVlm1dMeFwfoRZ6dOhCTqkjT+qi+vUCQZU0K3y52oWEvQSF9ipCm3SxLWrIvqKRjXlh71wDRzNDj2aG95WNOWYeuwnqfwHZ8IzN8HH0ujvDZftXAB/WeG3zzDrGfU1tQlhyeNhgR/c4gWLebDEHxZ5Q3Ik3AYgHw1/yR7zkWn2o+t4W26Bx9bzd4sin7kO3loOB1QTvldNOGGauVcz4YGrgRD3+eXw0mq/55f7PHQdvLIm9NW1YYYI+FAW9tR8+L8b4L103heZoa8nwOtLYLfYl01ximjWDjftYSsskkDEImIvOMba2XDSZKihQfbLOdh08ru7tylzZXOTlodlbwgslvNr83225NPs4m0y2J4JVSIwJIJBFF6aMzNfNGfVksuff+7BI0f2cstyLFY7l9DkQFuKQp1PiG1RhPQVxTcXhL67GBqV4Q3K6Gb9mHrNqI7yK/bkRB8xTD5eesW3yvFbJ0OuL8gBVgWA4UrYKY57v/TqTyvnfWqa80X51Xtq5n2km/KJfvK3piu+ME59eiVsnAnProQTW6/7YJ3vn66Ht5fDnqyofXmx9cbJh3Tj/5oAzy6E8jh4ZiGcrJ77iXz0g1fDM7fCR9IxBK6PXQevroT96lHHjOM/SvV64RYg93AmYof73yFiEbEXmpxOp9XG5fja7U4WirQQhG3w+ON/KBKvnypbG22QRZdlhpRl+JRJYUce6NdAURJsUY7RpMasnB9ozF303t+etg61scuEnOSehiy2EanCKBTqfEHsSQW/rzi6RRl8QAat+shWQ3S9MqxRH3tCH39IGXVMN/Z7VVx98eT92rHHSqbWl874XDVuT9H0Y1VXHq24/HvjhO/U8Xs1cYf0oxpKxzeVjm8sHnvCGHdMF12vjT6uj2owxjQUxB3ICDmaT8ndZBjTWjqRxAFFxN680GP60V11szuqrzqoHn9YO7mhaOZB1dRD+nGHDeOPGsYdLyJ3OKm5ZMLJwjF0LayOSyTm+rcLPEYWEYuIveAQy1gHLE6LiytY4XQwdhtDHC2tJDzU+MV7T1ao70i6XZCfGFGZM1afGlS4Abblx27MHytfEZB8e8jG4lV7Pv8LWzWC/Ind5rBbbQ6HyznMVzu+wSjU+YPYRoVPT1Foo9KvszCsVR/aURhxJM+/pTD6hDacAJJgcm+uYF9+6BFtTH3R6EO62O6y8SfUEd/L/Q9l0nvrKY7vMUZ3aEIas73blbx2hX9zNnQpebaScGthSHsu9BhG9RbGd+qiW9QRrdqIk+rQBmVwozq0zRjdVhx3JD/4G7HPwdyw1qKJLYUTjiriKNe1cfW6+MPqqMPKyOOaqAZNZL0qZHitTqinizsXiFhE7IWHWPL1pNm/9uF2ck7GYWfY9azkq9t57Lu3t5UlJ90RI10Rb8q+qkw2pjJ7Ss6q6KTbwn5fJzK3f8UwHZaBVmJhiQO2O1we/+pyWmnjARQKdd4gtlnl11EYdFzhNVAR1aTmtRmDjuRCe1FYgzqwmUC3KKqjLK61JLrdFN9YGHVcH96Rz+9WCgZ0YZaiCKsxfEAj6M7368iEAaWvXRdo1/D686BHDoO5YC0AiwJa8/g9muDWPP+TOV7EdHaoeM153p26wDY1v0XNa1ZSD22uGHtSGVKfE2gun1SvjiSetbUwvss0rqNkDHm2DapQAmaOrx7EerwsIhYRe6HJfqpzgHWIuli6eIbNVuru7KHJUK7ejuO7f78xX7LmqrTlM4yS2WtuCpGunPraM3XO3v2Mq5Nx9FI+n7a0lfpXF7HGLkQsCnUeIbZVzyNBENtXFnGiwLtVH1Cv8GozCjqLQ1v0gQS3JzW8Q7lwrMD7cB4064O4RrDdioD2HJ/WLK+OXN8BlcBeGN5H7jALOuUwpPJnikNJ2FV+fVnkQfn9uiDia7uUfuZCwaAxsFfrO2DgNefAYKHAXBTSqfJrK/Bryfc9kQkNWV5dxXHE5h7N5R9XCI7lBRzK8jmRH9BVGM3xletwNzIQsYjYC0wui81ls3OIddoZq5Va2CG25ywHTNtgH2PpYYba3njx0bWLr024eXxJ3uoPXnvM1X+ccfU7hnodliH67XbQ+VwHLW1BnKxjeLUQIhaFOo8Q224Iatbw6UBxUXCD0qdFx2/SkN14J9V+ZJtcWqtiu4xBPUUhjQrvVg1vsDC2VxdJ/rZDE9ati+41xJLLDnVEjyGqTx/Vow1rVwQ2yOCEDDrz/IjN7VTxOpT+HUrfDqV3RwGQ6FRARz44SoPa86BBDt1qb8LaXl1Anz7IZopq14bQNrH6MOJuSZCNVnUQiTPIiohFxF64slkH+4b6+9zFH9gvKC0jPOxlWdbSBbZtJ4+9+fJz0g1L33n5WcZJV7U6LYMUzy46feuxsASxtA4yi1iXvQ/fXxTq/EFshzG8SS2g7lAffFLFJ7jq1IecVPi36wK7DcEn8316C0MbcqHbENSS79OtDTypDGnWRLTqopp10Sc1kcdVkQ2a6EZ9LLdBriRBbu00xrTro1pVIW3agGaVf6cusEsvaFP6dKl9B438fr1ftxJ6tT79ev92BTRkAvGy3Tp+vZySsltLs4WbFL4tBfwuXUinNphsjMQq17OWC0QsIvaCk3m4iLGT+1o6hsPmZC/ZjCW6yt1p//CDd2vKyg7u2UMhzLWktbsrHrtcZ9QcRheLQp13iB3ZgL1NHcoF2//cXaeQC7ZaYQCJNkNEq57cSXhX2ZgDuYFH1ZFHNJGNRWPq9XFcvzmaSKyLatRFNNHHCm/QhhxR8DtLoo4X+HXoBU0Kn4Gi4KYcIKzlolNDwr9d49+m5ZPoUtPopOFe+frDNuweviJiEbEXogZGUNbO5hQPI5b9slocTnau1uVwOd//8L3Kstr9ew65W+SwvQRcIxDLnEKsfZiyKBTqvEGsOopEmyqqTR3BRbuKRDgJsgMXXapgTxyVQ3OBd7PKr1knOK7kNxsjGvWRJwyR9fqo4/oIEg368EZdaJMumOxAorNmyhFN+MmiuGPq4FZjxHEFr684kjyfdg3lKLkkQZxuCxvNugBELCL2UnCxXFhclKq0UY+DBa3N5WQRa2e/si4bY9/10XsVZZv37TnmdLqxegqxzMgi/tg5B4U6HxFL4Xp6sIil0Ukp644uJYlQEv0acJTyh4p57UroNgR2FYe0GoJPaAWN+mAShKxNOgGBJWtJqTc9VDx2d27QIV3siaL4ltLRx5RB1ARTXnLUpNGipTBuohHsISuFq+ZsfNUiYhGxF7Y8be9sHsS6QeuyEtZanPSSXdFDEVtevnHv3iNuxLJt38/skONihvvCO7H0BAp1fiF22LyOsLCeCB+2s6fCogGbHsi9NWdBr96vVeV7UuXfog9i4RrcrKXmlbhSAld2BNh3X+H4bzXxR0smNpomnDCOOqYKI0+gscCDzGHEaumfE0h7mDoM4DPzmxCxiNgLWyP6yDnZOA2xNJPJMcSaVZuNse766N3yitq9+w45h4eFHZ4qxK4fCRQKdd4gtp3OvP5otP0g+hXQkw9d+dCnD+wxhjQqA+oVgtbiUY3GeBJNxthmY3SrIarNENFuCOvUh3xfOvmQaWp92bT64vH788IaNNGdheT5RLW7Z3xPuVjWyJ6Gzx8NbbAnELGI2AtM3LTrWRukO11WZhixTsZGiPv+hwSxVXv373O693U6HLbhMWG7uwWspwssCoU63xCrCXDHad7xNAc5Emn9+rAebViHNnywYtKnybBrHby6FF5fBa8Ox2ur4M0V8PYKeIeNF1bA6+vgQ2ngEf3oQwURHYVj2rRRzQpBJ53ZpVlU3IBw2/BcLHmGP4izIBYX7SBiL1jEutxx+nwqJSWHWLtjiBmJ2Mqyvfv3uNwt321OB9chz50tdarROoMuFoU6DxHrf3rwucxeTwoSSz6BJ06o4pv0Ew7ljzqgmrBxPDw0D+6dC9tmw53X0Nh+Ney4Gu6+Gu6ZA/fOgd/PgQfmwd2z4PEb4ZA69qR+VHfxmIbcwOZsWiKqWxVAgktu8jwueXqnx1lAi+tiEbEXMmKdbAxnBZ+iI1sB0YNYwlSnG7Gl+/Z/46JfaUpWp3NwOCHZQ1knDhSjUOcnYjs13iTatSR8uWjT+pNo0ZHgk6DOckQcUEw4WTJ7n2r618rL1OHwwrqYf+Re9Zp85utZs0m8kTX7zcwr386c9U7mZX+XT/+HfPpHGcGPXAf3zYQ9uSEN2rg2fezJXIG1JI6tEhVAnka3it+l9u8cBnyjNsodujMpi4hFxF7kctKqE4Sx7NfV5SL//OCDDyoqKvbt2+caLkWM7epQqHOoe+uKytZc8S/NZc26SEKdE+qIQ/oxx4zjCauIXySUJYiq18Uf049u0MUQbhHC/azoVUQ0KuJ2G2Y/XbR87vSgrfpVH1beeFzBs6h5R7Jj9hXf9lXJ4u8Uk3r1wWYVDKrgoHHqy8th+ySo140+oYlt0UZ2aMPZhbYBnuikA9TB3Fxvp3vFzn8aTBYwedCX739cN/Gt7MtrV0z98i8PMvZeNlvztCkrOzYhQcQiYlEo1K/RQ7WamtVTPldPod5O7deiEtRrYxsMo4jh61N5DyhpLX7iETm+0pnXs+U0/UQw2TCU692ojfub7soVE+EJ1TUHKqcTi+wsgMPyiGcSYrXj4a2k0DYVdZaDCthXePkLCbB5MhzWTziqHdOoi2/Rx3BlLrjqFmSDK3zRrCGGNeo/SncaEeT5MPnQqQg4op/6Wu6V1Sun7f7LHxhHP+Oyc2Vz7KeK59CirvgNQcQiYlEo1C/UYzU5m1eO/VY5mvB1SAmdBb5NmrBWfSTxr5YCIEFAS3DbrCWEE/SqfBu1MT8rmAwgxtFWAHsUoTnx8HZubLsp2l4ADpX39wVTSq8OuhLg6Q1jjujHNhd4Dxb7fls0+7kVPnVT4HvDtEO68fX6cSf1owhKCVA9VaU4uHL3T9j/s4KhLX28m5Rh+40zX867pmzVzE9ffpTWcx1RM4dbH4GLBhGxiFgUCvWr9FR1xp0rYvcVxJhV4CBAVUCbKrBTSyysl10BDuIsCXfVAYSyXWo+MbU/G7E5QIwjuZ9DOb66OHg/N6K5MKxZCt+lwSuZ1yRdOyMcoGr9vL9kTn9bxD9ZM3VP4cw/JXhtnAIHDZMO68ad0I8a6WK5IeKRXvbnInZIQU4aAo5pYr8umvNnxfXFq+d8/Ncn2Labp/jKHb1wqBgRi4hFoVC/DrFVmQSxe5VxgypvmxJ6CqBVHdShCyWG1VpAe7iyLlbQogkloCVXesoR/4dh1YSZC3gdCr+vc4KUo+BveWP3aOLeXw+GWNgwIzQmZhJA5FWTxqydCNmT4KXsiSPnYhs00a3aiG5tcLeK36f0HVB6Dyq9yCUJ8k8ufqyjzo9Ff4EXm9U1+ouSuc8pbjauuebDvz51JmK56je0qwkiFhGLiEWhUL9Uj9YoNq0a/5VqfLcmgBjZdiWvQRPZpI9pVwsGKM9OzcU2aSPYsWL/nxUnDVOatfH1quhPcuJyRsEbiulfaCe9sQ7SA+GmWK+A4HjwiRwd5HdrGIji4E/SMUcNE15fCvdMhCYtbbzTrQ4cUPsTsloI7xVgZ8PKBjeO3a3y/1nRq4R2rRuxzxbcaFw758NhF3tqBQOuaEDEImJRKNSv1wO1uso10z5TT2vVhhHKnlSHHtaN8mQUk2jWhh/Tjz5sGF+viydX/tyM4mPaSU260Y26+M9yYrJj4e386d8XTftC5POXFV471k9fMXvSBH/Q3By9c33Ea+u9D5dMOWSc/Moy2DERjuviT2oi2zVhPRoBcauEsuYCN1bNw0Gu5J7kfx4DBdCl5R3VxnxVMvvPinnFa2b966/sXKzL3SgMq8shYhGxKBTqv6P76oxlay7jFu10aILYRTvjjhZObKA1C9k+rNqII/qxBw0TCGgbaQZv+M+KEwURXbqILn3kVzkRuVHwz/wpLRWXt+T7duoi9hguN10LNwC8uta/rzS+s8C/VeH3ddGcP67wq5kC+4wzDugnculOzZoIz1ysZzqWm5E9W3WnnwpC6H41nNCE7im67KX8q8pWT/v8pQcZRy9tUzKCrPSCW+iPQsQiYlH/3955wEdRpn98Lcepp97JKQocp0ixnYhyqKgIKHZFikCAgCCdAOkJNSQQICGUkAChpRAI6dnNltmZ2ZKe0KSX9N4TEiCkbnbn/+wuxgiRE8jfD9Hf9/NzeOed2Xd2F8bvPrMzswDcHQe8XZcMfybb/YPSpU9UWj1MEipyfj7f4V/ldt2vWP/1mvVfqmyfLLTvWWDfq8TuGap0C+173VFqHJ6qsup2xaE7+XLqY6K0ZcPTHQaWzH/ouu1fi2ye8XlT5PSUKOVLUaP1Q7ULRFftnoqzf+ewRS+nAaKjDm+edhx0wfGVDIf+Gda9ybUF9v9qG5baFFNnrw5T6NC7wwh2omsLRbXLuh+b/zSz+PV1Y/udkuwQWqsFfUNbIWv46e4T+CYWioViAQB3jzbMd9vUQccW9cmaLhJW/aNgtqjUvkfOgkerl3S7vkjUsMh4AlT50r+RgKuW/JXaBdY97ihlNo8XL3yw1Pqp40sGjHlcFGP7yRGn90qX9S1Y+Ej16n7HFzyjmfxgmV33+uXP5ix5Nt3p9bTl7x+c8LRDP9EJx8EXlg8+7/Byun2/fOf+OTa9c216toVms2z7UPJsenSYXOtnOsz1maKyaaKrzt0v2v9bOn+gyxd9ak5IhJo8obXBfHJT29WxzTdu+QSgWCgWAHBXVJ7m5r8humDXt2bJX1psHqh3fOyy49N1y3s02jwsLBVRdNYiqjiv2T5KPa3Woqu2j99RKuweL1v8cJVDj7OOgy37iPwXfK60HZm18s2Li7rnrBhwfsV/Ttv3y3Xqm273QrL1G7zz18eX9Av9TLRxoCjLaWDxioE5S5/JXvgkGbrK/unLdt3NoXal/TOUCodnr9g93mFqbf/WYQSXxyrmimqW98pxfevAtOftRj0nlJ0UdJfbV7FQLBQLxQIAOgNdpdMn/1J9/7TxctiFoquOT53/4cGiJU/UWD3QtEDUvEBUt0hE7Wqrh65SUbtQ1LDgzlLn2O2qlejKkocuLug5/1mR09Anlr0m8hkkOvCOyHuwaMOboi3/Fe18U7RjiMj9nQcchvUMeEPk84LIp48ob/4jl23/XrP00auLH2qwfvjaIlGd8caHxtAzqV30QI3VQ5T6haIO07Co45TOeyB3/qM5zgNSHYf4TOof5DxZaCo23d2pxXyjCUM7y+JAMRQLxQIA7h7aD0uPSdePeEgzUVSzum++0wuXnAbkLBtYat/jiu1jdTbdLts+UW7/NM1W2/39iu0jTUsfuKNcX9at0VqkXyKqtflnyAhR0Bf/3PuBKPwDkeRj0e53RbtGiUK+EkV/Ior+SOQ/+hH3ET1kI0XcaFHy1yKqVqkIbnJ8osX+EeNFO6bRGq2Nur1u3a3O5pGrNo9RGq0f6DBNNg92mFzHFys9hqfZD9k2to+7xdCac1ryq76pXrj55zhxXycoFooFANwb+aY9NWbdzJBp/w777knZ3AEJK0emrhpxyvmNTIcXcuz7ZDi8eNbpNUq6Y/9shz4Fdj3vKJccehRbd7uyWFRn/9xFqwHnHd9PdxpatOzlTIfnjzoNPOL69lm3wen2vTOXPHfRYcgR58/P2vZPdxiYs+zlgmUD0q165Nr0LLR5NmPe327cLtH2ORoz3653nl2fHPvns+1fyDfOdpA82+c6TJrjMOXiobsmv+Y9+yMuwEtovW5o1ZsPC+tuKNb4I5umK2UbTAFQLBQLALgrTgpCCe2qtYVH9ztunfTSurED3C2GrJ/4esDM18OnPx81vXfYjL7B379MoUa0Za/DM/rfUfbOe1Uyu7dqUjdu3COxk54P+e6lsEn9Iyf1CLXsvW/Oa75z39xl2SdkfDf52Idlk3pGf/+m/7SXdk984eCMl4Kn9Tto8W/JD/2ZeS/JZ78YbdmbEmXZJ3L682HT+9HIh75/mRLyff8Oc3D6ix1m1fDuTiOe27t03EUu1HiKkyDUNAp1glBv+jVN0/+MWoxmNdQJhquC/ir+hUCxUCwA4C7hBCHDrJaqc0ImF+Tyg9/qeZ5W4zws3/Gd8pKfRT/fKa9snTaYQo09Fi96WA67o1hZDt80a4j/5H8FftsrePrbvhPe2W0xdNeklz0nDlwz632HmcNXT3s9cPoLsZa9D0/o4/ftK1vmjF417o31U97eNPW/XlPf2mIxaPOkV73GD/CZ8h/vqYO2TR28depbXtOGek57hwbfaPneRuO0g3hMf6/DxCyfciJwbWt2mnCllIrW6jrjLwBUtgrXf6FY8mutoL8sGKrxLwSKhWIBAJ1DS0tLbW1tSUnJ6S7OuXPnzp49e8YEten/ORkZGVlZWfgrhmKhWADA74d532ybpV2yoaFB38Uxv6hWE+1fHYBioVgAwO+qWNpJ4SEAxUKxAID/F8UShnb35P0jVbH00sy1rAE3HYZioVgAwO+s2DYttZkJbwuAYqFYAMA98WtfVf4BqthbwcFwKBaKBQD8rorF4VMAxUKxAID/37217fzbP4B022pZ/M1CsVAsAAAAKBZAsQAAAKBYKBYAAAAUC8UCAACAYgEUCwAAAIqFYgEAAECxUCwAAAAoFkCxAAAAoFgoFgAAABQLxQIAAIBiARQLAAAAioViAQAAQLFQLAAAACgWQLEAAACgWCgWAAAAFAvFAgAAgGIBFAsAAACKhWIBAABAsVAsAAAAKBZAsQAAAKBYKBYAAAAUC8UCAACAYgEUCwAAAIqFYgEAAECxnUQDWY5UJjQ1G0x/CrW1VwVyX4teqLnSkJ1XceJkRpz2R4UsTSo+ohCnKOWdkqMqNpVVxEvFlDSOoVk+Kvzw7p32c38I3L41SSGlpYny2CO88piao0ZnbTdNzabwTAIjTWBlR+NV2edOXqsqEXSNgkFXWVFG70dTq9BsEBr0QjO1sUMAAAAUe/e0CPoGg75FMJj8er2p2VQ86oX6+hRxrIeVte2EiQu/+XraqJEThw+z+PjDiR+P6KxMGj3yu48+nDBqODWmfj6a2l8NGzpy0Ku0aOaYLyd/Mmrsh+9RJy0dN+L9ztrotyOHT/z04ylffjb580+mjfncYf4sf5+tWoXE0FBHltXrWnU6PX3aaDV98KhrwR4BAABQ7F2jExqvNVPZalZs7dUrgqDPOH1mo4PTxqW2u1es4vbsPSuLTefZS1o+M0lDFW2n5KJalRkfl5UQn67VmNs5SYm5yUkFaakUalPnOY6laWdt0Zzzav5SnCY9XnuWZ+MiQkN9tm1d6eyyeFHE/j3V+fnGzxZUwVMMgsFgrOQBAABAsXdLqymC0NjU0mo0i76kIH+zq+uuteuSDocVJyYVxcXl8GxRQlxRckKGmslTqToluTyfr1YXaDQ5HJfJMBTqMc9mKBSUbJalHpql0Jqdtd3C+Lh8raYgTluSmlx+NC0vIe6EJJoLDFi9YP4ud3dBpzNWr3qhtdFw/WqTYMAeAQAAUOy9KNYkkrr66wZj+abb7+frvHDBGY4rTk4p0cadj4g8FxFWpOFLk+LSlZJytaZTUqHR0rRMpTanhOOLWY5C7VJeRUtrEpMuJyRS29zZWdvNlSsKlCwlRya/JBZfipXkqriSpERtUMDquXNTpTLhWr35y2mzawEAAECxd4veKFiSq07fYhB0+XmZi+d+f3iHz0Wey5TJ8+WKKo3mSlJ8VZy6gJflMOISmbxTUipXFEpiKWUKppLlyhkldRZLZdSgUE8Vx9OiolgpdVYo2c7abrFcXs6yl+lFxcfXxMfTqytR8wUqvjQ5Mch9ne2UKUIzFbJ64zFiqmXxXSwAAECxd43B0KrTNZNcTWlRSCOtZloeU0jz4rVFHF+tVl+Lj6vimdyYiOzYyGK1vJpjOyWXea5cIa9gFDUq/opGTVPqqVWrKpWMeWkVq6SYO2lpZ223PiGJhi0UxxSIY8oZRbWaL+WYHLk0g5HHBwdNGTVCuHZNaGkWWlpxlBgAAKDYeyxim1sNRsXqDI2t+kb/vb4utlZHJFGZSkWZSWxlsbGlkpgqXnFZy5bx8nKlrFNSwcpL5BIKtcsYaZE0htrkcmpXcgoKNWidapWSVsiLieis7ebHRJNZr2g1tVqexi+m58DKyuJV+RruhCRq3rgxF9KShZYmemPqGxt02CEAAACKvfsq1lS/6oRm84WgG9et2OBsd1opK1Dx5SxbzTCXFXJKJfmJk5VysnJW3sXDtJ8t5eQlvLxQxeRq2CPiiKXTJ6VpOUHf1Er/CQZcFwsAAFBspynWc+0qUuxZRlbI31Bs7S8VS0Lq2uEYc4PkavZrES8vUDPZGjZNErF4+qTUOE5vaGoW9A2CoR47BAAAQLH3othWo2JbTIptIcV6ONmdUxgVW6FkLyuoimWqGXkFa/arrMjkpK4bKlgpRTzlhnR/qVgLUmyroZkUe10wXMcOAQAAUOw9KFbfKuhNim0hxW5yW+VpVKyiiFNVKPlqBWtWbDkrN/pVJTMrquuGbNpmWWNFyxkbhTxrOlAcudTSIk2j0huadaYqFooFAAAo9l4Vqzfe1ogUq9vk6uLp6ECKLeRvKJZSoWRKOar/qNqTm+q/LhyyKaWIZ0mupeaw1GYLVPyJ6EibaVOPqVXGc79MNyhuxA4BAABQ7D0o1hhSrMHQKhhIsWs8HZ3OKZSFnKZcqapk+EqGLWfJScb6L1djFFKXThHHm/xqfFEVyhspV/IlrOpUZLSjxdSTvFpo0Rk/ehgEnFEMAABQ7L0r1mBSrP5mxSqMtSyZiSq/PDWTrWXITF06JRxPIaeSWenTg6lMp48R1KM6Fx69fJLlGVYtNOqNd3fSC7g0FgAAoNhOUazhl4qNK1dqKpifFUslbJaWqTDKqQuH/FrKGhVLWiW5XpYbQw16pRfCxCsmWp5ntEbFtuAGigAAAMXeu2NNv4guGFooXq4rPJ1szjFScmo5y1QqjalQGk8LKlBRIcuWtn2F+dtCta85BepfnGrUdrZR+5Vpi3c6/p1+F1vCsaYYH9t2uhO9tGwNf0QcaW1pcUSjElqNv+inE3CgGAAAoNj7W7EF7XIbxZpuCgHFAgAAFAvF/rbcJNfb+/UuFFtyx4FiAQAAiv1DKPYms7bJteQWuUKxAAAAxUKxd3kg9ya53upX84buKHeuZCgWAACg2D+EYm9jxFv9CsUCAAAUC8XeU26Sa/vcqWJvM9SvjA/FAgAAFPtHVOxvUCAUCwAAUCwU2/4ALKsoYmTUqI5TV2lVxUp5gTy2UCEtUyoqefayRkWp1PDlKpbWpH5aoTYxjtYxr2xu0woFrLwiXp3HSEvUbJmWL+QUVYlamuYrZbSIOotVSmqXx6kqEzTUoI3SgGW8ktK2UerJkUTTtmhYGtP89KifOqFYAACAYrteFUtKM3uOQg2aNbvN3JknFZPkSHjkVOOZxgppdbymQCEt5ZXZpEM1lysVG7fCys0SJa1Smxo0Sw0Sodm41MiMjSYHZ0ljsmViGp82VKHmaGpu0ObIuyRXWpQVE5ktjqL+aymJFNI/FAsAAFBs16tiyZ1kOKojzfUo+czsXQopNl8mIfNVxanJrKTVYlZBZSjpkwrTHLmEqlJz2Wr2a6mGM/uV/GeuaHMVsbSUnEpr0iJzaXvtaApJmoaiFFI5SzV0vKZSq6I29RSR7FmFeVv59KyousXpTgAAAMV2RcWabdpW0ZqPCZPhKFSkkvkqNLy5TqWQOHOV0jxWVsArspWxxXFcaYIqX6Uo0XI5TGxpHE/9+Zy8WMNmKyQ0W6hiKhI1lySR5hWo/1xUaJFaSTIm6ZKYydMU89VB5GNzuzoprjY1kdYhN5slDcUCAAAU28UUS/UrCZVCjfZtc4lJciXbUTFqPLprUib5ksxapGVLEtWFcVyBli1L1ub+pFjyK+mTPEpTMrG5TY/KkovL4lU0W5mkpXVoEFqf+mlKsxRqlCeoaSl10jik5ExZjLmfeuixUCwAAECxXUyxeVJxvkxiPtWora41nn/EyqmIpOQrZWQ78mVJPF+VGn/5SOJZcXi+RlmcpM7kpCciD2VpFJlqObmQbEpqJB2SKUmTVMumx0ZRP5W8tIhGuBATTj3UpqXk6Qx5DE1pWEoOK70kjaKePF5Os+VJGpqSyClUIhtXg2IBAACK7VqKZTw3nD4YRIotkMeSXKvj1CTdtL1+pw4HF6uUZMeUgD2pgXsL1Aw572RkSA4vV+/bcUYSfp6JSTi4L9TT7VjUoSwtQ3UqOTVh705z8ZoWtO98dJh6p3ehijkXFUpaVe3Ytt/RxsdqnmKrZ+Aye6cJ31B8rBcmH9hXFM9TQZypjM3mZHudbdV7fNMVYmpnMJI8NUOLaArFAgAAFNvFFLtp+tR4X+8iRkaKpdm61KRLEaGHlzvJNm+kipPsGLHORbzJnerIi7GRwWuWzxj+9vu9/jnxnTemfvDfUf16931YNOHtQY4WY48c2H828vDhNSuOHwo8ExES6b5G6+ezz8Ga+jmfLdkKSajryvUzprhMHk/ePbR6GQ21fekCEqpqt492305qz/rw3W9e7T/ob3+h6eIvPnafNS3U3YUqZhKw8UA0FAsAAFDs/anYWo67FqetVvM5cgkpc9K7gz/s18tm6rdzxn6y4Lsvpnz87ps9/z5r9AcFSSqqUMlt2kB/SpDbGomPd4zPNvEuH8X+3akxEWHem6kRvcM7evvW6G1bGL+d4Zs8Eg8eOBV9YP5nHwx/4bnpo0dE7PR58R//GDpg4Ku9//3RkLdjAgIlAQGfDH79i8Gvvd3zKdtxn1p/M2rowOeH9O/z6TuD93u5Kw8F7Fy7avHk8U+JRN1FonFD31rw9ReuP8ykYc/KYmtO/Zh1w69QLAAAQLH3n2KrFIqciHBaVBWnPhsdmhoWpAneE7Bh1YThb5FoD3uvz4pXJob4p4Uf2LpkXk+RaPCTT6ydPSty8yaXmTNe7PbQpOHD5Pv84sMO7Xd3favnM0N69Qjx3BDsvtbH3na7nQ27a+fRUP+CBOZo5MHZn308ecTwHg8+9Pm7w0YOHvJ30YOj3nxLFhh0kpG7zZ21Z6V98qE9gavtlYcDmZAA6YG9JziZxN9vl9sq7eFgPjjAdf7sJePGLPr6y/0uq4pTkjKUTCarvCCTQrEAAADF3qeKvaJRF0tjKzjjdTjGa2ySNamRweusZs0f9+le9+Xey5b+KA3PiWdz1IpL8hhfm0U77Gy/euXl3iKRzfixoV4eVMjK9u66oGYXfPvVa92fHPBot31rVoV6bgzz3HjYfV2+VlOWyCUH747ctC7AbfVpRnF4u/feDR77Pb0uJadqI6IKThw/pZB/+9agQU9085hn+dYTD498/aXxI4a991Jfm2mTQn222Fh893afngOfePTlxx99RiR64x9PWgx759BatxMR4SUJCVVpaVAsAABAsfepYktl0rqE+DKlIlcqJsVq9u/cbD3fbd50G4sxkb6ejtMneNtbaQL9snhZNic76LJs/3LnxKCA0PXrIrw8FLt38AF7FXt2HhFH+LuvidzmtcV2ifaA/4G1a6TeW4NdXZKCAnKUMZrd2yI2ugW6rozY6rVy9uzRgwZbfv7lpI9GR/vtOcvzij27nCaPj/FaH+LqnKWI2rthzdYV9sFe64/John/3YEb3FLCQ+j5a4L2R272PBcrlvt4szt9M5WKQrXqokQMxQIAABR7nyo2PeRQrUZ92XTvw5SAPWtmTP7q9QFjhrw66J+Pffzy8++/8Oz4oa8vHvOpZu+Oi7GRfnaLN82fG7xmdYTHBpKo66wZ6+b9ELtz+xlFrPO0yaMG9P2wbx9NwL59y50p0m1bclVcliyS8/EKXOHgY7t4z4plAWvd3OYvitm5e9/a9Xtd1yWHhUq2bzu4dvVht5U5jHj2+0NGDOz72aBXLEd+sN3Rlt2/m8x6RiquOJZ2OiYqyGXVJbk0Uy7LZZVVKcmlWm1FQgIUCwAAUOx9qtjWY0dptlAhPRkcGLHORb7d66IiJkMt37PKIeHQ/ku8tCBJla6UpCvEIa4rXKdNtPpktJ+tTcDK5ao9fsrdOxm/HVTLJhwKYvbsPCGODPPckBYWEum5Md5/X5L/fu1uvyI2dvuCH5Z+9pHP4oVRmzzF27y32dpLfHdutbUPcF17Jlaq3ruHFhlPJ17pGO7i7DZnpuWI99fPmx291cvfZWVCoP/l40fPiqOPhBwMWeNClWtVUlJ5fHwRx2eIJTkyORQLAABQ7H2q2BKZNF8ScyUp/lpqkvEaVpWiNEV7RhK+do5laljQqdjwDDa2NElzSRp1eM0KzmfLucjIU6GhKyd+Z/ftNxvm/OD03XibcWNkO7ZHeHlIfb3XzpyRGBRwIjzUa+4cr9mzSYc50qg8eUxVorZIxSm2bY3cuDF8o4fCZ9dKC8tdDo7nJZI8jlXv8Albs/LwSseVY7+kEVxnWK6wmLRqyuTAVStoqCyFPF0au3nOnAAnp2JelStXkFyLlGx1fEKlNg6KBQAAKPY+VWyxXFqjVVMVmy+PvZwcn8fLz4rDy5K1rJ93robJ1ygrUuPTZdHmOyCSg6uTEs6Eh+6xtd5tZ71xzkz32TP2rXBMORR4PCKEpgdcV16QxpwKP7zfwU7isaGIZ2sS1AVycZmKLdOqUwMCjhwIzpDKTkfFhLitS9i7rzwhse748ViPDRUaPismvIiJvSCOKo3XFGn44yHBF6Ijq5MTcxn5j4eCg5wdlVs31yYllqtVxawyXy6rVKvLedyjGAAAoNj7VbHUWfqTn0hOhSqmQH1zqJNiviM/+YzESaEBzSPcFOos5G+sQyuXcnJTfnogxxfyFJUpPM3+tBpjXrOQ//nhRe3GaUsp+4tAsQAAAMXep4r9+ad1uBsSNQu1fcz9Jr8ybVZrs2ybaM3twnZSNFrQ5NefNmF6IMe35cZqP99VSn6TUG8jVygWAACg2C6g2JJ2aRNqe7O25SYlmx18U7Fb8ms/kPfzz7u2z63r3Jzb/G48FAsAAFDsfarYkl+x2q+FBrn12HL7erfNiO3X7MTc8pSgWAAAgGK7gmL/p+EqTA83u/PWqrdtEFqh4qcN3X783/IEbmt9KBYAAKDY+1Kx7Y8Dl/6yAG2fmxTboWVv9SvlluPM8pty0zHq3+jXdlKHYgEAAIq9XxXb3o4d+rW9ZSt/qdjSjmrQNsVWGhXbbhO8nPILv5p62imTvb1fO/qeGIoFAAAotksptn212t6ybSP8T8VW3k6xMlPuTLG3+hWKBQAAKPb+VWynn470OweKBQAAKBaKhWIBAACKhWKhWAAAAFAsFAvFAgAAFAvFQrEAAADFQrFQLAAAACgWioViAQAAioVioVgAAIBiu7JidTqTUAwtHqudvZbZ/QkVmxunTokKs7L47licRjAY3dpigGIBAACK7TzFblm7apOz7SlpzJ9NsVlqLjU6fOm0yW2KRRULAABQ7L0qVq8XWltbzYrdvsHV08nmR0nUn02x6RyTFhNhM33KUa3arFh6R5oMBuwSAAAAxd6TYo1/6ptJsT4b3Twcrc/IJX9CxR6VRNnOmJqm5gVdExQLAABQbKcptlXXKLQ0kGI3Oiy9xDN/wgPFpFhrS4sUnhVaGs2KxYFiAACAYu9Jsa2txlpN19JAivXbvGGD/ZKLnOLPptgcrSo1OnzxlInGA8X6FgHfxQIAABR774ptajIapaW5XjC0HNjl7W5rdTI2+k+o2ISwQ1YW351OSRIMOoPB0NCig2IBAACKvSfF1tc3tik2InDPOptFKWGH/oQHijWHghZNnnDx+FFSrF6vv9bQCMUCAAAUe/e0CkJd/XW98U+9IOiK87Imj/lCK45IT9JmaJU5akVpHFuuYkpl4ipG0hDHVzCyLp0yRlytktdo2ApOUaaUkWLpw0QOx2VrtZG+vjPHjhWaGluaG1sFfb3BdNYTAAAAKPZui1hj9IKhtbXF+OVjS4OPp/sau8U/8vLzKiZTxRTwijI1U61iKhhpsTiyglN26ZSxUnNKWFkpryxWcXkcm84ojsVE21pO4yMiBD3VrvoWg762qR6KBQAAKPbu0bUaL0wxGAw6XbNJtbqsc6cd588J9/M9IokqSI4vilfnc3IqZGu0fI2Gy2eZLp0yLVvEy4tViqpEbVmcJkfJZPNcUUqK7/IV623thMZGQxOJVU9FPb0dUCwAAECx96TYG+WswVTNklz0ugSlwmn+nJ1rXfgD/mcVsbkqZaGKyVfK8mSSnHh1l845SURpkqYwjjsZHZbOyAuTEmR+u6zGjfdxWVOeniG06hvrrgmmuydCsQAAAMXeEwa90NJstKzxvGKDoGtsMF4nq9dlnvrR191t/Ij3xw8butzSwtfBdoftkk3zZ69burBLx3/tyjVzpq+Z/f02J1ubyRNG/+fVeePGMsEHSy9eEnT6+tpaehPo7ai6evVaSysUCwAAUOw9oL/xfWxtTZ250drUaLas0Fh/tTAvPSUhwsfbbcGc9QvnBa119Vju0KWz0WHpJmfbbauc9nm4yw74X0hKuFpQINTVUf0qNDaZb8RBb0ODzkBVbG1jC3YJAADoLP4Prjcb6GkN7BwAAAAASUVORK5CYII=', '男', null, null, '123123123', '2022-07-12 11:29:32', '1', 'admin', '127.0.0.1', '2022-07-12 11:29:32', '1', 'admin', '127.0.0.1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_user_message
-- ----------------------------
DROP TABLE IF EXISTS `base_user_message`;
CREATE TABLE `base_user_message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `reading_status` tinyint DEFAULT NULL,
  `reading_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_user_message
-- ----------------------------

-- ----------------------------
-- Table structure for gate_log
-- ----------------------------
DROP TABLE IF EXISTS `gate_log`;
CREATE TABLE `gate_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `opt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `body` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_13178398875749` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of gate_log
-- ----------------------------
INSERT INTO `gate_log` VALUES ('102', '用户管理', '修改', '/user/{*}', '2020-12-03 19:56:04', '1', 'liwen', '127.0.0.1', '{  \"id\" : 4,  \"username\" : \"blog\",  \"name\" : \"Mr.Liwen(博主)\",  \"sex\" : \"女\",  \"description\" : \"12\"}');
INSERT INTO `gate_log` VALUES ('103', '用户管理', '重置密码', '/user/password/{*}', '2020-12-03 19:56:36', '1', 'liwen', '127.0.0.1', '');
INSERT INTO `gate_log` VALUES ('104', '角色权限管理', '分配权限', '/group/{*}/authority/menu', '2020-12-03 19:57:06', '1', 'liwen', '127.0.0.1', '[ {  \"id\" : 1,  \"code\" : \"userManager\",  \"title\" : \"用户管理\",  \"parentId\" : 5,  \"href\" : \"com.epri.fx.client.gui.uicomponents.admin.user.UserManagementController\",  \"icon\" : \"yonghuguanli_huaban\",  \"type\" : \"menu\",  \"orderNum\" : 11,  \"description\" : \"aaaa\",  \"path\" : \"/adminSys/baseManager/userManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 37,    \"code\" : \"userManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"添加\",    \"uri\" : \"/user\",    \"menuId\" : \"1\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 38,    \"code\" : \"userManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/user/{*}\",    \"menuId\" : \"1\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 39,    \"code\" : \"userManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"修改\",    \"uri\" : \"/user/{*}\",    \"menuId\" : \"1\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 40,    \"code\" : \"userManage:rest\",    \"type\" : \"button\",    \"name\" : \"重置密码\",    \"uri\" : \"/user/password/{*}\",    \"menuId\" : \"1\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  } ]}, {  \"id\" : 5,  \"code\" : \"baseManager\",  \"title\" : \"基础配置管理\",  \"parentId\" : 13,  \"href\" : \"/admin\",  \"icon\" : \"jichupeizhi\",  \"type\" : \"dirt\",  \"orderNum\" : 2,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 6,  \"code\" : \"menuManager\",  \"title\" : \"菜单管理\",  \"parentId\" : 5,  \"href\" : \"com.epri.fx.client.gui.uicomponents.admin.menu.MenuManagementController\",  \"icon\" : \"jiaoyixulie\",  \"type\" : \"menu\",  \"orderNum\" : 12,  \"description\" : \"你大爷\",  \"path\" : \"/adminSys/baseManager/menuManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 4,    \"code\" : \"menuManager:element\",    \"type\" : \"uri\",    \"name\" : \"按钮页面\",    \"uri\" : \"/admin/element\",    \"menuId\" : \"6\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 5,    \"code\" : \"menuManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/menu/{*}\",    \"menuId\" : \"6\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 6,    \"code\" : \"menuManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/menu\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 7,    \"code\" : \"menuManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/menu/{*}\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 8,    \"code\" : \"menuManager:btn_element_add\",    \"type\" : \"button\",    \"name\" : \"新增元素\",    \"uri\" : \"/element\",    \"menuId\" : \"6\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 9,    \"code\" : \"menuManager:btn_element_edit\",    \"type\" : \"button\",    \"name\" : \"编辑元素\",    \"uri\" : \"/element\",    \"menuId\" : \"6\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 10,    \"code\" : \"menuManager:btn_element_del\",    \"type\" : \"button\",    \"name\" : \"删除元素\",    \"uri\" : \"/element/{*}\",    \"menuId\" : \"6\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 19,    \"code\" : \"menuManager:view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/menu/{*}\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 20,    \"code\" : \"menuManager:element_view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/element/{*}\",    \"menuId\" : \"6\",    \"method\" : \"GET\",    \"sel\" : true  } ]}, {  \"id\" : 7,  \"code\" : \"groupManager\",  \"title\" : \"角色权限管理\",  \"parentId\" : 5,  \"href\" : \"com.epri.fx.client.gui.uicomponents.admin.group.GroupManagementController\",  \"icon\" : \"gongnengjiaosequanxianguanli\",  \"type\" : \"menu\",  \"orderNum\" : 13,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/groupManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 12,    \"code\" : \"groupManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 14,    \"code\" : \"groupManager:btn_userManager\",    \"type\" : \"button\",    \"name\" : \"分配用户\",    \"uri\" : \"/group/{*}/user\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 15,    \"code\" : \"groupManager:btn_resourceManager\",    \"type\" : \"button\",    \"name\" : \"分配权限\",    \"uri\" : \"/group/{*}/authority/menu\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 42,    \"code\" : \"groupManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 41,    \"code\" : \"groupManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  } ]}, {  \"id\" : 8,  \"code\" : \"groupTypeManager\",  \"title\" : \"角色类型管理\",  \"parentId\" : 5,  \"href\" : \"com.epri.fx.client.gui.uicomponents.admin.grouptype.GroupTypeManagementController\",  \"icon\" : \"jiaoseleixing\",  \"type\" : \"menu\",  \"orderNum\" : 14,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/groupTypeManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 22,    \"code\" : \"groupTypeManager:view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/groupType/{*}\",    \"menuId\" : \"8\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 23,    \"code\" : \"groupTypeManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/groupType\",    \"menuId\" : \"8\",    \"method\" : \"POST\",    \"sel\" : true  }, {    \"id\" : 24,    \"code\" : \"groupTypeManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/groupType\",    \"menuId\" : \"8\",    \"method\" : \"PUT\",    \"sel\" : true  }, {    \"id\" : 25,    \"code\" : \"groupTypeManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/groupType/{*}\",    \"menuId\" : \"8\",    \"method\" : \"DELETE\",    \"sel\" : true  } ]}, {  \"id\" : 27,  \"code\" : \"gateLogManager\",  \"title\" : \"操作日志\",  \"parentId\" : 5,  \"href\" : \"/admin/gateLog\",  \"icon\" : \"caozuorizhi\",  \"type\" : \"menu\",  \"orderNum\" : 15,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/gateLogManager\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 44,  \"code\" : \"home\",  \"title\" : \"主页\",  \"parentId\" : 13,  \"href\" : \"com.epri.fx.client.gui.uicomponents.home.HomeController\",  \"icon\" : \"home-outline\",  \"orderNum\" : 0,  \"description\" : \"\",  \"path\" : \"com.epri.fx.client.gui.uicomponents.home.HomeController\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 45,  \"code\" : \"baseInfo\",  \"title\" : \"基础信息录入\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"jichuxinxi\",  \"orderNum\" : 1,  \"description\" : \"基础信息录入\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 46,  \"code\" : \"base\",  \"title\" : \"基础参数\",  \"parentId\" : 45,  \"href\" : \"com.epri.fx.client.gui.uicomponents.basicInfo.BasicDataSetController\",  \"icon\" : \"jichucanshu\",  \"orderNum\" : 0,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 47,  \"code\" : \"\",  \"title\" : \"煤价差\",  \"parentId\" : 45,  \"href\" : \"com.epri.fx.client.gui.uicomponents.basicInfo.CoalPriceDiffController\",  \"icon\" : \"Energy-\",  \"orderNum\" : 1,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 48,  \"code\" : \"\",  \"title\" : \"铁路成本\",  \"parentId\" : 45,  \"href\" : \"com.epri.fx.client.gui.uicomponents.basicInfo.RailwayCostController\",  \"icon\" : \"tieluyunshu\",  \"orderNum\" : 2,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 49,  \"code\" : \"\",  \"title\" : \"现行运价\",  \"parentId\" : 45,  \"href\" : \"com.epri.fx.client.gui.uicomponents.basicInfo.CurrentFreightController\",  \"icon\" : \"hangzheng\",  \"orderNum\" : 3,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]} ]');
INSERT INTO `gate_log` VALUES ('101', '用户管理', '修改', '/user/{*}', '2020-12-03 17:19:07', '1', 'liwen', '127.0.0.1', '{  \"id\" : 1,  \"username\" : \"admin\",  \"name\" : \"liwen\",  \"sex\" : \"男\",  \"description\" : \"\"}');
INSERT INTO `gate_log` VALUES ('105', '角色类型管理', '编辑', '/groupType', '2020-12-03 19:57:40', '1', 'liwen', '127.0.0.1', '{  \"id\" : 3,  \"code\" : \"freeaa\",  \"name\" : \"自定义类型\",  \"description\" : \"sadf\"}');
INSERT INTO `gate_log` VALUES ('106', '菜单管理', '编辑', '/menu', '2020-12-04 10:30:20', '1', 'liwen', '127.0.0.1', '{  \"id\" : 27,  \"code\" : \"gateLogManager\",  \"title\" : \"操作日志\",  \"parentId\" : 5,  \"href\" : \"com.epri.fx.client.gui.uicomponents.admin.log.LogManagementController\",  \"icon\" : \"caozuorizhi\",  \"type\" : \"menu\",  \"orderNum\" : 15,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/gateLogManager\",  \"sel\" : false,  \"elementVOS\" : [ ]}');
INSERT INTO `gate_log` VALUES ('107', '菜单管理', '编辑', '/menu', '2021-01-14 16:29:04', '1', 'liwen', '127.0.0.1', '{  \"id\" : 50,  \"code\" : \"Systemmonitoring\",  \"title\" : \"系统监控\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"jiankong\",  \"orderNum\" : 2,  \"description\" : \"\",  \"sel\" : false,  \"elementVOS\" : [ ]}');
INSERT INTO `gate_log` VALUES ('108', '菜单管理', '编辑', '/menu', '2021-01-14 16:29:09', '1', 'liwen', '127.0.0.1', '{  \"id\" : 51,  \"code\" : \"log\",  \"title\" : \"日志管理\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"rizhiguanli\",  \"orderNum\" : 3,  \"description\" : \"\",  \"sel\" : false,  \"elementVOS\" : [ ]}');
INSERT INTO `gate_log` VALUES ('109', '菜单管理', '编辑', '/menu', '2021-01-14 16:29:13', '1', 'liwen', '127.0.0.1', '{  \"id\" : 5,  \"code\" : \"baseManager\",  \"title\" : \"基础配置管理\",  \"parentId\" : 13,  \"href\" : \"/admin\",  \"icon\" : \"jichupeizhi\",  \"type\" : \"dirt\",  \"orderNum\" : 4,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager\",  \"sel\" : false,  \"elementVOS\" : [ ]}');
INSERT INTO `gate_log` VALUES ('110', '菜单管理', '编辑', '/menu', '2021-01-14 16:30:45', '1', 'liwen', '127.0.0.1', '{  \"id\" : 53,  \"code\" : \"job\",  \"title\" : \"定时任务\",  \"parentId\" : 50,  \"href\" : \"com.fx.client.gui.uicomponents.monitor.quartz.SysJobController\",  \"icon\" : \"time\",  \"orderNum\" : 1,  \"description\" : \"\",  \"sel\" : false,  \"elementVOS\" : [ ]}');
INSERT INTO `gate_log` VALUES ('111', '角色权限管理', '分配权限', '/group/{*}/authority/menu', '2021-01-14 16:32:32', '1', 'liwen', '127.0.0.1', '[ {  \"id\" : 1,  \"code\" : \"userManager\",  \"title\" : \"用户管理\",  \"parentId\" : 5,  \"href\" : \"com.fx.client.gui.uicomponents.admin.user.UserManagementController\",  \"icon\" : \"yonghuguanli_huaban\",  \"type\" : \"menu\",  \"orderNum\" : 11,  \"description\" : \"aaaa\",  \"path\" : \"/adminSys/baseManager/userManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 37,    \"code\" : \"userManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"添加\",    \"uri\" : \"/user\",    \"menuId\" : \"1\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 38,    \"code\" : \"userManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/user/{*}\",    \"menuId\" : \"1\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 39,    \"code\" : \"userManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"修改\",    \"uri\" : \"/user/{*}\",    \"menuId\" : \"1\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 40,    \"code\" : \"userManage:rest\",    \"type\" : \"button\",    \"name\" : \"重置密码\",    \"uri\" : \"/user/password/{*}\",    \"menuId\" : \"1\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  } ]}, {  \"id\" : 5,  \"code\" : \"baseManager\",  \"title\" : \"基础配置管理\",  \"parentId\" : 13,  \"href\" : \"/admin\",  \"icon\" : \"jichupeizhi\",  \"type\" : \"dirt\",  \"orderNum\" : 4,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 6,  \"code\" : \"menuManager\",  \"title\" : \"菜单管理\",  \"parentId\" : 5,  \"href\" : \"com.fx.client.gui.uicomponents.admin.menu.MenuManagementController\",  \"icon\" : \"jiaoyixulie\",  \"type\" : \"menu\",  \"orderNum\" : 12,  \"description\" : \"你大爷\",  \"path\" : \"/adminSys/baseManager/menuManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 4,    \"code\" : \"menuManager:element\",    \"type\" : \"uri\",    \"name\" : \"按钮页面\",    \"uri\" : \"/admin/element\",    \"menuId\" : \"6\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 5,    \"code\" : \"menuManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/menu/{*}\",    \"menuId\" : \"6\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 6,    \"code\" : \"menuManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/menu\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 7,    \"code\" : \"menuManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/menu/{*}\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 8,    \"code\" : \"menuManager:btn_element_add\",    \"type\" : \"button\",    \"name\" : \"新增元素\",    \"uri\" : \"/element\",    \"menuId\" : \"6\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 9,    \"code\" : \"menuManager:btn_element_edit\",    \"type\" : \"button\",    \"name\" : \"编辑元素\",    \"uri\" : \"/element\",    \"menuId\" : \"6\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 10,    \"code\" : \"menuManager:btn_element_del\",    \"type\" : \"button\",    \"name\" : \"删除元素\",    \"uri\" : \"/element/{*}\",    \"menuId\" : \"6\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 19,    \"code\" : \"menuManager:view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/menu/{*}\",    \"menuId\" : \"6\",    \"parentId\" : \"\",    \"path\" : \"\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 20,    \"code\" : \"menuManager:element_view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/element/{*}\",    \"menuId\" : \"6\",    \"method\" : \"GET\",    \"sel\" : true  } ]}, {  \"id\" : 7,  \"code\" : \"groupManager\",  \"title\" : \"角色权限管理\",  \"parentId\" : 5,  \"href\" : \"com.fx.client.gui.uicomponents.admin.group.GroupManagementController\",  \"icon\" : \"gongnengjiaosequanxianguanli\",  \"type\" : \"menu\",  \"orderNum\" : 13,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/groupManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 12,    \"code\" : \"groupManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 14,    \"code\" : \"groupManager:btn_userManager\",    \"type\" : \"button\",    \"name\" : \"分配用户\",    \"uri\" : \"/group/{*}/user\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 15,    \"code\" : \"groupManager:btn_resourceManager\",    \"type\" : \"button\",    \"name\" : \"分配权限\",    \"uri\" : \"/group/{*}/authority/menu\",    \"menuId\" : \"7\",    \"method\" : \"PUT\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 42,    \"code\" : \"groupManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"DELETE\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 41,    \"code\" : \"groupManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/group\",    \"menuId\" : \"7\",    \"method\" : \"POST\",    \"description\" : \"\",    \"sel\" : true  } ]}, {  \"id\" : 8,  \"code\" : \"groupTypeManager\",  \"title\" : \"角色类型管理\",  \"parentId\" : 5,  \"href\" : \"com.fx.client.gui.uicomponents.admin.grouptype.GroupTypeManagementController\",  \"icon\" : \"jiaoseleixing\",  \"type\" : \"menu\",  \"orderNum\" : 14,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/groupTypeManager\",  \"sel\" : true,  \"elementVOS\" : [ {    \"id\" : 22,    \"code\" : \"groupTypeManager:view\",    \"type\" : \"uri\",    \"name\" : \"查看\",    \"uri\" : \"/admin/groupType/{*}\",    \"menuId\" : \"8\",    \"method\" : \"GET\",    \"description\" : \"\",    \"sel\" : true  }, {    \"id\" : 23,    \"code\" : \"groupTypeManager:btn_add\",    \"type\" : \"button\",    \"name\" : \"新增\",    \"uri\" : \"/groupType\",    \"menuId\" : \"8\",    \"method\" : \"POST\",    \"sel\" : true  }, {    \"id\" : 24,    \"code\" : \"groupTypeManager:btn_edit\",    \"type\" : \"button\",    \"name\" : \"编辑\",    \"uri\" : \"/groupType\",    \"menuId\" : \"8\",    \"method\" : \"PUT\",    \"sel\" : true  }, {    \"id\" : 25,    \"code\" : \"groupTypeManager:btn_del\",    \"type\" : \"button\",    \"name\" : \"删除\",    \"uri\" : \"/groupType/{*}\",    \"menuId\" : \"8\",    \"method\" : \"DELETE\",    \"sel\" : true  } ]}, {  \"id\" : 27,  \"code\" : \"gateLogManager\",  \"title\" : \"操作日志\",  \"parentId\" : 5,  \"href\" : \"com.fx.client.gui.uicomponents.admin.log.LogManagementController\",  \"icon\" : \"caozuorizhi\",  \"type\" : \"menu\",  \"orderNum\" : 15,  \"description\" : \"\",  \"path\" : \"/adminSys/baseManager/gateLogManager\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 44,  \"code\" : \"home\",  \"title\" : \"主页\",  \"parentId\" : 13,  \"href\" : \"com.fx.client.gui.uicomponents.home.HomeController\",  \"icon\" : \"home-outline\",  \"orderNum\" : 0,  \"description\" : \"\",  \"path\" : \"com.epri.fx.client.gui.uicomponents.home.HomeController\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 45,  \"code\" : \"baseInfo\",  \"title\" : \"基础信息录入\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"jichuxinxi\",  \"orderNum\" : 1,  \"description\" : \"基础信息录入\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 46,  \"code\" : \"base\",  \"title\" : \"基础参数\",  \"parentId\" : 45,  \"href\" : \"com.fx.client.gui.uicomponents.basicInfo.BasicDataSetController\",  \"icon\" : \"jichucanshu\",  \"orderNum\" : 0,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 47,  \"code\" : \"\",  \"title\" : \"煤价差\",  \"parentId\" : 45,  \"href\" : \"com.fx.client.gui.uicomponents.basicInfo.CoalPriceDiffController\",  \"icon\" : \"Energy-\",  \"orderNum\" : 1,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 48,  \"code\" : \"\",  \"title\" : \"铁路成本\",  \"parentId\" : 45,  \"href\" : \"com.fx.client.gui.uicomponents.basicInfo.RailwayCostController\",  \"icon\" : \"tieluyunshu\",  \"orderNum\" : 2,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 49,  \"code\" : \"\",  \"title\" : \"现行运价\",  \"parentId\" : 45,  \"href\" : \"com.fx.client.gui.uicomponents.basicInfo.CurrentFreightController\",  \"icon\" : \"hangzheng\",  \"orderNum\" : 3,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 50,  \"code\" : \"Systemmonitoring\",  \"title\" : \"系统监控\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"jiankong\",  \"orderNum\" : 2,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 51,  \"code\" : \"log\",  \"title\" : \"日志管理\",  \"parentId\" : 13,  \"href\" : \"\",  \"icon\" : \"rizhiguanli\",  \"orderNum\" : 3,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 52,  \"code\" : \"online_user\",  \"title\" : \"在线用户\",  \"parentId\" : 50,  \"href\" : \"com.fx.client.gui.uicomponents.monitor.onlineuser.OnlineUserController\",  \"icon\" : \"ic_user_zx\",  \"orderNum\" : 0,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 53,  \"code\" : \"job\",  \"title\" : \"定时任务\",  \"parentId\" : 50,  \"href\" : \"com.fx.client.gui.uicomponents.monitor.quartz.SysJobController\",  \"icon\" : \"time\",  \"orderNum\" : 1,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 54,  \"code\" : \"login_log\",  \"title\" : \"登录日志\",  \"parentId\" : 51,  \"href\" : \"com.fx.client.gui.uicomponents.admin.logManagement.LoginLogController\",  \"icon\" : \"denglurizhi\",  \"orderNum\" : 1,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]}, {  \"id\" : 55,  \"code\" : \"opt_log\",  \"title\" : \"操作日志\",  \"parentId\" : 51,  \"href\" : \"com.fx.client.gui.uicomponents.admin.logManagement.OperationLogController\",  \"icon\" : \"caozuorizhi\",  \"orderNum\" : 0,  \"description\" : \"\",  \"sel\" : true,  \"elementVOS\" : [ ]} ]');

-- ----------------------------
-- Table structure for store_draft
-- ----------------------------
DROP TABLE IF EXISTS `store_draft`;
CREATE TABLE `store_draft` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `publisher` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `content` text,
  `file_path` varchar(200) DEFAULT NULL,
  `size` double(12,0) DEFAULT NULL,
  `draft_type` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store_draft
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2692 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('276', 'admin', '/127.0.0.1:63620', '', '', '', '0', '登录成功！', '2021-01-14 16:36:30');
INSERT INTO `sys_logininfor` VALUES ('277', 'admin', '/127.0.0.1:59037', '', '', '', '0', '登录成功！', '2022-05-24 16:33:58');
INSERT INTO `sys_logininfor` VALUES ('278', 'admin', '/127.0.0.1:59037', '', '', '', '1', '退出登录', '2022-05-24 16:33:58');
INSERT INTO `sys_logininfor` VALUES ('279', 'admin', '/127.0.0.1:61259', '', '', '', '0', '登录成功！', '2022-05-24 17:18:32');
INSERT INTO `sys_logininfor` VALUES ('280', 'admin', '/127.0.0.1:61259', '', '', '', '1', '连接异常', '2022-05-24 17:18:32');
INSERT INTO `sys_logininfor` VALUES ('281', 'admin', '/127.0.0.1:61259', '', '', '', '1', '退出登录', '2022-05-24 17:18:32');
INSERT INTO `sys_logininfor` VALUES ('282', 'admin', '/127.0.0.1:61259', '', '', '', '1', '连接异常', '2022-05-24 17:18:32');
INSERT INTO `sys_logininfor` VALUES ('283', 'admin', '/127.0.0.1:55390', '', '', '', '0', '登录成功！', '2022-05-25 09:09:55');
INSERT INTO `sys_logininfor` VALUES ('284', 'admin', '/127.0.0.1:55390', '', '', '', '1', '退出登录', '2022-05-25 09:09:55');
INSERT INTO `sys_logininfor` VALUES ('285', 'admin', '/127.0.0.1:55419', '', '', '', '0', '登录成功！', '2022-05-25 09:10:31');
INSERT INTO `sys_logininfor` VALUES ('286', 'admin', '/127.0.0.1:55419', '', '', '', '1', '退出登录', '2022-05-25 09:10:31');
INSERT INTO `sys_logininfor` VALUES ('287', 'admin', '/127.0.0.1:55483', '', '', '', '0', '登录成功！', '2022-05-26 11:08:58');
INSERT INTO `sys_logininfor` VALUES ('288', 'admin', '/127.0.0.1:56143', '', '', '', '0', '登录成功！', '2022-05-26 11:14:33');
INSERT INTO `sys_logininfor` VALUES ('289', 'admin', '/127.0.0.1:56483', '', '', '', '0', '登录成功！', '2022-05-26 11:17:40');
INSERT INTO `sys_logininfor` VALUES ('290', 'admin', '/127.0.0.1:56483', '', '', '', '1', '连接异常', '2022-05-26 11:17:40');
INSERT INTO `sys_logininfor` VALUES ('291', 'admin', '/127.0.0.1:56483', '', '', '', '1', '退出登录', '2022-05-26 11:17:40');
INSERT INTO `sys_logininfor` VALUES ('292', 'admin', '/127.0.0.1:56483', '', '', '', '1', '连接异常', '2022-05-26 11:17:40');
INSERT INTO `sys_logininfor` VALUES ('293', 'admin', '/127.0.0.1:57240', '', '', '', '0', '登录成功！', '2022-05-26 11:24:34');
INSERT INTO `sys_logininfor` VALUES ('294', 'admin', '/127.0.0.1:57240', '', '', '', '1', '连接异常', '2022-05-26 11:24:34');
INSERT INTO `sys_logininfor` VALUES ('295', 'admin', '/127.0.0.1:57240', '', '', '', '1', '退出登录', '2022-05-26 11:24:34');
INSERT INTO `sys_logininfor` VALUES ('296', 'admin', '/127.0.0.1:57240', '', '', '', '1', '连接异常', '2022-05-26 11:24:34');
INSERT INTO `sys_logininfor` VALUES ('297', 'admin', '/127.0.0.1:58665', '', '', '', '0', '登录成功！', '2022-05-26 11:35:39');
INSERT INTO `sys_logininfor` VALUES ('298', 'admin', '/127.0.0.1:58665', '', '', '', '1', '连接异常', '2022-05-26 11:35:39');
INSERT INTO `sys_logininfor` VALUES ('299', 'admin', '/127.0.0.1:58665', '', '', '', '1', '退出登录', '2022-05-26 11:35:39');
INSERT INTO `sys_logininfor` VALUES ('300', 'admin', '/127.0.0.1:58665', '', '', '', '1', '连接异常', '2022-05-26 11:35:39');
INSERT INTO `sys_logininfor` VALUES ('301', 'admin', '/127.0.0.1:59007', '', '', '', '0', '登录成功！', '2022-05-26 11:38:35');
INSERT INTO `sys_logininfor` VALUES ('302', 'admin', '/127.0.0.1:59007', '', '', '', '1', '连接异常', '2022-05-26 11:38:35');
INSERT INTO `sys_logininfor` VALUES ('303', 'admin', '/127.0.0.1:59007', '', '', '', '1', '退出登录', '2022-05-26 11:38:35');
INSERT INTO `sys_logininfor` VALUES ('304', 'admin', '/127.0.0.1:59007', '', '', '', '1', '连接异常', '2022-05-26 11:38:35');
INSERT INTO `sys_logininfor` VALUES ('305', 'admin', '/127.0.0.1:59144', '', '', '', '0', '登录成功！', '2022-05-26 11:40:00');
INSERT INTO `sys_logininfor` VALUES ('306', 'admin', '/127.0.0.1:59144', '', '', '', '1', '连接异常', '2022-05-26 11:40:00');
INSERT INTO `sys_logininfor` VALUES ('307', 'admin', '/127.0.0.1:59144', '', '', '', '1', '退出登录', '2022-05-26 11:40:00');
INSERT INTO `sys_logininfor` VALUES ('308', 'admin', '/127.0.0.1:59144', '', '', '', '1', '连接异常', '2022-05-26 11:40:00');
INSERT INTO `sys_logininfor` VALUES ('309', 'admin', '/127.0.0.1:59520', '', '', '', '0', '登录成功！', '2022-05-26 11:42:44');
INSERT INTO `sys_logininfor` VALUES ('310', 'admin', '/127.0.0.1:59520', '', '', '', '1', '连接异常', '2022-05-26 11:42:44');
INSERT INTO `sys_logininfor` VALUES ('311', 'admin', '/127.0.0.1:59520', '', '', '', '1', '退出登录', '2022-05-26 11:42:44');
INSERT INTO `sys_logininfor` VALUES ('312', 'admin', '/127.0.0.1:59520', '', '', '', '1', '连接异常', '2022-05-26 11:42:44');
INSERT INTO `sys_logininfor` VALUES ('313', 'admin', '/127.0.0.1:59699', '', '', '', '0', '登录成功！', '2022-05-26 11:44:28');
INSERT INTO `sys_logininfor` VALUES ('314', 'admin', '/127.0.0.1:59699', '', '', '', '1', '连接异常', '2022-05-26 11:44:28');
INSERT INTO `sys_logininfor` VALUES ('315', 'admin', '/127.0.0.1:59699', '', '', '', '1', '退出登录', '2022-05-26 11:44:28');
INSERT INTO `sys_logininfor` VALUES ('316', 'admin', '/127.0.0.1:59699', '', '', '', '1', '连接异常', '2022-05-26 11:44:28');
INSERT INTO `sys_logininfor` VALUES ('317', 'admin', '/127.0.0.1:59891', '', '', '', '0', '登录成功！', '2022-05-26 11:46:20');
INSERT INTO `sys_logininfor` VALUES ('318', 'admin', '/127.0.0.1:59891', '', '', '', '1', '连接异常', '2022-05-26 11:46:20');
INSERT INTO `sys_logininfor` VALUES ('319', 'admin', '/127.0.0.1:59891', '', '', '', '1', '退出登录', '2022-05-26 11:46:20');
INSERT INTO `sys_logininfor` VALUES ('320', 'admin', '/127.0.0.1:59891', '', '', '', '1', '连接异常', '2022-05-26 11:46:20');
INSERT INTO `sys_logininfor` VALUES ('321', 'admin', '/127.0.0.1:60038', '', '', '', '0', '登录成功！', '2022-05-26 11:48:22');
INSERT INTO `sys_logininfor` VALUES ('322', 'admin', '/127.0.0.1:60038', '', '', '', '1', '连接异常', '2022-05-26 11:48:22');
INSERT INTO `sys_logininfor` VALUES ('323', 'admin', '/127.0.0.1:60038', '', '', '', '1', '退出登录', '2022-05-26 11:48:22');
INSERT INTO `sys_logininfor` VALUES ('324', 'admin', '/127.0.0.1:60038', '', '', '', '1', '连接异常', '2022-05-26 11:48:22');
INSERT INTO `sys_logininfor` VALUES ('325', 'admin', '/127.0.0.1:60137', '', '', '', '0', '登录成功！', '2022-05-26 11:49:14');
INSERT INTO `sys_logininfor` VALUES ('326', 'admin', '/127.0.0.1:60137', '', '', '', '1', '连接异常', '2022-05-26 11:49:14');
INSERT INTO `sys_logininfor` VALUES ('327', 'admin', '/127.0.0.1:60137', '', '', '', '1', '退出登录', '2022-05-26 11:49:14');
INSERT INTO `sys_logininfor` VALUES ('328', 'admin', '/127.0.0.1:60137', '', '', '', '1', '连接异常', '2022-05-26 11:49:14');
INSERT INTO `sys_logininfor` VALUES ('329', 'admin', '/127.0.0.1:60213', '', '', '', '0', '登录成功！', '2022-05-26 11:50:07');
INSERT INTO `sys_logininfor` VALUES ('330', 'admin', '/127.0.0.1:60213', '', '', '', '1', '连接异常', '2022-05-26 11:50:07');
INSERT INTO `sys_logininfor` VALUES ('331', 'admin', '/127.0.0.1:60213', '', '', '', '1', '退出登录', '2022-05-26 11:50:07');
INSERT INTO `sys_logininfor` VALUES ('332', 'admin', '/127.0.0.1:60213', '', '', '', '1', '连接异常', '2022-05-26 11:50:07');
INSERT INTO `sys_logininfor` VALUES ('333', 'admin', '/127.0.0.1:60452', '', '', '', '0', '登录成功！', '2022-05-26 11:52:59');
INSERT INTO `sys_logininfor` VALUES ('334', 'admin', '/127.0.0.1:60452', '', '', '', '1', '连接异常', '2022-05-26 11:52:59');
INSERT INTO `sys_logininfor` VALUES ('335', 'admin', '/127.0.0.1:60452', '', '', '', '1', '退出登录', '2022-05-26 11:52:59');
INSERT INTO `sys_logininfor` VALUES ('336', 'admin', '/127.0.0.1:60452', '', '', '', '1', '连接异常', '2022-05-26 11:52:59');
INSERT INTO `sys_logininfor` VALUES ('337', 'admin', '/127.0.0.1:60586', '', '', '', '0', '登录成功！', '2022-05-26 11:54:18');
INSERT INTO `sys_logininfor` VALUES ('338', 'admin', '/127.0.0.1:60586', '', '', '', '1', '退出登录', '2022-05-26 11:54:18');
INSERT INTO `sys_logininfor` VALUES ('339', 'admin', '/127.0.0.1:52828', '', '', '', '0', '登录成功！', '2022-05-30 17:24:34');
INSERT INTO `sys_logininfor` VALUES ('340', 'admin', '/127.0.0.1:52828', '', '', '', '1', '退出登录', '2022-05-30 17:24:34');
INSERT INTO `sys_logininfor` VALUES ('341', 'admin', '/127.0.0.1:51288', '', '', '', '0', '登录成功！', '2022-05-31 08:53:12');
INSERT INTO `sys_logininfor` VALUES ('342', 'admin', '/127.0.0.1:51288', '', '', '', '1', '连接异常', '2022-05-31 08:53:12');
INSERT INTO `sys_logininfor` VALUES ('343', 'admin', '/127.0.0.1:51288', '', '', '', '1', '退出登录', '2022-05-31 08:53:12');
INSERT INTO `sys_logininfor` VALUES ('344', 'admin', '/127.0.0.1:51288', '', '', '', '1', '连接异常', '2022-05-31 08:53:12');
INSERT INTO `sys_logininfor` VALUES ('345', 'admin', '/127.0.0.1:51642', '', '', '', '0', '登录成功！', '2022-05-31 09:07:48');
INSERT INTO `sys_logininfor` VALUES ('346', 'admin', '/127.0.0.1:51642', '', '', '', '1', '连接异常', '2022-05-31 09:07:48');
INSERT INTO `sys_logininfor` VALUES ('347', 'admin', '/127.0.0.1:51642', '', '', '', '1', '退出登录', '2022-05-31 09:07:48');
INSERT INTO `sys_logininfor` VALUES ('348', 'admin', '/127.0.0.1:51642', '', '', '', '1', '连接异常', '2022-05-31 09:07:48');
INSERT INTO `sys_logininfor` VALUES ('349', 'admin', '/127.0.0.1:51729', '', '', '', '0', '登录成功！', '2022-05-31 09:10:47');
INSERT INTO `sys_logininfor` VALUES ('350', 'admin', '/127.0.0.1:51729', '', '', '', '1', '连接异常', '2022-05-31 09:10:47');
INSERT INTO `sys_logininfor` VALUES ('351', 'admin', '/127.0.0.1:51729', '', '', '', '1', '退出登录', '2022-05-31 09:10:47');
INSERT INTO `sys_logininfor` VALUES ('352', 'admin', '/127.0.0.1:51729', '', '', '', '1', '连接异常', '2022-05-31 09:10:47');
INSERT INTO `sys_logininfor` VALUES ('353', 'admin', '/127.0.0.1:52091', '', '', '', '0', '登录成功！', '2022-05-31 09:24:55');
INSERT INTO `sys_logininfor` VALUES ('354', 'admin', '/127.0.0.1:52091', '', '', '', '1', '连接异常', '2022-05-31 09:24:55');
INSERT INTO `sys_logininfor` VALUES ('355', 'admin', '/127.0.0.1:52091', '', '', '', '1', '退出登录', '2022-05-31 09:24:55');
INSERT INTO `sys_logininfor` VALUES ('356', 'admin', '/127.0.0.1:52091', '', '', '', '1', '连接异常', '2022-05-31 09:24:55');
INSERT INTO `sys_logininfor` VALUES ('357', 'admin', '/127.0.0.1:52104', '', '', '', '0', '登录成功！', '2022-05-31 09:25:15');
INSERT INTO `sys_logininfor` VALUES ('358', 'admin', '/127.0.0.1:52104', '', '', '', '1', '连接异常', '2022-05-31 09:25:15');
INSERT INTO `sys_logininfor` VALUES ('359', 'admin', '/127.0.0.1:52104', '', '', '', '1', '退出登录', '2022-05-31 09:25:15');
INSERT INTO `sys_logininfor` VALUES ('360', 'admin', '/127.0.0.1:52104', '', '', '', '1', '连接异常', '2022-05-31 09:25:15');
INSERT INTO `sys_logininfor` VALUES ('361', 'admin', '/127.0.0.1:52216', '', '', '', '0', '登录成功！', '2022-05-31 09:27:20');
INSERT INTO `sys_logininfor` VALUES ('362', 'admin', '/127.0.0.1:52216', '', '', '', '1', '连接异常', '2022-05-31 09:27:20');
INSERT INTO `sys_logininfor` VALUES ('363', 'admin', '/127.0.0.1:52216', '', '', '', '1', '退出登录', '2022-05-31 09:27:20');
INSERT INTO `sys_logininfor` VALUES ('364', 'admin', '/127.0.0.1:52216', '', '', '', '1', '连接异常', '2022-05-31 09:27:20');
INSERT INTO `sys_logininfor` VALUES ('365', 'admin', '/127.0.0.1:64153', '', '', '', '0', '登录成功！', '2022-05-31 09:37:41');
INSERT INTO `sys_logininfor` VALUES ('366', 'admin', '/127.0.0.1:64153', '', '', '', '1', '连接异常', '2022-05-31 09:37:41');
INSERT INTO `sys_logininfor` VALUES ('367', 'admin', '/127.0.0.1:64153', '', '', '', '1', '退出登录', '2022-05-31 09:37:41');
INSERT INTO `sys_logininfor` VALUES ('368', 'admin', '/127.0.0.1:64153', '', '', '', '1', '连接异常', '2022-05-31 09:37:41');
INSERT INTO `sys_logininfor` VALUES ('369', 'admin', '/127.0.0.1:64212', '', '', '', '0', '登录成功！', '2022-05-31 09:39:28');
INSERT INTO `sys_logininfor` VALUES ('370', 'admin', '/127.0.0.1:64212', '', '', '', '1', '连接异常', '2022-05-31 09:39:28');
INSERT INTO `sys_logininfor` VALUES ('371', 'admin', '/127.0.0.1:64212', '', '', '', '1', '退出登录', '2022-05-31 09:39:28');
INSERT INTO `sys_logininfor` VALUES ('372', 'admin', '/127.0.0.1:64212', '', '', '', '1', '连接异常', '2022-05-31 09:39:28');
INSERT INTO `sys_logininfor` VALUES ('373', 'admin', '/127.0.0.1:64254', '', '', '', '0', '登录成功！', '2022-05-31 09:40:51');
INSERT INTO `sys_logininfor` VALUES ('374', 'admin', '/127.0.0.1:64254', '', '', '', '1', '连接异常', '2022-05-31 09:40:51');
INSERT INTO `sys_logininfor` VALUES ('375', 'admin', '/127.0.0.1:64254', '', '', '', '1', '退出登录', '2022-05-31 09:40:51');
INSERT INTO `sys_logininfor` VALUES ('376', 'admin', '/127.0.0.1:64254', '', '', '', '1', '连接异常', '2022-05-31 09:40:51');
INSERT INTO `sys_logininfor` VALUES ('377', 'admin', '/127.0.0.1:64355', '', '', '', '0', '登录成功！', '2022-05-31 09:44:38');
INSERT INTO `sys_logininfor` VALUES ('378', 'admin', '/127.0.0.1:64355', '', '', '', '1', '连接异常', '2022-05-31 09:44:38');
INSERT INTO `sys_logininfor` VALUES ('379', 'admin', '/127.0.0.1:64355', '', '', '', '1', '退出登录', '2022-05-31 09:44:38');
INSERT INTO `sys_logininfor` VALUES ('380', 'admin', '/127.0.0.1:64355', '', '', '', '1', '连接异常', '2022-05-31 09:44:38');
INSERT INTO `sys_logininfor` VALUES ('381', 'admin', '/127.0.0.1:64420', '', '', '', '0', '登录成功！', '2022-05-31 09:47:19');
INSERT INTO `sys_logininfor` VALUES ('382', 'admin', '/127.0.0.1:64420', '', '', '', '1', '连接异常', '2022-05-31 09:47:19');
INSERT INTO `sys_logininfor` VALUES ('383', 'admin', '/127.0.0.1:64420', '', '', '', '1', '退出登录', '2022-05-31 09:47:19');
INSERT INTO `sys_logininfor` VALUES ('384', 'admin', '/127.0.0.1:64420', '', '', '', '1', '连接异常', '2022-05-31 09:47:19');
INSERT INTO `sys_logininfor` VALUES ('385', 'admin', '/127.0.0.1:64482', '', '', '', '0', '登录成功！', '2022-05-31 09:49:50');
INSERT INTO `sys_logininfor` VALUES ('386', 'admin', '/127.0.0.1:64482', '', '', '', '1', '连接异常', '2022-05-31 09:49:50');
INSERT INTO `sys_logininfor` VALUES ('387', 'admin', '/127.0.0.1:64482', '', '', '', '1', '退出登录', '2022-05-31 09:49:50');
INSERT INTO `sys_logininfor` VALUES ('388', 'admin', '/127.0.0.1:64482', '', '', '', '1', '连接异常', '2022-05-31 09:49:50');
INSERT INTO `sys_logininfor` VALUES ('389', 'admin', '/127.0.0.1:64566', '', '', '', '0', '登录成功！', '2022-05-31 09:52:21');
INSERT INTO `sys_logininfor` VALUES ('390', 'admin', '/127.0.0.1:64566', '', '', '', '1', '连接异常', '2022-05-31 09:52:21');
INSERT INTO `sys_logininfor` VALUES ('391', 'admin', '/127.0.0.1:64566', '', '', '', '1', '退出登录', '2022-05-31 09:52:21');
INSERT INTO `sys_logininfor` VALUES ('392', 'admin', '/127.0.0.1:64566', '', '', '', '1', '连接异常', '2022-05-31 09:52:21');
INSERT INTO `sys_logininfor` VALUES ('393', 'admin', '/127.0.0.1:64595', '', '', '', '0', '登录成功！', '2022-05-31 09:53:34');
INSERT INTO `sys_logininfor` VALUES ('394', 'admin', '/127.0.0.1:64595', '', '', '', '1', '连接异常', '2022-05-31 09:53:34');
INSERT INTO `sys_logininfor` VALUES ('395', 'admin', '/127.0.0.1:64595', '', '', '', '1', '退出登录', '2022-05-31 09:53:34');
INSERT INTO `sys_logininfor` VALUES ('396', 'admin', '/127.0.0.1:64595', '', '', '', '1', '连接异常', '2022-05-31 09:53:34');
INSERT INTO `sys_logininfor` VALUES ('397', 'admin', '/127.0.0.1:64637', '', '', '', '0', '登录成功！', '2022-05-31 09:55:04');
INSERT INTO `sys_logininfor` VALUES ('398', 'admin', '/127.0.0.1:64637', '', '', '', '1', '连接异常', '2022-05-31 09:55:04');
INSERT INTO `sys_logininfor` VALUES ('399', 'admin', '/127.0.0.1:64637', '', '', '', '1', '退出登录', '2022-05-31 09:55:04');
INSERT INTO `sys_logininfor` VALUES ('400', 'admin', '/127.0.0.1:64637', '', '', '', '1', '连接异常', '2022-05-31 09:55:04');
INSERT INTO `sys_logininfor` VALUES ('401', 'admin', '/127.0.0.1:64713', '', '', '', '0', '登录成功！', '2022-05-31 09:57:40');
INSERT INTO `sys_logininfor` VALUES ('402', 'admin', '/127.0.0.1:64713', '', '', '', '1', '连接异常', '2022-05-31 09:57:40');
INSERT INTO `sys_logininfor` VALUES ('403', 'admin', '/127.0.0.1:64713', '', '', '', '1', '退出登录', '2022-05-31 09:57:40');
INSERT INTO `sys_logininfor` VALUES ('404', 'admin', '/127.0.0.1:64713', '', '', '', '1', '连接异常', '2022-05-31 09:57:40');
INSERT INTO `sys_logininfor` VALUES ('405', 'admin', '/127.0.0.1:64742', '', '', '', '0', '登录成功！', '2022-05-31 09:58:14');
INSERT INTO `sys_logininfor` VALUES ('406', 'admin', '/127.0.0.1:64742', '', '', '', '1', '连接异常', '2022-05-31 09:58:14');
INSERT INTO `sys_logininfor` VALUES ('407', 'admin', '/127.0.0.1:64742', '', '', '', '1', '退出登录', '2022-05-31 09:58:14');
INSERT INTO `sys_logininfor` VALUES ('408', 'admin', '/127.0.0.1:64742', '', '', '', '1', '连接异常', '2022-05-31 09:58:14');
INSERT INTO `sys_logininfor` VALUES ('409', 'admin', '/127.0.0.1:64775', '', '', '', '0', '登录成功！', '2022-05-31 09:59:20');
INSERT INTO `sys_logininfor` VALUES ('410', 'admin', '/127.0.0.1:64775', '', '', '', '1', '连接异常', '2022-05-31 09:59:20');
INSERT INTO `sys_logininfor` VALUES ('411', 'admin', '/127.0.0.1:64775', '', '', '', '1', '退出登录', '2022-05-31 09:59:20');
INSERT INTO `sys_logininfor` VALUES ('412', 'admin', '/127.0.0.1:64775', '', '', '', '1', '连接异常', '2022-05-31 09:59:20');
INSERT INTO `sys_logininfor` VALUES ('413', 'admin', '/127.0.0.1:64806', '', '', '', '0', '登录成功！', '2022-05-31 10:00:23');
INSERT INTO `sys_logininfor` VALUES ('414', 'admin', '/127.0.0.1:64806', '', '', '', '1', '连接异常', '2022-05-31 10:00:23');
INSERT INTO `sys_logininfor` VALUES ('415', 'admin', '/127.0.0.1:64806', '', '', '', '1', '退出登录', '2022-05-31 10:00:23');
INSERT INTO `sys_logininfor` VALUES ('416', 'admin', '/127.0.0.1:64806', '', '', '', '1', '连接异常', '2022-05-31 10:00:23');
INSERT INTO `sys_logininfor` VALUES ('417', 'admin', '/127.0.0.1:64840', '', '', '', '0', '登录成功！', '2022-05-31 10:01:14');
INSERT INTO `sys_logininfor` VALUES ('418', 'admin', '/127.0.0.1:64840', '', '', '', '1', '连接异常', '2022-05-31 10:01:14');
INSERT INTO `sys_logininfor` VALUES ('419', 'admin', '/127.0.0.1:64840', '', '', '', '1', '退出登录', '2022-05-31 10:01:14');
INSERT INTO `sys_logininfor` VALUES ('420', 'admin', '/127.0.0.1:64840', '', '', '', '1', '连接异常', '2022-05-31 10:01:14');
INSERT INTO `sys_logininfor` VALUES ('421', 'admin', '/127.0.0.1:64886', '', '', '', '0', '登录成功！', '2022-05-31 10:03:19');
INSERT INTO `sys_logininfor` VALUES ('422', 'admin', '/127.0.0.1:64886', '', '', '', '1', '退出登录', '2022-05-31 10:03:19');
INSERT INTO `sys_logininfor` VALUES ('423', 'admin', '/127.0.0.1:52335', '', '', '', '0', '登录成功！', '2022-06-01 10:16:09');
INSERT INTO `sys_logininfor` VALUES ('424', 'admin', '/127.0.0.1:52335', '', '', '', '1', '连接异常', '2022-06-01 10:16:09');
INSERT INTO `sys_logininfor` VALUES ('425', 'admin', '/127.0.0.1:52335', '', '', '', '1', '退出登录', '2022-06-01 10:16:09');
INSERT INTO `sys_logininfor` VALUES ('426', 'admin', '/127.0.0.1:52335', '', '', '', '1', '连接异常', '2022-06-01 10:16:09');
INSERT INTO `sys_logininfor` VALUES ('427', 'admin', '/127.0.0.1:52409', '', '', '', '0', '登录成功！', '2022-06-01 10:18:09');
INSERT INTO `sys_logininfor` VALUES ('428', 'admin', '/127.0.0.1:52409', '', '', '', '1', '连接异常', '2022-06-01 10:18:09');
INSERT INTO `sys_logininfor` VALUES ('429', 'admin', '/127.0.0.1:52409', '', '', '', '1', '退出登录', '2022-06-01 10:18:09');
INSERT INTO `sys_logininfor` VALUES ('430', 'admin', '/127.0.0.1:52409', '', '', '', '1', '连接异常', '2022-06-01 10:18:09');
INSERT INTO `sys_logininfor` VALUES ('431', 'admin', '/127.0.0.1:55706', '', '', '', '0', '登录成功！', '2022-06-01 10:29:37');
INSERT INTO `sys_logininfor` VALUES ('432', 'admin', '/127.0.0.1:55706', '', '', '', '1', '连接异常', '2022-06-01 10:29:37');
INSERT INTO `sys_logininfor` VALUES ('433', 'admin', '/127.0.0.1:55706', '', '', '', '1', '退出登录', '2022-06-01 10:29:37');
INSERT INTO `sys_logininfor` VALUES ('434', 'admin', '/127.0.0.1:55706', '', '', '', '1', '连接异常', '2022-06-01 10:29:37');
INSERT INTO `sys_logininfor` VALUES ('435', 'admin', '/127.0.0.1:55762', '', '', '', '0', '登录成功！', '2022-06-01 10:31:02');
INSERT INTO `sys_logininfor` VALUES ('436', 'admin', '/127.0.0.1:55762', '', '', '', '1', '退出登录', '2022-06-01 10:31:02');
INSERT INTO `sys_logininfor` VALUES ('437', 'admin', '/127.0.0.1:56463', '', '', '', '0', '登录成功！', '2022-06-01 10:50:38');
INSERT INTO `sys_logininfor` VALUES ('438', 'admin', '/127.0.0.1:56463', '', '', '', '1', '连接异常', '2022-06-01 10:50:38');
INSERT INTO `sys_logininfor` VALUES ('439', 'admin', '/127.0.0.1:56463', '', '', '', '1', '退出登录', '2022-06-01 10:50:38');
INSERT INTO `sys_logininfor` VALUES ('440', 'admin', '/127.0.0.1:56463', '', '', '', '1', '连接异常', '2022-06-01 10:50:38');
INSERT INTO `sys_logininfor` VALUES ('441', 'admin', '/127.0.0.1:56476', '', '', '', '0', '登录成功！', '2022-06-01 10:51:41');
INSERT INTO `sys_logininfor` VALUES ('442', 'admin', '/127.0.0.1:56476', '', '', '', '1', '连接异常', '2022-06-01 10:51:41');
INSERT INTO `sys_logininfor` VALUES ('443', 'admin', '/127.0.0.1:56476', '', '', '', '1', '退出登录', '2022-06-01 10:51:41');
INSERT INTO `sys_logininfor` VALUES ('444', 'admin', '/127.0.0.1:56476', '', '', '', '1', '连接异常', '2022-06-01 10:51:41');
INSERT INTO `sys_logininfor` VALUES ('445', 'admin', '/127.0.0.1:56497', '', '', '', '0', '登录成功！', '2022-06-01 10:53:23');
INSERT INTO `sys_logininfor` VALUES ('446', 'admin', '/127.0.0.1:56497', '', '', '', '1', '连接异常', '2022-06-01 10:53:23');
INSERT INTO `sys_logininfor` VALUES ('447', 'admin', '/127.0.0.1:56497', '', '', '', '1', '退出登录', '2022-06-01 10:53:23');
INSERT INTO `sys_logininfor` VALUES ('448', 'admin', '/127.0.0.1:56497', '', '', '', '1', '连接异常', '2022-06-01 10:53:23');
INSERT INTO `sys_logininfor` VALUES ('449', 'admin', '/127.0.0.1:54894', '', '', '', '0', '登录成功！', '2022-06-01 10:57:09');
INSERT INTO `sys_logininfor` VALUES ('450', 'admin', '/127.0.0.1:54894', '', '', '', '1', '连接异常', '2022-06-01 10:57:09');
INSERT INTO `sys_logininfor` VALUES ('451', 'admin', '/127.0.0.1:54894', '', '', '', '1', '退出登录', '2022-06-01 10:57:09');
INSERT INTO `sys_logininfor` VALUES ('452', 'admin', '/127.0.0.1:54894', '', '', '', '1', '连接异常', '2022-06-01 10:57:09');
INSERT INTO `sys_logininfor` VALUES ('453', 'admin', '/127.0.0.1:54937', '', '', '', '0', '登录成功！', '2022-06-01 10:58:54');
INSERT INTO `sys_logininfor` VALUES ('454', 'admin', '/127.0.0.1:54937', '', '', '', '1', '连接异常', '2022-06-01 10:58:54');
INSERT INTO `sys_logininfor` VALUES ('455', 'admin', '/127.0.0.1:54937', '', '', '', '1', '退出登录', '2022-06-01 10:58:54');
INSERT INTO `sys_logininfor` VALUES ('456', 'admin', '/127.0.0.1:54937', '', '', '', '1', '连接异常', '2022-06-01 10:58:54');
INSERT INTO `sys_logininfor` VALUES ('457', 'admin', '/127.0.0.1:55011', '', '', '', '0', '登录成功！', '2022-06-01 11:05:18');
INSERT INTO `sys_logininfor` VALUES ('458', 'admin', '/127.0.0.1:55011', '', '', '', '1', '连接异常', '2022-06-01 11:05:18');
INSERT INTO `sys_logininfor` VALUES ('459', 'admin', '/127.0.0.1:55011', '', '', '', '1', '退出登录', '2022-06-01 11:05:18');
INSERT INTO `sys_logininfor` VALUES ('460', 'admin', '/127.0.0.1:55011', '', '', '', '1', '连接异常', '2022-06-01 11:05:18');
INSERT INTO `sys_logininfor` VALUES ('461', 'admin', '/127.0.0.1:55046', '', '', '', '0', '登录成功！', '2022-06-01 11:09:35');
INSERT INTO `sys_logininfor` VALUES ('462', 'admin', '/127.0.0.1:55046', '', '', '', '1', '连接异常', '2022-06-01 11:09:35');
INSERT INTO `sys_logininfor` VALUES ('463', 'admin', '/127.0.0.1:55046', '', '', '', '1', '退出登录', '2022-06-01 11:09:35');
INSERT INTO `sys_logininfor` VALUES ('464', 'admin', '/127.0.0.1:55046', '', '', '', '1', '连接异常', '2022-06-01 11:09:35');
INSERT INTO `sys_logininfor` VALUES ('465', 'admin', '/127.0.0.1:55059', '', '', '', '0', '登录成功！', '2022-06-01 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('466', 'admin', '/127.0.0.1:55059', '', '', '', '1', '连接异常', '2022-06-01 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('467', 'admin', '/127.0.0.1:55059', '', '', '', '1', '退出登录', '2022-06-01 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('468', 'admin', '/127.0.0.1:55059', '', '', '', '1', '连接异常', '2022-06-01 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('469', 'admin', '/127.0.0.1:55073', '', '', '', '0', '登录成功！', '2022-06-01 11:11:23');
INSERT INTO `sys_logininfor` VALUES ('470', 'admin', '/127.0.0.1:55073', '', '', '', '1', '连接异常', '2022-06-01 11:11:23');
INSERT INTO `sys_logininfor` VALUES ('471', 'admin', '/127.0.0.1:55073', '', '', '', '1', '退出登录', '2022-06-01 11:11:23');
INSERT INTO `sys_logininfor` VALUES ('472', 'admin', '/127.0.0.1:55073', '', '', '', '1', '连接异常', '2022-06-01 11:11:23');
INSERT INTO `sys_logininfor` VALUES ('473', 'admin', '/127.0.0.1:55091', '', '', '', '0', '登录成功！', '2022-06-01 11:12:12');
INSERT INTO `sys_logininfor` VALUES ('474', 'admin', '/127.0.0.1:55091', '', '', '', '1', '连接异常', '2022-06-01 11:12:12');
INSERT INTO `sys_logininfor` VALUES ('475', 'admin', '/127.0.0.1:55091', '', '', '', '1', '退出登录', '2022-06-01 11:12:12');
INSERT INTO `sys_logininfor` VALUES ('476', 'admin', '/127.0.0.1:55091', '', '', '', '1', '连接异常', '2022-06-01 11:12:12');
INSERT INTO `sys_logininfor` VALUES ('477', 'admin', '/127.0.0.1:55140', '', '', '', '0', '登录成功！', '2022-06-01 11:14:34');
INSERT INTO `sys_logininfor` VALUES ('478', 'admin', '/127.0.0.1:55140', '', '', '', '1', '连接异常', '2022-06-01 11:14:34');
INSERT INTO `sys_logininfor` VALUES ('479', 'admin', '/127.0.0.1:55140', '', '', '', '1', '退出登录', '2022-06-01 11:14:34');
INSERT INTO `sys_logininfor` VALUES ('480', 'admin', '/127.0.0.1:55140', '', '', '', '1', '连接异常', '2022-06-01 11:14:34');
INSERT INTO `sys_logininfor` VALUES ('481', 'admin', '/127.0.0.1:55310', '', '', '', '0', '登录成功！', '2022-06-01 11:15:51');
INSERT INTO `sys_logininfor` VALUES ('482', 'admin', '/127.0.0.1:55310', '', '', '', '1', '连接异常', '2022-06-01 11:15:51');
INSERT INTO `sys_logininfor` VALUES ('483', 'admin', '/127.0.0.1:55310', '', '', '', '1', '退出登录', '2022-06-01 11:15:51');
INSERT INTO `sys_logininfor` VALUES ('484', 'admin', '/127.0.0.1:55310', '', '', '', '1', '连接异常', '2022-06-01 11:15:51');
INSERT INTO `sys_logininfor` VALUES ('485', 'admin', '/127.0.0.1:55446', '', '', '', '0', '登录成功！', '2022-06-01 11:20:47');
INSERT INTO `sys_logininfor` VALUES ('486', 'admin', '/127.0.0.1:55446', '', '', '', '1', '连接异常', '2022-06-01 11:20:47');
INSERT INTO `sys_logininfor` VALUES ('487', 'admin', '/127.0.0.1:55446', '', '', '', '1', '退出登录', '2022-06-01 11:20:47');
INSERT INTO `sys_logininfor` VALUES ('488', 'admin', '/127.0.0.1:55446', '', '', '', '1', '连接异常', '2022-06-01 11:20:47');
INSERT INTO `sys_logininfor` VALUES ('489', 'admin', '/127.0.0.1:55474', '', '', '', '0', '登录成功！', '2022-06-01 11:22:27');
INSERT INTO `sys_logininfor` VALUES ('490', 'admin', '/127.0.0.1:55474', '', '', '', '1', '连接异常', '2022-06-01 11:22:27');
INSERT INTO `sys_logininfor` VALUES ('491', 'admin', '/127.0.0.1:55474', '', '', '', '1', '退出登录', '2022-06-01 11:22:27');
INSERT INTO `sys_logininfor` VALUES ('492', 'admin', '/127.0.0.1:55474', '', '', '', '1', '连接异常', '2022-06-01 11:22:27');
INSERT INTO `sys_logininfor` VALUES ('493', 'admin', '/127.0.0.1:55653', '', '', '', '0', '登录成功！', '2022-06-01 11:34:54');
INSERT INTO `sys_logininfor` VALUES ('494', 'admin', '/127.0.0.1:55653', '', '', '', '1', '连接异常', '2022-06-01 11:34:54');
INSERT INTO `sys_logininfor` VALUES ('495', 'admin', '/127.0.0.1:55653', '', '', '', '1', '退出登录', '2022-06-01 11:34:54');
INSERT INTO `sys_logininfor` VALUES ('496', 'admin', '/127.0.0.1:55653', '', '', '', '1', '连接异常', '2022-06-01 11:34:54');
INSERT INTO `sys_logininfor` VALUES ('497', 'admin', '/127.0.0.1:55677', '', '', '', '0', '登录成功！', '2022-06-01 11:36:05');
INSERT INTO `sys_logininfor` VALUES ('498', 'admin', '/127.0.0.1:55677', '', '', '', '1', '连接异常', '2022-06-01 11:36:05');
INSERT INTO `sys_logininfor` VALUES ('499', 'admin', '/127.0.0.1:55677', '', '', '', '1', '退出登录', '2022-06-01 11:36:05');
INSERT INTO `sys_logininfor` VALUES ('500', 'admin', '/127.0.0.1:55677', '', '', '', '1', '连接异常', '2022-06-01 11:36:05');
INSERT INTO `sys_logininfor` VALUES ('501', 'admin', '/127.0.0.1:55743', '', '', '', '0', '登录成功！', '2022-06-01 11:39:21');
INSERT INTO `sys_logininfor` VALUES ('502', 'admin', '/127.0.0.1:55743', '', '', '', '1', '连接异常', '2022-06-01 11:39:21');
INSERT INTO `sys_logininfor` VALUES ('503', 'admin', '/127.0.0.1:55743', '', '', '', '1', '退出登录', '2022-06-01 11:39:21');
INSERT INTO `sys_logininfor` VALUES ('504', 'admin', '/127.0.0.1:55743', '', '', '', '1', '连接异常', '2022-06-01 11:39:21');
INSERT INTO `sys_logininfor` VALUES ('505', 'admin', '/127.0.0.1:55824', '', '', '', '0', '登录成功！', '2022-06-01 11:44:20');
INSERT INTO `sys_logininfor` VALUES ('506', 'admin', '/127.0.0.1:55824', '', '', '', '1', '连接异常', '2022-06-01 11:44:20');
INSERT INTO `sys_logininfor` VALUES ('507', 'admin', '/127.0.0.1:55824', '', '', '', '1', '退出登录', '2022-06-01 11:44:20');
INSERT INTO `sys_logininfor` VALUES ('508', 'admin', '/127.0.0.1:55824', '', '', '', '1', '连接异常', '2022-06-01 11:44:20');
INSERT INTO `sys_logininfor` VALUES ('509', 'admin', '/127.0.0.1:55909', '', '', '', '0', '登录成功！', '2022-06-01 11:47:51');
INSERT INTO `sys_logininfor` VALUES ('510', 'admin', '/127.0.0.1:55909', '', '', '', '1', '连接异常', '2022-06-01 11:47:51');
INSERT INTO `sys_logininfor` VALUES ('511', 'admin', '/127.0.0.1:55909', '', '', '', '1', '退出登录', '2022-06-01 11:47:51');
INSERT INTO `sys_logininfor` VALUES ('512', 'admin', '/127.0.0.1:55909', '', '', '', '1', '连接异常', '2022-06-01 11:47:51');
INSERT INTO `sys_logininfor` VALUES ('513', 'admin', '/127.0.0.1:56002', '', '', '', '0', '登录成功！', '2022-06-01 11:53:02');
INSERT INTO `sys_logininfor` VALUES ('514', 'admin', '/127.0.0.1:56002', '', '', '', '1', '连接异常', '2022-06-01 11:53:02');
INSERT INTO `sys_logininfor` VALUES ('515', 'admin', '/127.0.0.1:56002', '', '', '', '1', '退出登录', '2022-06-01 11:53:02');
INSERT INTO `sys_logininfor` VALUES ('516', 'admin', '/127.0.0.1:56002', '', '', '', '1', '连接异常', '2022-06-01 11:53:02');
INSERT INTO `sys_logininfor` VALUES ('517', 'admin', '/127.0.0.1:57625', '', '', '', '0', '登录成功！', '2022-06-01 13:10:58');
INSERT INTO `sys_logininfor` VALUES ('518', 'admin', '/127.0.0.1:57625', '', '', '', '1', '连接异常', '2022-06-01 13:10:58');
INSERT INTO `sys_logininfor` VALUES ('519', 'admin', '/127.0.0.1:57625', '', '', '', '1', '退出登录', '2022-06-01 13:10:58');
INSERT INTO `sys_logininfor` VALUES ('520', 'admin', '/127.0.0.1:57625', '', '', '', '1', '连接异常', '2022-06-01 13:10:58');
INSERT INTO `sys_logininfor` VALUES ('521', 'admin', '/127.0.0.1:57661', '', '', '', '0', '登录成功！', '2022-06-01 13:12:33');
INSERT INTO `sys_logininfor` VALUES ('522', 'admin', '/127.0.0.1:57661', '', '', '', '1', '连接异常', '2022-06-01 13:12:33');
INSERT INTO `sys_logininfor` VALUES ('523', 'admin', '/127.0.0.1:57661', '', '', '', '1', '退出登录', '2022-06-01 13:12:33');
INSERT INTO `sys_logininfor` VALUES ('524', 'admin', '/127.0.0.1:57661', '', '', '', '1', '连接异常', '2022-06-01 13:12:33');
INSERT INTO `sys_logininfor` VALUES ('525', 'admin', '/127.0.0.1:57811', '', '', '', '0', '登录成功！', '2022-06-01 13:18:17');
INSERT INTO `sys_logininfor` VALUES ('526', 'admin', '/127.0.0.1:57811', '', '', '', '1', '连接异常', '2022-06-01 13:18:17');
INSERT INTO `sys_logininfor` VALUES ('527', 'admin', '/127.0.0.1:57811', '', '', '', '1', '退出登录', '2022-06-01 13:18:17');
INSERT INTO `sys_logininfor` VALUES ('528', 'admin', '/127.0.0.1:57811', '', '', '', '1', '连接异常', '2022-06-01 13:18:17');
INSERT INTO `sys_logininfor` VALUES ('529', 'admin', '/127.0.0.1:57883', '', '', '', '0', '登录成功！', '2022-06-01 13:21:15');
INSERT INTO `sys_logininfor` VALUES ('530', 'admin', '/127.0.0.1:57883', '', '', '', '1', '连接异常', '2022-06-01 13:21:15');
INSERT INTO `sys_logininfor` VALUES ('531', 'admin', '/127.0.0.1:57883', '', '', '', '1', '退出登录', '2022-06-01 13:21:15');
INSERT INTO `sys_logininfor` VALUES ('532', 'admin', '/127.0.0.1:57883', '', '', '', '1', '连接异常', '2022-06-01 13:21:15');
INSERT INTO `sys_logininfor` VALUES ('533', 'admin', '/127.0.0.1:57903', '', '', '', '0', '登录成功！', '2022-06-01 13:22:09');
INSERT INTO `sys_logininfor` VALUES ('534', 'admin', '/127.0.0.1:57903', '', '', '', '1', '连接异常', '2022-06-01 13:22:09');
INSERT INTO `sys_logininfor` VALUES ('535', 'admin', '/127.0.0.1:57903', '', '', '', '1', '退出登录', '2022-06-01 13:22:09');
INSERT INTO `sys_logininfor` VALUES ('536', 'admin', '/127.0.0.1:57903', '', '', '', '1', '连接异常', '2022-06-01 13:22:09');
INSERT INTO `sys_logininfor` VALUES ('537', 'admin', '/127.0.0.1:58200', '', '', '', '0', '登录成功！', '2022-06-01 13:31:18');
INSERT INTO `sys_logininfor` VALUES ('538', 'admin', '/127.0.0.1:58200', '', '', '', '1', '退出登录', '2022-06-01 13:31:18');
INSERT INTO `sys_logininfor` VALUES ('539', 'admin', '/127.0.0.1:53962', '', '', '', '0', '登录成功！', '2022-06-01 17:12:01');
INSERT INTO `sys_logininfor` VALUES ('540', 'admin', '/127.0.0.1:53962', '', '', '', '1', '连接异常', '2022-06-01 17:12:01');
INSERT INTO `sys_logininfor` VALUES ('541', 'admin', '/127.0.0.1:53962', '', '', '', '1', '退出登录', '2022-06-01 17:12:01');
INSERT INTO `sys_logininfor` VALUES ('542', 'admin', '/127.0.0.1:53962', '', '', '', '1', '连接异常', '2022-06-01 17:12:01');
INSERT INTO `sys_logininfor` VALUES ('543', 'admin', '/127.0.0.1:54189', '', '', '', '0', '登录成功！', '2022-06-01 17:17:43');
INSERT INTO `sys_logininfor` VALUES ('544', 'admin', '/127.0.0.1:54189', '', '', '', '1', '连接异常', '2022-06-01 17:17:43');
INSERT INTO `sys_logininfor` VALUES ('545', 'admin', '/127.0.0.1:54189', '', '', '', '1', '退出登录', '2022-06-01 17:17:43');
INSERT INTO `sys_logininfor` VALUES ('546', 'admin', '/127.0.0.1:54189', '', '', '', '1', '连接异常', '2022-06-01 17:17:43');
INSERT INTO `sys_logininfor` VALUES ('547', 'admin', '/127.0.0.1:54282', '', '', '', '0', '登录成功！', '2022-06-01 17:19:40');
INSERT INTO `sys_logininfor` VALUES ('548', 'admin', '/127.0.0.1:54282', '', '', '', '1', '连接异常', '2022-06-01 17:19:40');
INSERT INTO `sys_logininfor` VALUES ('549', 'admin', '/127.0.0.1:54282', '', '', '', '1', '退出登录', '2022-06-01 17:19:40');
INSERT INTO `sys_logininfor` VALUES ('550', 'admin', '/127.0.0.1:54282', '', '', '', '1', '连接异常', '2022-06-01 17:19:40');
INSERT INTO `sys_logininfor` VALUES ('551', 'admin', '/127.0.0.1:54324', '', '', '', '0', '登录成功！', '2022-06-01 17:20:24');
INSERT INTO `sys_logininfor` VALUES ('552', 'admin', '/127.0.0.1:54324', '', '', '', '1', '连接异常', '2022-06-01 17:20:24');
INSERT INTO `sys_logininfor` VALUES ('553', 'admin', '/127.0.0.1:54324', '', '', '', '1', '退出登录', '2022-06-01 17:20:24');
INSERT INTO `sys_logininfor` VALUES ('554', 'admin', '/127.0.0.1:54324', '', '', '', '1', '连接异常', '2022-06-01 17:20:24');
INSERT INTO `sys_logininfor` VALUES ('555', 'admin', '/127.0.0.1:54694', '', '', '', '0', '登录成功！', '2022-06-01 17:27:28');
INSERT INTO `sys_logininfor` VALUES ('556', 'admin', '/127.0.0.1:54694', '', '', '', '1', '连接异常', '2022-06-01 17:27:28');
INSERT INTO `sys_logininfor` VALUES ('557', 'admin', '/127.0.0.1:54694', '', '', '', '1', '退出登录', '2022-06-01 17:27:28');
INSERT INTO `sys_logininfor` VALUES ('558', 'admin', '/127.0.0.1:54694', '', '', '', '1', '连接异常', '2022-06-01 17:27:28');
INSERT INTO `sys_logininfor` VALUES ('559', 'admin', '/127.0.0.1:54748', '', '', '', '0', '登录成功！', '2022-06-01 17:28:33');
INSERT INTO `sys_logininfor` VALUES ('560', 'admin', '/127.0.0.1:54748', '', '', '', '1', '连接异常', '2022-06-01 17:28:33');
INSERT INTO `sys_logininfor` VALUES ('561', 'admin', '/127.0.0.1:54748', '', '', '', '1', '退出登录', '2022-06-01 17:28:33');
INSERT INTO `sys_logininfor` VALUES ('562', 'admin', '/127.0.0.1:54748', '', '', '', '1', '连接异常', '2022-06-01 17:28:33');
INSERT INTO `sys_logininfor` VALUES ('563', 'admin', '/127.0.0.1:54796', '', '', '', '0', '登录成功！', '2022-06-01 17:29:37');
INSERT INTO `sys_logininfor` VALUES ('564', 'admin', '/127.0.0.1:53780', '', '', '', '0', '登录成功！', '2022-06-02 13:10:20');
INSERT INTO `sys_logininfor` VALUES ('565', 'admin', '/127.0.0.1:53780', '', '', '', '1', '连接异常', '2022-06-02 13:10:20');
INSERT INTO `sys_logininfor` VALUES ('566', 'admin', '/127.0.0.1:53780', '', '', '', '1', '退出登录', '2022-06-02 13:10:20');
INSERT INTO `sys_logininfor` VALUES ('567', 'admin', '/127.0.0.1:53780', '', '', '', '1', '连接异常', '2022-06-02 13:10:20');
INSERT INTO `sys_logininfor` VALUES ('568', 'admin', '/127.0.0.1:53847', '', '', '', '0', '登录成功！', '2022-06-02 13:12:13');
INSERT INTO `sys_logininfor` VALUES ('569', 'admin', '/127.0.0.1:53847', '', '', '', '1', '连接异常', '2022-06-02 13:12:13');
INSERT INTO `sys_logininfor` VALUES ('570', 'admin', '/127.0.0.1:53847', '', '', '', '1', '退出登录', '2022-06-02 13:12:13');
INSERT INTO `sys_logininfor` VALUES ('571', 'admin', '/127.0.0.1:53847', '', '', '', '1', '连接异常', '2022-06-02 13:12:13');
INSERT INTO `sys_logininfor` VALUES ('572', 'admin', '/127.0.0.1:53877', '', '', '', '0', '登录成功！', '2022-06-02 13:12:59');
INSERT INTO `sys_logininfor` VALUES ('573', 'admin', '/127.0.0.1:53877', '', '', '', '1', '连接异常', '2022-06-02 13:12:59');
INSERT INTO `sys_logininfor` VALUES ('574', 'admin', '/127.0.0.1:53877', '', '', '', '1', '退出登录', '2022-06-02 13:12:59');
INSERT INTO `sys_logininfor` VALUES ('575', 'admin', '/127.0.0.1:53877', '', '', '', '1', '连接异常', '2022-06-02 13:12:59');
INSERT INTO `sys_logininfor` VALUES ('576', 'admin', '/127.0.0.1:54049', '', '', '', '0', '登录成功！', '2022-06-02 13:18:22');
INSERT INTO `sys_logininfor` VALUES ('577', 'admin', '/127.0.0.1:54049', '', '', '', '1', '连接异常', '2022-06-02 13:18:22');
INSERT INTO `sys_logininfor` VALUES ('578', 'admin', '/127.0.0.1:54049', '', '', '', '1', '退出登录', '2022-06-02 13:18:22');
INSERT INTO `sys_logininfor` VALUES ('579', 'admin', '/127.0.0.1:54049', '', '', '', '1', '连接异常', '2022-06-02 13:18:22');
INSERT INTO `sys_logininfor` VALUES ('580', 'admin', '/127.0.0.1:54102', '', '', '', '0', '登录成功！', '2022-06-02 13:19:53');
INSERT INTO `sys_logininfor` VALUES ('581', 'admin', '/127.0.0.1:54102', '', '', '', '1', '连接异常', '2022-06-02 13:19:53');
INSERT INTO `sys_logininfor` VALUES ('582', 'admin', '/127.0.0.1:54102', '', '', '', '1', '退出登录', '2022-06-02 13:19:53');
INSERT INTO `sys_logininfor` VALUES ('583', 'admin', '/127.0.0.1:54102', '', '', '', '1', '连接异常', '2022-06-02 13:19:53');
INSERT INTO `sys_logininfor` VALUES ('584', 'admin', '/127.0.0.1:54520', '', '', '', '0', '登录成功！', '2022-06-02 13:28:00');
INSERT INTO `sys_logininfor` VALUES ('585', 'admin', '/127.0.0.1:54520', '', '', '', '1', '连接异常', '2022-06-02 13:28:00');
INSERT INTO `sys_logininfor` VALUES ('586', 'admin', '/127.0.0.1:54520', '', '', '', '1', '退出登录', '2022-06-02 13:28:00');
INSERT INTO `sys_logininfor` VALUES ('587', 'admin', '/127.0.0.1:54520', '', '', '', '1', '连接异常', '2022-06-02 13:28:00');
INSERT INTO `sys_logininfor` VALUES ('588', 'admin', '/127.0.0.1:54616', '', '', '', '0', '登录成功！', '2022-06-02 13:30:41');
INSERT INTO `sys_logininfor` VALUES ('589', 'admin', '/127.0.0.1:54616', '', '', '', '1', '退出登录', '2022-06-02 13:30:41');
INSERT INTO `sys_logininfor` VALUES ('590', 'admin', '/127.0.0.1:54817', '', '', '', '0', '登录成功！', '2022-06-02 13:36:21');
INSERT INTO `sys_logininfor` VALUES ('591', 'admin', '/127.0.0.1:54817', '', '', '', '1', '退出登录', '2022-06-02 13:36:21');
INSERT INTO `sys_logininfor` VALUES ('592', 'admin', '/127.0.0.1:54860', '', '', '', '0', '登录成功！', '2022-06-02 13:37:33');
INSERT INTO `sys_logininfor` VALUES ('593', 'admin', '/127.0.0.1:54860', '', '', '', '1', '退出登录', '2022-06-02 13:37:33');
INSERT INTO `sys_logininfor` VALUES ('594', 'admin', '/127.0.0.1:54900', '', '', '', '0', '登录成功！', '2022-06-02 13:38:35');
INSERT INTO `sys_logininfor` VALUES ('595', 'admin', '/127.0.0.1:54900', '', '', '', '1', '退出登录', '2022-06-02 13:38:35');
INSERT INTO `sys_logininfor` VALUES ('596', 'admin', '/127.0.0.1:55037', '', '', '', '0', '登录成功！', '2022-06-02 13:42:26');
INSERT INTO `sys_logininfor` VALUES ('597', 'admin', '/127.0.0.1:55037', '', '', '', '1', '退出登录', '2022-06-02 13:42:26');
INSERT INTO `sys_logininfor` VALUES ('598', 'admin', '/127.0.0.1:55414', '', '', '', '0', '登录成功！', '2022-06-02 13:53:38');
INSERT INTO `sys_logininfor` VALUES ('599', 'admin', '/127.0.0.1:55414', '', '', '', '1', '退出登录', '2022-06-02 13:53:38');
INSERT INTO `sys_logininfor` VALUES ('600', 'admin', '/127.0.0.1:55468', '', '', '', '0', '登录成功！', '2022-06-02 13:54:55');
INSERT INTO `sys_logininfor` VALUES ('601', 'admin', '/127.0.0.1:55468', '', '', '', '1', '连接异常', '2022-06-02 13:54:55');
INSERT INTO `sys_logininfor` VALUES ('602', 'admin', '/127.0.0.1:55468', '', '', '', '1', '退出登录', '2022-06-02 13:54:55');
INSERT INTO `sys_logininfor` VALUES ('603', 'admin', '/127.0.0.1:55468', '', '', '', '1', '连接异常', '2022-06-02 13:54:55');
INSERT INTO `sys_logininfor` VALUES ('604', 'admin', '/127.0.0.1:55496', '', '', '', '0', '登录成功！', '2022-06-02 13:55:29');
INSERT INTO `sys_logininfor` VALUES ('605', 'admin', '/127.0.0.1:55496', '', '', '', '1', '连接异常', '2022-06-02 13:55:29');
INSERT INTO `sys_logininfor` VALUES ('606', 'admin', '/127.0.0.1:55496', '', '', '', '1', '退出登录', '2022-06-02 13:55:29');
INSERT INTO `sys_logininfor` VALUES ('607', 'admin', '/127.0.0.1:55496', '', '', '', '1', '连接异常', '2022-06-02 13:55:29');
INSERT INTO `sys_logininfor` VALUES ('608', 'admin', '/127.0.0.1:55581', '', '', '', '0', '登录成功！', '2022-06-02 13:58:15');
INSERT INTO `sys_logininfor` VALUES ('609', 'admin', '/127.0.0.1:55581', '', '', '', '1', '连接异常', '2022-06-02 13:58:15');
INSERT INTO `sys_logininfor` VALUES ('610', 'admin', '/127.0.0.1:55581', '', '', '', '1', '退出登录', '2022-06-02 13:58:15');
INSERT INTO `sys_logininfor` VALUES ('611', 'admin', '/127.0.0.1:55581', '', '', '', '1', '连接异常', '2022-06-02 13:58:15');
INSERT INTO `sys_logininfor` VALUES ('612', 'admin', '/127.0.0.1:55622', '', '', '', '0', '登录成功！', '2022-06-02 13:59:14');
INSERT INTO `sys_logininfor` VALUES ('613', 'admin', '/127.0.0.1:55622', '', '', '', '1', '退出登录', '2022-06-02 13:59:14');
INSERT INTO `sys_logininfor` VALUES ('614', 'admin', '/127.0.0.1:55914', '', '', '', '0', '登录成功！', '2022-06-02 14:08:35');
INSERT INTO `sys_logininfor` VALUES ('615', 'admin', '/127.0.0.1:55914', '', '', '', '1', '连接异常', '2022-06-02 14:08:35');
INSERT INTO `sys_logininfor` VALUES ('616', 'admin', '/127.0.0.1:55914', '', '', '', '1', '退出登录', '2022-06-02 14:08:35');
INSERT INTO `sys_logininfor` VALUES ('617', 'admin', '/127.0.0.1:55914', '', '', '', '1', '连接异常', '2022-06-02 14:08:35');
INSERT INTO `sys_logininfor` VALUES ('618', 'admin', '/127.0.0.1:53780', '', '', '', '0', '登录成功！', '2022-06-02 15:00:50');
INSERT INTO `sys_logininfor` VALUES ('619', 'admin', '/127.0.0.1:53780', '', '', '', '1', '退出登录', '2022-06-02 15:00:50');
INSERT INTO `sys_logininfor` VALUES ('620', 'admin', '/127.0.0.1:53848', '', '', '', '0', '登录成功！', '2022-06-02 15:02:39');
INSERT INTO `sys_logininfor` VALUES ('621', 'admin', '/127.0.0.1:53848', '', '', '', '1', '连接异常', '2022-06-02 15:02:39');
INSERT INTO `sys_logininfor` VALUES ('622', 'admin', '/127.0.0.1:53848', '', '', '', '1', '退出登录', '2022-06-02 15:02:39');
INSERT INTO `sys_logininfor` VALUES ('623', 'admin', '/127.0.0.1:53848', '', '', '', '1', '连接异常', '2022-06-02 15:02:39');
INSERT INTO `sys_logininfor` VALUES ('624', 'admin', '/127.0.0.1:53913', '', '', '', '0', '登录成功！', '2022-06-02 15:04:45');
INSERT INTO `sys_logininfor` VALUES ('625', 'admin', '/127.0.0.1:53913', '', '', '', '1', '连接异常', '2022-06-02 15:04:45');
INSERT INTO `sys_logininfor` VALUES ('626', 'admin', '/127.0.0.1:53913', '', '', '', '1', '退出登录', '2022-06-02 15:04:45');
INSERT INTO `sys_logininfor` VALUES ('627', 'admin', '/127.0.0.1:53913', '', '', '', '1', '连接异常', '2022-06-02 15:04:45');
INSERT INTO `sys_logininfor` VALUES ('628', 'admin', '/127.0.0.1:54000', '', '', '', '0', '登录成功！', '2022-06-02 15:07:34');
INSERT INTO `sys_logininfor` VALUES ('629', 'admin', '/127.0.0.1:54000', '', '', '', '1', '连接异常', '2022-06-02 15:07:34');
INSERT INTO `sys_logininfor` VALUES ('630', 'admin', '/127.0.0.1:54000', '', '', '', '1', '退出登录', '2022-06-02 15:07:34');
INSERT INTO `sys_logininfor` VALUES ('631', 'admin', '/127.0.0.1:54000', '', '', '', '1', '连接异常', '2022-06-02 15:07:34');
INSERT INTO `sys_logininfor` VALUES ('632', 'admin', '/127.0.0.1:54057', '', '', '', '0', '登录成功！', '2022-06-02 15:09:09');
INSERT INTO `sys_logininfor` VALUES ('633', 'admin', '/127.0.0.1:54057', '', '', '', '1', '连接异常', '2022-06-02 15:09:09');
INSERT INTO `sys_logininfor` VALUES ('634', 'admin', '/127.0.0.1:54057', '', '', '', '1', '退出登录', '2022-06-02 15:09:09');
INSERT INTO `sys_logininfor` VALUES ('635', 'admin', '/127.0.0.1:54057', '', '', '', '1', '连接异常', '2022-06-02 15:09:09');
INSERT INTO `sys_logininfor` VALUES ('636', 'admin', '/127.0.0.1:54125', '', '', '', '0', '登录成功！', '2022-06-02 15:10:45');
INSERT INTO `sys_logininfor` VALUES ('637', 'admin', '/127.0.0.1:54125', '', '', '', '1', '连接异常', '2022-06-02 15:10:45');
INSERT INTO `sys_logininfor` VALUES ('638', 'admin', '/127.0.0.1:54125', '', '', '', '1', '退出登录', '2022-06-02 15:10:45');
INSERT INTO `sys_logininfor` VALUES ('639', 'admin', '/127.0.0.1:54125', '', '', '', '1', '连接异常', '2022-06-02 15:10:45');
INSERT INTO `sys_logininfor` VALUES ('640', 'admin', '/127.0.0.1:54160', '', '', '', '0', '登录成功！', '2022-06-02 15:11:46');
INSERT INTO `sys_logininfor` VALUES ('641', 'admin', '/127.0.0.1:54160', '', '', '', '1', '连接异常', '2022-06-02 15:11:46');
INSERT INTO `sys_logininfor` VALUES ('642', 'admin', '/127.0.0.1:54160', '', '', '', '1', '退出登录', '2022-06-02 15:11:46');
INSERT INTO `sys_logininfor` VALUES ('643', 'admin', '/127.0.0.1:54160', '', '', '', '1', '连接异常', '2022-06-02 15:11:46');
INSERT INTO `sys_logininfor` VALUES ('644', 'admin', '/127.0.0.1:54250', '', '', '', '0', '登录成功！', '2022-06-02 15:14:34');
INSERT INTO `sys_logininfor` VALUES ('645', 'admin', '/127.0.0.1:54250', '', '', '', '1', '连接异常', '2022-06-02 15:14:34');
INSERT INTO `sys_logininfor` VALUES ('646', 'admin', '/127.0.0.1:54250', '', '', '', '1', '退出登录', '2022-06-02 15:14:34');
INSERT INTO `sys_logininfor` VALUES ('647', 'admin', '/127.0.0.1:54250', '', '', '', '1', '连接异常', '2022-06-02 15:14:34');
INSERT INTO `sys_logininfor` VALUES ('648', 'admin', '/127.0.0.1:54282', '', '', '', '0', '登录成功！', '2022-06-02 15:15:22');
INSERT INTO `sys_logininfor` VALUES ('649', 'admin', '/127.0.0.1:54282', '', '', '', '1', '连接异常', '2022-06-02 15:15:22');
INSERT INTO `sys_logininfor` VALUES ('650', 'admin', '/127.0.0.1:54282', '', '', '', '1', '退出登录', '2022-06-02 15:15:22');
INSERT INTO `sys_logininfor` VALUES ('651', 'admin', '/127.0.0.1:54282', '', '', '', '1', '连接异常', '2022-06-02 15:15:22');
INSERT INTO `sys_logininfor` VALUES ('652', 'admin', '/127.0.0.1:54309', '', '', '', '0', '登录成功！', '2022-06-02 15:15:57');
INSERT INTO `sys_logininfor` VALUES ('653', 'admin', '/127.0.0.1:54309', '', '', '', '1', '连接异常', '2022-06-02 15:15:57');
INSERT INTO `sys_logininfor` VALUES ('654', 'admin', '/127.0.0.1:54309', '', '', '', '1', '退出登录', '2022-06-02 15:15:57');
INSERT INTO `sys_logininfor` VALUES ('655', 'admin', '/127.0.0.1:54309', '', '', '', '1', '连接异常', '2022-06-02 15:15:57');
INSERT INTO `sys_logininfor` VALUES ('656', 'admin', '/127.0.0.1:54365', '', '', '', '0', '登录成功！', '2022-06-02 15:17:43');
INSERT INTO `sys_logininfor` VALUES ('657', 'admin', '/127.0.0.1:54365', '', '', '', '1', '连接异常', '2022-06-02 15:17:43');
INSERT INTO `sys_logininfor` VALUES ('658', 'admin', '/127.0.0.1:54365', '', '', '', '1', '退出登录', '2022-06-02 15:17:43');
INSERT INTO `sys_logininfor` VALUES ('659', 'admin', '/127.0.0.1:54365', '', '', '', '1', '连接异常', '2022-06-02 15:17:43');
INSERT INTO `sys_logininfor` VALUES ('660', 'admin', '/127.0.0.1:54411', '', '', '', '0', '登录成功！', '2022-06-02 15:18:50');
INSERT INTO `sys_logininfor` VALUES ('661', 'admin', '/127.0.0.1:54411', '', '', '', '1', '连接异常', '2022-06-02 15:18:50');
INSERT INTO `sys_logininfor` VALUES ('662', 'admin', '/127.0.0.1:54411', '', '', '', '1', '退出登录', '2022-06-02 15:18:50');
INSERT INTO `sys_logininfor` VALUES ('663', 'admin', '/127.0.0.1:54411', '', '', '', '1', '连接异常', '2022-06-02 15:18:50');
INSERT INTO `sys_logininfor` VALUES ('664', 'admin', '/127.0.0.1:54449', '', '', '', '0', '登录成功！', '2022-06-02 15:19:56');
INSERT INTO `sys_logininfor` VALUES ('665', 'admin', '/127.0.0.1:54449', '', '', '', '1', '连接异常', '2022-06-02 15:19:56');
INSERT INTO `sys_logininfor` VALUES ('666', 'admin', '/127.0.0.1:54449', '', '', '', '1', '退出登录', '2022-06-02 15:19:56');
INSERT INTO `sys_logininfor` VALUES ('667', 'admin', '/127.0.0.1:54449', '', '', '', '1', '连接异常', '2022-06-02 15:19:56');
INSERT INTO `sys_logininfor` VALUES ('668', 'admin', '/127.0.0.1:54505', '', '', '', '0', '登录成功！', '2022-06-02 15:21:17');
INSERT INTO `sys_logininfor` VALUES ('669', 'admin', '/127.0.0.1:54505', '', '', '', '1', '连接异常', '2022-06-02 15:21:17');
INSERT INTO `sys_logininfor` VALUES ('670', 'admin', '/127.0.0.1:54505', '', '', '', '1', '退出登录', '2022-06-02 15:21:17');
INSERT INTO `sys_logininfor` VALUES ('671', 'admin', '/127.0.0.1:54505', '', '', '', '1', '连接异常', '2022-06-02 15:21:17');
INSERT INTO `sys_logininfor` VALUES ('672', 'admin', '/127.0.0.1:54566', '', '', '', '0', '登录成功！', '2022-06-02 15:23:09');
INSERT INTO `sys_logininfor` VALUES ('673', 'admin', '/127.0.0.1:54566', '', '', '', '1', '退出登录', '2022-06-02 15:23:09');
INSERT INTO `sys_logininfor` VALUES ('674', 'admin', '/127.0.0.1:54599', '', '', '', '0', '登录成功！', '2022-06-02 15:24:07');
INSERT INTO `sys_logininfor` VALUES ('675', 'admin', '/127.0.0.1:54599', '', '', '', '1', '连接异常', '2022-06-02 15:24:07');
INSERT INTO `sys_logininfor` VALUES ('676', 'admin', '/127.0.0.1:54599', '', '', '', '1', '退出登录', '2022-06-02 15:24:07');
INSERT INTO `sys_logininfor` VALUES ('677', 'admin', '/127.0.0.1:54599', '', '', '', '1', '连接异常', '2022-06-02 15:24:07');
INSERT INTO `sys_logininfor` VALUES ('678', 'admin', '/127.0.0.1:54631', '', '', '', '0', '登录成功！', '2022-06-02 15:24:42');
INSERT INTO `sys_logininfor` VALUES ('679', 'admin', '/127.0.0.1:54631', '', '', '', '1', '连接异常', '2022-06-02 15:24:42');
INSERT INTO `sys_logininfor` VALUES ('680', 'admin', '/127.0.0.1:54631', '', '', '', '1', '退出登录', '2022-06-02 15:24:42');
INSERT INTO `sys_logininfor` VALUES ('681', 'admin', '/127.0.0.1:54631', '', '', '', '1', '连接异常', '2022-06-02 15:24:42');
INSERT INTO `sys_logininfor` VALUES ('682', 'admin', '/127.0.0.1:54683', '', '', '', '0', '登录成功！', '2022-06-02 15:26:15');
INSERT INTO `sys_logininfor` VALUES ('683', 'admin', '/127.0.0.1:54683', '', '', '', '1', '连接异常', '2022-06-02 15:26:15');
INSERT INTO `sys_logininfor` VALUES ('684', 'admin', '/127.0.0.1:54683', '', '', '', '1', '退出登录', '2022-06-02 15:26:15');
INSERT INTO `sys_logininfor` VALUES ('685', 'admin', '/127.0.0.1:54683', '', '', '', '1', '连接异常', '2022-06-02 15:26:15');
INSERT INTO `sys_logininfor` VALUES ('686', 'admin', '/127.0.0.1:54737', '', '', '', '0', '登录成功！', '2022-06-02 15:27:43');
INSERT INTO `sys_logininfor` VALUES ('687', 'admin', '/127.0.0.1:54737', '', '', '', '1', '连接异常', '2022-06-02 15:27:43');
INSERT INTO `sys_logininfor` VALUES ('688', 'admin', '/127.0.0.1:54737', '', '', '', '1', '退出登录', '2022-06-02 15:27:43');
INSERT INTO `sys_logininfor` VALUES ('689', 'admin', '/127.0.0.1:54737', '', '', '', '1', '连接异常', '2022-06-02 15:27:43');
INSERT INTO `sys_logininfor` VALUES ('690', 'admin', '/127.0.0.1:54766', '', '', '', '0', '登录成功！', '2022-06-02 15:28:15');
INSERT INTO `sys_logininfor` VALUES ('691', 'admin', '/127.0.0.1:54766', '', '', '', '1', '连接异常', '2022-06-02 15:28:15');
INSERT INTO `sys_logininfor` VALUES ('692', 'admin', '/127.0.0.1:54766', '', '', '', '1', '退出登录', '2022-06-02 15:28:15');
INSERT INTO `sys_logininfor` VALUES ('693', 'admin', '/127.0.0.1:54766', '', '', '', '1', '连接异常', '2022-06-02 15:28:15');
INSERT INTO `sys_logininfor` VALUES ('694', 'admin', '/127.0.0.1:54795', '', '', '', '0', '登录成功！', '2022-06-02 15:29:02');
INSERT INTO `sys_logininfor` VALUES ('695', 'admin', '/127.0.0.1:54795', '', '', '', '1', '连接异常', '2022-06-02 15:29:02');
INSERT INTO `sys_logininfor` VALUES ('696', 'admin', '/127.0.0.1:54795', '', '', '', '1', '退出登录', '2022-06-02 15:29:02');
INSERT INTO `sys_logininfor` VALUES ('697', 'admin', '/127.0.0.1:54795', '', '', '', '1', '连接异常', '2022-06-02 15:29:02');
INSERT INTO `sys_logininfor` VALUES ('698', 'admin', '/127.0.0.1:54822', '', '', '', '0', '登录成功！', '2022-06-02 15:29:41');
INSERT INTO `sys_logininfor` VALUES ('699', 'admin', '/127.0.0.1:54822', '', '', '', '1', '连接异常', '2022-06-02 15:29:41');
INSERT INTO `sys_logininfor` VALUES ('700', 'admin', '/127.0.0.1:54822', '', '', '', '1', '退出登录', '2022-06-02 15:29:41');
INSERT INTO `sys_logininfor` VALUES ('701', 'admin', '/127.0.0.1:54822', '', '', '', '1', '连接异常', '2022-06-02 15:29:41');
INSERT INTO `sys_logininfor` VALUES ('702', 'admin', '/127.0.0.1:54872', '', '', '', '0', '登录成功！', '2022-06-02 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('703', 'admin', '/127.0.0.1:54872', '', '', '', '1', '连接异常', '2022-06-02 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('704', 'admin', '/127.0.0.1:54872', '', '', '', '1', '退出登录', '2022-06-02 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('705', 'admin', '/127.0.0.1:54872', '', '', '', '1', '连接异常', '2022-06-02 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('706', 'admin', '/127.0.0.1:54983', '', '', '', '0', '登录成功！', '2022-06-02 15:34:24');
INSERT INTO `sys_logininfor` VALUES ('707', 'admin', '/127.0.0.1:54983', '', '', '', '1', '连接异常', '2022-06-02 15:34:24');
INSERT INTO `sys_logininfor` VALUES ('708', 'admin', '/127.0.0.1:54983', '', '', '', '1', '退出登录', '2022-06-02 15:34:24');
INSERT INTO `sys_logininfor` VALUES ('709', 'admin', '/127.0.0.1:54983', '', '', '', '1', '连接异常', '2022-06-02 15:34:24');
INSERT INTO `sys_logininfor` VALUES ('710', 'admin', '/127.0.0.1:55061', '', '', '', '0', '登录成功！', '2022-06-02 15:36:21');
INSERT INTO `sys_logininfor` VALUES ('711', 'admin', '/127.0.0.1:55061', '', '', '', '1', '连接异常', '2022-06-02 15:36:21');
INSERT INTO `sys_logininfor` VALUES ('712', 'admin', '/127.0.0.1:55061', '', '', '', '1', '退出登录', '2022-06-02 15:36:21');
INSERT INTO `sys_logininfor` VALUES ('713', 'admin', '/127.0.0.1:55061', '', '', '', '1', '连接异常', '2022-06-02 15:36:21');
INSERT INTO `sys_logininfor` VALUES ('714', 'admin', '/127.0.0.1:55088', '', '', '', '0', '登录成功！', '2022-06-02 15:37:06');
INSERT INTO `sys_logininfor` VALUES ('715', 'admin', '/127.0.0.1:55088', '', '', '', '1', '连接异常', '2022-06-02 15:37:06');
INSERT INTO `sys_logininfor` VALUES ('716', 'admin', '/127.0.0.1:55088', '', '', '', '1', '退出登录', '2022-06-02 15:37:06');
INSERT INTO `sys_logininfor` VALUES ('717', 'admin', '/127.0.0.1:55088', '', '', '', '1', '连接异常', '2022-06-02 15:37:06');
INSERT INTO `sys_logininfor` VALUES ('718', 'admin', '/127.0.0.1:55119', '', '', '', '0', '登录成功！', '2022-06-02 15:37:57');
INSERT INTO `sys_logininfor` VALUES ('719', 'admin', '/127.0.0.1:55119', '', '', '', '1', '连接异常', '2022-06-02 15:37:57');
INSERT INTO `sys_logininfor` VALUES ('720', 'admin', '/127.0.0.1:55119', '', '', '', '1', '退出登录', '2022-06-02 15:37:57');
INSERT INTO `sys_logininfor` VALUES ('721', 'admin', '/127.0.0.1:55119', '', '', '', '1', '连接异常', '2022-06-02 15:37:57');
INSERT INTO `sys_logininfor` VALUES ('722', 'admin', '/127.0.0.1:55181', '', '', '', '0', '登录成功！', '2022-06-02 15:39:18');
INSERT INTO `sys_logininfor` VALUES ('723', 'admin', '/127.0.0.1:55181', '', '', '', '1', '连接异常', '2022-06-02 15:39:18');
INSERT INTO `sys_logininfor` VALUES ('724', 'admin', '/127.0.0.1:55181', '', '', '', '1', '退出登录', '2022-06-02 15:39:18');
INSERT INTO `sys_logininfor` VALUES ('725', 'admin', '/127.0.0.1:55181', '', '', '', '1', '连接异常', '2022-06-02 15:39:18');
INSERT INTO `sys_logininfor` VALUES ('726', 'admin', '/127.0.0.1:55198', '', '', '', '0', '登录成功！', '2022-06-02 15:39:37');
INSERT INTO `sys_logininfor` VALUES ('727', 'admin', '/127.0.0.1:55198', '', '', '', '1', '连接异常', '2022-06-02 15:39:37');
INSERT INTO `sys_logininfor` VALUES ('728', 'admin', '/127.0.0.1:55198', '', '', '', '1', '退出登录', '2022-06-02 15:39:37');
INSERT INTO `sys_logininfor` VALUES ('729', 'admin', '/127.0.0.1:55198', '', '', '', '1', '连接异常', '2022-06-02 15:39:37');
INSERT INTO `sys_logininfor` VALUES ('730', 'admin', '/127.0.0.1:55245', '', '', '', '0', '登录成功！', '2022-06-02 15:40:57');
INSERT INTO `sys_logininfor` VALUES ('731', 'admin', '/127.0.0.1:55245', '', '', '', '1', '连接异常', '2022-06-02 15:40:57');
INSERT INTO `sys_logininfor` VALUES ('732', 'admin', '/127.0.0.1:55245', '', '', '', '1', '退出登录', '2022-06-02 15:40:57');
INSERT INTO `sys_logininfor` VALUES ('733', 'admin', '/127.0.0.1:55245', '', '', '', '1', '连接异常', '2022-06-02 15:40:57');
INSERT INTO `sys_logininfor` VALUES ('734', 'admin', '/127.0.0.1:55311', '', '', '', '0', '登录成功！', '2022-06-02 15:42:54');
INSERT INTO `sys_logininfor` VALUES ('735', 'admin', '/127.0.0.1:55311', '', '', '', '1', '连接异常', '2022-06-02 15:42:54');
INSERT INTO `sys_logininfor` VALUES ('736', 'admin', '/127.0.0.1:55311', '', '', '', '1', '退出登录', '2022-06-02 15:42:54');
INSERT INTO `sys_logininfor` VALUES ('737', 'admin', '/127.0.0.1:55311', '', '', '', '1', '连接异常', '2022-06-02 15:42:54');
INSERT INTO `sys_logininfor` VALUES ('738', 'admin', '/127.0.0.1:55334', '', '', '', '0', '登录成功！', '2022-06-02 15:43:29');
INSERT INTO `sys_logininfor` VALUES ('739', 'admin', '/127.0.0.1:55334', '', '', '', '1', '连接异常', '2022-06-02 15:43:29');
INSERT INTO `sys_logininfor` VALUES ('740', 'admin', '/127.0.0.1:55334', '', '', '', '1', '退出登录', '2022-06-02 15:43:29');
INSERT INTO `sys_logininfor` VALUES ('741', 'admin', '/127.0.0.1:55334', '', '', '', '1', '连接异常', '2022-06-02 15:43:29');
INSERT INTO `sys_logininfor` VALUES ('742', 'admin', '/127.0.0.1:55380', '', '', '', '0', '登录成功！', '2022-06-02 15:44:37');
INSERT INTO `sys_logininfor` VALUES ('743', 'admin', '/127.0.0.1:55380', '', '', '', '1', '连接异常', '2022-06-02 15:44:37');
INSERT INTO `sys_logininfor` VALUES ('744', 'admin', '/127.0.0.1:55380', '', '', '', '1', '退出登录', '2022-06-02 15:44:37');
INSERT INTO `sys_logininfor` VALUES ('745', 'admin', '/127.0.0.1:55380', '', '', '', '1', '连接异常', '2022-06-02 15:44:37');
INSERT INTO `sys_logininfor` VALUES ('746', 'admin', '/127.0.0.1:55565', '', '', '', '0', '登录成功！', '2022-06-02 15:49:34');
INSERT INTO `sys_logininfor` VALUES ('747', 'admin', '/127.0.0.1:55565', '', '', '', '1', '连接异常', '2022-06-02 15:49:34');
INSERT INTO `sys_logininfor` VALUES ('748', 'admin', '/127.0.0.1:55565', '', '', '', '1', '退出登录', '2022-06-02 15:49:34');
INSERT INTO `sys_logininfor` VALUES ('749', 'admin', '/127.0.0.1:55565', '', '', '', '1', '连接异常', '2022-06-02 15:49:34');
INSERT INTO `sys_logininfor` VALUES ('750', 'admin', '/127.0.0.1:55613', '', '', '', '0', '登录成功！', '2022-06-02 15:51:01');
INSERT INTO `sys_logininfor` VALUES ('751', 'admin', '/127.0.0.1:55613', '', '', '', '1', '连接异常', '2022-06-02 15:51:01');
INSERT INTO `sys_logininfor` VALUES ('752', 'admin', '/127.0.0.1:55613', '', '', '', '1', '退出登录', '2022-06-02 15:51:01');
INSERT INTO `sys_logininfor` VALUES ('753', 'admin', '/127.0.0.1:55613', '', '', '', '1', '连接异常', '2022-06-02 15:51:01');
INSERT INTO `sys_logininfor` VALUES ('754', 'admin', '/127.0.0.1:55722', '', '', '', '0', '登录成功！', '2022-06-02 15:53:57');
INSERT INTO `sys_logininfor` VALUES ('755', 'admin', '/127.0.0.1:55722', '', '', '', '1', '连接异常', '2022-06-02 15:53:57');
INSERT INTO `sys_logininfor` VALUES ('756', 'admin', '/127.0.0.1:55722', '', '', '', '1', '退出登录', '2022-06-02 15:53:57');
INSERT INTO `sys_logininfor` VALUES ('757', 'admin', '/127.0.0.1:55722', '', '', '', '1', '连接异常', '2022-06-02 15:53:57');
INSERT INTO `sys_logininfor` VALUES ('758', 'admin', '/127.0.0.1:55757', '', '', '', '0', '登录成功！', '2022-06-02 15:54:45');
INSERT INTO `sys_logininfor` VALUES ('759', 'admin', '/127.0.0.1:55757', '', '', '', '1', '连接异常', '2022-06-02 15:54:45');
INSERT INTO `sys_logininfor` VALUES ('760', 'admin', '/127.0.0.1:55757', '', '', '', '1', '退出登录', '2022-06-02 15:54:45');
INSERT INTO `sys_logininfor` VALUES ('761', 'admin', '/127.0.0.1:55757', '', '', '', '1', '连接异常', '2022-06-02 15:54:45');
INSERT INTO `sys_logininfor` VALUES ('762', 'admin', '/127.0.0.1:55805', '', '', '', '0', '登录成功！', '2022-06-02 15:56:17');
INSERT INTO `sys_logininfor` VALUES ('763', 'admin', '/127.0.0.1:55805', '', '', '', '1', '连接异常', '2022-06-02 15:56:17');
INSERT INTO `sys_logininfor` VALUES ('764', 'admin', '/127.0.0.1:55805', '', '', '', '1', '退出登录', '2022-06-02 15:56:17');
INSERT INTO `sys_logininfor` VALUES ('765', 'admin', '/127.0.0.1:55805', '', '', '', '1', '连接异常', '2022-06-02 15:56:17');
INSERT INTO `sys_logininfor` VALUES ('766', 'admin', '/127.0.0.1:55838', '', '', '', '0', '登录成功！', '2022-06-02 15:57:14');
INSERT INTO `sys_logininfor` VALUES ('767', 'admin', '/127.0.0.1:55838', '', '', '', '1', '连接异常', '2022-06-02 15:57:14');
INSERT INTO `sys_logininfor` VALUES ('768', 'admin', '/127.0.0.1:55838', '', '', '', '1', '退出登录', '2022-06-02 15:57:14');
INSERT INTO `sys_logininfor` VALUES ('769', 'admin', '/127.0.0.1:55838', '', '', '', '1', '连接异常', '2022-06-02 15:57:14');
INSERT INTO `sys_logininfor` VALUES ('770', 'admin', '/127.0.0.1:55895', '', '', '', '0', '登录成功！', '2022-06-02 15:58:55');
INSERT INTO `sys_logininfor` VALUES ('771', 'admin', '/127.0.0.1:55895', '', '', '', '1', '连接异常', '2022-06-02 15:58:55');
INSERT INTO `sys_logininfor` VALUES ('772', 'admin', '/127.0.0.1:55895', '', '', '', '1', '退出登录', '2022-06-02 15:58:55');
INSERT INTO `sys_logininfor` VALUES ('773', 'admin', '/127.0.0.1:55895', '', '', '', '1', '连接异常', '2022-06-02 15:58:55');
INSERT INTO `sys_logininfor` VALUES ('774', 'admin', '/127.0.0.1:55934', '', '', '', '0', '登录成功！', '2022-06-02 16:00:01');
INSERT INTO `sys_logininfor` VALUES ('775', 'admin', '/127.0.0.1:55934', '', '', '', '1', '连接异常', '2022-06-02 16:00:01');
INSERT INTO `sys_logininfor` VALUES ('776', 'admin', '/127.0.0.1:55934', '', '', '', '1', '退出登录', '2022-06-02 16:00:01');
INSERT INTO `sys_logininfor` VALUES ('777', 'admin', '/127.0.0.1:55934', '', '', '', '1', '连接异常', '2022-06-02 16:00:01');
INSERT INTO `sys_logininfor` VALUES ('778', 'admin', '/127.0.0.1:56189', '', '', '', '0', '登录成功！', '2022-06-02 16:05:31');
INSERT INTO `sys_logininfor` VALUES ('779', 'admin', '/127.0.0.1:56189', '', '', '', '1', '退出登录', '2022-06-02 16:05:31');
INSERT INTO `sys_logininfor` VALUES ('780', 'admin', '/127.0.0.1:56211', '', '', '', '0', '登录成功！', '2022-06-02 16:06:04');
INSERT INTO `sys_logininfor` VALUES ('781', 'admin', '/127.0.0.1:56211', '', '', '', '1', '连接异常', '2022-06-02 16:06:04');
INSERT INTO `sys_logininfor` VALUES ('782', 'admin', '/127.0.0.1:56211', '', '', '', '1', '退出登录', '2022-06-02 16:06:04');
INSERT INTO `sys_logininfor` VALUES ('783', 'admin', '/127.0.0.1:56211', '', '', '', '1', '连接异常', '2022-06-02 16:06:04');
INSERT INTO `sys_logininfor` VALUES ('784', 'admin', '/127.0.0.1:56757', '', '', '', '0', '登录成功！', '2022-06-02 16:24:09');
INSERT INTO `sys_logininfor` VALUES ('785', 'admin', '/127.0.0.1:56757', '', '', '', '1', '连接异常', '2022-06-02 16:24:09');
INSERT INTO `sys_logininfor` VALUES ('786', 'admin', '/127.0.0.1:56757', '', '', '', '1', '退出登录', '2022-06-02 16:24:09');
INSERT INTO `sys_logininfor` VALUES ('787', 'admin', '/127.0.0.1:56757', '', '', '', '1', '连接异常', '2022-06-02 16:24:09');
INSERT INTO `sys_logininfor` VALUES ('788', 'admin', '/127.0.0.1:56794', '', '', '', '0', '登录成功！', '2022-06-02 16:24:55');
INSERT INTO `sys_logininfor` VALUES ('789', 'admin', '/127.0.0.1:56794', '', '', '', '1', '连接异常', '2022-06-02 16:24:55');
INSERT INTO `sys_logininfor` VALUES ('790', 'admin', '/127.0.0.1:56794', '', '', '', '1', '退出登录', '2022-06-02 16:24:55');
INSERT INTO `sys_logininfor` VALUES ('791', 'admin', '/127.0.0.1:56794', '', '', '', '1', '连接异常', '2022-06-02 16:24:55');
INSERT INTO `sys_logininfor` VALUES ('792', 'admin', '/127.0.0.1:57049', '', '', '', '0', '登录成功！', '2022-06-02 16:30:54');
INSERT INTO `sys_logininfor` VALUES ('793', 'admin', '/127.0.0.1:57049', '', '', '', '1', '连接异常', '2022-06-02 16:30:54');
INSERT INTO `sys_logininfor` VALUES ('794', 'admin', '/127.0.0.1:57049', '', '', '', '1', '退出登录', '2022-06-02 16:30:54');
INSERT INTO `sys_logininfor` VALUES ('795', 'admin', '/127.0.0.1:57049', '', '', '', '1', '连接异常', '2022-06-02 16:30:54');
INSERT INTO `sys_logininfor` VALUES ('796', 'admin', '/127.0.0.1:57079', '', '', '', '0', '登录成功！', '2022-06-02 16:31:33');
INSERT INTO `sys_logininfor` VALUES ('797', 'admin', '/127.0.0.1:57079', '', '', '', '1', '退出登录', '2022-06-02 16:31:33');
INSERT INTO `sys_logininfor` VALUES ('798', 'admin', '/127.0.0.1:57079', '', '', '', '1', '连接异常', '2022-06-02 16:31:33');
INSERT INTO `sys_logininfor` VALUES ('799', 'admin', '/127.0.0.1:57173', '', '', '', '0', '登录成功！', '2022-06-02 16:34:15');
INSERT INTO `sys_logininfor` VALUES ('800', 'admin', '/127.0.0.1:57173', '', '', '', '1', '连接异常', '2022-06-02 16:34:15');
INSERT INTO `sys_logininfor` VALUES ('801', 'admin', '/127.0.0.1:57173', '', '', '', '1', '退出登录', '2022-06-02 16:34:15');
INSERT INTO `sys_logininfor` VALUES ('802', 'admin', '/127.0.0.1:57173', '', '', '', '1', '连接异常', '2022-06-02 16:34:15');
INSERT INTO `sys_logininfor` VALUES ('803', 'admin', '/127.0.0.1:57279', '', '', '', '0', '登录成功！', '2022-06-02 16:37:53');
INSERT INTO `sys_logininfor` VALUES ('804', 'admin', '/127.0.0.1:57279', '', '', '', '1', '连接异常', '2022-06-02 16:37:53');
INSERT INTO `sys_logininfor` VALUES ('805', 'admin', '/127.0.0.1:57279', '', '', '', '1', '退出登录', '2022-06-02 16:37:53');
INSERT INTO `sys_logininfor` VALUES ('806', 'admin', '/127.0.0.1:57279', '', '', '', '1', '连接异常', '2022-06-02 16:37:53');
INSERT INTO `sys_logininfor` VALUES ('807', 'admin', '/127.0.0.1:57420', '', '', '', '0', '登录成功！', '2022-06-02 16:41:48');
INSERT INTO `sys_logininfor` VALUES ('808', 'admin', '/127.0.0.1:57420', '', '', '', '1', '连接异常', '2022-06-02 16:41:48');
INSERT INTO `sys_logininfor` VALUES ('809', 'admin', '/127.0.0.1:57420', '', '', '', '1', '退出登录', '2022-06-02 16:41:48');
INSERT INTO `sys_logininfor` VALUES ('810', 'admin', '/127.0.0.1:57420', '', '', '', '1', '连接异常', '2022-06-02 16:41:48');
INSERT INTO `sys_logininfor` VALUES ('811', 'admin', '/127.0.0.1:57574', '', '', '', '0', '登录成功！', '2022-06-02 16:44:04');
INSERT INTO `sys_logininfor` VALUES ('812', 'admin', '/127.0.0.1:57574', '', '', '', '1', '连接异常', '2022-06-02 16:44:04');
INSERT INTO `sys_logininfor` VALUES ('813', 'admin', '/127.0.0.1:57574', '', '', '', '1', '退出登录', '2022-06-02 16:44:04');
INSERT INTO `sys_logininfor` VALUES ('814', 'admin', '/127.0.0.1:57574', '', '', '', '1', '连接异常', '2022-06-02 16:44:04');
INSERT INTO `sys_logininfor` VALUES ('815', 'admin', '/127.0.0.1:57662', '', '', '', '0', '登录成功！', '2022-06-02 16:47:00');
INSERT INTO `sys_logininfor` VALUES ('816', 'admin', '/127.0.0.1:57662', '', '', '', '1', '连接异常', '2022-06-02 16:47:00');
INSERT INTO `sys_logininfor` VALUES ('817', 'admin', '/127.0.0.1:57662', '', '', '', '1', '退出登录', '2022-06-02 16:47:00');
INSERT INTO `sys_logininfor` VALUES ('818', 'admin', '/127.0.0.1:57662', '', '', '', '1', '连接异常', '2022-06-02 16:47:00');
INSERT INTO `sys_logininfor` VALUES ('819', 'admin', '/127.0.0.1:57695', '', '', '', '0', '登录成功！', '2022-06-02 16:47:46');
INSERT INTO `sys_logininfor` VALUES ('820', 'admin', '/127.0.0.1:57695', '', '', '', '1', '连接异常', '2022-06-02 16:47:46');
INSERT INTO `sys_logininfor` VALUES ('821', 'admin', '/127.0.0.1:57695', '', '', '', '1', '退出登录', '2022-06-02 16:47:46');
INSERT INTO `sys_logininfor` VALUES ('822', 'admin', '/127.0.0.1:57695', '', '', '', '1', '连接异常', '2022-06-02 16:47:46');
INSERT INTO `sys_logininfor` VALUES ('823', 'admin', '/127.0.0.1:57727', '', '', '', '0', '登录成功！', '2022-06-02 16:48:34');
INSERT INTO `sys_logininfor` VALUES ('824', 'admin', '/127.0.0.1:57727', '', '', '', '1', '连接异常', '2022-06-02 16:48:34');
INSERT INTO `sys_logininfor` VALUES ('825', 'admin', '/127.0.0.1:57727', '', '', '', '1', '退出登录', '2022-06-02 16:48:34');
INSERT INTO `sys_logininfor` VALUES ('826', 'admin', '/127.0.0.1:57727', '', '', '', '1', '连接异常', '2022-06-02 16:48:34');
INSERT INTO `sys_logininfor` VALUES ('827', 'admin', '/127.0.0.1:57768', '', '', '', '0', '登录成功！', '2022-06-02 16:49:40');
INSERT INTO `sys_logininfor` VALUES ('828', 'admin', '/127.0.0.1:57768', '', '', '', '1', '连接异常', '2022-06-02 16:49:40');
INSERT INTO `sys_logininfor` VALUES ('829', 'admin', '/127.0.0.1:57768', '', '', '', '1', '退出登录', '2022-06-02 16:49:40');
INSERT INTO `sys_logininfor` VALUES ('830', 'admin', '/127.0.0.1:57768', '', '', '', '1', '连接异常', '2022-06-02 16:49:40');
INSERT INTO `sys_logininfor` VALUES ('831', 'admin', '/127.0.0.1:57818', '', '', '', '0', '登录成功！', '2022-06-02 16:50:50');
INSERT INTO `sys_logininfor` VALUES ('832', 'admin', '/127.0.0.1:57818', '', '', '', '1', '连接异常', '2022-06-02 16:50:50');
INSERT INTO `sys_logininfor` VALUES ('833', 'admin', '/127.0.0.1:57818', '', '', '', '1', '退出登录', '2022-06-02 16:50:50');
INSERT INTO `sys_logininfor` VALUES ('834', 'admin', '/127.0.0.1:57818', '', '', '', '1', '连接异常', '2022-06-02 16:50:50');
INSERT INTO `sys_logininfor` VALUES ('835', 'admin', '/127.0.0.1:57851', '', '', '', '0', '登录成功！', '2022-06-02 16:51:37');
INSERT INTO `sys_logininfor` VALUES ('836', 'admin', '/127.0.0.1:57851', '', '', '', '1', '连接异常', '2022-06-02 16:51:37');
INSERT INTO `sys_logininfor` VALUES ('837', 'admin', '/127.0.0.1:57851', '', '', '', '1', '退出登录', '2022-06-02 16:51:37');
INSERT INTO `sys_logininfor` VALUES ('838', 'admin', '/127.0.0.1:57851', '', '', '', '1', '连接异常', '2022-06-02 16:51:37');
INSERT INTO `sys_logininfor` VALUES ('839', 'admin', '/127.0.0.1:57964', '', '', '', '0', '登录成功！', '2022-06-02 16:54:47');
INSERT INTO `sys_logininfor` VALUES ('840', 'admin', '/127.0.0.1:57964', '', '', '', '1', '连接异常', '2022-06-02 16:54:47');
INSERT INTO `sys_logininfor` VALUES ('841', 'admin', '/127.0.0.1:57964', '', '', '', '1', '退出登录', '2022-06-02 16:54:47');
INSERT INTO `sys_logininfor` VALUES ('842', 'admin', '/127.0.0.1:57964', '', '', '', '1', '连接异常', '2022-06-02 16:54:47');
INSERT INTO `sys_logininfor` VALUES ('843', 'admin', '/127.0.0.1:58573', '', '', '', '0', '登录成功！', '2022-06-02 17:12:41');
INSERT INTO `sys_logininfor` VALUES ('844', 'admin', '/127.0.0.1:58573', '', '', '', '1', '连接异常', '2022-06-02 17:12:41');
INSERT INTO `sys_logininfor` VALUES ('845', 'admin', '/127.0.0.1:58573', '', '', '', '1', '退出登录', '2022-06-02 17:12:41');
INSERT INTO `sys_logininfor` VALUES ('846', 'admin', '/127.0.0.1:58573', '', '', '', '1', '连接异常', '2022-06-02 17:12:41');
INSERT INTO `sys_logininfor` VALUES ('847', 'admin', '/127.0.0.1:58839', '', '', '', '0', '登录成功！', '2022-06-02 17:20:20');
INSERT INTO `sys_logininfor` VALUES ('848', 'admin', '/127.0.0.1:58839', '', '', '', '1', '连接异常', '2022-06-02 17:20:20');
INSERT INTO `sys_logininfor` VALUES ('849', 'admin', '/127.0.0.1:58839', '', '', '', '1', '退出登录', '2022-06-02 17:20:20');
INSERT INTO `sys_logininfor` VALUES ('850', 'admin', '/127.0.0.1:58839', '', '', '', '1', '连接异常', '2022-06-02 17:20:20');
INSERT INTO `sys_logininfor` VALUES ('851', 'admin', '/127.0.0.1:58900', '', '', '', '0', '登录成功！', '2022-06-02 17:22:13');
INSERT INTO `sys_logininfor` VALUES ('852', 'admin', '/127.0.0.1:58900', '', '', '', '1', '连接异常', '2022-06-02 17:22:13');
INSERT INTO `sys_logininfor` VALUES ('853', 'admin', '/127.0.0.1:58900', '', '', '', '1', '退出登录', '2022-06-02 17:22:13');
INSERT INTO `sys_logininfor` VALUES ('854', 'admin', '/127.0.0.1:58900', '', '', '', '1', '连接异常', '2022-06-02 17:22:13');
INSERT INTO `sys_logininfor` VALUES ('855', 'admin', '/127.0.0.1:59039', '', '', '', '0', '登录成功！', '2022-06-02 17:25:54');
INSERT INTO `sys_logininfor` VALUES ('856', 'admin', '/127.0.0.1:59039', '', '', '', '1', '连接异常', '2022-06-02 17:25:54');
INSERT INTO `sys_logininfor` VALUES ('857', 'admin', '/127.0.0.1:59039', '', '', '', '1', '退出登录', '2022-06-02 17:25:54');
INSERT INTO `sys_logininfor` VALUES ('858', 'admin', '/127.0.0.1:59039', '', '', '', '1', '连接异常', '2022-06-02 17:25:54');
INSERT INTO `sys_logininfor` VALUES ('859', 'admin', '/127.0.0.1:59082', '', '', '', '0', '登录成功！', '2022-06-02 17:26:48');
INSERT INTO `sys_logininfor` VALUES ('860', 'admin', '/127.0.0.1:59082', '', '', '', '1', '连接异常', '2022-06-02 17:26:48');
INSERT INTO `sys_logininfor` VALUES ('861', 'admin', '/127.0.0.1:59082', '', '', '', '1', '退出登录', '2022-06-02 17:26:48');
INSERT INTO `sys_logininfor` VALUES ('862', 'admin', '/127.0.0.1:59082', '', '', '', '1', '连接异常', '2022-06-02 17:26:48');
INSERT INTO `sys_logininfor` VALUES ('863', 'admin', '/127.0.0.1:59209', '', '', '', '0', '登录成功！', '2022-06-02 17:30:19');
INSERT INTO `sys_logininfor` VALUES ('864', 'admin', '/127.0.0.1:59209', '', '', '', '1', '退出登录', '2022-06-02 17:30:19');
INSERT INTO `sys_logininfor` VALUES ('865', 'admin', '/127.0.0.1:54057', '', '', '', '0', '登录成功！', '2022-06-06 13:54:25');
INSERT INTO `sys_logininfor` VALUES ('866', 'admin', '/127.0.0.1:54057', '', '', '', '1', '连接异常', '2022-06-06 13:54:25');
INSERT INTO `sys_logininfor` VALUES ('867', 'admin', '/127.0.0.1:54057', '', '', '', '1', '退出登录', '2022-06-06 13:54:25');
INSERT INTO `sys_logininfor` VALUES ('868', 'admin', '/127.0.0.1:54057', '', '', '', '1', '连接异常', '2022-06-06 13:54:25');
INSERT INTO `sys_logininfor` VALUES ('869', 'admin', '/127.0.0.1:54563', '', '', '', '0', '登录成功！', '2022-06-06 14:02:53');
INSERT INTO `sys_logininfor` VALUES ('870', 'admin', '/127.0.0.1:54563', '', '', '', '1', '连接异常', '2022-06-06 14:02:53');
INSERT INTO `sys_logininfor` VALUES ('871', 'admin', '/127.0.0.1:54563', '', '', '', '1', '退出登录', '2022-06-06 14:02:53');
INSERT INTO `sys_logininfor` VALUES ('872', 'admin', '/127.0.0.1:54563', '', '', '', '1', '连接异常', '2022-06-06 14:02:53');
INSERT INTO `sys_logininfor` VALUES ('873', 'admin', '/127.0.0.1:54604', '', '', '', '0', '登录成功！', '2022-06-06 14:03:32');
INSERT INTO `sys_logininfor` VALUES ('874', 'admin', '/127.0.0.1:54604', '', '', '', '1', '连接异常', '2022-06-06 14:03:32');
INSERT INTO `sys_logininfor` VALUES ('875', 'admin', '/127.0.0.1:54604', '', '', '', '1', '退出登录', '2022-06-06 14:03:32');
INSERT INTO `sys_logininfor` VALUES ('876', 'admin', '/127.0.0.1:54604', '', '', '', '1', '连接异常', '2022-06-06 14:03:32');
INSERT INTO `sys_logininfor` VALUES ('877', 'admin', '/127.0.0.1:54662', '', '', '', '0', '登录成功！', '2022-06-06 14:04:26');
INSERT INTO `sys_logininfor` VALUES ('878', 'admin', '/127.0.0.1:54662', '', '', '', '1', '连接异常', '2022-06-06 14:04:26');
INSERT INTO `sys_logininfor` VALUES ('879', 'admin', '/127.0.0.1:54662', '', '', '', '1', '退出登录', '2022-06-06 14:04:26');
INSERT INTO `sys_logininfor` VALUES ('880', 'admin', '/127.0.0.1:54662', '', '', '', '1', '连接异常', '2022-06-06 14:04:26');
INSERT INTO `sys_logininfor` VALUES ('881', 'admin', '/127.0.0.1:54746', '', '', '', '0', '登录成功！', '2022-06-06 14:05:48');
INSERT INTO `sys_logininfor` VALUES ('882', 'admin', '/127.0.0.1:54746', '', '', '', '1', '连接异常', '2022-06-06 14:05:48');
INSERT INTO `sys_logininfor` VALUES ('883', 'admin', '/127.0.0.1:54746', '', '', '', '1', '退出登录', '2022-06-06 14:05:48');
INSERT INTO `sys_logininfor` VALUES ('884', 'admin', '/127.0.0.1:54746', '', '', '', '1', '连接异常', '2022-06-06 14:05:48');
INSERT INTO `sys_logininfor` VALUES ('885', 'admin', '/127.0.0.1:54991', '', '', '', '0', '登录成功！', '2022-06-06 14:10:06');
INSERT INTO `sys_logininfor` VALUES ('886', 'admin', '/127.0.0.1:54991', '', '', '', '1', '连接异常', '2022-06-06 14:10:06');
INSERT INTO `sys_logininfor` VALUES ('887', 'admin', '/127.0.0.1:54991', '', '', '', '1', '退出登录', '2022-06-06 14:10:06');
INSERT INTO `sys_logininfor` VALUES ('888', 'admin', '/127.0.0.1:54991', '', '', '', '1', '连接异常', '2022-06-06 14:10:06');
INSERT INTO `sys_logininfor` VALUES ('889', 'admin', '/127.0.0.1:55041', '', '', '', '0', '登录成功！', '2022-06-06 14:10:59');
INSERT INTO `sys_logininfor` VALUES ('890', 'admin', '/127.0.0.1:55041', '', '', '', '1', '连接异常', '2022-06-06 14:10:59');
INSERT INTO `sys_logininfor` VALUES ('891', 'admin', '/127.0.0.1:55041', '', '', '', '1', '退出登录', '2022-06-06 14:10:59');
INSERT INTO `sys_logininfor` VALUES ('892', 'admin', '/127.0.0.1:55041', '', '', '', '1', '连接异常', '2022-06-06 14:10:59');
INSERT INTO `sys_logininfor` VALUES ('893', 'admin', '/127.0.0.1:55091', '', '', '', '0', '登录成功！', '2022-06-06 14:11:45');
INSERT INTO `sys_logininfor` VALUES ('894', 'admin', '/127.0.0.1:55091', '', '', '', '1', '连接异常', '2022-06-06 14:11:45');
INSERT INTO `sys_logininfor` VALUES ('895', 'admin', '/127.0.0.1:55091', '', '', '', '1', '退出登录', '2022-06-06 14:11:45');
INSERT INTO `sys_logininfor` VALUES ('896', 'admin', '/127.0.0.1:55091', '', '', '', '1', '连接异常', '2022-06-06 14:11:45');
INSERT INTO `sys_logininfor` VALUES ('897', 'admin', '/127.0.0.1:55154', '', '', '', '0', '登录成功！', '2022-06-06 14:12:39');
INSERT INTO `sys_logininfor` VALUES ('898', 'admin', '/127.0.0.1:55154', '', '', '', '1', '连接异常', '2022-06-06 14:12:39');
INSERT INTO `sys_logininfor` VALUES ('899', 'admin', '/127.0.0.1:55154', '', '', '', '1', '退出登录', '2022-06-06 14:12:39');
INSERT INTO `sys_logininfor` VALUES ('900', 'admin', '/127.0.0.1:55154', '', '', '', '1', '连接异常', '2022-06-06 14:12:39');
INSERT INTO `sys_logininfor` VALUES ('901', 'admin', '/127.0.0.1:55235', '', '', '', '0', '登录成功！', '2022-06-06 14:13:55');
INSERT INTO `sys_logininfor` VALUES ('902', 'admin', '/127.0.0.1:55235', '', '', '', '1', '连接异常', '2022-06-06 14:13:55');
INSERT INTO `sys_logininfor` VALUES ('903', 'admin', '/127.0.0.1:55235', '', '', '', '1', '退出登录', '2022-06-06 14:13:55');
INSERT INTO `sys_logininfor` VALUES ('904', 'admin', '/127.0.0.1:55235', '', '', '', '1', '连接异常', '2022-06-06 14:13:55');
INSERT INTO `sys_logininfor` VALUES ('905', 'admin', '/127.0.0.1:55338', '', '', '', '0', '登录成功！', '2022-06-06 14:15:46');
INSERT INTO `sys_logininfor` VALUES ('906', 'admin', '/127.0.0.1:55338', '', '', '', '1', '连接异常', '2022-06-06 14:15:46');
INSERT INTO `sys_logininfor` VALUES ('907', 'admin', '/127.0.0.1:55338', '', '', '', '1', '退出登录', '2022-06-06 14:15:46');
INSERT INTO `sys_logininfor` VALUES ('908', 'admin', '/127.0.0.1:55338', '', '', '', '1', '连接异常', '2022-06-06 14:15:46');
INSERT INTO `sys_logininfor` VALUES ('909', 'admin', '/127.0.0.1:55439', '', '', '', '0', '登录成功！', '2022-06-06 14:17:17');
INSERT INTO `sys_logininfor` VALUES ('910', 'admin', '/127.0.0.1:55439', '', '', '', '1', '连接异常', '2022-06-06 14:17:17');
INSERT INTO `sys_logininfor` VALUES ('911', 'admin', '/127.0.0.1:55439', '', '', '', '1', '退出登录', '2022-06-06 14:17:17');
INSERT INTO `sys_logininfor` VALUES ('912', 'admin', '/127.0.0.1:55439', '', '', '', '1', '连接异常', '2022-06-06 14:17:17');
INSERT INTO `sys_logininfor` VALUES ('913', 'admin', '/127.0.0.1:55605', '', '', '', '0', '登录成功！', '2022-06-06 14:20:08');
INSERT INTO `sys_logininfor` VALUES ('914', 'admin', '/127.0.0.1:55605', '', '', '', '1', '连接异常', '2022-06-06 14:20:08');
INSERT INTO `sys_logininfor` VALUES ('915', 'admin', '/127.0.0.1:55605', '', '', '', '1', '退出登录', '2022-06-06 14:20:08');
INSERT INTO `sys_logininfor` VALUES ('916', 'admin', '/127.0.0.1:55605', '', '', '', '1', '连接异常', '2022-06-06 14:20:08');
INSERT INTO `sys_logininfor` VALUES ('917', 'admin', '/127.0.0.1:55681', '', '', '', '0', '登录成功！', '2022-06-06 14:21:27');
INSERT INTO `sys_logininfor` VALUES ('918', 'admin', '/127.0.0.1:55681', '', '', '', '1', '连接异常', '2022-06-06 14:21:27');
INSERT INTO `sys_logininfor` VALUES ('919', 'admin', '/127.0.0.1:55681', '', '', '', '1', '退出登录', '2022-06-06 14:21:27');
INSERT INTO `sys_logininfor` VALUES ('920', 'admin', '/127.0.0.1:55681', '', '', '', '1', '连接异常', '2022-06-06 14:21:27');
INSERT INTO `sys_logininfor` VALUES ('921', 'admin', '/127.0.0.1:55891', '', '', '', '0', '登录成功！', '2022-06-06 14:23:37');
INSERT INTO `sys_logininfor` VALUES ('922', 'admin', '/127.0.0.1:55891', '', '', '', '1', '连接异常', '2022-06-06 14:23:37');
INSERT INTO `sys_logininfor` VALUES ('923', 'admin', '/127.0.0.1:55891', '', '', '', '1', '退出登录', '2022-06-06 14:23:37');
INSERT INTO `sys_logininfor` VALUES ('924', 'admin', '/127.0.0.1:55891', '', '', '', '1', '连接异常', '2022-06-06 14:23:37');
INSERT INTO `sys_logininfor` VALUES ('925', 'admin', '/127.0.0.1:55930', '', '', '', '0', '登录成功！', '2022-06-06 14:24:15');
INSERT INTO `sys_logininfor` VALUES ('926', 'admin', '/127.0.0.1:55930', '', '', '', '1', '连接异常', '2022-06-06 14:24:15');
INSERT INTO `sys_logininfor` VALUES ('927', 'admin', '/127.0.0.1:55930', '', '', '', '1', '退出登录', '2022-06-06 14:24:15');
INSERT INTO `sys_logininfor` VALUES ('928', 'admin', '/127.0.0.1:55930', '', '', '', '1', '连接异常', '2022-06-06 14:24:15');
INSERT INTO `sys_logininfor` VALUES ('929', 'admin', '/127.0.0.1:56085', '', '', '', '0', '登录成功！', '2022-06-06 14:27:02');
INSERT INTO `sys_logininfor` VALUES ('930', 'admin', '/127.0.0.1:56085', '', '', '', '1', '连接异常', '2022-06-06 14:27:02');
INSERT INTO `sys_logininfor` VALUES ('931', 'admin', '/127.0.0.1:56085', '', '', '', '1', '退出登录', '2022-06-06 14:27:02');
INSERT INTO `sys_logininfor` VALUES ('932', 'admin', '/127.0.0.1:56085', '', '', '', '1', '连接异常', '2022-06-06 14:27:02');
INSERT INTO `sys_logininfor` VALUES ('933', 'admin', '/127.0.0.1:56302', '', '', '', '0', '登录成功！', '2022-06-06 14:31:17');
INSERT INTO `sys_logininfor` VALUES ('934', 'admin', '/127.0.0.1:56302', '', '', '', '1', '连接异常', '2022-06-06 14:31:17');
INSERT INTO `sys_logininfor` VALUES ('935', 'admin', '/127.0.0.1:56302', '', '', '', '1', '退出登录', '2022-06-06 14:31:17');
INSERT INTO `sys_logininfor` VALUES ('936', 'admin', '/127.0.0.1:56302', '', '', '', '1', '连接异常', '2022-06-06 14:31:17');
INSERT INTO `sys_logininfor` VALUES ('937', 'admin', '/127.0.0.1:56363', '', '', '', '0', '登录成功！', '2022-06-06 14:32:15');
INSERT INTO `sys_logininfor` VALUES ('938', 'admin', '/127.0.0.1:56363', '', '', '', '1', '连接异常', '2022-06-06 14:32:15');
INSERT INTO `sys_logininfor` VALUES ('939', 'admin', '/127.0.0.1:56363', '', '', '', '1', '退出登录', '2022-06-06 14:32:15');
INSERT INTO `sys_logininfor` VALUES ('940', 'admin', '/127.0.0.1:56363', '', '', '', '1', '连接异常', '2022-06-06 14:32:15');
INSERT INTO `sys_logininfor` VALUES ('941', 'admin', '/127.0.0.1:56433', '', '', '', '0', '登录成功！', '2022-06-06 14:33:30');
INSERT INTO `sys_logininfor` VALUES ('942', 'admin', '/127.0.0.1:56433', '', '', '', '1', '退出登录', '2022-06-06 14:33:30');
INSERT INTO `sys_logininfor` VALUES ('943', 'admin', '/127.0.0.1:56520', '', '', '', '0', '登录成功！', '2022-06-06 14:35:09');
INSERT INTO `sys_logininfor` VALUES ('944', 'admin', '/127.0.0.1:56520', '', '', '', '1', '连接异常', '2022-06-06 14:35:09');
INSERT INTO `sys_logininfor` VALUES ('945', 'admin', '/127.0.0.1:56520', '', '', '', '1', '退出登录', '2022-06-06 14:35:09');
INSERT INTO `sys_logininfor` VALUES ('946', 'admin', '/127.0.0.1:56520', '', '', '', '1', '连接异常', '2022-06-06 14:35:09');
INSERT INTO `sys_logininfor` VALUES ('947', 'admin', '/127.0.0.1:56792', '', '', '', '0', '登录成功！', '2022-06-06 14:38:41');
INSERT INTO `sys_logininfor` VALUES ('948', 'admin', '/127.0.0.1:56792', '', '', '', '1', '连接异常', '2022-06-06 14:38:41');
INSERT INTO `sys_logininfor` VALUES ('949', 'admin', '/127.0.0.1:56792', '', '', '', '1', '退出登录', '2022-06-06 14:38:41');
INSERT INTO `sys_logininfor` VALUES ('950', 'admin', '/127.0.0.1:56792', '', '', '', '1', '连接异常', '2022-06-06 14:38:41');
INSERT INTO `sys_logininfor` VALUES ('951', 'admin', '/127.0.0.1:56996', '', '', '', '0', '登录成功！', '2022-06-06 14:42:31');
INSERT INTO `sys_logininfor` VALUES ('952', 'admin', '/127.0.0.1:56996', '', '', '', '1', '连接异常', '2022-06-06 14:42:31');
INSERT INTO `sys_logininfor` VALUES ('953', 'admin', '/127.0.0.1:56996', '', '', '', '1', '退出登录', '2022-06-06 14:42:31');
INSERT INTO `sys_logininfor` VALUES ('954', 'admin', '/127.0.0.1:56996', '', '', '', '1', '连接异常', '2022-06-06 14:42:31');
INSERT INTO `sys_logininfor` VALUES ('955', 'admin', '/127.0.0.1:57090', '', '', '', '0', '登录成功！', '2022-06-06 14:44:14');
INSERT INTO `sys_logininfor` VALUES ('956', 'admin', '/127.0.0.1:57090', '', '', '', '1', '连接异常', '2022-06-06 14:44:14');
INSERT INTO `sys_logininfor` VALUES ('957', 'admin', '/127.0.0.1:57090', '', '', '', '1', '退出登录', '2022-06-06 14:44:14');
INSERT INTO `sys_logininfor` VALUES ('958', 'admin', '/127.0.0.1:57090', '', '', '', '1', '连接异常', '2022-06-06 14:44:14');
INSERT INTO `sys_logininfor` VALUES ('959', 'admin', '/127.0.0.1:57375', '', '', '', '0', '登录成功！', '2022-06-06 14:49:17');
INSERT INTO `sys_logininfor` VALUES ('960', 'admin', '/127.0.0.1:57375', '', '', '', '1', '连接异常', '2022-06-06 14:49:17');
INSERT INTO `sys_logininfor` VALUES ('961', 'admin', '/127.0.0.1:57375', '', '', '', '1', '退出登录', '2022-06-06 14:49:17');
INSERT INTO `sys_logininfor` VALUES ('962', 'admin', '/127.0.0.1:57375', '', '', '', '1', '连接异常', '2022-06-06 14:49:17');
INSERT INTO `sys_logininfor` VALUES ('963', 'admin', '/127.0.0.1:57702', '', '', '', '0', '登录成功！', '2022-06-06 14:55:19');
INSERT INTO `sys_logininfor` VALUES ('964', 'admin', '/127.0.0.1:57702', '', '', '', '1', '连接异常', '2022-06-06 14:55:19');
INSERT INTO `sys_logininfor` VALUES ('965', 'admin', '/127.0.0.1:57702', '', '', '', '1', '退出登录', '2022-06-06 14:55:19');
INSERT INTO `sys_logininfor` VALUES ('966', 'admin', '/127.0.0.1:57702', '', '', '', '1', '连接异常', '2022-06-06 14:55:19');
INSERT INTO `sys_logininfor` VALUES ('967', 'admin', '/127.0.0.1:57775', '', '', '', '0', '登录成功！', '2022-06-06 14:56:31');
INSERT INTO `sys_logininfor` VALUES ('968', 'admin', '/127.0.0.1:57775', '', '', '', '1', '连接异常', '2022-06-06 14:56:31');
INSERT INTO `sys_logininfor` VALUES ('969', 'admin', '/127.0.0.1:57775', '', '', '', '1', '退出登录', '2022-06-06 14:56:31');
INSERT INTO `sys_logininfor` VALUES ('970', 'admin', '/127.0.0.1:57775', '', '', '', '1', '连接异常', '2022-06-06 14:56:31');
INSERT INTO `sys_logininfor` VALUES ('971', 'admin', '/127.0.0.1:57807', '', '', '', '0', '登录成功！', '2022-06-06 14:56:58');
INSERT INTO `sys_logininfor` VALUES ('972', 'admin', '/127.0.0.1:57807', '', '', '', '1', '连接异常', '2022-06-06 14:56:58');
INSERT INTO `sys_logininfor` VALUES ('973', 'admin', '/127.0.0.1:57807', '', '', '', '1', '退出登录', '2022-06-06 14:56:58');
INSERT INTO `sys_logininfor` VALUES ('974', 'admin', '/127.0.0.1:57807', '', '', '', '1', '连接异常', '2022-06-06 14:56:58');
INSERT INTO `sys_logininfor` VALUES ('975', 'admin', '/127.0.0.1:58424', '', '', '', '0', '登录成功！', '2022-06-06 15:07:14');
INSERT INTO `sys_logininfor` VALUES ('976', 'admin', '/127.0.0.1:58424', '', '', '', '1', '连接异常', '2022-06-06 15:07:14');
INSERT INTO `sys_logininfor` VALUES ('977', 'admin', '/127.0.0.1:58424', '', '', '', '1', '退出登录', '2022-06-06 15:07:14');
INSERT INTO `sys_logininfor` VALUES ('978', 'admin', '/127.0.0.1:58424', '', '', '', '1', '连接异常', '2022-06-06 15:07:14');
INSERT INTO `sys_logininfor` VALUES ('979', 'admin', '/127.0.0.1:59167', '', '', '', '0', '登录成功！', '2022-06-06 15:19:22');
INSERT INTO `sys_logininfor` VALUES ('980', 'admin', '/127.0.0.1:59167', '', '', '', '1', '退出登录', '2022-06-06 15:19:22');
INSERT INTO `sys_logininfor` VALUES ('981', 'admin', '/127.0.0.1:60126', '', '', '', '0', '登录成功！', '2022-06-06 16:47:55');
INSERT INTO `sys_logininfor` VALUES ('982', 'admin', '/127.0.0.1:60126', '', '', '', '1', '连接异常', '2022-06-06 16:47:55');
INSERT INTO `sys_logininfor` VALUES ('983', 'admin', '/127.0.0.1:60126', '', '', '', '1', '退出登录', '2022-06-06 16:47:55');
INSERT INTO `sys_logininfor` VALUES ('984', 'admin', '/127.0.0.1:60126', '', '', '', '1', '连接异常', '2022-06-06 16:47:55');
INSERT INTO `sys_logininfor` VALUES ('985', 'admin', '/127.0.0.1:60159', '', '', '', '0', '登录成功！', '2022-06-06 16:48:25');
INSERT INTO `sys_logininfor` VALUES ('986', 'admin', '/127.0.0.1:60159', '', '', '', '1', '连接异常', '2022-06-06 16:48:25');
INSERT INTO `sys_logininfor` VALUES ('987', 'admin', '/127.0.0.1:60159', '', '', '', '1', '退出登录', '2022-06-06 16:48:25');
INSERT INTO `sys_logininfor` VALUES ('988', 'admin', '/127.0.0.1:60159', '', '', '', '1', '连接异常', '2022-06-06 16:48:25');
INSERT INTO `sys_logininfor` VALUES ('989', 'admin', '/127.0.0.1:60196', '', '', '', '0', '登录成功！', '2022-06-06 16:49:00');
INSERT INTO `sys_logininfor` VALUES ('990', 'admin', '/127.0.0.1:60196', '', '', '', '1', '连接异常', '2022-06-06 16:49:00');
INSERT INTO `sys_logininfor` VALUES ('991', 'admin', '/127.0.0.1:60196', '', '', '', '1', '退出登录', '2022-06-06 16:49:00');
INSERT INTO `sys_logininfor` VALUES ('992', 'admin', '/127.0.0.1:60196', '', '', '', '1', '连接异常', '2022-06-06 16:49:00');
INSERT INTO `sys_logininfor` VALUES ('993', 'admin', '/127.0.0.1:60232', '', '', '', '0', '登录成功！', '2022-06-06 16:49:35');
INSERT INTO `sys_logininfor` VALUES ('994', 'admin', '/127.0.0.1:60232', '', '', '', '1', '连接异常', '2022-06-06 16:49:35');
INSERT INTO `sys_logininfor` VALUES ('995', 'admin', '/127.0.0.1:60232', '', '', '', '1', '退出登录', '2022-06-06 16:49:35');
INSERT INTO `sys_logininfor` VALUES ('996', 'admin', '/127.0.0.1:60232', '', '', '', '1', '连接异常', '2022-06-06 16:49:35');
INSERT INTO `sys_logininfor` VALUES ('997', 'admin', '/127.0.0.1:60277', '', '', '', '0', '登录成功！', '2022-06-06 16:50:14');
INSERT INTO `sys_logininfor` VALUES ('998', 'admin', '/127.0.0.1:60277', '', '', '', '1', '连接异常', '2022-06-06 16:50:14');
INSERT INTO `sys_logininfor` VALUES ('999', 'admin', '/127.0.0.1:60277', '', '', '', '1', '退出登录', '2022-06-06 16:50:14');
INSERT INTO `sys_logininfor` VALUES ('1000', 'admin', '/127.0.0.1:60277', '', '', '', '1', '连接异常', '2022-06-06 16:50:14');
INSERT INTO `sys_logininfor` VALUES ('1001', 'admin', '/127.0.0.1:60319', '', '', '', '0', '登录成功！', '2022-06-06 16:50:52');
INSERT INTO `sys_logininfor` VALUES ('1002', 'admin', '/127.0.0.1:60319', '', '', '', '1', '连接异常', '2022-06-06 16:50:52');
INSERT INTO `sys_logininfor` VALUES ('1003', 'admin', '/127.0.0.1:60319', '', '', '', '1', '退出登录', '2022-06-06 16:50:52');
INSERT INTO `sys_logininfor` VALUES ('1004', 'admin', '/127.0.0.1:60319', '', '', '', '1', '连接异常', '2022-06-06 16:50:52');
INSERT INTO `sys_logininfor` VALUES ('1005', 'admin', '/127.0.0.1:60351', '', '', '', '0', '登录成功！', '2022-06-06 16:51:17');
INSERT INTO `sys_logininfor` VALUES ('1006', 'admin', '/127.0.0.1:60351', '', '', '', '1', '连接异常', '2022-06-06 16:51:17');
INSERT INTO `sys_logininfor` VALUES ('1007', 'admin', '/127.0.0.1:60351', '', '', '', '1', '退出登录', '2022-06-06 16:51:17');
INSERT INTO `sys_logininfor` VALUES ('1008', 'admin', '/127.0.0.1:60351', '', '', '', '1', '连接异常', '2022-06-06 16:51:17');
INSERT INTO `sys_logininfor` VALUES ('1009', 'admin', '/127.0.0.1:60385', '', '', '', '0', '登录成功！', '2022-06-06 16:51:44');
INSERT INTO `sys_logininfor` VALUES ('1010', 'admin', '/127.0.0.1:60385', '', '', '', '1', '连接异常', '2022-06-06 16:51:44');
INSERT INTO `sys_logininfor` VALUES ('1011', 'admin', '/127.0.0.1:60385', '', '', '', '1', '退出登录', '2022-06-06 16:51:44');
INSERT INTO `sys_logininfor` VALUES ('1012', 'admin', '/127.0.0.1:60385', '', '', '', '1', '连接异常', '2022-06-06 16:51:44');
INSERT INTO `sys_logininfor` VALUES ('1013', 'admin', '/127.0.0.1:60453', '', '', '', '0', '登录成功！', '2022-06-06 16:52:42');
INSERT INTO `sys_logininfor` VALUES ('1014', 'admin', '/127.0.0.1:60453', '', '', '', '1', '连接异常', '2022-06-06 16:52:42');
INSERT INTO `sys_logininfor` VALUES ('1015', 'admin', '/127.0.0.1:60453', '', '', '', '1', '退出登录', '2022-06-06 16:52:42');
INSERT INTO `sys_logininfor` VALUES ('1016', 'admin', '/127.0.0.1:60453', '', '', '', '1', '连接异常', '2022-06-06 16:52:42');
INSERT INTO `sys_logininfor` VALUES ('1017', 'admin', '/127.0.0.1:60564', '', '', '', '0', '登录成功！', '2022-06-06 16:54:32');
INSERT INTO `sys_logininfor` VALUES ('1018', 'admin', '/127.0.0.1:60564', '', '', '', '1', '连接异常', '2022-06-06 16:54:32');
INSERT INTO `sys_logininfor` VALUES ('1019', 'admin', '/127.0.0.1:60564', '', '', '', '1', '退出登录', '2022-06-06 16:54:32');
INSERT INTO `sys_logininfor` VALUES ('1020', 'admin', '/127.0.0.1:60564', '', '', '', '1', '连接异常', '2022-06-06 16:54:32');
INSERT INTO `sys_logininfor` VALUES ('1021', 'admin', '/127.0.0.1:60719', '', '', '', '0', '登录成功！', '2022-06-06 16:57:21');
INSERT INTO `sys_logininfor` VALUES ('1022', 'admin', '/127.0.0.1:60719', '', '', '', '1', '连接异常', '2022-06-06 16:57:21');
INSERT INTO `sys_logininfor` VALUES ('1023', 'admin', '/127.0.0.1:60719', '', '', '', '1', '退出登录', '2022-06-06 16:57:21');
INSERT INTO `sys_logininfor` VALUES ('1024', 'admin', '/127.0.0.1:60719', '', '', '', '1', '连接异常', '2022-06-06 16:57:21');
INSERT INTO `sys_logininfor` VALUES ('1025', 'admin', '/127.0.0.1:60772', '', '', '', '0', '登录成功！', '2022-06-06 16:58:00');
INSERT INTO `sys_logininfor` VALUES ('1026', 'admin', '/127.0.0.1:60772', '', '', '', '1', '连接异常', '2022-06-06 16:58:00');
INSERT INTO `sys_logininfor` VALUES ('1027', 'admin', '/127.0.0.1:60772', '', '', '', '1', '退出登录', '2022-06-06 16:58:00');
INSERT INTO `sys_logininfor` VALUES ('1028', 'admin', '/127.0.0.1:60772', '', '', '', '1', '连接异常', '2022-06-06 16:58:00');
INSERT INTO `sys_logininfor` VALUES ('1029', 'admin', '/127.0.0.1:60833', '', '', '', '0', '登录成功！', '2022-06-06 16:59:04');
INSERT INTO `sys_logininfor` VALUES ('1030', 'admin', '/127.0.0.1:60833', '', '', '', '1', '连接异常', '2022-06-06 16:59:04');
INSERT INTO `sys_logininfor` VALUES ('1031', 'admin', '/127.0.0.1:60833', '', '', '', '1', '退出登录', '2022-06-06 16:59:04');
INSERT INTO `sys_logininfor` VALUES ('1032', 'admin', '/127.0.0.1:60833', '', '', '', '1', '连接异常', '2022-06-06 16:59:04');
INSERT INTO `sys_logininfor` VALUES ('1033', 'admin', '/127.0.0.1:60878', '', '', '', '0', '登录成功！', '2022-06-06 16:59:45');
INSERT INTO `sys_logininfor` VALUES ('1034', 'admin', '/127.0.0.1:60878', '', '', '', '1', '连接异常', '2022-06-06 16:59:45');
INSERT INTO `sys_logininfor` VALUES ('1035', 'admin', '/127.0.0.1:60878', '', '', '', '1', '退出登录', '2022-06-06 16:59:45');
INSERT INTO `sys_logininfor` VALUES ('1036', 'admin', '/127.0.0.1:60878', '', '', '', '1', '连接异常', '2022-06-06 16:59:45');
INSERT INTO `sys_logininfor` VALUES ('1037', 'admin', '/127.0.0.1:60914', '', '', '', '0', '登录成功！', '2022-06-06 17:00:12');
INSERT INTO `sys_logininfor` VALUES ('1038', 'admin', '/127.0.0.1:60914', '', '', '', '1', '连接异常', '2022-06-06 17:00:12');
INSERT INTO `sys_logininfor` VALUES ('1039', 'admin', '/127.0.0.1:60914', '', '', '', '1', '退出登录', '2022-06-06 17:00:12');
INSERT INTO `sys_logininfor` VALUES ('1040', 'admin', '/127.0.0.1:60914', '', '', '', '1', '连接异常', '2022-06-06 17:00:12');
INSERT INTO `sys_logininfor` VALUES ('1041', 'admin', '/127.0.0.1:60952', '', '', '', '0', '登录成功！', '2022-06-06 17:00:43');
INSERT INTO `sys_logininfor` VALUES ('1042', 'admin', '/127.0.0.1:60952', '', '', '', '1', '连接异常', '2022-06-06 17:00:43');
INSERT INTO `sys_logininfor` VALUES ('1043', 'admin', '/127.0.0.1:60952', '', '', '', '1', '退出登录', '2022-06-06 17:00:43');
INSERT INTO `sys_logininfor` VALUES ('1044', 'admin', '/127.0.0.1:60952', '', '', '', '1', '连接异常', '2022-06-06 17:00:43');
INSERT INTO `sys_logininfor` VALUES ('1045', 'admin', '/127.0.0.1:60988', '', '', '', '0', '登录成功！', '2022-06-06 17:01:15');
INSERT INTO `sys_logininfor` VALUES ('1046', 'admin', '/127.0.0.1:60988', '', '', '', '1', '连接异常', '2022-06-06 17:01:15');
INSERT INTO `sys_logininfor` VALUES ('1047', 'admin', '/127.0.0.1:60988', '', '', '', '1', '退出登录', '2022-06-06 17:01:15');
INSERT INTO `sys_logininfor` VALUES ('1048', 'admin', '/127.0.0.1:60988', '', '', '', '1', '连接异常', '2022-06-06 17:01:15');
INSERT INTO `sys_logininfor` VALUES ('1049', 'admin', '/127.0.0.1:61022', '', '', '', '0', '登录成功！', '2022-06-06 17:01:44');
INSERT INTO `sys_logininfor` VALUES ('1050', 'admin', '/127.0.0.1:61022', '', '', '', '1', '连接异常', '2022-06-06 17:01:44');
INSERT INTO `sys_logininfor` VALUES ('1051', 'admin', '/127.0.0.1:61022', '', '', '', '1', '退出登录', '2022-06-06 17:01:44');
INSERT INTO `sys_logininfor` VALUES ('1052', 'admin', '/127.0.0.1:61022', '', '', '', '1', '连接异常', '2022-06-06 17:01:44');
INSERT INTO `sys_logininfor` VALUES ('1053', 'admin', '/127.0.0.1:61103', '', '', '', '0', '登录成功！', '2022-06-06 17:02:50');
INSERT INTO `sys_logininfor` VALUES ('1054', 'admin', '/127.0.0.1:61103', '', '', '', '1', '连接异常', '2022-06-06 17:02:50');
INSERT INTO `sys_logininfor` VALUES ('1055', 'admin', '/127.0.0.1:61103', '', '', '', '1', '退出登录', '2022-06-06 17:02:50');
INSERT INTO `sys_logininfor` VALUES ('1056', 'admin', '/127.0.0.1:61103', '', '', '', '1', '连接异常', '2022-06-06 17:02:50');
INSERT INTO `sys_logininfor` VALUES ('1057', 'admin', '/127.0.0.1:61129', '', '', '', '0', '登录成功！', '2022-06-06 17:03:12');
INSERT INTO `sys_logininfor` VALUES ('1058', 'admin', '/127.0.0.1:61129', '', '', '', '1', '连接异常', '2022-06-06 17:03:12');
INSERT INTO `sys_logininfor` VALUES ('1059', 'admin', '/127.0.0.1:61129', '', '', '', '1', '退出登录', '2022-06-06 17:03:12');
INSERT INTO `sys_logininfor` VALUES ('1060', 'admin', '/127.0.0.1:61129', '', '', '', '1', '连接异常', '2022-06-06 17:03:12');
INSERT INTO `sys_logininfor` VALUES ('1061', 'admin', '/127.0.0.1:61188', '', '', '', '0', '登录成功！', '2022-06-06 17:03:59');
INSERT INTO `sys_logininfor` VALUES ('1062', 'admin', '/127.0.0.1:61188', '', '', '', '1', '退出登录', '2022-06-06 17:03:59');
INSERT INTO `sys_logininfor` VALUES ('1063', 'admin', '/127.0.0.1:61654', '', '', '', '0', '登录成功！', '2022-06-06 17:11:39');
INSERT INTO `sys_logininfor` VALUES ('1064', 'admin', '/127.0.0.1:61654', '', '', '', '1', '连接异常', '2022-06-06 17:11:39');
INSERT INTO `sys_logininfor` VALUES ('1065', 'admin', '/127.0.0.1:61654', '', '', '', '1', '退出登录', '2022-06-06 17:11:39');
INSERT INTO `sys_logininfor` VALUES ('1066', 'admin', '/127.0.0.1:61654', '', '', '', '1', '连接异常', '2022-06-06 17:11:39');
INSERT INTO `sys_logininfor` VALUES ('1067', 'admin', '/127.0.0.1:61816', '', '', '', '0', '登录成功！', '2022-06-06 17:13:32');
INSERT INTO `sys_logininfor` VALUES ('1068', 'admin', '/127.0.0.1:61816', '', '', '', '1', '退出登录', '2022-06-06 17:13:32');
INSERT INTO `sys_logininfor` VALUES ('1069', 'admin', '/127.0.0.1:62792', '', '', '', '0', '登录成功！', '2022-06-06 17:25:12');
INSERT INTO `sys_logininfor` VALUES ('1070', 'admin', '/127.0.0.1:62792', '', '', '', '1', '退出登录', '2022-06-06 17:25:12');
INSERT INTO `sys_logininfor` VALUES ('1071', 'admin', '/127.0.0.1:49472', '', '', '', '0', '登录成功！', '2022-06-07 14:53:40');
INSERT INTO `sys_logininfor` VALUES ('1072', 'admin', '/127.0.0.1:49472', '', '', '', '1', '连接异常', '2022-06-07 14:53:40');
INSERT INTO `sys_logininfor` VALUES ('1073', 'admin', '/127.0.0.1:49472', '', '', '', '1', '退出登录', '2022-06-07 14:53:40');
INSERT INTO `sys_logininfor` VALUES ('1074', 'admin', '/127.0.0.1:49472', '', '', '', '1', '连接异常', '2022-06-07 14:53:40');
INSERT INTO `sys_logininfor` VALUES ('1075', 'admin', '/127.0.0.1:55181', '', '', '', '0', '登录成功！', '2022-06-07 15:19:10');
INSERT INTO `sys_logininfor` VALUES ('1076', 'admin', '/127.0.0.1:55181', '', '', '', '1', '退出登录', '2022-06-07 15:19:10');
INSERT INTO `sys_logininfor` VALUES ('1077', 'admin', '/127.0.0.1:55224', '', '', '', '0', '登录成功！', '2022-06-07 15:20:12');
INSERT INTO `sys_logininfor` VALUES ('1078', 'admin', '/127.0.0.1:55224', '', '', '', '1', '退出登录', '2022-06-07 15:20:12');
INSERT INTO `sys_logininfor` VALUES ('1079', 'admin', '/127.0.0.1:55330', '', '', '', '0', '登录成功！', '2022-06-07 15:22:47');
INSERT INTO `sys_logininfor` VALUES ('1080', 'admin', '/127.0.0.1:55330', '', '', '', '1', '连接异常', '2022-06-07 15:22:47');
INSERT INTO `sys_logininfor` VALUES ('1081', 'admin', '/127.0.0.1:55330', '', '', '', '1', '退出登录', '2022-06-07 15:22:47');
INSERT INTO `sys_logininfor` VALUES ('1082', 'admin', '/127.0.0.1:55330', '', '', '', '1', '连接异常', '2022-06-07 15:22:47');
INSERT INTO `sys_logininfor` VALUES ('1083', 'admin', '/127.0.0.1:55359', '', '', '', '0', '登录成功！', '2022-06-07 15:23:27');
INSERT INTO `sys_logininfor` VALUES ('1084', 'admin', '/127.0.0.1:55359', '', '', '', '1', '连接异常', '2022-06-07 15:23:27');
INSERT INTO `sys_logininfor` VALUES ('1085', 'admin', '/127.0.0.1:55359', '', '', '', '1', '退出登录', '2022-06-07 15:23:27');
INSERT INTO `sys_logininfor` VALUES ('1086', 'admin', '/127.0.0.1:55359', '', '', '', '1', '连接异常', '2022-06-07 15:23:27');
INSERT INTO `sys_logininfor` VALUES ('1087', 'admin', '/127.0.0.1:55413', '', '', '', '0', '登录成功！', '2022-06-07 15:24:45');
INSERT INTO `sys_logininfor` VALUES ('1088', 'admin', '/127.0.0.1:55413', '', '', '', '1', '连接异常', '2022-06-07 15:24:45');
INSERT INTO `sys_logininfor` VALUES ('1089', 'admin', '/127.0.0.1:55413', '', '', '', '1', '退出登录', '2022-06-07 15:24:45');
INSERT INTO `sys_logininfor` VALUES ('1090', 'admin', '/127.0.0.1:55413', '', '', '', '1', '连接异常', '2022-06-07 15:24:45');
INSERT INTO `sys_logininfor` VALUES ('1091', 'admin', '/127.0.0.1:55453', '', '', '', '0', '登录成功！', '2022-06-07 15:25:38');
INSERT INTO `sys_logininfor` VALUES ('1092', 'admin', '/127.0.0.1:55453', '', '', '', '1', '连接异常', '2022-06-07 15:25:38');
INSERT INTO `sys_logininfor` VALUES ('1093', 'admin', '/127.0.0.1:55453', '', '', '', '1', '退出登录', '2022-06-07 15:25:38');
INSERT INTO `sys_logininfor` VALUES ('1094', 'admin', '/127.0.0.1:55453', '', '', '', '1', '连接异常', '2022-06-07 15:25:38');
INSERT INTO `sys_logininfor` VALUES ('1095', 'admin', '/127.0.0.1:55797', '', '', '', '0', '登录成功！', '2022-06-07 15:33:12');
INSERT INTO `sys_logininfor` VALUES ('1096', 'admin', '/127.0.0.1:55797', '', '', '', '1', '连接异常', '2022-06-07 15:33:12');
INSERT INTO `sys_logininfor` VALUES ('1097', 'admin', '/127.0.0.1:55797', '', '', '', '1', '退出登录', '2022-06-07 15:33:12');
INSERT INTO `sys_logininfor` VALUES ('1098', 'admin', '/127.0.0.1:55797', '', '', '', '1', '连接异常', '2022-06-07 15:33:12');
INSERT INTO `sys_logininfor` VALUES ('1099', 'admin', '/127.0.0.1:55974', '', '', '', '0', '登录成功！', '2022-06-07 15:35:44');
INSERT INTO `sys_logininfor` VALUES ('1100', 'admin', '/127.0.0.1:55974', '', '', '', '1', '连接异常', '2022-06-07 15:35:44');
INSERT INTO `sys_logininfor` VALUES ('1101', 'admin', '/127.0.0.1:55974', '', '', '', '1', '退出登录', '2022-06-07 15:35:44');
INSERT INTO `sys_logininfor` VALUES ('1102', 'admin', '/127.0.0.1:55974', '', '', '', '1', '连接异常', '2022-06-07 15:35:44');
INSERT INTO `sys_logininfor` VALUES ('1103', 'admin', '/127.0.0.1:56023', '', '', '', '0', '登录成功！', '2022-06-07 15:36:36');
INSERT INTO `sys_logininfor` VALUES ('1104', 'admin', '/127.0.0.1:56023', '', '', '', '1', '连接异常', '2022-06-07 15:36:36');
INSERT INTO `sys_logininfor` VALUES ('1105', 'admin', '/127.0.0.1:56023', '', '', '', '1', '退出登录', '2022-06-07 15:36:36');
INSERT INTO `sys_logininfor` VALUES ('1106', 'admin', '/127.0.0.1:56023', '', '', '', '1', '连接异常', '2022-06-07 15:36:36');
INSERT INTO `sys_logininfor` VALUES ('1107', 'admin', '/127.0.0.1:56378', '', '', '', '0', '登录成功！', '2022-06-07 15:39:30');
INSERT INTO `sys_logininfor` VALUES ('1108', 'admin', '/127.0.0.1:56378', '', '', '', '1', '连接异常', '2022-06-07 15:39:30');
INSERT INTO `sys_logininfor` VALUES ('1109', 'admin', '/127.0.0.1:56378', '', '', '', '1', '退出登录', '2022-06-07 15:39:30');
INSERT INTO `sys_logininfor` VALUES ('1110', 'admin', '/127.0.0.1:56378', '', '', '', '1', '连接异常', '2022-06-07 15:39:30');
INSERT INTO `sys_logininfor` VALUES ('1111', 'admin', '/127.0.0.1:56595', '', '', '', '0', '登录成功！', '2022-06-07 15:42:44');
INSERT INTO `sys_logininfor` VALUES ('1112', 'admin', '/127.0.0.1:56595', '', '', '', '1', '连接异常', '2022-06-07 15:42:44');
INSERT INTO `sys_logininfor` VALUES ('1113', 'admin', '/127.0.0.1:56595', '', '', '', '1', '退出登录', '2022-06-07 15:42:44');
INSERT INTO `sys_logininfor` VALUES ('1114', 'admin', '/127.0.0.1:56595', '', '', '', '1', '连接异常', '2022-06-07 15:42:44');
INSERT INTO `sys_logininfor` VALUES ('1115', 'admin', '/127.0.0.1:56809', '', '', '', '0', '登录成功！', '2022-06-07 15:45:32');
INSERT INTO `sys_logininfor` VALUES ('1116', 'admin', '/127.0.0.1:56809', '', '', '', '1', '连接异常', '2022-06-07 15:45:32');
INSERT INTO `sys_logininfor` VALUES ('1117', 'admin', '/127.0.0.1:56809', '', '', '', '1', '退出登录', '2022-06-07 15:45:32');
INSERT INTO `sys_logininfor` VALUES ('1118', 'admin', '/127.0.0.1:56809', '', '', '', '1', '连接异常', '2022-06-07 15:45:32');
INSERT INTO `sys_logininfor` VALUES ('1119', 'admin', '/127.0.0.1:57072', '', '', '', '0', '登录成功！', '2022-06-07 15:48:07');
INSERT INTO `sys_logininfor` VALUES ('1120', 'admin', '/127.0.0.1:57072', '', '', '', '1', '连接异常', '2022-06-07 15:48:07');
INSERT INTO `sys_logininfor` VALUES ('1121', 'admin', '/127.0.0.1:57072', '', '', '', '1', '退出登录', '2022-06-07 15:48:07');
INSERT INTO `sys_logininfor` VALUES ('1122', 'admin', '/127.0.0.1:57072', '', '', '', '1', '连接异常', '2022-06-07 15:48:07');
INSERT INTO `sys_logininfor` VALUES ('1123', 'admin', '/127.0.0.1:57139', '', '', '', '0', '登录成功！', '2022-06-07 15:48:41');
INSERT INTO `sys_logininfor` VALUES ('1124', 'admin', '/127.0.0.1:57139', '', '', '', '1', '连接异常', '2022-06-07 15:48:41');
INSERT INTO `sys_logininfor` VALUES ('1125', 'admin', '/127.0.0.1:57139', '', '', '', '1', '退出登录', '2022-06-07 15:48:41');
INSERT INTO `sys_logininfor` VALUES ('1126', 'admin', '/127.0.0.1:57139', '', '', '', '1', '连接异常', '2022-06-07 15:48:41');
INSERT INTO `sys_logininfor` VALUES ('1127', 'admin', '/127.0.0.1:59409', '', '', '', '0', '登录成功！', '2022-06-07 16:20:37');
INSERT INTO `sys_logininfor` VALUES ('1128', 'admin', '/127.0.0.1:59409', '', '', '', '1', '连接异常', '2022-06-07 16:20:37');
INSERT INTO `sys_logininfor` VALUES ('1129', 'admin', '/127.0.0.1:59409', '', '', '', '1', '退出登录', '2022-06-07 16:20:37');
INSERT INTO `sys_logininfor` VALUES ('1130', 'admin', '/127.0.0.1:59409', '', '', '', '1', '连接异常', '2022-06-07 16:20:37');
INSERT INTO `sys_logininfor` VALUES ('1131', 'admin', '/127.0.0.1:59430', '', '', '', '0', '登录成功！', '2022-06-07 16:21:03');
INSERT INTO `sys_logininfor` VALUES ('1132', 'admin', '/127.0.0.1:59430', '', '', '', '1', '连接异常', '2022-06-07 16:21:03');
INSERT INTO `sys_logininfor` VALUES ('1133', 'admin', '/127.0.0.1:59430', '', '', '', '1', '退出登录', '2022-06-07 16:21:03');
INSERT INTO `sys_logininfor` VALUES ('1134', 'admin', '/127.0.0.1:59430', '', '', '', '1', '连接异常', '2022-06-07 16:21:03');
INSERT INTO `sys_logininfor` VALUES ('1135', 'admin', '/127.0.0.1:59841', '', '', '', '0', '登录成功！', '2022-06-07 16:28:57');
INSERT INTO `sys_logininfor` VALUES ('1136', 'admin', '/127.0.0.1:59841', '', '', '', '1', '连接异常', '2022-06-07 16:28:57');
INSERT INTO `sys_logininfor` VALUES ('1137', 'admin', '/127.0.0.1:59841', '', '', '', '1', '退出登录', '2022-06-07 16:28:57');
INSERT INTO `sys_logininfor` VALUES ('1138', 'admin', '/127.0.0.1:59841', '', '', '', '1', '连接异常', '2022-06-07 16:28:57');
INSERT INTO `sys_logininfor` VALUES ('1139', 'admin', '/127.0.0.1:59906', '', '', '', '0', '登录成功！', '2022-06-07 16:30:27');
INSERT INTO `sys_logininfor` VALUES ('1140', 'admin', '/127.0.0.1:59906', '', '', '', '1', '连接异常', '2022-06-07 16:30:27');
INSERT INTO `sys_logininfor` VALUES ('1141', 'admin', '/127.0.0.1:59906', '', '', '', '1', '退出登录', '2022-06-07 16:30:27');
INSERT INTO `sys_logininfor` VALUES ('1142', 'admin', '/127.0.0.1:59906', '', '', '', '1', '连接异常', '2022-06-07 16:30:27');
INSERT INTO `sys_logininfor` VALUES ('1143', 'admin', '/127.0.0.1:60325', '', '', '', '0', '登录成功！', '2022-06-07 16:39:55');
INSERT INTO `sys_logininfor` VALUES ('1144', 'admin', '/127.0.0.1:60325', '', '', '', '1', '连接异常', '2022-06-07 16:39:55');
INSERT INTO `sys_logininfor` VALUES ('1145', 'admin', '/127.0.0.1:60325', '', '', '', '1', '退出登录', '2022-06-07 16:39:55');
INSERT INTO `sys_logininfor` VALUES ('1146', 'admin', '/127.0.0.1:60325', '', '', '', '1', '连接异常', '2022-06-07 16:39:55');
INSERT INTO `sys_logininfor` VALUES ('1147', 'admin', '/127.0.0.1:60492', '', '', '', '0', '登录成功！', '2022-06-07 16:44:23');
INSERT INTO `sys_logininfor` VALUES ('1148', 'admin', '/127.0.0.1:60492', '', '', '', '1', '连接异常', '2022-06-07 16:44:23');
INSERT INTO `sys_logininfor` VALUES ('1149', 'admin', '/127.0.0.1:60492', '', '', '', '1', '退出登录', '2022-06-07 16:44:23');
INSERT INTO `sys_logininfor` VALUES ('1150', 'admin', '/127.0.0.1:60492', '', '', '', '1', '连接异常', '2022-06-07 16:44:23');
INSERT INTO `sys_logininfor` VALUES ('1151', 'admin', '/127.0.0.1:61023', '', '', '', '0', '登录成功！', '2022-06-07 16:52:22');
INSERT INTO `sys_logininfor` VALUES ('1152', 'admin', '/127.0.0.1:61023', '', '', '', '1', '连接异常', '2022-06-07 16:52:22');
INSERT INTO `sys_logininfor` VALUES ('1153', 'admin', '/127.0.0.1:61023', '', '', '', '1', '退出登录', '2022-06-07 16:52:22');
INSERT INTO `sys_logininfor` VALUES ('1154', 'admin', '/127.0.0.1:61023', '', '', '', '1', '连接异常', '2022-06-07 16:52:22');
INSERT INTO `sys_logininfor` VALUES ('1155', 'admin', '/127.0.0.1:61044', '', '', '', '0', '登录成功！', '2022-06-07 16:52:49');
INSERT INTO `sys_logininfor` VALUES ('1156', 'admin', '/127.0.0.1:61044', '', '', '', '1', '连接异常', '2022-06-07 16:52:49');
INSERT INTO `sys_logininfor` VALUES ('1157', 'admin', '/127.0.0.1:61044', '', '', '', '1', '退出登录', '2022-06-07 16:52:49');
INSERT INTO `sys_logininfor` VALUES ('1158', 'admin', '/127.0.0.1:61044', '', '', '', '1', '连接异常', '2022-06-07 16:52:49');
INSERT INTO `sys_logininfor` VALUES ('1159', 'admin', '/127.0.0.1:61128', '', '', '', '0', '登录成功！', '2022-06-07 16:54:45');
INSERT INTO `sys_logininfor` VALUES ('1160', 'admin', '/127.0.0.1:61128', '', '', '', '1', '连接异常', '2022-06-07 16:54:45');
INSERT INTO `sys_logininfor` VALUES ('1161', 'admin', '/127.0.0.1:61128', '', '', '', '1', '退出登录', '2022-06-07 16:54:45');
INSERT INTO `sys_logininfor` VALUES ('1162', 'admin', '/127.0.0.1:61128', '', '', '', '1', '连接异常', '2022-06-07 16:54:45');
INSERT INTO `sys_logininfor` VALUES ('1163', 'admin', '/127.0.0.1:61258', '', '', '', '0', '登录成功！', '2022-06-07 16:58:27');
INSERT INTO `sys_logininfor` VALUES ('1164', 'admin', '/127.0.0.1:61258', '', '', '', '1', '连接异常', '2022-06-07 16:58:27');
INSERT INTO `sys_logininfor` VALUES ('1165', 'admin', '/127.0.0.1:61258', '', '', '', '1', '退出登录', '2022-06-07 16:58:27');
INSERT INTO `sys_logininfor` VALUES ('1166', 'admin', '/127.0.0.1:61258', '', '', '', '1', '连接异常', '2022-06-07 16:58:27');
INSERT INTO `sys_logininfor` VALUES ('1167', 'admin', '/127.0.0.1:61493', '', '', '', '0', '登录成功！', '2022-06-07 17:03:52');
INSERT INTO `sys_logininfor` VALUES ('1168', 'admin', '/127.0.0.1:61493', '', '', '', '1', '连接异常', '2022-06-07 17:03:52');
INSERT INTO `sys_logininfor` VALUES ('1169', 'admin', '/127.0.0.1:61493', '', '', '', '1', '退出登录', '2022-06-07 17:03:52');
INSERT INTO `sys_logininfor` VALUES ('1170', 'admin', '/127.0.0.1:61493', '', '', '', '1', '连接异常', '2022-06-07 17:03:52');
INSERT INTO `sys_logininfor` VALUES ('1171', 'admin', '/127.0.0.1:62171', '', '', '', '0', '登录成功！', '2022-06-07 17:08:51');
INSERT INTO `sys_logininfor` VALUES ('1172', 'admin', '/127.0.0.1:62171', '', '', '', '1', '连接异常', '2022-06-07 17:08:51');
INSERT INTO `sys_logininfor` VALUES ('1173', 'admin', '/127.0.0.1:62171', '', '', '', '1', '退出登录', '2022-06-07 17:08:51');
INSERT INTO `sys_logininfor` VALUES ('1174', 'admin', '/127.0.0.1:62171', '', '', '', '1', '连接异常', '2022-06-07 17:08:51');
INSERT INTO `sys_logininfor` VALUES ('1175', 'admin', '/127.0.0.1:62270', '', '', '', '0', '登录成功！', '2022-06-07 17:10:53');
INSERT INTO `sys_logininfor` VALUES ('1176', 'admin', '/127.0.0.1:62270', '', '', '', '1', '连接异常', '2022-06-07 17:10:53');
INSERT INTO `sys_logininfor` VALUES ('1177', 'admin', '/127.0.0.1:62270', '', '', '', '1', '退出登录', '2022-06-07 17:10:53');
INSERT INTO `sys_logininfor` VALUES ('1178', 'admin', '/127.0.0.1:62270', '', '', '', '1', '连接异常', '2022-06-07 17:10:53');
INSERT INTO `sys_logininfor` VALUES ('1179', 'admin', '/127.0.0.1:62432', '', '', '', '0', '登录成功！', '2022-06-07 17:14:58');
INSERT INTO `sys_logininfor` VALUES ('1180', 'admin', '/127.0.0.1:62432', '', '', '', '1', '连接异常', '2022-06-07 17:14:58');
INSERT INTO `sys_logininfor` VALUES ('1181', 'admin', '/127.0.0.1:62432', '', '', '', '1', '退出登录', '2022-06-07 17:14:58');
INSERT INTO `sys_logininfor` VALUES ('1182', 'admin', '/127.0.0.1:62432', '', '', '', '1', '连接异常', '2022-06-07 17:14:58');
INSERT INTO `sys_logininfor` VALUES ('1183', 'admin', '/127.0.0.1:64682', '', '', '', '0', '登录成功！', '2022-06-07 17:19:13');
INSERT INTO `sys_logininfor` VALUES ('1184', 'admin', '/127.0.0.1:64682', '', '', '', '1', '连接异常', '2022-06-07 17:19:13');
INSERT INTO `sys_logininfor` VALUES ('1185', 'admin', '/127.0.0.1:64682', '', '', '', '1', '退出登录', '2022-06-07 17:19:13');
INSERT INTO `sys_logininfor` VALUES ('1186', 'admin', '/127.0.0.1:64682', '', '', '', '1', '连接异常', '2022-06-07 17:19:13');
INSERT INTO `sys_logininfor` VALUES ('1187', 'admin', '/127.0.0.1:64855', '', '', '', '0', '登录成功！', '2022-06-07 17:24:02');
INSERT INTO `sys_logininfor` VALUES ('1188', 'admin', '/127.0.0.1:64855', '', '', '', '1', '连接异常', '2022-06-07 17:24:02');
INSERT INTO `sys_logininfor` VALUES ('1189', 'admin', '/127.0.0.1:64855', '', '', '', '1', '退出登录', '2022-06-07 17:24:02');
INSERT INTO `sys_logininfor` VALUES ('1190', 'admin', '/127.0.0.1:64855', '', '', '', '1', '连接异常', '2022-06-07 17:24:02');
INSERT INTO `sys_logininfor` VALUES ('1191', 'admin', '/127.0.0.1:64923', '', '', '', '0', '登录成功！', '2022-06-07 17:25:38');
INSERT INTO `sys_logininfor` VALUES ('1192', 'admin', '/127.0.0.1:64923', '', '', '', '1', '退出登录', '2022-06-07 17:25:38');
INSERT INTO `sys_logininfor` VALUES ('1193', 'admin', '/127.0.0.1:60262', '', '', '', '0', '登录成功！', '2022-06-08 10:44:05');
INSERT INTO `sys_logininfor` VALUES ('1194', 'admin', '/127.0.0.1:60262', '', '', '', '1', '退出登录', '2022-06-08 10:44:05');
INSERT INTO `sys_logininfor` VALUES ('1195', 'admin', '/127.0.0.1:60380', '', '', '', '0', '登录成功！', '2022-06-08 10:46:26');
INSERT INTO `sys_logininfor` VALUES ('1196', 'admin', '/127.0.0.1:60380', '', '', '', '1', '连接异常', '2022-06-08 10:46:26');
INSERT INTO `sys_logininfor` VALUES ('1197', 'admin', '/127.0.0.1:60380', '', '', '', '1', '退出登录', '2022-06-08 10:46:26');
INSERT INTO `sys_logininfor` VALUES ('1198', 'admin', '/127.0.0.1:60380', '', '', '', '1', '连接异常', '2022-06-08 10:46:26');
INSERT INTO `sys_logininfor` VALUES ('1199', 'admin', '/127.0.0.1:61045', '', '', '', '0', '登录成功！', '2022-06-08 10:59:25');
INSERT INTO `sys_logininfor` VALUES ('1200', 'admin', '/127.0.0.1:61045', '', '', '', '1', '连接异常', '2022-06-08 10:59:25');
INSERT INTO `sys_logininfor` VALUES ('1201', 'admin', '/127.0.0.1:61045', '', '', '', '1', '退出登录', '2022-06-08 10:59:25');
INSERT INTO `sys_logininfor` VALUES ('1202', 'admin', '/127.0.0.1:61045', '', '', '', '1', '连接异常', '2022-06-08 10:59:25');
INSERT INTO `sys_logininfor` VALUES ('1203', 'admin', '/127.0.0.1:61113', '', '', '', '0', '登录成功！', '2022-06-08 11:00:33');
INSERT INTO `sys_logininfor` VALUES ('1204', 'admin', '/127.0.0.1:61113', '', '', '', '1', '连接异常', '2022-06-08 11:00:33');
INSERT INTO `sys_logininfor` VALUES ('1205', 'admin', '/127.0.0.1:61113', '', '', '', '1', '退出登录', '2022-06-08 11:00:33');
INSERT INTO `sys_logininfor` VALUES ('1206', 'admin', '/127.0.0.1:61113', '', '', '', '1', '连接异常', '2022-06-08 11:00:33');
INSERT INTO `sys_logininfor` VALUES ('1207', 'admin', '/127.0.0.1:61153', '', '', '', '0', '登录成功！', '2022-06-08 11:01:12');
INSERT INTO `sys_logininfor` VALUES ('1208', 'admin', '/127.0.0.1:61153', '', '', '', '1', '连接异常', '2022-06-08 11:01:12');
INSERT INTO `sys_logininfor` VALUES ('1209', 'admin', '/127.0.0.1:61153', '', '', '', '1', '退出登录', '2022-06-08 11:01:12');
INSERT INTO `sys_logininfor` VALUES ('1210', 'admin', '/127.0.0.1:61153', '', '', '', '1', '连接异常', '2022-06-08 11:01:12');
INSERT INTO `sys_logininfor` VALUES ('1211', 'admin', '/127.0.0.1:63959', '', '', '', '0', '登录成功！', '2022-06-08 11:12:42');
INSERT INTO `sys_logininfor` VALUES ('1212', 'admin', '/127.0.0.1:63959', '', '', '', '1', '连接异常', '2022-06-08 11:12:42');
INSERT INTO `sys_logininfor` VALUES ('1213', 'admin', '/127.0.0.1:63959', '', '', '', '1', '退出登录', '2022-06-08 11:12:42');
INSERT INTO `sys_logininfor` VALUES ('1214', 'admin', '/127.0.0.1:63959', '', '', '', '1', '连接异常', '2022-06-08 11:12:42');
INSERT INTO `sys_logininfor` VALUES ('1215', 'admin', '/127.0.0.1:64023', '', '', '', '0', '登录成功！', '2022-06-08 11:13:44');
INSERT INTO `sys_logininfor` VALUES ('1216', 'admin', '/127.0.0.1:64023', '', '', '', '1', '连接异常', '2022-06-08 11:13:44');
INSERT INTO `sys_logininfor` VALUES ('1217', 'admin', '/127.0.0.1:64023', '', '', '', '1', '退出登录', '2022-06-08 11:13:44');
INSERT INTO `sys_logininfor` VALUES ('1218', 'admin', '/127.0.0.1:64023', '', '', '', '1', '连接异常', '2022-06-08 11:13:44');
INSERT INTO `sys_logininfor` VALUES ('1219', 'admin', '/127.0.0.1:64166', '', '', '', '0', '登录成功！', '2022-06-08 11:15:44');
INSERT INTO `sys_logininfor` VALUES ('1220', 'admin', '/127.0.0.1:64166', '', '', '', '1', '连接异常', '2022-06-08 11:15:44');
INSERT INTO `sys_logininfor` VALUES ('1221', 'admin', '/127.0.0.1:64166', '', '', '', '1', '退出登录', '2022-06-08 11:15:44');
INSERT INTO `sys_logininfor` VALUES ('1222', 'admin', '/127.0.0.1:64166', '', '', '', '1', '连接异常', '2022-06-08 11:15:44');
INSERT INTO `sys_logininfor` VALUES ('1223', 'admin', '/127.0.0.1:64741', '', '', '', '0', '登录成功！', '2022-06-08 11:23:37');
INSERT INTO `sys_logininfor` VALUES ('1224', 'admin', '/127.0.0.1:64741', '', '', '', '1', '连接异常', '2022-06-08 11:23:37');
INSERT INTO `sys_logininfor` VALUES ('1225', 'admin', '/127.0.0.1:64741', '', '', '', '1', '退出登录', '2022-06-08 11:23:37');
INSERT INTO `sys_logininfor` VALUES ('1226', 'admin', '/127.0.0.1:64741', '', '', '', '1', '连接异常', '2022-06-08 11:23:37');
INSERT INTO `sys_logininfor` VALUES ('1227', 'admin', '/127.0.0.1:64847', '', '', '', '0', '登录成功！', '2022-06-08 11:25:33');
INSERT INTO `sys_logininfor` VALUES ('1228', 'admin', '/127.0.0.1:64847', '', '', '', '1', '连接异常', '2022-06-08 11:25:33');
INSERT INTO `sys_logininfor` VALUES ('1229', 'admin', '/127.0.0.1:64847', '', '', '', '1', '退出登录', '2022-06-08 11:25:33');
INSERT INTO `sys_logininfor` VALUES ('1230', 'admin', '/127.0.0.1:64847', '', '', '', '1', '连接异常', '2022-06-08 11:25:33');
INSERT INTO `sys_logininfor` VALUES ('1231', 'admin', '/127.0.0.1:64941', '', '', '', '0', '登录成功！', '2022-06-08 11:27:09');
INSERT INTO `sys_logininfor` VALUES ('1232', 'admin', '/127.0.0.1:64941', '', '', '', '1', '连接异常', '2022-06-08 11:27:09');
INSERT INTO `sys_logininfor` VALUES ('1233', 'admin', '/127.0.0.1:64941', '', '', '', '1', '退出登录', '2022-06-08 11:27:09');
INSERT INTO `sys_logininfor` VALUES ('1234', 'admin', '/127.0.0.1:64941', '', '', '', '1', '连接异常', '2022-06-08 11:27:09');
INSERT INTO `sys_logininfor` VALUES ('1235', 'admin', '/127.0.0.1:49545', '', '', '', '0', '登录成功！', '2022-06-08 11:34:58');
INSERT INTO `sys_logininfor` VALUES ('1236', 'admin', '/127.0.0.1:49545', '', '', '', '1', '连接异常', '2022-06-08 11:34:58');
INSERT INTO `sys_logininfor` VALUES ('1237', 'admin', '/127.0.0.1:49545', '', '', '', '1', '退出登录', '2022-06-08 11:34:58');
INSERT INTO `sys_logininfor` VALUES ('1238', 'admin', '/127.0.0.1:49545', '', '', '', '1', '连接异常', '2022-06-08 11:34:58');
INSERT INTO `sys_logininfor` VALUES ('1239', 'admin', '/127.0.0.1:51588', '', '', '', '0', '登录成功！', '2022-06-08 11:51:44');
INSERT INTO `sys_logininfor` VALUES ('1240', 'admin', '/127.0.0.1:51588', '', '', '', '1', '连接异常', '2022-06-08 11:51:44');
INSERT INTO `sys_logininfor` VALUES ('1241', 'admin', '/127.0.0.1:51588', '', '', '', '1', '退出登录', '2022-06-08 11:51:44');
INSERT INTO `sys_logininfor` VALUES ('1242', 'admin', '/127.0.0.1:51588', '', '', '', '1', '连接异常', '2022-06-08 11:51:44');
INSERT INTO `sys_logininfor` VALUES ('1243', 'admin', '/127.0.0.1:51631', '', '', '', '0', '登录成功！', '2022-06-08 11:52:20');
INSERT INTO `sys_logininfor` VALUES ('1244', 'admin', '/127.0.0.1:51631', '', '', '', '1', '连接异常', '2022-06-08 11:52:20');
INSERT INTO `sys_logininfor` VALUES ('1245', 'admin', '/127.0.0.1:51631', '', '', '', '1', '退出登录', '2022-06-08 11:52:20');
INSERT INTO `sys_logininfor` VALUES ('1246', 'admin', '/127.0.0.1:51631', '', '', '', '1', '连接异常', '2022-06-08 11:52:20');
INSERT INTO `sys_logininfor` VALUES ('1247', 'admin', '/127.0.0.1:56293', '', '', '', '0', '登录成功！', '2022-06-08 13:12:57');
INSERT INTO `sys_logininfor` VALUES ('1248', 'admin', '/127.0.0.1:56293', '', '', '', '1', '连接异常', '2022-06-08 13:12:57');
INSERT INTO `sys_logininfor` VALUES ('1249', 'admin', '/127.0.0.1:56293', '', '', '', '1', '退出登录', '2022-06-08 13:12:57');
INSERT INTO `sys_logininfor` VALUES ('1250', 'admin', '/127.0.0.1:56293', '', '', '', '1', '连接异常', '2022-06-08 13:12:57');
INSERT INTO `sys_logininfor` VALUES ('1251', 'admin', '/127.0.0.1:57057', '', '', '', '0', '登录成功！', '2022-06-08 13:25:04');
INSERT INTO `sys_logininfor` VALUES ('1252', 'admin', '/127.0.0.1:57057', '', '', '', '1', '连接异常', '2022-06-08 13:25:04');
INSERT INTO `sys_logininfor` VALUES ('1253', 'admin', '/127.0.0.1:57057', '', '', '', '1', '退出登录', '2022-06-08 13:25:04');
INSERT INTO `sys_logininfor` VALUES ('1254', 'admin', '/127.0.0.1:57057', '', '', '', '1', '连接异常', '2022-06-08 13:25:04');
INSERT INTO `sys_logininfor` VALUES ('1255', 'admin', '/127.0.0.1:57150', '', '', '', '0', '登录成功！', '2022-06-08 13:26:51');
INSERT INTO `sys_logininfor` VALUES ('1256', 'admin', '/127.0.0.1:57150', '', '', '', '1', '连接异常', '2022-06-08 13:26:51');
INSERT INTO `sys_logininfor` VALUES ('1257', 'admin', '/127.0.0.1:57150', '', '', '', '1', '退出登录', '2022-06-08 13:26:51');
INSERT INTO `sys_logininfor` VALUES ('1258', 'admin', '/127.0.0.1:57150', '', '', '', '1', '连接异常', '2022-06-08 13:26:51');
INSERT INTO `sys_logininfor` VALUES ('1259', 'admin', '/127.0.0.1:57194', '', '', '', '0', '登录成功！', '2022-06-08 13:27:37');
INSERT INTO `sys_logininfor` VALUES ('1260', 'admin', '/127.0.0.1:57194', '', '', '', '1', '连接异常', '2022-06-08 13:27:37');
INSERT INTO `sys_logininfor` VALUES ('1261', 'admin', '/127.0.0.1:57194', '', '', '', '1', '退出登录', '2022-06-08 13:27:37');
INSERT INTO `sys_logininfor` VALUES ('1262', 'admin', '/127.0.0.1:57194', '', '', '', '1', '连接异常', '2022-06-08 13:27:37');
INSERT INTO `sys_logininfor` VALUES ('1263', 'admin', '/127.0.0.1:57406', '', '', '', '0', '登录成功！', '2022-06-08 13:32:10');
INSERT INTO `sys_logininfor` VALUES ('1264', 'admin', '/127.0.0.1:57406', '', '', '', '1', '连接异常', '2022-06-08 13:32:10');
INSERT INTO `sys_logininfor` VALUES ('1265', 'admin', '/127.0.0.1:57406', '', '', '', '1', '退出登录', '2022-06-08 13:32:10');
INSERT INTO `sys_logininfor` VALUES ('1266', 'admin', '/127.0.0.1:57406', '', '', '', '1', '连接异常', '2022-06-08 13:32:10');
INSERT INTO `sys_logininfor` VALUES ('1267', 'admin', '/127.0.0.1:57498', '', '', '', '0', '登录成功！', '2022-06-08 13:34:03');
INSERT INTO `sys_logininfor` VALUES ('1268', 'admin', '/127.0.0.1:57498', '', '', '', '1', '连接异常', '2022-06-08 13:34:03');
INSERT INTO `sys_logininfor` VALUES ('1269', 'admin', '/127.0.0.1:57498', '', '', '', '1', '退出登录', '2022-06-08 13:34:03');
INSERT INTO `sys_logininfor` VALUES ('1270', 'admin', '/127.0.0.1:57498', '', '', '', '1', '连接异常', '2022-06-08 13:34:03');
INSERT INTO `sys_logininfor` VALUES ('1271', 'admin', '/127.0.0.1:57586', '', '', '', '0', '登录成功！', '2022-06-08 13:35:30');
INSERT INTO `sys_logininfor` VALUES ('1272', 'admin', '/127.0.0.1:57586', '', '', '', '1', '连接异常', '2022-06-08 13:35:30');
INSERT INTO `sys_logininfor` VALUES ('1273', 'admin', '/127.0.0.1:57586', '', '', '', '1', '退出登录', '2022-06-08 13:35:30');
INSERT INTO `sys_logininfor` VALUES ('1274', 'admin', '/127.0.0.1:57586', '', '', '', '1', '连接异常', '2022-06-08 13:35:30');
INSERT INTO `sys_logininfor` VALUES ('1275', 'admin', '/127.0.0.1:58018', '', '', '', '0', '登录成功！', '2022-06-08 13:43:10');
INSERT INTO `sys_logininfor` VALUES ('1276', 'admin', '/127.0.0.1:58018', '', '', '', '1', '连接异常', '2022-06-08 13:43:10');
INSERT INTO `sys_logininfor` VALUES ('1277', 'admin', '/127.0.0.1:58018', '', '', '', '1', '退出登录', '2022-06-08 13:43:10');
INSERT INTO `sys_logininfor` VALUES ('1278', 'admin', '/127.0.0.1:58018', '', '', '', '1', '连接异常', '2022-06-08 13:43:10');
INSERT INTO `sys_logininfor` VALUES ('1279', 'admin', '/127.0.0.1:58229', '', '', '', '0', '登录成功！', '2022-06-08 13:45:29');
INSERT INTO `sys_logininfor` VALUES ('1280', 'admin', '/127.0.0.1:58229', '', '', '', '1', '连接异常', '2022-06-08 13:45:29');
INSERT INTO `sys_logininfor` VALUES ('1281', 'admin', '/127.0.0.1:58229', '', '', '', '1', '退出登录', '2022-06-08 13:45:29');
INSERT INTO `sys_logininfor` VALUES ('1282', 'admin', '/127.0.0.1:58229', '', '', '', '1', '连接异常', '2022-06-08 13:45:29');
INSERT INTO `sys_logininfor` VALUES ('1283', 'admin', '/127.0.0.1:58275', '', '', '', '0', '登录成功！', '2022-06-08 13:46:02');
INSERT INTO `sys_logininfor` VALUES ('1284', 'admin', '/127.0.0.1:58275', '', '', '', '1', '连接异常', '2022-06-08 13:46:02');
INSERT INTO `sys_logininfor` VALUES ('1285', 'admin', '/127.0.0.1:58275', '', '', '', '1', '退出登录', '2022-06-08 13:46:02');
INSERT INTO `sys_logininfor` VALUES ('1286', 'admin', '/127.0.0.1:58275', '', '', '', '1', '连接异常', '2022-06-08 13:46:02');
INSERT INTO `sys_logininfor` VALUES ('1287', 'admin', '/127.0.0.1:59831', '', '', '', '0', '登录成功！', '2022-06-08 14:07:15');
INSERT INTO `sys_logininfor` VALUES ('1288', 'admin', '/127.0.0.1:59831', '', '', '', '1', '退出登录', '2022-06-08 14:07:15');
INSERT INTO `sys_logininfor` VALUES ('1289', 'admin', '/127.0.0.1:59943', '', '', '', '0', '登录成功！', '2022-06-08 14:08:45');
INSERT INTO `sys_logininfor` VALUES ('1290', 'admin', '/127.0.0.1:59943', '', '', '', '1', '连接异常', '2022-06-08 14:08:45');
INSERT INTO `sys_logininfor` VALUES ('1291', 'admin', '/127.0.0.1:59943', '', '', '', '1', '退出登录', '2022-06-08 14:08:45');
INSERT INTO `sys_logininfor` VALUES ('1292', 'admin', '/127.0.0.1:59943', '', '', '', '1', '连接异常', '2022-06-08 14:08:45');
INSERT INTO `sys_logininfor` VALUES ('1293', 'admin', '/127.0.0.1:60014', '', '', '', '0', '登录成功！', '2022-06-08 14:09:22');
INSERT INTO `sys_logininfor` VALUES ('1294', 'admin', '/127.0.0.1:60014', '', '', '', '1', '连接异常', '2022-06-08 14:09:22');
INSERT INTO `sys_logininfor` VALUES ('1295', 'admin', '/127.0.0.1:60014', '', '', '', '1', '退出登录', '2022-06-08 14:09:22');
INSERT INTO `sys_logininfor` VALUES ('1296', 'admin', '/127.0.0.1:60014', '', '', '', '1', '连接异常', '2022-06-08 14:09:22');
INSERT INTO `sys_logininfor` VALUES ('1297', 'admin', '/127.0.0.1:60067', '', '', '', '0', '登录成功！', '2022-06-08 14:10:02');
INSERT INTO `sys_logininfor` VALUES ('1298', 'admin', '/127.0.0.1:60067', '', '', '', '1', '连接异常', '2022-06-08 14:10:02');
INSERT INTO `sys_logininfor` VALUES ('1299', 'admin', '/127.0.0.1:60067', '', '', '', '1', '退出登录', '2022-06-08 14:10:02');
INSERT INTO `sys_logininfor` VALUES ('1300', 'admin', '/127.0.0.1:60067', '', '', '', '1', '连接异常', '2022-06-08 14:10:02');
INSERT INTO `sys_logininfor` VALUES ('1301', 'admin', '/127.0.0.1:60103', '', '', '', '0', '登录成功！', '2022-06-08 14:10:30');
INSERT INTO `sys_logininfor` VALUES ('1302', 'admin', '/127.0.0.1:60103', '', '', '', '1', '连接异常', '2022-06-08 14:10:30');
INSERT INTO `sys_logininfor` VALUES ('1303', 'admin', '/127.0.0.1:60103', '', '', '', '1', '退出登录', '2022-06-08 14:10:30');
INSERT INTO `sys_logininfor` VALUES ('1304', 'admin', '/127.0.0.1:60103', '', '', '', '1', '连接异常', '2022-06-08 14:10:30');
INSERT INTO `sys_logininfor` VALUES ('1305', 'admin', '/127.0.0.1:60140', '', '', '', '0', '登录成功！', '2022-06-08 14:11:01');
INSERT INTO `sys_logininfor` VALUES ('1306', 'admin', '/127.0.0.1:60140', '', '', '', '1', '连接异常', '2022-06-08 14:11:01');
INSERT INTO `sys_logininfor` VALUES ('1307', 'admin', '/127.0.0.1:60140', '', '', '', '1', '退出登录', '2022-06-08 14:11:01');
INSERT INTO `sys_logininfor` VALUES ('1308', 'admin', '/127.0.0.1:60140', '', '', '', '1', '连接异常', '2022-06-08 14:11:01');
INSERT INTO `sys_logininfor` VALUES ('1309', 'admin', '/127.0.0.1:60258', '', '', '', '0', '登录成功！', '2022-06-08 14:12:30');
INSERT INTO `sys_logininfor` VALUES ('1310', 'admin', '/127.0.0.1:60258', '', '', '', '1', '连接异常', '2022-06-08 14:12:30');
INSERT INTO `sys_logininfor` VALUES ('1311', 'admin', '/127.0.0.1:60258', '', '', '', '1', '退出登录', '2022-06-08 14:12:30');
INSERT INTO `sys_logininfor` VALUES ('1312', 'admin', '/127.0.0.1:60258', '', '', '', '1', '连接异常', '2022-06-08 14:12:30');
INSERT INTO `sys_logininfor` VALUES ('1313', 'admin', '/127.0.0.1:60414', '', '', '', '0', '登录成功！', '2022-06-08 14:15:10');
INSERT INTO `sys_logininfor` VALUES ('1314', 'admin', '/127.0.0.1:60414', '', '', '', '1', '连接异常', '2022-06-08 14:15:10');
INSERT INTO `sys_logininfor` VALUES ('1315', 'admin', '/127.0.0.1:60414', '', '', '', '1', '退出登录', '2022-06-08 14:15:10');
INSERT INTO `sys_logininfor` VALUES ('1316', 'admin', '/127.0.0.1:60414', '', '', '', '1', '连接异常', '2022-06-08 14:15:10');
INSERT INTO `sys_logininfor` VALUES ('1317', 'admin', '/127.0.0.1:58512', '', '', '', '0', '登录成功！', '2022-06-08 14:24:37');
INSERT INTO `sys_logininfor` VALUES ('1318', 'admin', '/127.0.0.1:58512', '', '', '', '1', '连接异常', '2022-06-08 14:24:37');
INSERT INTO `sys_logininfor` VALUES ('1319', 'admin', '/127.0.0.1:58512', '', '', '', '1', '退出登录', '2022-06-08 14:24:37');
INSERT INTO `sys_logininfor` VALUES ('1320', 'admin', '/127.0.0.1:58512', '', '', '', '1', '连接异常', '2022-06-08 14:24:37');
INSERT INTO `sys_logininfor` VALUES ('1321', 'admin', '/127.0.0.1:58573', '', '', '', '0', '登录成功！', '2022-06-08 14:25:29');
INSERT INTO `sys_logininfor` VALUES ('1322', 'admin', '/127.0.0.1:58573', '', '', '', '1', '连接异常', '2022-06-08 14:25:29');
INSERT INTO `sys_logininfor` VALUES ('1323', 'admin', '/127.0.0.1:58573', '', '', '', '1', '退出登录', '2022-06-08 14:25:29');
INSERT INTO `sys_logininfor` VALUES ('1324', 'admin', '/127.0.0.1:58573', '', '', '', '1', '连接异常', '2022-06-08 14:25:29');
INSERT INTO `sys_logininfor` VALUES ('1325', 'admin', '/127.0.0.1:58686', '', '', '', '0', '登录成功！', '2022-06-08 14:27:01');
INSERT INTO `sys_logininfor` VALUES ('1326', 'admin', '/127.0.0.1:58686', '', '', '', '1', '退出登录', '2022-06-08 14:27:01');
INSERT INTO `sys_logininfor` VALUES ('1327', 'admin', '/127.0.0.1:58889', '', '', '', '0', '登录成功！', '2022-06-08 14:30:34');
INSERT INTO `sys_logininfor` VALUES ('1328', 'admin', '/127.0.0.1:58889', '', '', '', '1', '连接异常', '2022-06-08 14:30:34');
INSERT INTO `sys_logininfor` VALUES ('1329', 'admin', '/127.0.0.1:58889', '', '', '', '1', '退出登录', '2022-06-08 14:30:34');
INSERT INTO `sys_logininfor` VALUES ('1330', 'admin', '/127.0.0.1:58889', '', '', '', '1', '连接异常', '2022-06-08 14:30:34');
INSERT INTO `sys_logininfor` VALUES ('1331', 'admin', '/127.0.0.1:59024', '', '', '', '0', '登录成功！', '2022-06-08 14:33:05');
INSERT INTO `sys_logininfor` VALUES ('1332', 'admin', '/127.0.0.1:59024', '', '', '', '1', '连接异常', '2022-06-08 14:33:05');
INSERT INTO `sys_logininfor` VALUES ('1333', 'admin', '/127.0.0.1:59024', '', '', '', '1', '退出登录', '2022-06-08 14:33:05');
INSERT INTO `sys_logininfor` VALUES ('1334', 'admin', '/127.0.0.1:59024', '', '', '', '1', '连接异常', '2022-06-08 14:33:05');
INSERT INTO `sys_logininfor` VALUES ('1335', 'admin', '/127.0.0.1:59376', '', '', '', '0', '登录成功！', '2022-06-08 14:39:04');
INSERT INTO `sys_logininfor` VALUES ('1336', 'admin', '/127.0.0.1:59376', '', '', '', '1', '连接异常', '2022-06-08 14:39:04');
INSERT INTO `sys_logininfor` VALUES ('1337', 'admin', '/127.0.0.1:59376', '', '', '', '1', '退出登录', '2022-06-08 14:39:04');
INSERT INTO `sys_logininfor` VALUES ('1338', 'admin', '/127.0.0.1:59376', '', '', '', '1', '连接异常', '2022-06-08 14:39:04');
INSERT INTO `sys_logininfor` VALUES ('1339', 'admin', '/127.0.0.1:59500', '', '', '', '0', '登录成功！', '2022-06-08 14:41:20');
INSERT INTO `sys_logininfor` VALUES ('1340', 'admin', '/127.0.0.1:59500', '', '', '', '1', '连接异常', '2022-06-08 14:41:20');
INSERT INTO `sys_logininfor` VALUES ('1341', 'admin', '/127.0.0.1:59500', '', '', '', '1', '退出登录', '2022-06-08 14:41:20');
INSERT INTO `sys_logininfor` VALUES ('1342', 'admin', '/127.0.0.1:59500', '', '', '', '1', '连接异常', '2022-06-08 14:41:20');
INSERT INTO `sys_logininfor` VALUES ('1343', 'admin', '/127.0.0.1:59547', '', '', '', '0', '登录成功！', '2022-06-08 14:42:09');
INSERT INTO `sys_logininfor` VALUES ('1344', 'admin', '/127.0.0.1:59547', '', '', '', '1', '连接异常', '2022-06-08 14:42:09');
INSERT INTO `sys_logininfor` VALUES ('1345', 'admin', '/127.0.0.1:59547', '', '', '', '1', '退出登录', '2022-06-08 14:42:09');
INSERT INTO `sys_logininfor` VALUES ('1346', 'admin', '/127.0.0.1:59547', '', '', '', '1', '连接异常', '2022-06-08 14:42:09');
INSERT INTO `sys_logininfor` VALUES ('1347', 'admin', '/127.0.0.1:60130', '', '', '', '0', '登录成功！', '2022-06-08 14:49:08');
INSERT INTO `sys_logininfor` VALUES ('1348', 'admin', '/127.0.0.1:60130', '', '', '', '1', '退出登录', '2022-06-08 14:49:08');
INSERT INTO `sys_logininfor` VALUES ('1349', 'admin', '/127.0.0.1:60607', '', '', '', '0', '登录成功！', '2022-06-08 14:56:40');
INSERT INTO `sys_logininfor` VALUES ('1350', 'admin', '/127.0.0.1:60607', '', '', '', '1', '连接异常', '2022-06-08 14:56:40');
INSERT INTO `sys_logininfor` VALUES ('1351', 'admin', '/127.0.0.1:60607', '', '', '', '1', '退出登录', '2022-06-08 14:56:40');
INSERT INTO `sys_logininfor` VALUES ('1352', 'admin', '/127.0.0.1:60607', '', '', '', '1', '连接异常', '2022-06-08 14:56:40');
INSERT INTO `sys_logininfor` VALUES ('1353', 'admin', '/127.0.0.1:60691', '', '', '', '0', '登录成功！', '2022-06-08 14:57:53');
INSERT INTO `sys_logininfor` VALUES ('1354', 'admin', '/127.0.0.1:60691', '', '', '', '1', '连接异常', '2022-06-08 14:57:53');
INSERT INTO `sys_logininfor` VALUES ('1355', 'admin', '/127.0.0.1:60691', '', '', '', '1', '退出登录', '2022-06-08 14:57:53');
INSERT INTO `sys_logininfor` VALUES ('1356', 'admin', '/127.0.0.1:60691', '', '', '', '1', '连接异常', '2022-06-08 14:57:53');
INSERT INTO `sys_logininfor` VALUES ('1357', 'admin', '/127.0.0.1:60847', '', '', '', '0', '登录成功！', '2022-06-08 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('1358', 'admin', '/127.0.0.1:60847', '', '', '', '1', '连接异常', '2022-06-08 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('1359', 'admin', '/127.0.0.1:60847', '', '', '', '1', '退出登录', '2022-06-08 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('1360', 'admin', '/127.0.0.1:60847', '', '', '', '1', '连接异常', '2022-06-08 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('1361', 'admin', '/127.0.0.1:61065', '', '', '', '0', '登录成功！', '2022-06-08 15:04:00');
INSERT INTO `sys_logininfor` VALUES ('1362', 'admin', '/127.0.0.1:61065', '', '', '', '1', '连接异常', '2022-06-08 15:04:00');
INSERT INTO `sys_logininfor` VALUES ('1363', 'admin', '/127.0.0.1:61065', '', '', '', '1', '退出登录', '2022-06-08 15:04:00');
INSERT INTO `sys_logininfor` VALUES ('1364', 'admin', '/127.0.0.1:61065', '', '', '', '1', '连接异常', '2022-06-08 15:04:00');
INSERT INTO `sys_logininfor` VALUES ('1365', 'admin', '/127.0.0.1:61112', '', '', '', '0', '登录成功！', '2022-06-08 15:04:48');
INSERT INTO `sys_logininfor` VALUES ('1366', 'admin', '/127.0.0.1:61112', '', '', '', '1', '连接异常', '2022-06-08 15:04:48');
INSERT INTO `sys_logininfor` VALUES ('1367', 'admin', '/127.0.0.1:61112', '', '', '', '1', '退出登录', '2022-06-08 15:04:48');
INSERT INTO `sys_logininfor` VALUES ('1368', 'admin', '/127.0.0.1:61112', '', '', '', '1', '连接异常', '2022-06-08 15:04:48');
INSERT INTO `sys_logininfor` VALUES ('1369', 'admin', '/127.0.0.1:61261', '', '', '', '0', '登录成功！', '2022-06-08 15:07:00');
INSERT INTO `sys_logininfor` VALUES ('1370', 'admin', '/127.0.0.1:61261', '', '', '', '1', '连接异常', '2022-06-08 15:07:00');
INSERT INTO `sys_logininfor` VALUES ('1371', 'admin', '/127.0.0.1:61261', '', '', '', '1', '退出登录', '2022-06-08 15:07:00');
INSERT INTO `sys_logininfor` VALUES ('1372', 'admin', '/127.0.0.1:61261', '', '', '', '1', '连接异常', '2022-06-08 15:07:00');
INSERT INTO `sys_logininfor` VALUES ('1373', 'admin', '/127.0.0.1:61316', '', '', '', '0', '登录成功！', '2022-06-08 15:07:50');
INSERT INTO `sys_logininfor` VALUES ('1374', 'admin', '/127.0.0.1:61316', '', '', '', '1', '连接异常', '2022-06-08 15:07:50');
INSERT INTO `sys_logininfor` VALUES ('1375', 'admin', '/127.0.0.1:61316', '', '', '', '1', '退出登录', '2022-06-08 15:07:50');
INSERT INTO `sys_logininfor` VALUES ('1376', 'admin', '/127.0.0.1:61316', '', '', '', '1', '连接异常', '2022-06-08 15:07:50');
INSERT INTO `sys_logininfor` VALUES ('1377', 'admin', '/127.0.0.1:61362', '', '', '', '0', '登录成功！', '2022-06-08 15:08:34');
INSERT INTO `sys_logininfor` VALUES ('1378', 'admin', '/127.0.0.1:61362', '', '', '', '1', '连接异常', '2022-06-08 15:08:34');
INSERT INTO `sys_logininfor` VALUES ('1379', 'admin', '/127.0.0.1:61362', '', '', '', '1', '退出登录', '2022-06-08 15:08:34');
INSERT INTO `sys_logininfor` VALUES ('1380', 'admin', '/127.0.0.1:61362', '', '', '', '1', '连接异常', '2022-06-08 15:08:34');
INSERT INTO `sys_logininfor` VALUES ('1381', 'admin', '/127.0.0.1:61395', '', '', '', '0', '登录成功！', '2022-06-08 15:09:01');
INSERT INTO `sys_logininfor` VALUES ('1382', 'admin', '/127.0.0.1:61395', '', '', '', '1', '连接异常', '2022-06-08 15:09:01');
INSERT INTO `sys_logininfor` VALUES ('1383', 'admin', '/127.0.0.1:61395', '', '', '', '1', '退出登录', '2022-06-08 15:09:01');
INSERT INTO `sys_logininfor` VALUES ('1384', 'admin', '/127.0.0.1:61395', '', '', '', '1', '连接异常', '2022-06-08 15:09:01');
INSERT INTO `sys_logininfor` VALUES ('1385', 'admin', '/127.0.0.1:61425', '', '', '', '0', '登录成功！', '2022-06-08 15:09:32');
INSERT INTO `sys_logininfor` VALUES ('1386', 'admin', '/127.0.0.1:61425', '', '', '', '1', '连接异常', '2022-06-08 15:09:32');
INSERT INTO `sys_logininfor` VALUES ('1387', 'admin', '/127.0.0.1:61425', '', '', '', '1', '退出登录', '2022-06-08 15:09:32');
INSERT INTO `sys_logininfor` VALUES ('1388', 'admin', '/127.0.0.1:61425', '', '', '', '1', '连接异常', '2022-06-08 15:09:32');
INSERT INTO `sys_logininfor` VALUES ('1389', 'admin', '/127.0.0.1:61459', '', '', '', '0', '登录成功！', '2022-06-08 15:09:57');
INSERT INTO `sys_logininfor` VALUES ('1390', 'admin', '/127.0.0.1:61459', '', '', '', '1', '连接异常', '2022-06-08 15:09:57');
INSERT INTO `sys_logininfor` VALUES ('1391', 'admin', '/127.0.0.1:61459', '', '', '', '1', '退出登录', '2022-06-08 15:09:57');
INSERT INTO `sys_logininfor` VALUES ('1392', 'admin', '/127.0.0.1:61459', '', '', '', '1', '连接异常', '2022-06-08 15:09:57');
INSERT INTO `sys_logininfor` VALUES ('1393', 'admin', '/127.0.0.1:61579', '', '', '', '0', '登录成功！', '2022-06-08 15:12:13');
INSERT INTO `sys_logininfor` VALUES ('1394', 'admin', '/127.0.0.1:61579', '', '', '', '1', '连接异常', '2022-06-08 15:12:13');
INSERT INTO `sys_logininfor` VALUES ('1395', 'admin', '/127.0.0.1:61579', '', '', '', '1', '退出登录', '2022-06-08 15:12:13');
INSERT INTO `sys_logininfor` VALUES ('1396', 'admin', '/127.0.0.1:61579', '', '', '', '1', '连接异常', '2022-06-08 15:12:13');
INSERT INTO `sys_logininfor` VALUES ('1397', 'admin', '/127.0.0.1:61614', '', '', '', '0', '登录成功！', '2022-06-08 15:12:50');
INSERT INTO `sys_logininfor` VALUES ('1398', 'admin', '/127.0.0.1:61614', '', '', '', '1', '连接异常', '2022-06-08 15:12:50');
INSERT INTO `sys_logininfor` VALUES ('1399', 'admin', '/127.0.0.1:61614', '', '', '', '1', '退出登录', '2022-06-08 15:12:50');
INSERT INTO `sys_logininfor` VALUES ('1400', 'admin', '/127.0.0.1:61614', '', '', '', '1', '连接异常', '2022-06-08 15:12:50');
INSERT INTO `sys_logininfor` VALUES ('1401', 'admin', '/127.0.0.1:64584', '', '', '', '0', '登录成功！', '2022-06-08 15:24:19');
INSERT INTO `sys_logininfor` VALUES ('1402', 'admin', '/127.0.0.1:64584', '', '', '', '1', '连接异常', '2022-06-08 15:24:19');
INSERT INTO `sys_logininfor` VALUES ('1403', 'admin', '/127.0.0.1:64584', '', '', '', '1', '退出登录', '2022-06-08 15:24:19');
INSERT INTO `sys_logininfor` VALUES ('1404', 'admin', '/127.0.0.1:64584', '', '', '', '1', '连接异常', '2022-06-08 15:24:19');
INSERT INTO `sys_logininfor` VALUES ('1405', 'admin', '/127.0.0.1:64666', '', '', '', '0', '登录成功！', '2022-06-08 15:26:02');
INSERT INTO `sys_logininfor` VALUES ('1406', 'admin', '/127.0.0.1:64666', '', '', '', '1', '连接异常', '2022-06-08 15:26:02');
INSERT INTO `sys_logininfor` VALUES ('1407', 'admin', '/127.0.0.1:64666', '', '', '', '1', '退出登录', '2022-06-08 15:26:02');
INSERT INTO `sys_logininfor` VALUES ('1408', 'admin', '/127.0.0.1:64666', '', '', '', '1', '连接异常', '2022-06-08 15:26:02');
INSERT INTO `sys_logininfor` VALUES ('1409', 'admin', '/127.0.0.1:64787', '', '', '', '0', '登录成功！', '2022-06-08 15:28:35');
INSERT INTO `sys_logininfor` VALUES ('1410', 'admin', '/127.0.0.1:64787', '', '', '', '1', '连接异常', '2022-06-08 15:28:35');
INSERT INTO `sys_logininfor` VALUES ('1411', 'admin', '/127.0.0.1:64787', '', '', '', '1', '退出登录', '2022-06-08 15:28:35');
INSERT INTO `sys_logininfor` VALUES ('1412', 'admin', '/127.0.0.1:64787', '', '', '', '1', '连接异常', '2022-06-08 15:28:35');
INSERT INTO `sys_logininfor` VALUES ('1413', 'admin', '/127.0.0.1:49193', '', '', '', '0', '登录成功！', '2022-06-08 15:41:32');
INSERT INTO `sys_logininfor` VALUES ('1414', 'admin', '/127.0.0.1:49193', '', '', '', '1', '连接异常', '2022-06-08 15:41:32');
INSERT INTO `sys_logininfor` VALUES ('1415', 'admin', '/127.0.0.1:49193', '', '', '', '1', '退出登录', '2022-06-08 15:41:32');
INSERT INTO `sys_logininfor` VALUES ('1416', 'admin', '/127.0.0.1:49193', '', '', '', '1', '连接异常', '2022-06-08 15:41:32');
INSERT INTO `sys_logininfor` VALUES ('1417', 'admin', '/127.0.0.1:49241', '', '', '', '0', '登录成功！', '2022-06-08 15:42:24');
INSERT INTO `sys_logininfor` VALUES ('1418', 'admin', '/127.0.0.1:49241', '', '', '', '1', '连接异常', '2022-06-08 15:42:24');
INSERT INTO `sys_logininfor` VALUES ('1419', 'admin', '/127.0.0.1:49241', '', '', '', '1', '退出登录', '2022-06-08 15:42:24');
INSERT INTO `sys_logininfor` VALUES ('1420', 'admin', '/127.0.0.1:49241', '', '', '', '1', '连接异常', '2022-06-08 15:42:24');
INSERT INTO `sys_logininfor` VALUES ('1421', 'admin', '/127.0.0.1:49449', '', '', '', '0', '登录成功！', '2022-06-08 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('1422', 'admin', '/127.0.0.1:49449', '', '', '', '1', '退出登录', '2022-06-08 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('1423', 'admin', '/127.0.0.1:49449', '', '', '', '1', '连接异常', '2022-06-08 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('1424', 'admin', '/127.0.0.1:49678', '', '', '', '0', '登录成功！', '2022-06-08 15:51:07');
INSERT INTO `sys_logininfor` VALUES ('1425', 'admin', '/127.0.0.1:49678', '', '', '', '1', '退出登录', '2022-06-08 15:51:07');
INSERT INTO `sys_logininfor` VALUES ('1426', 'admin', '/127.0.0.1:49678', '', '', '', '1', '连接异常', '2022-06-08 15:51:07');
INSERT INTO `sys_logininfor` VALUES ('1427', 'admin', '/127.0.0.1:53029', '', '', '', '0', '登录成功！', '2022-06-08 16:40:45');
INSERT INTO `sys_logininfor` VALUES ('1428', 'admin', '/127.0.0.1:53029', '', '', '', '1', '连接异常', '2022-06-08 16:40:45');
INSERT INTO `sys_logininfor` VALUES ('1429', 'admin', '/127.0.0.1:53029', '', '', '', '1', '退出登录', '2022-06-08 16:40:45');
INSERT INTO `sys_logininfor` VALUES ('1430', 'admin', '/127.0.0.1:53029', '', '', '', '1', '连接异常', '2022-06-08 16:40:45');
INSERT INTO `sys_logininfor` VALUES ('1431', 'admin', '/127.0.0.1:53401', '', '', '', '0', '登录成功！', '2022-06-08 16:47:53');
INSERT INTO `sys_logininfor` VALUES ('1432', 'admin', '/127.0.0.1:53401', '', '', '', '1', '连接异常', '2022-06-08 16:47:53');
INSERT INTO `sys_logininfor` VALUES ('1433', 'admin', '/127.0.0.1:53401', '', '', '', '1', '退出登录', '2022-06-08 16:47:53');
INSERT INTO `sys_logininfor` VALUES ('1434', 'admin', '/127.0.0.1:53401', '', '', '', '1', '连接异常', '2022-06-08 16:47:53');
INSERT INTO `sys_logininfor` VALUES ('1435', 'admin', '/127.0.0.1:53601', '', '', '', '0', '登录成功！', '2022-06-08 16:52:05');
INSERT INTO `sys_logininfor` VALUES ('1436', 'admin', '/127.0.0.1:53601', '', '', '', '1', '退出登录', '2022-06-08 16:52:05');
INSERT INTO `sys_logininfor` VALUES ('1437', 'admin', '/127.0.0.1:53601', '', '', '', '1', '连接异常', '2022-06-08 16:52:05');
INSERT INTO `sys_logininfor` VALUES ('1438', 'admin', '/127.0.0.1:53875', '', '', '', '0', '登录成功！', '2022-06-08 16:57:09');
INSERT INTO `sys_logininfor` VALUES ('1439', 'admin', '/127.0.0.1:53875', '', '', '', '1', '退出登录', '2022-06-08 16:57:09');
INSERT INTO `sys_logininfor` VALUES ('1440', 'admin', '/127.0.0.1:53875', '', '', '', '1', '连接异常', '2022-06-08 16:57:09');
INSERT INTO `sys_logininfor` VALUES ('1441', 'admin', '/127.0.0.1:51238', '', '', '', '0', '登录成功！', '2022-06-08 17:00:42');
INSERT INTO `sys_logininfor` VALUES ('1442', 'admin', '/127.0.0.1:51238', '', '', '', '1', '退出登录', '2022-06-08 17:00:42');
INSERT INTO `sys_logininfor` VALUES ('1443', 'admin', '/127.0.0.1:65385', '', '', '', '0', '登录成功！', '2022-06-09 10:01:04');
INSERT INTO `sys_logininfor` VALUES ('1444', 'admin', '/127.0.0.1:65385', '', '', '', '1', '连接异常', '2022-06-09 10:01:04');
INSERT INTO `sys_logininfor` VALUES ('1445', 'admin', '/127.0.0.1:65385', '', '', '', '1', '退出登录', '2022-06-09 10:01:04');
INSERT INTO `sys_logininfor` VALUES ('1446', 'admin', '/127.0.0.1:65385', '', '', '', '1', '连接异常', '2022-06-09 10:01:04');
INSERT INTO `sys_logininfor` VALUES ('1447', 'admin', '/127.0.0.1:65438', '', '', '', '0', '登录成功！', '2022-06-09 10:03:27');
INSERT INTO `sys_logininfor` VALUES ('1448', 'admin', '/127.0.0.1:65438', '', '', '', '1', '连接异常', '2022-06-09 10:03:27');
INSERT INTO `sys_logininfor` VALUES ('1449', 'admin', '/127.0.0.1:65438', '', '', '', '1', '退出登录', '2022-06-09 10:03:27');
INSERT INTO `sys_logininfor` VALUES ('1450', 'admin', '/127.0.0.1:65438', '', '', '', '1', '连接异常', '2022-06-09 10:03:27');
INSERT INTO `sys_logininfor` VALUES ('1451', 'admin', '/127.0.0.1:65479', '', '', '', '0', '登录成功！', '2022-06-09 10:05:19');
INSERT INTO `sys_logininfor` VALUES ('1452', 'admin', '/127.0.0.1:65479', '', '', '', '1', '连接异常', '2022-06-09 10:05:19');
INSERT INTO `sys_logininfor` VALUES ('1453', 'admin', '/127.0.0.1:65479', '', '', '', '1', '退出登录', '2022-06-09 10:05:19');
INSERT INTO `sys_logininfor` VALUES ('1454', 'admin', '/127.0.0.1:65479', '', '', '', '1', '连接异常', '2022-06-09 10:05:19');
INSERT INTO `sys_logininfor` VALUES ('1455', 'admin', '/127.0.0.1:49224', '', '', '', '0', '登录成功！', '2022-06-09 10:12:59');
INSERT INTO `sys_logininfor` VALUES ('1456', 'admin', '/127.0.0.1:49224', '', '', '', '1', '连接异常', '2022-06-09 10:12:59');
INSERT INTO `sys_logininfor` VALUES ('1457', 'admin', '/127.0.0.1:49224', '', '', '', '1', '退出登录', '2022-06-09 10:12:59');
INSERT INTO `sys_logininfor` VALUES ('1458', 'admin', '/127.0.0.1:49224', '', '', '', '1', '连接异常', '2022-06-09 10:12:59');
INSERT INTO `sys_logininfor` VALUES ('1459', 'admin', '/127.0.0.1:49322', '', '', '', '0', '登录成功！', '2022-06-09 10:19:09');
INSERT INTO `sys_logininfor` VALUES ('1460', 'admin', '/127.0.0.1:49322', '', '', '', '1', '连接异常', '2022-06-09 10:19:09');
INSERT INTO `sys_logininfor` VALUES ('1461', 'admin', '/127.0.0.1:49322', '', '', '', '1', '退出登录', '2022-06-09 10:19:09');
INSERT INTO `sys_logininfor` VALUES ('1462', 'admin', '/127.0.0.1:49322', '', '', '', '1', '连接异常', '2022-06-09 10:19:09');
INSERT INTO `sys_logininfor` VALUES ('1463', 'admin', '/127.0.0.1:49972', '', '', '', '0', '登录成功！', '2022-06-09 10:34:13');
INSERT INTO `sys_logininfor` VALUES ('1464', 'admin', '/127.0.0.1:49972', '', '', '', '1', '退出登录', '2022-06-09 10:34:13');
INSERT INTO `sys_logininfor` VALUES ('1465', 'admin', '/127.0.0.1:50028', '', '', '', '0', '登录成功！', '2022-06-09 10:35:59');
INSERT INTO `sys_logininfor` VALUES ('1466', 'admin', '/127.0.0.1:50028', '', '', '', '1', '连接异常', '2022-06-09 10:35:59');
INSERT INTO `sys_logininfor` VALUES ('1467', 'admin', '/127.0.0.1:50028', '', '', '', '1', '退出登录', '2022-06-09 10:35:59');
INSERT INTO `sys_logininfor` VALUES ('1468', 'admin', '/127.0.0.1:50028', '', '', '', '1', '连接异常', '2022-06-09 10:35:59');
INSERT INTO `sys_logininfor` VALUES ('1469', 'admin', '/127.0.0.1:50527', '', '', '', '0', '登录成功！', '2022-06-09 10:45:09');
INSERT INTO `sys_logininfor` VALUES ('1470', 'admin', '/127.0.0.1:50527', '', '', '', '1', '连接异常', '2022-06-09 10:45:09');
INSERT INTO `sys_logininfor` VALUES ('1471', 'admin', '/127.0.0.1:50527', '', '', '', '1', '退出登录', '2022-06-09 10:45:09');
INSERT INTO `sys_logininfor` VALUES ('1472', 'admin', '/127.0.0.1:50527', '', '', '', '1', '连接异常', '2022-06-09 10:45:09');
INSERT INTO `sys_logininfor` VALUES ('1473', 'admin', '/127.0.0.1:50623', '', '', '', '0', '登录成功！', '2022-06-09 10:51:19');
INSERT INTO `sys_logininfor` VALUES ('1474', 'admin', '/127.0.0.1:50623', '', '', '', '1', '连接异常', '2022-06-09 10:51:19');
INSERT INTO `sys_logininfor` VALUES ('1475', 'admin', '/127.0.0.1:50623', '', '', '', '1', '退出登录', '2022-06-09 10:51:19');
INSERT INTO `sys_logininfor` VALUES ('1476', 'admin', '/127.0.0.1:50623', '', '', '', '1', '连接异常', '2022-06-09 10:51:19');
INSERT INTO `sys_logininfor` VALUES ('1477', 'admin', '/127.0.0.1:50684', '', '', '', '0', '登录成功！', '2022-06-09 10:55:12');
INSERT INTO `sys_logininfor` VALUES ('1478', 'admin', '/127.0.0.1:50684', '', '', '', '1', '连接异常', '2022-06-09 10:55:12');
INSERT INTO `sys_logininfor` VALUES ('1479', 'admin', '/127.0.0.1:50684', '', '', '', '1', '退出登录', '2022-06-09 10:55:12');
INSERT INTO `sys_logininfor` VALUES ('1480', 'admin', '/127.0.0.1:50684', '', '', '', '1', '连接异常', '2022-06-09 10:55:12');
INSERT INTO `sys_logininfor` VALUES ('1481', 'admin', '/127.0.0.1:50718', '', '', '', '0', '登录成功！', '2022-06-09 10:57:24');
INSERT INTO `sys_logininfor` VALUES ('1482', 'admin', '/127.0.0.1:50718', '', '', '', '1', '连接异常', '2022-06-09 10:57:24');
INSERT INTO `sys_logininfor` VALUES ('1483', 'admin', '/127.0.0.1:50718', '', '', '', '1', '退出登录', '2022-06-09 10:57:24');
INSERT INTO `sys_logininfor` VALUES ('1484', 'admin', '/127.0.0.1:50718', '', '', '', '1', '连接异常', '2022-06-09 10:57:24');
INSERT INTO `sys_logininfor` VALUES ('1485', 'admin', '/127.0.0.1:50761', '', '', '', '0', '登录成功！', '2022-06-09 10:59:43');
INSERT INTO `sys_logininfor` VALUES ('1486', 'admin', '/127.0.0.1:50761', '', '', '', '1', '连接异常', '2022-06-09 10:59:43');
INSERT INTO `sys_logininfor` VALUES ('1487', 'admin', '/127.0.0.1:50761', '', '', '', '1', '退出登录', '2022-06-09 10:59:43');
INSERT INTO `sys_logininfor` VALUES ('1488', 'admin', '/127.0.0.1:50761', '', '', '', '1', '连接异常', '2022-06-09 10:59:43');
INSERT INTO `sys_logininfor` VALUES ('1489', 'admin', '/127.0.0.1:50775', '', '', '', '0', '登录成功！', '2022-06-09 11:00:18');
INSERT INTO `sys_logininfor` VALUES ('1490', 'admin', '/127.0.0.1:50775', '', '', '', '1', '连接异常', '2022-06-09 11:00:18');
INSERT INTO `sys_logininfor` VALUES ('1491', 'admin', '/127.0.0.1:50775', '', '', '', '1', '退出登录', '2022-06-09 11:00:18');
INSERT INTO `sys_logininfor` VALUES ('1492', 'admin', '/127.0.0.1:50775', '', '', '', '1', '连接异常', '2022-06-09 11:00:18');
INSERT INTO `sys_logininfor` VALUES ('1493', 'admin', '/127.0.0.1:63933', '', '', '', '0', '登录成功！', '2022-06-09 11:01:04');
INSERT INTO `sys_logininfor` VALUES ('1494', 'admin', '/127.0.0.1:63933', '', '', '', '1', '连接异常', '2022-06-09 11:01:04');
INSERT INTO `sys_logininfor` VALUES ('1495', 'admin', '/127.0.0.1:63933', '', '', '', '1', '退出登录', '2022-06-09 11:01:04');
INSERT INTO `sys_logininfor` VALUES ('1496', 'admin', '/127.0.0.1:63933', '', '', '', '1', '连接异常', '2022-06-09 11:01:04');
INSERT INTO `sys_logininfor` VALUES ('1497', 'admin', '/127.0.0.1:52468', '', '', '', '0', '登录成功！', '2022-06-09 11:01:41');
INSERT INTO `sys_logininfor` VALUES ('1498', 'admin', '/127.0.0.1:52468', '', '', '', '1', '连接异常', '2022-06-09 11:01:41');
INSERT INTO `sys_logininfor` VALUES ('1499', 'admin', '/127.0.0.1:52468', '', '', '', '1', '退出登录', '2022-06-09 11:01:41');
INSERT INTO `sys_logininfor` VALUES ('1500', 'admin', '/127.0.0.1:52468', '', '', '', '1', '连接异常', '2022-06-09 11:01:41');
INSERT INTO `sys_logininfor` VALUES ('1501', 'admin', '/127.0.0.1:52489', '', '', '', '0', '登录成功！', '2022-06-09 11:02:30');
INSERT INTO `sys_logininfor` VALUES ('1502', 'admin', '/127.0.0.1:52489', '', '', '', '1', '连接异常', '2022-06-09 11:02:30');
INSERT INTO `sys_logininfor` VALUES ('1503', 'admin', '/127.0.0.1:52489', '', '', '', '1', '退出登录', '2022-06-09 11:02:30');
INSERT INTO `sys_logininfor` VALUES ('1504', 'admin', '/127.0.0.1:52489', '', '', '', '1', '连接异常', '2022-06-09 11:02:30');
INSERT INTO `sys_logininfor` VALUES ('1505', 'admin', '/127.0.0.1:52534', '', '', '', '0', '登录成功！', '2022-06-09 11:03:03');
INSERT INTO `sys_logininfor` VALUES ('1506', 'admin', '/127.0.0.1:52534', '', '', '', '1', '连接异常', '2022-06-09 11:03:03');
INSERT INTO `sys_logininfor` VALUES ('1507', 'admin', '/127.0.0.1:52534', '', '', '', '1', '退出登录', '2022-06-09 11:03:03');
INSERT INTO `sys_logininfor` VALUES ('1508', 'admin', '/127.0.0.1:52534', '', '', '', '1', '连接异常', '2022-06-09 11:03:03');
INSERT INTO `sys_logininfor` VALUES ('1509', 'admin', '/127.0.0.1:52625', '', '', '', '0', '登录成功！', '2022-06-09 11:04:40');
INSERT INTO `sys_logininfor` VALUES ('1510', 'admin', '/127.0.0.1:52625', '', '', '', '1', '退出登录', '2022-06-09 11:04:40');
INSERT INTO `sys_logininfor` VALUES ('1511', 'admin', '/127.0.0.1:53034', '', '', '', '0', '登录成功！', '2022-06-09 11:21:02');
INSERT INTO `sys_logininfor` VALUES ('1512', 'admin', '/127.0.0.1:53034', '', '', '', '1', '退出登录', '2022-06-09 11:21:02');
INSERT INTO `sys_logininfor` VALUES ('1513', 'admin', '/127.0.0.1:53057', '', '', '', '0', '登录成功！', '2022-06-09 11:22:25');
INSERT INTO `sys_logininfor` VALUES ('1514', 'admin', '/127.0.0.1:53057', '', '', '', '1', '连接异常', '2022-06-09 11:22:25');
INSERT INTO `sys_logininfor` VALUES ('1515', 'admin', '/127.0.0.1:53057', '', '', '', '1', '退出登录', '2022-06-09 11:22:25');
INSERT INTO `sys_logininfor` VALUES ('1516', 'admin', '/127.0.0.1:53057', '', '', '', '1', '连接异常', '2022-06-09 11:22:25');
INSERT INTO `sys_logininfor` VALUES ('1517', 'admin', '/127.0.0.1:53072', '', '', '', '0', '登录成功！', '2022-06-09 11:23:06');
INSERT INTO `sys_logininfor` VALUES ('1518', 'admin', '/127.0.0.1:53072', '', '', '', '1', '连接异常', '2022-06-09 11:23:06');
INSERT INTO `sys_logininfor` VALUES ('1519', 'admin', '/127.0.0.1:53072', '', '', '', '1', '退出登录', '2022-06-09 11:23:06');
INSERT INTO `sys_logininfor` VALUES ('1520', 'admin', '/127.0.0.1:53072', '', '', '', '1', '连接异常', '2022-06-09 11:23:06');
INSERT INTO `sys_logininfor` VALUES ('1521', 'admin', '/127.0.0.1:53688', '', '', '', '0', '登录成功！', '2022-06-09 11:30:51');
INSERT INTO `sys_logininfor` VALUES ('1522', 'admin', '/127.0.0.1:53688', '', '', '', '1', '退出登录', '2022-06-09 11:30:51');
INSERT INTO `sys_logininfor` VALUES ('1523', 'admin', '/127.0.0.1:55601', '', '', '', '0', '登录成功！', '2022-06-09 13:21:04');
INSERT INTO `sys_logininfor` VALUES ('1524', 'admin', '/127.0.0.1:55601', '', '', '', '1', '连接异常', '2022-06-09 13:21:04');
INSERT INTO `sys_logininfor` VALUES ('1525', 'admin', '/127.0.0.1:55601', '', '', '', '1', '退出登录', '2022-06-09 13:21:04');
INSERT INTO `sys_logininfor` VALUES ('1526', 'admin', '/127.0.0.1:55601', '', '', '', '1', '连接异常', '2022-06-09 13:21:04');
INSERT INTO `sys_logininfor` VALUES ('1527', 'admin', '/127.0.0.1:55620', '', '', '', '0', '登录成功！', '2022-06-09 13:23:09');
INSERT INTO `sys_logininfor` VALUES ('1528', 'admin', '/127.0.0.1:55620', '', '', '', '1', '连接异常', '2022-06-09 13:23:09');
INSERT INTO `sys_logininfor` VALUES ('1529', 'admin', '/127.0.0.1:55620', '', '', '', '1', '退出登录', '2022-06-09 13:23:09');
INSERT INTO `sys_logininfor` VALUES ('1530', 'admin', '/127.0.0.1:55620', '', '', '', '1', '连接异常', '2022-06-09 13:23:09');
INSERT INTO `sys_logininfor` VALUES ('1531', 'admin', '/127.0.0.1:55640', '', '', '', '0', '登录成功！', '2022-06-09 13:24:15');
INSERT INTO `sys_logininfor` VALUES ('1532', 'admin', '/127.0.0.1:55640', '', '', '', '1', '退出登录', '2022-06-09 13:24:15');
INSERT INTO `sys_logininfor` VALUES ('1533', 'admin', '/127.0.0.1:57784', '', '', '', '0', '登录成功！', '2022-06-09 15:34:29');
INSERT INTO `sys_logininfor` VALUES ('1534', 'admin', '/127.0.0.1:57784', '', '', '', '1', '连接异常', '2022-06-09 15:34:29');
INSERT INTO `sys_logininfor` VALUES ('1535', 'admin', '/127.0.0.1:57784', '', '', '', '1', '退出登录', '2022-06-09 15:34:29');
INSERT INTO `sys_logininfor` VALUES ('1536', 'admin', '/127.0.0.1:57784', '', '', '', '1', '连接异常', '2022-06-09 15:34:29');
INSERT INTO `sys_logininfor` VALUES ('1537', 'admin', '/127.0.0.1:57802', '', '', '', '0', '登录成功！', '2022-06-09 15:35:50');
INSERT INTO `sys_logininfor` VALUES ('1538', 'admin', '/127.0.0.1:57802', '', '', '', '1', '连接异常', '2022-06-09 15:35:50');
INSERT INTO `sys_logininfor` VALUES ('1539', 'admin', '/127.0.0.1:57802', '', '', '', '1', '退出登录', '2022-06-09 15:35:50');
INSERT INTO `sys_logininfor` VALUES ('1540', 'admin', '/127.0.0.1:57802', '', '', '', '1', '连接异常', '2022-06-09 15:35:50');
INSERT INTO `sys_logininfor` VALUES ('1541', 'admin', '/127.0.0.1:57820', '', '', '', '0', '登录成功！', '2022-06-09 15:37:53');
INSERT INTO `sys_logininfor` VALUES ('1542', 'admin', '/127.0.0.1:57820', '', '', '', '1', '连接异常', '2022-06-09 15:37:53');
INSERT INTO `sys_logininfor` VALUES ('1543', 'admin', '/127.0.0.1:57820', '', '', '', '1', '退出登录', '2022-06-09 15:37:53');
INSERT INTO `sys_logininfor` VALUES ('1544', 'admin', '/127.0.0.1:57820', '', '', '', '1', '连接异常', '2022-06-09 15:37:53');
INSERT INTO `sys_logininfor` VALUES ('1545', 'admin', '/127.0.0.1:57836', '', '', '', '0', '登录成功！', '2022-06-09 15:38:53');
INSERT INTO `sys_logininfor` VALUES ('1546', 'admin', '/127.0.0.1:57836', '', '', '', '1', '连接异常', '2022-06-09 15:38:53');
INSERT INTO `sys_logininfor` VALUES ('1547', 'admin', '/127.0.0.1:57836', '', '', '', '1', '退出登录', '2022-06-09 15:38:53');
INSERT INTO `sys_logininfor` VALUES ('1548', 'admin', '/127.0.0.1:57836', '', '', '', '1', '连接异常', '2022-06-09 15:38:53');
INSERT INTO `sys_logininfor` VALUES ('1549', 'admin', '/127.0.0.1:58525', '', '', '', '0', '登录成功！', '2022-06-09 16:03:00');
INSERT INTO `sys_logininfor` VALUES ('1550', 'admin', '/127.0.0.1:58525', '', '', '', '1', '连接异常', '2022-06-09 16:03:00');
INSERT INTO `sys_logininfor` VALUES ('1551', 'admin', '/127.0.0.1:58525', '', '', '', '1', '退出登录', '2022-06-09 16:03:00');
INSERT INTO `sys_logininfor` VALUES ('1552', 'admin', '/127.0.0.1:58525', '', '', '', '1', '连接异常', '2022-06-09 16:03:00');
INSERT INTO `sys_logininfor` VALUES ('1553', 'admin', '/127.0.0.1:58544', '', '', '', '0', '登录成功！', '2022-06-09 16:05:54');
INSERT INTO `sys_logininfor` VALUES ('1554', 'admin', '/127.0.0.1:58544', '', '', '', '1', '连接异常', '2022-06-09 16:05:54');
INSERT INTO `sys_logininfor` VALUES ('1555', 'admin', '/127.0.0.1:58544', '', '', '', '1', '退出登录', '2022-06-09 16:05:54');
INSERT INTO `sys_logininfor` VALUES ('1556', 'admin', '/127.0.0.1:58544', '', '', '', '1', '连接异常', '2022-06-09 16:05:54');
INSERT INTO `sys_logininfor` VALUES ('1557', 'admin', '/127.0.0.1:58584', '', '', '', '0', '登录成功！', '2022-06-09 16:08:21');
INSERT INTO `sys_logininfor` VALUES ('1558', 'admin', '/127.0.0.1:58584', '', '', '', '1', '连接异常', '2022-06-09 16:08:21');
INSERT INTO `sys_logininfor` VALUES ('1559', 'admin', '/127.0.0.1:58584', '', '', '', '1', '退出登录', '2022-06-09 16:08:21');
INSERT INTO `sys_logininfor` VALUES ('1560', 'admin', '/127.0.0.1:58584', '', '', '', '1', '连接异常', '2022-06-09 16:08:21');
INSERT INTO `sys_logininfor` VALUES ('1561', 'admin', '/127.0.0.1:58598', '', '', '', '0', '登录成功！', '2022-06-09 16:11:06');
INSERT INTO `sys_logininfor` VALUES ('1562', 'admin', '/127.0.0.1:58598', '', '', '', '1', '连接异常', '2022-06-09 16:11:06');
INSERT INTO `sys_logininfor` VALUES ('1563', 'admin', '/127.0.0.1:58598', '', '', '', '1', '退出登录', '2022-06-09 16:11:06');
INSERT INTO `sys_logininfor` VALUES ('1564', 'admin', '/127.0.0.1:58598', '', '', '', '1', '连接异常', '2022-06-09 16:11:06');
INSERT INTO `sys_logininfor` VALUES ('1565', 'admin', '/127.0.0.1:58622', '', '', '', '0', '登录成功！', '2022-06-09 16:14:23');
INSERT INTO `sys_logininfor` VALUES ('1566', 'admin', '/127.0.0.1:58622', '', '', '', '1', '连接异常', '2022-06-09 16:14:23');
INSERT INTO `sys_logininfor` VALUES ('1567', 'admin', '/127.0.0.1:58622', '', '', '', '1', '退出登录', '2022-06-09 16:14:23');
INSERT INTO `sys_logininfor` VALUES ('1568', 'admin', '/127.0.0.1:58622', '', '', '', '1', '连接异常', '2022-06-09 16:14:23');
INSERT INTO `sys_logininfor` VALUES ('1569', 'admin', '/127.0.0.1:58633', '', '', '', '0', '登录成功！', '2022-06-09 16:15:18');
INSERT INTO `sys_logininfor` VALUES ('1570', 'admin', '/127.0.0.1:58633', '', '', '', '1', '连接异常', '2022-06-09 16:15:18');
INSERT INTO `sys_logininfor` VALUES ('1571', 'admin', '/127.0.0.1:58633', '', '', '', '1', '退出登录', '2022-06-09 16:15:18');
INSERT INTO `sys_logininfor` VALUES ('1572', 'admin', '/127.0.0.1:58633', '', '', '', '1', '连接异常', '2022-06-09 16:15:18');
INSERT INTO `sys_logininfor` VALUES ('1573', 'admin', '/127.0.0.1:58645', '', '', '', '0', '登录成功！', '2022-06-09 16:16:35');
INSERT INTO `sys_logininfor` VALUES ('1574', 'admin', '/127.0.0.1:58645', '', '', '', '1', '连接异常', '2022-06-09 16:16:35');
INSERT INTO `sys_logininfor` VALUES ('1575', 'admin', '/127.0.0.1:58645', '', '', '', '1', '退出登录', '2022-06-09 16:16:35');
INSERT INTO `sys_logininfor` VALUES ('1576', 'admin', '/127.0.0.1:58645', '', '', '', '1', '连接异常', '2022-06-09 16:16:35');
INSERT INTO `sys_logininfor` VALUES ('1577', 'admin', '/127.0.0.1:58666', '', '', '', '0', '登录成功！', '2022-06-09 16:19:56');
INSERT INTO `sys_logininfor` VALUES ('1578', 'admin', '/127.0.0.1:58666', '', '', '', '1', '退出登录', '2022-06-09 16:19:56');
INSERT INTO `sys_logininfor` VALUES ('1579', 'admin', '/127.0.0.1:58680', '', '', '', '0', '登录成功！', '2022-06-09 16:21:02');
INSERT INTO `sys_logininfor` VALUES ('1580', 'admin', '/127.0.0.1:58680', '', '', '', '1', '退出登录', '2022-06-09 16:21:02');
INSERT INTO `sys_logininfor` VALUES ('1581', 'admin', '/127.0.0.1:58681', '', '', '', '0', '登录成功！', '2022-06-09 16:21:08');
INSERT INTO `sys_logininfor` VALUES ('1582', 'admin', '/127.0.0.1:58681', '', '', '', '1', '连接异常', '2022-06-09 16:21:08');
INSERT INTO `sys_logininfor` VALUES ('1583', 'admin', '/127.0.0.1:58681', '', '', '', '1', '退出登录', '2022-06-09 16:21:08');
INSERT INTO `sys_logininfor` VALUES ('1584', 'admin', '/127.0.0.1:58681', '', '', '', '1', '连接异常', '2022-06-09 16:21:08');
INSERT INTO `sys_logininfor` VALUES ('1585', 'admin', '/127.0.0.1:58698', '', '', '', '0', '登录成功！', '2022-06-09 16:23:03');
INSERT INTO `sys_logininfor` VALUES ('1586', 'admin', '/127.0.0.1:58698', '', '', '', '1', '退出登录', '2022-06-09 16:23:03');
INSERT INTO `sys_logininfor` VALUES ('1587', 'admin', '/127.0.0.1:58699', '', '', '', '0', '登录成功！', '2022-06-09 16:23:08');
INSERT INTO `sys_logininfor` VALUES ('1588', 'admin', '/127.0.0.1:58699', '', '', '', '1', '连接异常', '2022-06-09 16:23:08');
INSERT INTO `sys_logininfor` VALUES ('1589', 'admin', '/127.0.0.1:58699', '', '', '', '1', '退出登录', '2022-06-09 16:23:08');
INSERT INTO `sys_logininfor` VALUES ('1590', 'admin', '/127.0.0.1:58699', '', '', '', '1', '连接异常', '2022-06-09 16:23:08');
INSERT INTO `sys_logininfor` VALUES ('1591', 'admin', '/127.0.0.1:58711', '', '', '', '0', '登录成功！', '2022-06-09 16:24:52');
INSERT INTO `sys_logininfor` VALUES ('1592', 'admin', '/127.0.0.1:58711', '', '', '', '1', '连接异常', '2022-06-09 16:24:52');
INSERT INTO `sys_logininfor` VALUES ('1593', 'admin', '/127.0.0.1:58711', '', '', '', '1', '退出登录', '2022-06-09 16:24:52');
INSERT INTO `sys_logininfor` VALUES ('1594', 'admin', '/127.0.0.1:58711', '', '', '', '1', '连接异常', '2022-06-09 16:24:52');
INSERT INTO `sys_logininfor` VALUES ('1595', 'admin', '/127.0.0.1:58727', '', '', '', '0', '登录成功！', '2022-06-09 16:26:31');
INSERT INTO `sys_logininfor` VALUES ('1596', 'admin', '/127.0.0.1:58727', '', '', '', '1', '连接异常', '2022-06-09 16:26:31');
INSERT INTO `sys_logininfor` VALUES ('1597', 'admin', '/127.0.0.1:58727', '', '', '', '1', '退出登录', '2022-06-09 16:26:31');
INSERT INTO `sys_logininfor` VALUES ('1598', 'admin', '/127.0.0.1:58727', '', '', '', '1', '连接异常', '2022-06-09 16:26:31');
INSERT INTO `sys_logininfor` VALUES ('1599', 'admin', '/127.0.0.1:58796', '', '', '', '0', '登录成功！', '2022-06-09 16:31:14');
INSERT INTO `sys_logininfor` VALUES ('1600', 'admin', '/127.0.0.1:58796', '', '', '', '1', '连接异常', '2022-06-09 16:31:14');
INSERT INTO `sys_logininfor` VALUES ('1601', 'admin', '/127.0.0.1:58796', '', '', '', '1', '退出登录', '2022-06-09 16:31:14');
INSERT INTO `sys_logininfor` VALUES ('1602', 'admin', '/127.0.0.1:58796', '', '', '', '1', '连接异常', '2022-06-09 16:31:14');
INSERT INTO `sys_logininfor` VALUES ('1603', 'admin', '/127.0.0.1:58911', '', '', '', '0', '登录成功！', '2022-06-09 16:34:20');
INSERT INTO `sys_logininfor` VALUES ('1604', 'admin', '/127.0.0.1:58911', '', '', '', '1', '连接异常', '2022-06-09 16:34:20');
INSERT INTO `sys_logininfor` VALUES ('1605', 'admin', '/127.0.0.1:58911', '', '', '', '1', '退出登录', '2022-06-09 16:34:20');
INSERT INTO `sys_logininfor` VALUES ('1606', 'admin', '/127.0.0.1:58911', '', '', '', '1', '连接异常', '2022-06-09 16:34:20');
INSERT INTO `sys_logininfor` VALUES ('1607', 'admin', '/127.0.0.1:58941', '', '', '', '0', '登录成功！', '2022-06-09 16:35:13');
INSERT INTO `sys_logininfor` VALUES ('1608', 'admin', '/127.0.0.1:58941', '', '', '', '1', '连接异常', '2022-06-09 16:35:13');
INSERT INTO `sys_logininfor` VALUES ('1609', 'admin', '/127.0.0.1:58941', '', '', '', '1', '退出登录', '2022-06-09 16:35:13');
INSERT INTO `sys_logininfor` VALUES ('1610', 'admin', '/127.0.0.1:58941', '', '', '', '1', '连接异常', '2022-06-09 16:35:13');
INSERT INTO `sys_logininfor` VALUES ('1611', 'admin', '/127.0.0.1:58955', '', '', '', '0', '登录成功！', '2022-06-09 16:36:13');
INSERT INTO `sys_logininfor` VALUES ('1612', 'admin', '/127.0.0.1:58955', '', '', '', '1', '连接异常', '2022-06-09 16:36:13');
INSERT INTO `sys_logininfor` VALUES ('1613', 'admin', '/127.0.0.1:58955', '', '', '', '1', '退出登录', '2022-06-09 16:36:13');
INSERT INTO `sys_logininfor` VALUES ('1614', 'admin', '/127.0.0.1:58955', '', '', '', '1', '连接异常', '2022-06-09 16:36:13');
INSERT INTO `sys_logininfor` VALUES ('1615', 'admin', '/127.0.0.1:58966', '', '', '', '0', '登录成功！', '2022-06-09 16:36:48');
INSERT INTO `sys_logininfor` VALUES ('1616', 'admin', '/127.0.0.1:58966', '', '', '', '1', '连接异常', '2022-06-09 16:36:48');
INSERT INTO `sys_logininfor` VALUES ('1617', 'admin', '/127.0.0.1:58966', '', '', '', '1', '退出登录', '2022-06-09 16:36:48');
INSERT INTO `sys_logininfor` VALUES ('1618', 'admin', '/127.0.0.1:58966', '', '', '', '1', '连接异常', '2022-06-09 16:36:48');
INSERT INTO `sys_logininfor` VALUES ('1619', 'admin', '/127.0.0.1:58979', '', '', '', '0', '登录成功！', '2022-06-09 16:37:34');
INSERT INTO `sys_logininfor` VALUES ('1620', 'admin', '/127.0.0.1:58979', '', '', '', '1', '连接异常', '2022-06-09 16:37:34');
INSERT INTO `sys_logininfor` VALUES ('1621', 'admin', '/127.0.0.1:58979', '', '', '', '1', '退出登录', '2022-06-09 16:37:34');
INSERT INTO `sys_logininfor` VALUES ('1622', 'admin', '/127.0.0.1:58979', '', '', '', '1', '连接异常', '2022-06-09 16:37:34');
INSERT INTO `sys_logininfor` VALUES ('1623', 'admin', '/127.0.0.1:58996', '', '', '', '0', '登录成功！', '2022-06-09 16:38:11');
INSERT INTO `sys_logininfor` VALUES ('1624', 'admin', '/127.0.0.1:58996', '', '', '', '1', '连接异常', '2022-06-09 16:38:11');
INSERT INTO `sys_logininfor` VALUES ('1625', 'admin', '/127.0.0.1:58996', '', '', '', '1', '退出登录', '2022-06-09 16:38:11');
INSERT INTO `sys_logininfor` VALUES ('1626', 'admin', '/127.0.0.1:58996', '', '', '', '1', '连接异常', '2022-06-09 16:38:11');
INSERT INTO `sys_logininfor` VALUES ('1627', 'admin', '/127.0.0.1:59008', '', '', '', '0', '登录成功！', '2022-06-09 16:39:22');
INSERT INTO `sys_logininfor` VALUES ('1628', 'admin', '/127.0.0.1:59008', '', '', '', '1', '退出登录', '2022-06-09 16:39:22');
INSERT INTO `sys_logininfor` VALUES ('1629', 'admin', '/127.0.0.1:59009', '', '', '', '0', '登录成功！', '2022-06-09 16:39:29');
INSERT INTO `sys_logininfor` VALUES ('1630', 'admin', '/127.0.0.1:59009', '', '', '', '1', '退出登录', '2022-06-09 16:39:29');
INSERT INTO `sys_logininfor` VALUES ('1631', 'admin', '/127.0.0.1:59165', '', '', '', '0', '登录成功！', '2022-06-09 17:06:25');
INSERT INTO `sys_logininfor` VALUES ('1632', 'admin', '/127.0.0.1:59165', '', '', '', '1', '连接异常', '2022-06-09 17:06:25');
INSERT INTO `sys_logininfor` VALUES ('1633', 'admin', '/127.0.0.1:59165', '', '', '', '1', '退出登录', '2022-06-09 17:06:25');
INSERT INTO `sys_logininfor` VALUES ('1634', 'admin', '/127.0.0.1:59165', '', '', '', '1', '连接异常', '2022-06-09 17:06:25');
INSERT INTO `sys_logininfor` VALUES ('1635', 'admin', '/127.0.0.1:59251', '', '', '', '0', '登录成功！', '2022-06-09 17:15:07');
INSERT INTO `sys_logininfor` VALUES ('1636', 'admin', '/127.0.0.1:59251', '', '', '', '1', '退出登录', '2022-06-09 17:15:07');
INSERT INTO `sys_logininfor` VALUES ('1637', 'admin', '/127.0.0.1:59252', '', '', '', '0', '登录成功！', '2022-06-09 17:15:23');
INSERT INTO `sys_logininfor` VALUES ('1638', 'admin', '/127.0.0.1:59252', '', '', '', '1', '连接异常', '2022-06-09 17:15:23');
INSERT INTO `sys_logininfor` VALUES ('1639', 'admin', '/127.0.0.1:59252', '', '', '', '1', '退出登录', '2022-06-09 17:15:23');
INSERT INTO `sys_logininfor` VALUES ('1640', 'admin', '/127.0.0.1:59252', '', '', '', '1', '连接异常', '2022-06-09 17:15:23');
INSERT INTO `sys_logininfor` VALUES ('1641', 'admin', '/127.0.0.1:59264', '', '', '', '0', '登录成功！', '2022-06-09 17:16:04');
INSERT INTO `sys_logininfor` VALUES ('1642', 'admin', '/127.0.0.1:59264', '', '', '', '1', '退出登录', '2022-06-09 17:16:04');
INSERT INTO `sys_logininfor` VALUES ('1643', 'admin', '/127.0.0.1:59277', '', '', '', '0', '登录成功！', '2022-06-09 17:16:59');
INSERT INTO `sys_logininfor` VALUES ('1644', 'admin', '/127.0.0.1:59277', '', '', '', '1', '退出登录', '2022-06-09 17:16:59');
INSERT INTO `sys_logininfor` VALUES ('1645', 'admin', '/127.0.0.1:64192', '', '', '', '0', '登录成功！', '2022-06-13 10:45:50');
INSERT INTO `sys_logininfor` VALUES ('1646', 'admin', '/127.0.0.1:64192', '', '', '', '1', '退出登录', '2022-06-13 10:45:50');
INSERT INTO `sys_logininfor` VALUES ('1647', 'admin', '/127.0.0.1:64323', '', '', '', '0', '登录成功！', '2022-06-13 10:57:14');
INSERT INTO `sys_logininfor` VALUES ('1648', 'admin', '/127.0.0.1:64323', '', '', '', '1', '连接异常', '2022-06-13 10:57:14');
INSERT INTO `sys_logininfor` VALUES ('1649', 'admin', '/127.0.0.1:64323', '', '', '', '1', '退出登录', '2022-06-13 10:57:14');
INSERT INTO `sys_logininfor` VALUES ('1650', 'admin', '/127.0.0.1:64323', '', '', '', '1', '连接异常', '2022-06-13 10:57:14');
INSERT INTO `sys_logininfor` VALUES ('1651', 'admin', '/127.0.0.1:64376', '', '', '', '0', '登录成功！', '2022-06-13 11:02:57');
INSERT INTO `sys_logininfor` VALUES ('1652', 'admin', '/127.0.0.1:64376', '', '', '', '1', '连接异常', '2022-06-13 11:02:57');
INSERT INTO `sys_logininfor` VALUES ('1653', 'admin', '/127.0.0.1:64376', '', '', '', '1', '退出登录', '2022-06-13 11:02:57');
INSERT INTO `sys_logininfor` VALUES ('1654', 'admin', '/127.0.0.1:64376', '', '', '', '1', '连接异常', '2022-06-13 11:02:57');
INSERT INTO `sys_logininfor` VALUES ('1655', 'admin', '/127.0.0.1:64392', '', '', '', '0', '登录成功！', '2022-06-13 11:03:32');
INSERT INTO `sys_logininfor` VALUES ('1656', 'admin', '/127.0.0.1:64392', '', '', '', '1', '连接异常', '2022-06-13 11:03:32');
INSERT INTO `sys_logininfor` VALUES ('1657', 'admin', '/127.0.0.1:64392', '', '', '', '1', '退出登录', '2022-06-13 11:03:32');
INSERT INTO `sys_logininfor` VALUES ('1658', 'admin', '/127.0.0.1:64392', '', '', '', '1', '连接异常', '2022-06-13 11:03:32');
INSERT INTO `sys_logininfor` VALUES ('1659', 'admin', '/127.0.0.1:64407', '', '', '', '0', '登录成功！', '2022-06-13 11:04:10');
INSERT INTO `sys_logininfor` VALUES ('1660', 'admin', '/127.0.0.1:64407', '', '', '', '1', '连接异常', '2022-06-13 11:04:10');
INSERT INTO `sys_logininfor` VALUES ('1661', 'admin', '/127.0.0.1:64407', '', '', '', '1', '退出登录', '2022-06-13 11:04:10');
INSERT INTO `sys_logininfor` VALUES ('1662', 'admin', '/127.0.0.1:64407', '', '', '', '1', '连接异常', '2022-06-13 11:04:10');
INSERT INTO `sys_logininfor` VALUES ('1663', 'admin', '/127.0.0.1:64452', '', '', '', '0', '登录成功！', '2022-06-13 11:07:11');
INSERT INTO `sys_logininfor` VALUES ('1664', 'admin', '/127.0.0.1:64452', '', '', '', '1', '连接异常', '2022-06-13 11:07:11');
INSERT INTO `sys_logininfor` VALUES ('1665', 'admin', '/127.0.0.1:64452', '', '', '', '1', '退出登录', '2022-06-13 11:07:11');
INSERT INTO `sys_logininfor` VALUES ('1666', 'admin', '/127.0.0.1:64452', '', '', '', '1', '连接异常', '2022-06-13 11:07:11');
INSERT INTO `sys_logininfor` VALUES ('1667', 'admin', '/127.0.0.1:64467', '', '', '', '0', '登录成功！', '2022-06-13 11:08:29');
INSERT INTO `sys_logininfor` VALUES ('1668', 'admin', '/127.0.0.1:64469', '', '', '', '0', '登录成功！', '2022-06-13 11:08:52');
INSERT INTO `sys_logininfor` VALUES ('1669', 'admin', '/127.0.0.1:64469', '', '', '', '1', '连接异常', '2022-06-13 11:08:52');
INSERT INTO `sys_logininfor` VALUES ('1670', 'admin', '/127.0.0.1:64469', '', '', '', '1', '退出登录', '2022-06-13 11:08:52');
INSERT INTO `sys_logininfor` VALUES ('1671', 'admin', '/127.0.0.1:64467', '', '', '', '1', '连接异常', '2022-06-13 11:08:29');
INSERT INTO `sys_logininfor` VALUES ('1672', 'admin', '/127.0.0.1:64467', '', '', '', '1', '退出登录', '2022-06-13 11:08:29');
INSERT INTO `sys_logininfor` VALUES ('1673', 'admin', '/127.0.0.1:64469', '', '', '', '1', '连接异常', '2022-06-13 11:08:52');
INSERT INTO `sys_logininfor` VALUES ('1674', 'admin', '/127.0.0.1:64467', '', '', '', '1', '连接异常', '2022-06-13 11:08:29');
INSERT INTO `sys_logininfor` VALUES ('1675', 'admin', '/127.0.0.1:64483', '', '', '', '0', '登录成功！', '2022-06-13 11:09:47');
INSERT INTO `sys_logininfor` VALUES ('1676', 'admin', '/127.0.0.1:64483', '', '', '', '1', '连接异常', '2022-06-13 11:09:47');
INSERT INTO `sys_logininfor` VALUES ('1677', 'admin', '/127.0.0.1:64483', '', '', '', '1', '退出登录', '2022-06-13 11:09:47');
INSERT INTO `sys_logininfor` VALUES ('1678', 'admin', '/127.0.0.1:64483', '', '', '', '1', '连接异常', '2022-06-13 11:09:47');
INSERT INTO `sys_logininfor` VALUES ('1679', 'admin', '/127.0.0.1:64495', '', '', '', '0', '登录成功！', '2022-06-13 11:10:08');
INSERT INTO `sys_logininfor` VALUES ('1680', 'admin', '/127.0.0.1:64495', '', '', '', '1', '连接异常', '2022-06-13 11:10:08');
INSERT INTO `sys_logininfor` VALUES ('1681', 'admin', '/127.0.0.1:64495', '', '', '', '1', '退出登录', '2022-06-13 11:10:08');
INSERT INTO `sys_logininfor` VALUES ('1682', 'admin', '/127.0.0.1:64495', '', '', '', '1', '连接异常', '2022-06-13 11:10:08');
INSERT INTO `sys_logininfor` VALUES ('1683', 'admin', '/127.0.0.1:64513', '', '', '', '0', '登录成功！', '2022-06-13 11:11:18');
INSERT INTO `sys_logininfor` VALUES ('1684', 'admin', '/127.0.0.1:64513', '', '', '', '1', '连接异常', '2022-06-13 11:11:18');
INSERT INTO `sys_logininfor` VALUES ('1685', 'admin', '/127.0.0.1:64513', '', '', '', '1', '退出登录', '2022-06-13 11:11:18');
INSERT INTO `sys_logininfor` VALUES ('1686', 'admin', '/127.0.0.1:64513', '', '', '', '1', '连接异常', '2022-06-13 11:11:18');
INSERT INTO `sys_logininfor` VALUES ('1687', 'admin', '/127.0.0.1:64527', '', '', '', '0', '登录成功！', '2022-06-13 11:11:34');
INSERT INTO `sys_logininfor` VALUES ('1688', 'admin', '/127.0.0.1:64527', '', '', '', '1', '连接异常', '2022-06-13 11:11:34');
INSERT INTO `sys_logininfor` VALUES ('1689', 'admin', '/127.0.0.1:64527', '', '', '', '1', '退出登录', '2022-06-13 11:11:34');
INSERT INTO `sys_logininfor` VALUES ('1690', 'admin', '/127.0.0.1:64527', '', '', '', '1', '连接异常', '2022-06-13 11:11:34');
INSERT INTO `sys_logininfor` VALUES ('1691', 'admin', '/127.0.0.1:64559', '', '', '', '0', '登录成功！', '2022-06-13 11:14:04');
INSERT INTO `sys_logininfor` VALUES ('1692', 'admin', '/127.0.0.1:64559', '', '', '', '1', '连接异常', '2022-06-13 11:14:04');
INSERT INTO `sys_logininfor` VALUES ('1693', 'admin', '/127.0.0.1:64559', '', '', '', '1', '退出登录', '2022-06-13 11:14:04');
INSERT INTO `sys_logininfor` VALUES ('1694', 'admin', '/127.0.0.1:64559', '', '', '', '1', '连接异常', '2022-06-13 11:14:04');
INSERT INTO `sys_logininfor` VALUES ('1695', 'admin', '/127.0.0.1:64573', '', '', '', '0', '登录成功！', '2022-06-13 11:15:13');
INSERT INTO `sys_logininfor` VALUES ('1696', 'admin', '/127.0.0.1:64573', '', '', '', '1', '连接异常', '2022-06-13 11:15:13');
INSERT INTO `sys_logininfor` VALUES ('1697', 'admin', '/127.0.0.1:64573', '', '', '', '1', '退出登录', '2022-06-13 11:15:13');
INSERT INTO `sys_logininfor` VALUES ('1698', 'admin', '/127.0.0.1:64573', '', '', '', '1', '连接异常', '2022-06-13 11:15:13');
INSERT INTO `sys_logininfor` VALUES ('1699', 'admin', '/127.0.0.1:64584', '', '', '', '0', '登录成功！', '2022-06-13 11:16:03');
INSERT INTO `sys_logininfor` VALUES ('1700', 'admin', '/127.0.0.1:64584', '', '', '', '1', '连接异常', '2022-06-13 11:16:03');
INSERT INTO `sys_logininfor` VALUES ('1701', 'admin', '/127.0.0.1:64584', '', '', '', '1', '退出登录', '2022-06-13 11:16:03');
INSERT INTO `sys_logininfor` VALUES ('1702', 'admin', '/127.0.0.1:64584', '', '', '', '1', '连接异常', '2022-06-13 11:16:03');
INSERT INTO `sys_logininfor` VALUES ('1703', 'admin', '/127.0.0.1:64599', '', '', '', '0', '登录成功！', '2022-06-13 11:16:46');
INSERT INTO `sys_logininfor` VALUES ('1704', 'admin', '/127.0.0.1:64599', '', '', '', '1', '连接异常', '2022-06-13 11:16:46');
INSERT INTO `sys_logininfor` VALUES ('1705', 'admin', '/127.0.0.1:64599', '', '', '', '1', '退出登录', '2022-06-13 11:16:46');
INSERT INTO `sys_logininfor` VALUES ('1706', 'admin', '/127.0.0.1:64599', '', '', '', '1', '连接异常', '2022-06-13 11:16:46');
INSERT INTO `sys_logininfor` VALUES ('1707', 'admin', '/127.0.0.1:64619', '', '', '', '0', '登录成功！', '2022-06-13 11:17:57');
INSERT INTO `sys_logininfor` VALUES ('1708', 'admin', '/127.0.0.1:64619', '', '', '', '1', '连接异常', '2022-06-13 11:17:57');
INSERT INTO `sys_logininfor` VALUES ('1709', 'admin', '/127.0.0.1:64619', '', '', '', '1', '退出登录', '2022-06-13 11:17:57');
INSERT INTO `sys_logininfor` VALUES ('1710', 'admin', '/127.0.0.1:64619', '', '', '', '1', '连接异常', '2022-06-13 11:17:57');
INSERT INTO `sys_logininfor` VALUES ('1711', 'admin', '/127.0.0.1:64641', '', '', '', '0', '登录成功！', '2022-06-13 11:20:14');
INSERT INTO `sys_logininfor` VALUES ('1712', 'admin', '/127.0.0.1:64641', '', '', '', '1', '连接异常', '2022-06-13 11:20:14');
INSERT INTO `sys_logininfor` VALUES ('1713', 'admin', '/127.0.0.1:64641', '', '', '', '1', '退出登录', '2022-06-13 11:20:14');
INSERT INTO `sys_logininfor` VALUES ('1714', 'admin', '/127.0.0.1:64641', '', '', '', '1', '连接异常', '2022-06-13 11:20:14');
INSERT INTO `sys_logininfor` VALUES ('1715', 'admin', '/127.0.0.1:64652', '', '', '', '0', '登录成功！', '2022-06-13 11:20:43');
INSERT INTO `sys_logininfor` VALUES ('1716', 'admin', '/127.0.0.1:64652', '', '', '', '1', '连接异常', '2022-06-13 11:20:43');
INSERT INTO `sys_logininfor` VALUES ('1717', 'admin', '/127.0.0.1:64652', '', '', '', '1', '退出登录', '2022-06-13 11:20:43');
INSERT INTO `sys_logininfor` VALUES ('1718', 'admin', '/127.0.0.1:64652', '', '', '', '1', '连接异常', '2022-06-13 11:20:43');
INSERT INTO `sys_logininfor` VALUES ('1719', 'admin', '/127.0.0.1:64805', '', '', '', '0', '登录成功！', '2022-06-13 11:23:35');
INSERT INTO `sys_logininfor` VALUES ('1720', 'admin', '/127.0.0.1:64805', '', '', '', '1', '连接异常', '2022-06-13 11:23:35');
INSERT INTO `sys_logininfor` VALUES ('1721', 'admin', '/127.0.0.1:64805', '', '', '', '1', '退出登录', '2022-06-13 11:23:35');
INSERT INTO `sys_logininfor` VALUES ('1722', 'admin', '/127.0.0.1:64805', '', '', '', '1', '连接异常', '2022-06-13 11:23:35');
INSERT INTO `sys_logininfor` VALUES ('1723', 'admin', '/127.0.0.1:64820', '', '', '', '0', '登录成功！', '2022-06-13 11:24:45');
INSERT INTO `sys_logininfor` VALUES ('1724', 'admin', '/127.0.0.1:64820', '', '', '', '1', '连接异常', '2022-06-13 11:24:45');
INSERT INTO `sys_logininfor` VALUES ('1725', 'admin', '/127.0.0.1:64820', '', '', '', '1', '退出登录', '2022-06-13 11:24:45');
INSERT INTO `sys_logininfor` VALUES ('1726', 'admin', '/127.0.0.1:64820', '', '', '', '1', '连接异常', '2022-06-13 11:24:45');
INSERT INTO `sys_logininfor` VALUES ('1727', 'admin', '/127.0.0.1:64841', '', '', '', '0', '登录成功！', '2022-06-13 11:26:22');
INSERT INTO `sys_logininfor` VALUES ('1728', 'admin', '/127.0.0.1:64841', '', '', '', '1', '连接异常', '2022-06-13 11:26:22');
INSERT INTO `sys_logininfor` VALUES ('1729', 'admin', '/127.0.0.1:64841', '', '', '', '1', '退出登录', '2022-06-13 11:26:22');
INSERT INTO `sys_logininfor` VALUES ('1730', 'admin', '/127.0.0.1:64841', '', '', '', '1', '连接异常', '2022-06-13 11:26:22');
INSERT INTO `sys_logininfor` VALUES ('1731', 'admin', '/127.0.0.1:64857', '', '', '', '0', '登录成功！', '2022-06-13 11:27:05');
INSERT INTO `sys_logininfor` VALUES ('1732', 'admin', '/127.0.0.1:64857', '', '', '', '1', '连接异常', '2022-06-13 11:27:05');
INSERT INTO `sys_logininfor` VALUES ('1733', 'admin', '/127.0.0.1:64857', '', '', '', '1', '退出登录', '2022-06-13 11:27:05');
INSERT INTO `sys_logininfor` VALUES ('1734', 'admin', '/127.0.0.1:64857', '', '', '', '1', '连接异常', '2022-06-13 11:27:05');
INSERT INTO `sys_logininfor` VALUES ('1735', 'admin', '/127.0.0.1:64867', '', '', '', '0', '登录成功！', '2022-06-13 11:27:19');
INSERT INTO `sys_logininfor` VALUES ('1736', 'admin', '/127.0.0.1:64867', '', '', '', '1', '连接异常', '2022-06-13 11:27:19');
INSERT INTO `sys_logininfor` VALUES ('1737', 'admin', '/127.0.0.1:64867', '', '', '', '1', '退出登录', '2022-06-13 11:27:19');
INSERT INTO `sys_logininfor` VALUES ('1738', 'admin', '/127.0.0.1:64867', '', '', '', '1', '连接异常', '2022-06-13 11:27:19');
INSERT INTO `sys_logininfor` VALUES ('1739', 'admin', '/127.0.0.1:49324', '', '', '', '0', '登录成功！', '2022-06-13 11:49:15');
INSERT INTO `sys_logininfor` VALUES ('1740', 'admin', '/127.0.0.1:49324', '', '', '', '1', '连接异常', '2022-06-13 11:49:15');
INSERT INTO `sys_logininfor` VALUES ('1741', 'admin', '/127.0.0.1:49324', '', '', '', '1', '退出登录', '2022-06-13 11:49:15');
INSERT INTO `sys_logininfor` VALUES ('1742', 'admin', '/127.0.0.1:49324', '', '', '', '1', '连接异常', '2022-06-13 11:49:15');
INSERT INTO `sys_logininfor` VALUES ('1743', 'admin', '/127.0.0.1:49351', '', '', '', '0', '登录成功！', '2022-06-13 11:52:21');
INSERT INTO `sys_logininfor` VALUES ('1744', 'admin', '/127.0.0.1:49351', '', '', '', '1', '连接异常', '2022-06-13 11:52:21');
INSERT INTO `sys_logininfor` VALUES ('1745', 'admin', '/127.0.0.1:49351', '', '', '', '1', '退出登录', '2022-06-13 11:52:21');
INSERT INTO `sys_logininfor` VALUES ('1746', 'admin', '/127.0.0.1:49351', '', '', '', '1', '连接异常', '2022-06-13 11:52:21');
INSERT INTO `sys_logininfor` VALUES ('1747', 'admin', '/127.0.0.1:49372', '', '', '', '0', '登录成功！', '2022-06-13 11:53:26');
INSERT INTO `sys_logininfor` VALUES ('1748', 'admin', '/127.0.0.1:49372', '', '', '', '1', '退出登录', '2022-06-13 11:53:26');
INSERT INTO `sys_logininfor` VALUES ('1749', 'admin', '/127.0.0.1:50607', '', '', '', '0', '登录成功！', '2022-06-16 11:20:45');
INSERT INTO `sys_logininfor` VALUES ('1750', 'admin', '/127.0.0.1:50607', '', '', '', '1', '退出登录', '2022-06-16 11:20:45');
INSERT INTO `sys_logininfor` VALUES ('1751', 'admin', '/127.0.0.1:3002', '', '', '', '0', '登录成功！', '2022-06-21 10:36:52');
INSERT INTO `sys_logininfor` VALUES ('1752', 'admin', '/127.0.0.1:3002', '', '', '', '1', '退出登录', '2022-06-21 10:36:52');
INSERT INTO `sys_logininfor` VALUES ('1753', 'admin', '/127.0.0.1:3022', '', '', '', '0', '登录成功！', '2022-06-21 10:37:17');
INSERT INTO `sys_logininfor` VALUES ('1754', 'admin', '/127.0.0.1:3022', '', '', '', '1', '退出登录', '2022-06-21 10:37:17');
INSERT INTO `sys_logininfor` VALUES ('1755', 'admin', '/127.0.0.1:13656', '', '', '', '0', '登录成功！', '2022-06-24 14:43:39');
INSERT INTO `sys_logininfor` VALUES ('1756', 'admin', '/127.0.0.1:13656', '', '', '', '1', '退出登录', '2022-06-24 14:43:39');
INSERT INTO `sys_logininfor` VALUES ('1757', 'admin', '/127.0.0.1:14148', '', '', '', '0', '登录成功！', '2022-06-24 14:52:52');
INSERT INTO `sys_logininfor` VALUES ('1758', 'admin', '/127.0.0.1:14148', '', '', '', '1', '退出登录', '2022-06-24 14:52:52');
INSERT INTO `sys_logininfor` VALUES ('1759', 'admin', '/127.0.0.1:11644', '', '', '', '0', '登录成功！', '2022-07-05 14:25:30');
INSERT INTO `sys_logininfor` VALUES ('1760', 'admin', '/127.0.0.1:11644', '', '', '', '1', '连接异常', '2022-07-05 14:25:30');
INSERT INTO `sys_logininfor` VALUES ('1761', 'admin', '/127.0.0.1:11644', '', '', '', '1', '退出登录', '2022-07-05 14:25:30');
INSERT INTO `sys_logininfor` VALUES ('1762', 'admin', '/127.0.0.1:11644', '', '', '', '1', '连接异常', '2022-07-05 14:25:30');
INSERT INTO `sys_logininfor` VALUES ('1763', 'admin', '/127.0.0.1:11705', '', '', '', '0', '登录成功！', '2022-07-05 14:29:30');
INSERT INTO `sys_logininfor` VALUES ('1764', 'admin', '/127.0.0.1:11705', '', '', '', '1', '连接异常', '2022-07-05 14:29:30');
INSERT INTO `sys_logininfor` VALUES ('1765', 'admin', '/127.0.0.1:11705', '', '', '', '1', '退出登录', '2022-07-05 14:29:30');
INSERT INTO `sys_logininfor` VALUES ('1766', 'admin', '/127.0.0.1:11705', '', '', '', '1', '连接异常', '2022-07-05 14:29:30');
INSERT INTO `sys_logininfor` VALUES ('1767', 'admin', '/127.0.0.1:12446', '', '', '', '0', '登录成功！', '2022-07-05 14:51:15');
INSERT INTO `sys_logininfor` VALUES ('1768', 'admin', '/127.0.0.1:12446', '', '', '', '1', '连接异常', '2022-07-05 14:51:15');
INSERT INTO `sys_logininfor` VALUES ('1769', 'admin', '/127.0.0.1:12446', '', '', '', '1', '退出登录', '2022-07-05 14:51:15');
INSERT INTO `sys_logininfor` VALUES ('1770', 'admin', '/127.0.0.1:12446', '', '', '', '1', '连接异常', '2022-07-05 14:51:15');
INSERT INTO `sys_logininfor` VALUES ('1771', 'admin', '/127.0.0.1:12480', '', '', '', '0', '登录成功！', '2022-07-05 14:53:08');
INSERT INTO `sys_logininfor` VALUES ('1772', 'admin', '/127.0.0.1:12480', '', '', '', '1', '连接异常', '2022-07-05 14:53:08');
INSERT INTO `sys_logininfor` VALUES ('1773', 'admin', '/127.0.0.1:12480', '', '', '', '1', '退出登录', '2022-07-05 14:53:08');
INSERT INTO `sys_logininfor` VALUES ('1774', 'admin', '/127.0.0.1:12480', '', '', '', '1', '连接异常', '2022-07-05 14:53:08');
INSERT INTO `sys_logininfor` VALUES ('1775', 'admin', '/127.0.0.1:12574', '', '', '', '0', '登录成功！', '2022-07-05 14:57:05');
INSERT INTO `sys_logininfor` VALUES ('1776', 'admin', '/127.0.0.1:12574', '', '', '', '1', '连接异常', '2022-07-05 14:57:05');
INSERT INTO `sys_logininfor` VALUES ('1777', 'admin', '/127.0.0.1:12574', '', '', '', '1', '退出登录', '2022-07-05 14:57:05');
INSERT INTO `sys_logininfor` VALUES ('1778', 'admin', '/127.0.0.1:12574', '', '', '', '1', '连接异常', '2022-07-05 14:57:05');
INSERT INTO `sys_logininfor` VALUES ('1779', 'admin', '/127.0.0.1:12644', '', '', '', '0', '登录成功！', '2022-07-05 15:01:13');
INSERT INTO `sys_logininfor` VALUES ('1780', 'admin', '/127.0.0.1:12644', '', '', '', '1', '连接异常', '2022-07-05 15:01:13');
INSERT INTO `sys_logininfor` VALUES ('1781', 'admin', '/127.0.0.1:12644', '', '', '', '1', '退出登录', '2022-07-05 15:01:13');
INSERT INTO `sys_logininfor` VALUES ('1782', 'admin', '/127.0.0.1:12644', '', '', '', '1', '连接异常', '2022-07-05 15:01:13');
INSERT INTO `sys_logininfor` VALUES ('1783', 'admin', '/127.0.0.1:12688', '', '', '', '0', '登录成功！', '2022-07-05 15:03:32');
INSERT INTO `sys_logininfor` VALUES ('1784', 'admin', '/127.0.0.1:12688', '', '', '', '1', '连接异常', '2022-07-05 15:03:32');
INSERT INTO `sys_logininfor` VALUES ('1785', 'admin', '/127.0.0.1:12688', '', '', '', '1', '退出登录', '2022-07-05 15:03:32');
INSERT INTO `sys_logininfor` VALUES ('1786', 'admin', '/127.0.0.1:12688', '', '', '', '1', '连接异常', '2022-07-05 15:03:32');
INSERT INTO `sys_logininfor` VALUES ('1787', 'admin', '/127.0.0.1:12708', '', '', '', '0', '登录成功！', '2022-07-05 15:04:46');
INSERT INTO `sys_logininfor` VALUES ('1788', 'admin', '/127.0.0.1:12708', '', '', '', '1', '连接异常', '2022-07-05 15:04:46');
INSERT INTO `sys_logininfor` VALUES ('1789', 'admin', '/127.0.0.1:12708', '', '', '', '1', '退出登录', '2022-07-05 15:04:46');
INSERT INTO `sys_logininfor` VALUES ('1790', 'admin', '/127.0.0.1:12708', '', '', '', '1', '连接异常', '2022-07-05 15:04:46');
INSERT INTO `sys_logininfor` VALUES ('1791', 'admin', '/127.0.0.1:12775', '', '', '', '0', '登录成功！', '2022-07-05 15:08:31');
INSERT INTO `sys_logininfor` VALUES ('1792', 'admin', '/127.0.0.1:12775', '', '', '', '1', '连接异常', '2022-07-05 15:08:31');
INSERT INTO `sys_logininfor` VALUES ('1793', 'admin', '/127.0.0.1:12775', '', '', '', '1', '退出登录', '2022-07-05 15:08:31');
INSERT INTO `sys_logininfor` VALUES ('1794', 'admin', '/127.0.0.1:12775', '', '', '', '1', '连接异常', '2022-07-05 15:08:31');
INSERT INTO `sys_logininfor` VALUES ('1795', 'admin', '/127.0.0.1:12820', '', '', '', '0', '登录成功！', '2022-07-05 15:11:03');
INSERT INTO `sys_logininfor` VALUES ('1796', 'admin', '/127.0.0.1:12820', '', '', '', '1', '连接异常', '2022-07-05 15:11:03');
INSERT INTO `sys_logininfor` VALUES ('1797', 'admin', '/127.0.0.1:12820', '', '', '', '1', '退出登录', '2022-07-05 15:11:03');
INSERT INTO `sys_logininfor` VALUES ('1798', 'admin', '/127.0.0.1:12820', '', '', '', '1', '连接异常', '2022-07-05 15:11:03');
INSERT INTO `sys_logininfor` VALUES ('1799', 'admin', '/127.0.0.1:12868', '', '', '', '0', '登录成功！', '2022-07-05 15:13:03');
INSERT INTO `sys_logininfor` VALUES ('1800', 'admin', '/127.0.0.1:12868', '', '', '', '1', '连接异常', '2022-07-05 15:13:03');
INSERT INTO `sys_logininfor` VALUES ('1801', 'admin', '/127.0.0.1:12868', '', '', '', '1', '退出登录', '2022-07-05 15:13:03');
INSERT INTO `sys_logininfor` VALUES ('1802', 'admin', '/127.0.0.1:12868', '', '', '', '1', '连接异常', '2022-07-05 15:13:03');
INSERT INTO `sys_logininfor` VALUES ('1803', 'admin', '/127.0.0.1:12958', '', '', '', '0', '登录成功！', '2022-07-05 15:17:41');
INSERT INTO `sys_logininfor` VALUES ('1804', 'admin', '/127.0.0.1:12958', '', '', '', '1', '连接异常', '2022-07-05 15:17:41');
INSERT INTO `sys_logininfor` VALUES ('1805', 'admin', '/127.0.0.1:12958', '', '', '', '1', '退出登录', '2022-07-05 15:17:41');
INSERT INTO `sys_logininfor` VALUES ('1806', 'admin', '/127.0.0.1:12958', '', '', '', '1', '连接异常', '2022-07-05 15:17:41');
INSERT INTO `sys_logininfor` VALUES ('1807', 'admin', '/127.0.0.1:13452', '', '', '', '0', '登录成功！', '2022-07-05 15:31:19');
INSERT INTO `sys_logininfor` VALUES ('1808', 'admin', '/127.0.0.1:13452', '', '', '', '1', '连接异常', '2022-07-05 15:31:19');
INSERT INTO `sys_logininfor` VALUES ('1809', 'admin', '/127.0.0.1:13452', '', '', '', '1', '退出登录', '2022-07-05 15:31:19');
INSERT INTO `sys_logininfor` VALUES ('1810', 'admin', '/127.0.0.1:13452', '', '', '', '1', '连接异常', '2022-07-05 15:31:19');
INSERT INTO `sys_logininfor` VALUES ('1811', 'admin', '/127.0.0.1:13885', '', '', '', '0', '登录成功！', '2022-07-05 15:42:46');
INSERT INTO `sys_logininfor` VALUES ('1812', 'admin', '/127.0.0.1:13885', '', '', '', '1', '连接异常', '2022-07-05 15:42:46');
INSERT INTO `sys_logininfor` VALUES ('1813', 'admin', '/127.0.0.1:13885', '', '', '', '1', '退出登录', '2022-07-05 15:42:46');
INSERT INTO `sys_logininfor` VALUES ('1814', 'admin', '/127.0.0.1:13885', '', '', '', '1', '连接异常', '2022-07-05 15:42:46');
INSERT INTO `sys_logininfor` VALUES ('1815', 'admin', '/127.0.0.1:4540', '', '', '', '0', '登录成功！', '2022-07-05 17:19:46');
INSERT INTO `sys_logininfor` VALUES ('1816', 'admin', '/127.0.0.1:4540', '', '', '', '1', '退出登录', '2022-07-05 17:19:46');
INSERT INTO `sys_logininfor` VALUES ('1817', 'admin', '/127.0.0.1:3653', '', '', '', '0', '登录成功！', '2022-07-06 11:04:36');
INSERT INTO `sys_logininfor` VALUES ('1818', 'admin', '/127.0.0.1:3653', '', '', '', '1', '连接异常', '2022-07-06 11:04:36');
INSERT INTO `sys_logininfor` VALUES ('1819', 'admin', '/127.0.0.1:3653', '', '', '', '1', '退出登录', '2022-07-06 11:04:36');
INSERT INTO `sys_logininfor` VALUES ('1820', 'admin', '/127.0.0.1:3653', '', '', '', '1', '连接异常', '2022-07-06 11:04:36');
INSERT INTO `sys_logininfor` VALUES ('1821', 'admin', '/127.0.0.1:3994', '', '', '', '0', '登录成功！', '2022-07-06 11:08:36');
INSERT INTO `sys_logininfor` VALUES ('1822', 'admin', '/127.0.0.1:3994', '', '', '', '1', '连接异常', '2022-07-06 11:08:36');
INSERT INTO `sys_logininfor` VALUES ('1823', 'admin', '/127.0.0.1:3994', '', '', '', '1', '退出登录', '2022-07-06 11:08:36');
INSERT INTO `sys_logininfor` VALUES ('1824', 'admin', '/127.0.0.1:3994', '', '', '', '1', '连接异常', '2022-07-06 11:08:36');
INSERT INTO `sys_logininfor` VALUES ('1825', 'admin', '/127.0.0.1:4115', '', '', '', '0', '登录成功！', '2022-07-06 11:10:09');
INSERT INTO `sys_logininfor` VALUES ('1826', 'admin', '/127.0.0.1:4115', '', '', '', '1', '连接异常', '2022-07-06 11:10:09');
INSERT INTO `sys_logininfor` VALUES ('1827', 'admin', '/127.0.0.1:4115', '', '', '', '1', '退出登录', '2022-07-06 11:10:09');
INSERT INTO `sys_logininfor` VALUES ('1828', 'admin', '/127.0.0.1:4115', '', '', '', '1', '连接异常', '2022-07-06 11:10:09');
INSERT INTO `sys_logininfor` VALUES ('1829', 'admin', '/127.0.0.1:4161', '', '', '', '0', '登录成功！', '2022-07-06 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('1830', 'admin', '/127.0.0.1:4161', '', '', '', '1', '退出登录', '2022-07-06 11:10:39');
INSERT INTO `sys_logininfor` VALUES ('1831', 'admin', '/127.0.0.1:4331', '', '', '', '0', '登录成功！', '2022-07-06 11:12:38');
INSERT INTO `sys_logininfor` VALUES ('1832', 'admin', '/127.0.0.1:4331', '', '', '', '1', '连接异常', '2022-07-06 11:12:38');
INSERT INTO `sys_logininfor` VALUES ('1833', 'admin', '/127.0.0.1:4331', '', '', '', '1', '退出登录', '2022-07-06 11:12:38');
INSERT INTO `sys_logininfor` VALUES ('1834', 'admin', '/127.0.0.1:4331', '', '', '', '1', '连接异常', '2022-07-06 11:12:38');
INSERT INTO `sys_logininfor` VALUES ('1835', 'admin', '/127.0.0.1:4385', '', '', '', '0', '登录成功！', '2022-07-06 11:13:13');
INSERT INTO `sys_logininfor` VALUES ('1836', 'admin', '/127.0.0.1:4385', '', '', '', '1', '连接异常', '2022-07-06 11:13:13');
INSERT INTO `sys_logininfor` VALUES ('1837', 'admin', '/127.0.0.1:4385', '', '', '', '1', '退出登录', '2022-07-06 11:13:13');
INSERT INTO `sys_logininfor` VALUES ('1838', 'admin', '/127.0.0.1:4385', '', '', '', '1', '连接异常', '2022-07-06 11:13:13');
INSERT INTO `sys_logininfor` VALUES ('1839', 'admin', '/127.0.0.1:5281', '', '', '', '0', '登录成功！', '2022-07-06 11:23:34');
INSERT INTO `sys_logininfor` VALUES ('1840', 'admin', '/127.0.0.1:5281', '', '', '', '1', '退出登录', '2022-07-06 11:23:34');
INSERT INTO `sys_logininfor` VALUES ('1841', 'admin', '/127.0.0.1:6712', '', '', '', '0', '登录成功！', '2022-07-06 11:39:27');
INSERT INTO `sys_logininfor` VALUES ('1842', 'admin', '/127.0.0.1:6712', '', '', '', '1', '连接异常', '2022-07-06 11:39:27');
INSERT INTO `sys_logininfor` VALUES ('1843', 'admin', '/127.0.0.1:6712', '', '', '', '1', '退出登录', '2022-07-06 11:39:27');
INSERT INTO `sys_logininfor` VALUES ('1844', 'admin', '/127.0.0.1:6712', '', '', '', '1', '连接异常', '2022-07-06 11:39:27');
INSERT INTO `sys_logininfor` VALUES ('1845', 'admin', '/127.0.0.1:7153', '', '', '', '0', '登录成功！', '2022-07-06 11:44:49');
INSERT INTO `sys_logininfor` VALUES ('1846', 'admin', '/127.0.0.1:7153', '', '', '', '1', '连接异常', '2022-07-06 11:44:49');
INSERT INTO `sys_logininfor` VALUES ('1847', 'admin', '/127.0.0.1:7153', '', '', '', '1', '退出登录', '2022-07-06 11:44:49');
INSERT INTO `sys_logininfor` VALUES ('1848', 'admin', '/127.0.0.1:7153', '', '', '', '1', '连接异常', '2022-07-06 11:44:49');
INSERT INTO `sys_logininfor` VALUES ('1849', 'admin', '/127.0.0.1:7194', '', '', '', '0', '登录成功！', '2022-07-06 11:45:12');
INSERT INTO `sys_logininfor` VALUES ('1850', 'admin', '/127.0.0.1:7194', '', '', '', '1', '连接异常', '2022-07-06 11:45:12');
INSERT INTO `sys_logininfor` VALUES ('1851', 'admin', '/127.0.0.1:7194', '', '', '', '1', '退出登录', '2022-07-06 11:45:12');
INSERT INTO `sys_logininfor` VALUES ('1852', 'admin', '/127.0.0.1:7194', '', '', '', '1', '连接异常', '2022-07-06 11:45:12');
INSERT INTO `sys_logininfor` VALUES ('1853', 'admin', '/127.0.0.1:1576', '', '', '', '0', '登录成功！', '2022-07-06 13:49:58');
INSERT INTO `sys_logininfor` VALUES ('1854', 'admin', '/127.0.0.1:1576', '', '', '', '1', '连接异常', '2022-07-06 13:49:58');
INSERT INTO `sys_logininfor` VALUES ('1855', 'admin', '/127.0.0.1:1576', '', '', '', '1', '退出登录', '2022-07-06 13:49:58');
INSERT INTO `sys_logininfor` VALUES ('1856', 'admin', '/127.0.0.1:1576', '', '', '', '1', '连接异常', '2022-07-06 13:49:58');
INSERT INTO `sys_logininfor` VALUES ('1857', 'admin', '/127.0.0.1:1710', '', '', '', '0', '登录成功！', '2022-07-06 13:52:06');
INSERT INTO `sys_logininfor` VALUES ('1858', 'admin', '/127.0.0.1:1710', '', '', '', '1', '连接异常', '2022-07-06 13:52:06');
INSERT INTO `sys_logininfor` VALUES ('1859', 'admin', '/127.0.0.1:1710', '', '', '', '1', '退出登录', '2022-07-06 13:52:06');
INSERT INTO `sys_logininfor` VALUES ('1860', 'admin', '/127.0.0.1:1710', '', '', '', '1', '连接异常', '2022-07-06 13:52:06');
INSERT INTO `sys_logininfor` VALUES ('1861', 'admin', '/127.0.0.1:2216', '', '', '', '0', '登录成功！', '2022-07-06 14:00:06');
INSERT INTO `sys_logininfor` VALUES ('1862', 'admin', '/127.0.0.1:2216', '', '', '', '1', '连接异常', '2022-07-06 14:00:06');
INSERT INTO `sys_logininfor` VALUES ('1863', 'admin', '/127.0.0.1:2216', '', '', '', '1', '退出登录', '2022-07-06 14:00:06');
INSERT INTO `sys_logininfor` VALUES ('1864', 'admin', '/127.0.0.1:2216', '', '', '', '1', '连接异常', '2022-07-06 14:00:06');
INSERT INTO `sys_logininfor` VALUES ('1865', 'admin', '/127.0.0.1:2374', '', '', '', '0', '登录成功！', '2022-07-06 14:02:36');
INSERT INTO `sys_logininfor` VALUES ('1866', 'admin', '/127.0.0.1:2374', '', '', '', '1', '连接异常', '2022-07-06 14:02:36');
INSERT INTO `sys_logininfor` VALUES ('1867', 'admin', '/127.0.0.1:2374', '', '', '', '1', '退出登录', '2022-07-06 14:02:36');
INSERT INTO `sys_logininfor` VALUES ('1868', 'admin', '/127.0.0.1:2374', '', '', '', '1', '连接异常', '2022-07-06 14:02:36');
INSERT INTO `sys_logininfor` VALUES ('1869', 'admin', '/127.0.0.1:2914', '', '', '', '0', '登录成功！', '2022-07-06 14:09:24');
INSERT INTO `sys_logininfor` VALUES ('1870', 'admin', '/127.0.0.1:2914', '', '', '', '1', '连接异常', '2022-07-06 14:09:24');
INSERT INTO `sys_logininfor` VALUES ('1871', 'admin', '/127.0.0.1:2914', '', '', '', '1', '退出登录', '2022-07-06 14:09:24');
INSERT INTO `sys_logininfor` VALUES ('1872', 'admin', '/127.0.0.1:2914', '', '', '', '1', '连接异常', '2022-07-06 14:09:24');
INSERT INTO `sys_logininfor` VALUES ('1873', 'admin', '/127.0.0.1:3139', '', '', '', '0', '登录成功！', '2022-07-06 14:12:22');
INSERT INTO `sys_logininfor` VALUES ('1874', 'admin', '/127.0.0.1:3139', '', '', '', '1', '连接异常', '2022-07-06 14:12:22');
INSERT INTO `sys_logininfor` VALUES ('1875', 'admin', '/127.0.0.1:3139', '', '', '', '1', '退出登录', '2022-07-06 14:12:22');
INSERT INTO `sys_logininfor` VALUES ('1876', 'admin', '/127.0.0.1:3139', '', '', '', '1', '连接异常', '2022-07-06 14:12:22');
INSERT INTO `sys_logininfor` VALUES ('1877', 'admin', '/127.0.0.1:3228', '', '', '', '0', '登录成功！', '2022-07-06 14:13:25');
INSERT INTO `sys_logininfor` VALUES ('1878', 'admin', '/127.0.0.1:3228', '', '', '', '1', '连接异常', '2022-07-06 14:13:25');
INSERT INTO `sys_logininfor` VALUES ('1879', 'admin', '/127.0.0.1:3228', '', '', '', '1', '退出登录', '2022-07-06 14:13:25');
INSERT INTO `sys_logininfor` VALUES ('1880', 'admin', '/127.0.0.1:3228', '', '', '', '1', '连接异常', '2022-07-06 14:13:25');
INSERT INTO `sys_logininfor` VALUES ('1881', 'admin', '/127.0.0.1:3828', '', '', '', '0', '登录成功！', '2022-07-06 14:20:35');
INSERT INTO `sys_logininfor` VALUES ('1882', 'admin', '/127.0.0.1:3828', '', '', '', '1', '连接异常', '2022-07-06 14:20:35');
INSERT INTO `sys_logininfor` VALUES ('1883', 'admin', '/127.0.0.1:3828', '', '', '', '1', '退出登录', '2022-07-06 14:20:35');
INSERT INTO `sys_logininfor` VALUES ('1884', 'admin', '/127.0.0.1:3828', '', '', '', '1', '连接异常', '2022-07-06 14:20:35');
INSERT INTO `sys_logininfor` VALUES ('1885', 'admin', '/127.0.0.1:4411', '', '', '', '0', '登录成功！', '2022-07-06 14:27:34');
INSERT INTO `sys_logininfor` VALUES ('1886', 'admin', '/127.0.0.1:4411', '', '', '', '1', '连接异常', '2022-07-06 14:27:34');
INSERT INTO `sys_logininfor` VALUES ('1887', 'admin', '/127.0.0.1:4411', '', '', '', '1', '退出登录', '2022-07-06 14:27:34');
INSERT INTO `sys_logininfor` VALUES ('1888', 'admin', '/127.0.0.1:4411', '', '', '', '1', '连接异常', '2022-07-06 14:27:34');
INSERT INTO `sys_logininfor` VALUES ('1889', 'admin', '/127.0.0.1:4538', '', '', '', '0', '登录成功！', '2022-07-06 14:28:53');
INSERT INTO `sys_logininfor` VALUES ('1890', 'admin', '/127.0.0.1:4538', '', '', '', '1', '连接异常', '2022-07-06 14:28:53');
INSERT INTO `sys_logininfor` VALUES ('1891', 'admin', '/127.0.0.1:4538', '', '', '', '1', '退出登录', '2022-07-06 14:28:53');
INSERT INTO `sys_logininfor` VALUES ('1892', 'admin', '/127.0.0.1:4538', '', '', '', '1', '连接异常', '2022-07-06 14:28:53');
INSERT INTO `sys_logininfor` VALUES ('1893', 'admin', '/127.0.0.1:5046', '', '', '', '0', '登录成功！', '2022-07-06 14:35:06');
INSERT INTO `sys_logininfor` VALUES ('1894', 'admin', '/127.0.0.1:5046', '', '', '', '1', '连接异常', '2022-07-06 14:35:06');
INSERT INTO `sys_logininfor` VALUES ('1895', 'admin', '/127.0.0.1:5046', '', '', '', '1', '退出登录', '2022-07-06 14:35:06');
INSERT INTO `sys_logininfor` VALUES ('1896', 'admin', '/127.0.0.1:5046', '', '', '', '1', '连接异常', '2022-07-06 14:35:06');
INSERT INTO `sys_logininfor` VALUES ('1897', 'admin', '/127.0.0.1:6480', '', '', '', '0', '登录成功！', '2022-07-06 14:51:09');
INSERT INTO `sys_logininfor` VALUES ('1898', 'admin', '/127.0.0.1:6480', '', '', '', '1', '连接异常', '2022-07-06 14:51:09');
INSERT INTO `sys_logininfor` VALUES ('1899', 'admin', '/127.0.0.1:6480', '', '', '', '1', '退出登录', '2022-07-06 14:51:09');
INSERT INTO `sys_logininfor` VALUES ('1900', 'admin', '/127.0.0.1:6480', '', '', '', '1', '连接异常', '2022-07-06 14:51:09');
INSERT INTO `sys_logininfor` VALUES ('1901', 'admin', '/127.0.0.1:8428', '', '', '', '0', '登录成功！', '2022-07-06 15:14:24');
INSERT INTO `sys_logininfor` VALUES ('1902', 'admin', '/127.0.0.1:8428', '', '', '', '1', '连接异常', '2022-07-06 15:14:24');
INSERT INTO `sys_logininfor` VALUES ('1903', 'admin', '/127.0.0.1:8428', '', '', '', '1', '退出登录', '2022-07-06 15:14:24');
INSERT INTO `sys_logininfor` VALUES ('1904', 'admin', '/127.0.0.1:8428', '', '', '', '1', '连接异常', '2022-07-06 15:14:24');
INSERT INTO `sys_logininfor` VALUES ('1905', 'admin', '/127.0.0.1:9249', '', '', '', '0', '登录成功！', '2022-07-06 15:22:53');
INSERT INTO `sys_logininfor` VALUES ('1906', 'admin', '/127.0.0.1:9249', '', '', '', '1', '连接异常', '2022-07-06 15:22:53');
INSERT INTO `sys_logininfor` VALUES ('1907', 'admin', '/127.0.0.1:9249', '', '', '', '1', '退出登录', '2022-07-06 15:22:53');
INSERT INTO `sys_logininfor` VALUES ('1908', 'admin', '/127.0.0.1:9249', '', '', '', '1', '连接异常', '2022-07-06 15:22:53');
INSERT INTO `sys_logininfor` VALUES ('1909', 'admin', '/127.0.0.1:9520', '', '', '', '0', '登录成功！', '2022-07-06 15:26:06');
INSERT INTO `sys_logininfor` VALUES ('1910', 'admin', '/127.0.0.1:9520', '', '', '', '1', '连接异常', '2022-07-06 15:26:06');
INSERT INTO `sys_logininfor` VALUES ('1911', 'admin', '/127.0.0.1:9520', '', '', '', '1', '退出登录', '2022-07-06 15:26:06');
INSERT INTO `sys_logininfor` VALUES ('1912', 'admin', '/127.0.0.1:9520', '', '', '', '1', '连接异常', '2022-07-06 15:26:06');
INSERT INTO `sys_logininfor` VALUES ('1913', 'admin', '/127.0.0.1:9977', '', '', '', '0', '登录成功！', '2022-07-06 15:31:33');
INSERT INTO `sys_logininfor` VALUES ('1914', 'admin', '/127.0.0.1:9977', '', '', '', '1', '连接异常', '2022-07-06 15:31:33');
INSERT INTO `sys_logininfor` VALUES ('1915', 'admin', '/127.0.0.1:9977', '', '', '', '1', '退出登录', '2022-07-06 15:31:33');
INSERT INTO `sys_logininfor` VALUES ('1916', 'admin', '/127.0.0.1:9977', '', '', '', '1', '连接异常', '2022-07-06 15:31:33');
INSERT INTO `sys_logininfor` VALUES ('1917', 'admin', '/127.0.0.1:10188', '', '', '', '0', '登录成功！', '2022-07-06 15:34:06');
INSERT INTO `sys_logininfor` VALUES ('1918', 'admin', '/127.0.0.1:10188', '', '', '', '1', '连接异常', '2022-07-06 15:34:06');
INSERT INTO `sys_logininfor` VALUES ('1919', 'admin', '/127.0.0.1:10188', '', '', '', '1', '退出登录', '2022-07-06 15:34:06');
INSERT INTO `sys_logininfor` VALUES ('1920', 'admin', '/127.0.0.1:10188', '', '', '', '1', '连接异常', '2022-07-06 15:34:06');
INSERT INTO `sys_logininfor` VALUES ('1921', 'admin', '/127.0.0.1:10439', '', '', '', '0', '登录成功！', '2022-07-06 15:36:59');
INSERT INTO `sys_logininfor` VALUES ('1922', 'admin', '/127.0.0.1:10439', '', '', '', '1', '连接异常', '2022-07-06 15:36:59');
INSERT INTO `sys_logininfor` VALUES ('1923', 'admin', '/127.0.0.1:10439', '', '', '', '1', '退出登录', '2022-07-06 15:36:59');
INSERT INTO `sys_logininfor` VALUES ('1924', 'admin', '/127.0.0.1:10439', '', '', '', '1', '连接异常', '2022-07-06 15:36:59');
INSERT INTO `sys_logininfor` VALUES ('1925', 'admin', '/127.0.0.1:10741', '', '', '', '0', '登录成功！', '2022-07-06 15:40:37');
INSERT INTO `sys_logininfor` VALUES ('1926', 'admin', '/127.0.0.1:10741', '', '', '', '1', '连接异常', '2022-07-06 15:40:37');
INSERT INTO `sys_logininfor` VALUES ('1927', 'admin', '/127.0.0.1:10741', '', '', '', '1', '退出登录', '2022-07-06 15:40:37');
INSERT INTO `sys_logininfor` VALUES ('1928', 'admin', '/127.0.0.1:10741', '', '', '', '1', '连接异常', '2022-07-06 15:40:37');
INSERT INTO `sys_logininfor` VALUES ('1929', 'admin', '/127.0.0.1:11086', '', '', '', '0', '登录成功！', '2022-07-06 15:44:45');
INSERT INTO `sys_logininfor` VALUES ('1930', 'admin', '/127.0.0.1:11086', '', '', '', '1', '连接异常', '2022-07-06 15:44:45');
INSERT INTO `sys_logininfor` VALUES ('1931', 'admin', '/127.0.0.1:11086', '', '', '', '1', '退出登录', '2022-07-06 15:44:45');
INSERT INTO `sys_logininfor` VALUES ('1932', 'admin', '/127.0.0.1:11086', '', '', '', '1', '连接异常', '2022-07-06 15:44:45');
INSERT INTO `sys_logininfor` VALUES ('1933', 'admin', '/127.0.0.1:11340', '', '', '', '0', '登录成功！', '2022-07-06 15:47:37');
INSERT INTO `sys_logininfor` VALUES ('1934', 'admin', '/127.0.0.1:11340', '', '', '', '1', '连接异常', '2022-07-06 15:47:37');
INSERT INTO `sys_logininfor` VALUES ('1935', 'admin', '/127.0.0.1:11340', '', '', '', '1', '退出登录', '2022-07-06 15:47:37');
INSERT INTO `sys_logininfor` VALUES ('1936', 'admin', '/127.0.0.1:11340', '', '', '', '1', '连接异常', '2022-07-06 15:47:37');
INSERT INTO `sys_logininfor` VALUES ('1937', 'admin', '/127.0.0.1:11622', '', '', '', '0', '登录成功！', '2022-07-06 15:50:36');
INSERT INTO `sys_logininfor` VALUES ('1938', 'admin', '/127.0.0.1:11622', '', '', '', '1', '连接异常', '2022-07-06 15:50:36');
INSERT INTO `sys_logininfor` VALUES ('1939', 'admin', '/127.0.0.1:11622', '', '', '', '1', '退出登录', '2022-07-06 15:50:36');
INSERT INTO `sys_logininfor` VALUES ('1940', 'admin', '/127.0.0.1:11622', '', '', '', '1', '连接异常', '2022-07-06 15:50:36');
INSERT INTO `sys_logininfor` VALUES ('1941', 'admin', '/127.0.0.1:12039', '', '', '', '0', '登录成功！', '2022-07-06 15:55:44');
INSERT INTO `sys_logininfor` VALUES ('1942', 'admin', '/127.0.0.1:12039', '', '', '', '1', '连接异常', '2022-07-06 15:55:44');
INSERT INTO `sys_logininfor` VALUES ('1943', 'admin', '/127.0.0.1:12039', '', '', '', '1', '退出登录', '2022-07-06 15:55:44');
INSERT INTO `sys_logininfor` VALUES ('1944', 'admin', '/127.0.0.1:12039', '', '', '', '1', '连接异常', '2022-07-06 15:55:44');
INSERT INTO `sys_logininfor` VALUES ('1945', 'admin', '/127.0.0.1:12744', '', '', '', '0', '登录成功！', '2022-07-06 16:04:13');
INSERT INTO `sys_logininfor` VALUES ('1946', 'admin', '/127.0.0.1:12744', '', '', '', '1', '连接异常', '2022-07-06 16:04:13');
INSERT INTO `sys_logininfor` VALUES ('1947', 'admin', '/127.0.0.1:12744', '', '', '', '1', '退出登录', '2022-07-06 16:04:13');
INSERT INTO `sys_logininfor` VALUES ('1948', 'admin', '/127.0.0.1:12744', '', '', '', '1', '连接异常', '2022-07-06 16:04:13');
INSERT INTO `sys_logininfor` VALUES ('1949', 'admin', '/127.0.0.1:12833', '', '', '', '0', '登录成功！', '2022-07-06 16:05:17');
INSERT INTO `sys_logininfor` VALUES ('1950', 'admin', '/127.0.0.1:12833', '', '', '', '1', '连接异常', '2022-07-06 16:05:17');
INSERT INTO `sys_logininfor` VALUES ('1951', 'admin', '/127.0.0.1:12833', '', '', '', '1', '退出登录', '2022-07-06 16:05:17');
INSERT INTO `sys_logininfor` VALUES ('1952', 'admin', '/127.0.0.1:12833', '', '', '', '1', '连接异常', '2022-07-06 16:05:17');
INSERT INTO `sys_logininfor` VALUES ('1953', 'admin', '/127.0.0.1:13411', '', '', '', '0', '登录成功！', '2022-07-06 16:11:07');
INSERT INTO `sys_logininfor` VALUES ('1954', 'admin', '/127.0.0.1:13411', '', '', '', '1', '连接异常', '2022-07-06 16:11:07');
INSERT INTO `sys_logininfor` VALUES ('1955', 'admin', '/127.0.0.1:13411', '', '', '', '1', '退出登录', '2022-07-06 16:11:07');
INSERT INTO `sys_logininfor` VALUES ('1956', 'admin', '/127.0.0.1:13411', '', '', '', '1', '连接异常', '2022-07-06 16:11:07');
INSERT INTO `sys_logininfor` VALUES ('1957', 'admin', '/127.0.0.1:14395', '', '', '', '0', '登录成功！', '2022-07-06 16:18:13');
INSERT INTO `sys_logininfor` VALUES ('1958', 'admin', '/127.0.0.1:14395', '', '', '', '1', '连接异常', '2022-07-06 16:18:13');
INSERT INTO `sys_logininfor` VALUES ('1959', 'admin', '/127.0.0.1:14395', '', '', '', '1', '退出登录', '2022-07-06 16:18:13');
INSERT INTO `sys_logininfor` VALUES ('1960', 'admin', '/127.0.0.1:14395', '', '', '', '1', '连接异常', '2022-07-06 16:18:13');
INSERT INTO `sys_logininfor` VALUES ('1961', 'admin', '/127.0.0.1:14461', '', '', '', '0', '登录成功！', '2022-07-06 16:18:58');
INSERT INTO `sys_logininfor` VALUES ('1962', 'admin', '/127.0.0.1:14461', '', '', '', '1', '连接异常', '2022-07-06 16:18:58');
INSERT INTO `sys_logininfor` VALUES ('1963', 'admin', '/127.0.0.1:14461', '', '', '', '1', '退出登录', '2022-07-06 16:18:58');
INSERT INTO `sys_logininfor` VALUES ('1964', 'admin', '/127.0.0.1:14461', '', '', '', '1', '连接异常', '2022-07-06 16:18:58');
INSERT INTO `sys_logininfor` VALUES ('1965', 'admin', '/127.0.0.1:14515', '', '', '', '0', '登录成功！', '2022-07-06 16:19:32');
INSERT INTO `sys_logininfor` VALUES ('1966', 'admin', '/127.0.0.1:12164', '', '', '', '0', '登录成功！', '2022-07-07 13:42:04');
INSERT INTO `sys_logininfor` VALUES ('1967', 'admin', '/127.0.0.1:13009', '', '', '', '0', '登录成功！', '2022-07-07 14:09:31');
INSERT INTO `sys_logininfor` VALUES ('1968', 'admin', '/127.0.0.1:13009', '', '', '', '1', '连接异常', '2022-07-07 14:09:31');
INSERT INTO `sys_logininfor` VALUES ('1969', 'admin', '/127.0.0.1:13009', '', '', '', '1', '退出登录', '2022-07-07 14:09:31');
INSERT INTO `sys_logininfor` VALUES ('1970', 'admin', '/127.0.0.1:13009', '', '', '', '1', '连接异常', '2022-07-07 14:09:31');
INSERT INTO `sys_logininfor` VALUES ('1971', 'admin', '/127.0.0.1:6210', '', '', '', '0', '登录成功！', '2022-07-07 15:11:49');
INSERT INTO `sys_logininfor` VALUES ('1972', 'admin', '/127.0.0.1:6210', '', '', '', '1', '连接异常', '2022-07-07 15:11:49');
INSERT INTO `sys_logininfor` VALUES ('1973', 'admin', '/127.0.0.1:6210', '', '', '', '1', '退出登录', '2022-07-07 15:11:49');
INSERT INTO `sys_logininfor` VALUES ('1974', 'admin', '/127.0.0.1:6210', '', '', '', '1', '连接异常', '2022-07-07 15:11:49');
INSERT INTO `sys_logininfor` VALUES ('1975', 'admin', '/127.0.0.1:10033', '', '', '', '0', '登录成功！', '2022-07-07 16:30:19');
INSERT INTO `sys_logininfor` VALUES ('1976', 'admin', '/127.0.0.1:10743', '', '', '', '0', '登录成功！', '2022-07-07 16:37:31');
INSERT INTO `sys_logininfor` VALUES ('1977', 'admin', '/127.0.0.1:10743', '', '', '', '1', '连接异常', '2022-07-07 16:37:31');
INSERT INTO `sys_logininfor` VALUES ('1978', 'admin', '/127.0.0.1:10743', '', '', '', '1', '退出登录', '2022-07-07 16:37:31');
INSERT INTO `sys_logininfor` VALUES ('1979', 'admin', '/127.0.0.1:10743', '', '', '', '1', '连接异常', '2022-07-07 16:37:31');
INSERT INTO `sys_logininfor` VALUES ('1980', 'admin', '/127.0.0.1:11097', '', '', '', '0', '登录成功！', '2022-07-07 16:43:30');
INSERT INTO `sys_logininfor` VALUES ('1981', 'admin', '/127.0.0.1:11097', '', '', '', '1', '退出登录', '2022-07-07 16:43:30');
INSERT INTO `sys_logininfor` VALUES ('1982', 'admin', '/127.0.0.1:11122', '', '', '', '0', '登录成功！', '2022-07-07 16:43:48');
INSERT INTO `sys_logininfor` VALUES ('1983', 'admin', '/127.0.0.1:11122', '', '', '', '1', '退出登录', '2022-07-07 16:43:48');
INSERT INTO `sys_logininfor` VALUES ('1984', 'admin', '/127.0.0.1:11195', '', '', '', '0', '登录成功！', '2022-07-07 16:44:59');
INSERT INTO `sys_logininfor` VALUES ('1985', 'admin', '/127.0.0.1:11195', '', '', '', '1', '连接异常', '2022-07-07 16:44:59');
INSERT INTO `sys_logininfor` VALUES ('1986', 'admin', '/127.0.0.1:11195', '', '', '', '1', '退出登录', '2022-07-07 16:44:59');
INSERT INTO `sys_logininfor` VALUES ('1987', 'admin', '/127.0.0.1:11195', '', '', '', '1', '连接异常', '2022-07-07 16:44:59');
INSERT INTO `sys_logininfor` VALUES ('1988', 'admin', '/127.0.0.1:11338', '', '', '', '0', '登录成功！', '2022-07-07 16:47:13');
INSERT INTO `sys_logininfor` VALUES ('1989', 'admin', '/127.0.0.1:11338', '', '', '', '1', '连接异常', '2022-07-07 16:47:13');
INSERT INTO `sys_logininfor` VALUES ('1990', 'admin', '/127.0.0.1:11338', '', '', '', '1', '退出登录', '2022-07-07 16:47:13');
INSERT INTO `sys_logininfor` VALUES ('1991', 'admin', '/127.0.0.1:11338', '', '', '', '1', '连接异常', '2022-07-07 16:47:13');
INSERT INTO `sys_logininfor` VALUES ('1992', 'admin', '/127.0.0.1:11664', '', '', '', '0', '登录成功！', '2022-07-07 16:52:43');
INSERT INTO `sys_logininfor` VALUES ('1993', 'admin', '/127.0.0.1:11664', '', '', '', '1', '连接异常', '2022-07-07 16:52:43');
INSERT INTO `sys_logininfor` VALUES ('1994', 'admin', '/127.0.0.1:11664', '', '', '', '1', '退出登录', '2022-07-07 16:52:43');
INSERT INTO `sys_logininfor` VALUES ('1995', 'admin', '/127.0.0.1:11664', '', '', '', '1', '连接异常', '2022-07-07 16:52:43');
INSERT INTO `sys_logininfor` VALUES ('1996', 'admin', '/127.0.0.1:12004', '', '', '', '0', '登录成功！', '2022-07-07 16:58:37');
INSERT INTO `sys_logininfor` VALUES ('1997', 'admin', '/127.0.0.1:12004', '', '', '', '1', '连接异常', '2022-07-07 16:58:37');
INSERT INTO `sys_logininfor` VALUES ('1998', 'admin', '/127.0.0.1:12004', '', '', '', '1', '退出登录', '2022-07-07 16:58:37');
INSERT INTO `sys_logininfor` VALUES ('1999', 'admin', '/127.0.0.1:12004', '', '', '', '1', '连接异常', '2022-07-07 16:58:37');
INSERT INTO `sys_logininfor` VALUES ('2000', 'admin', '/127.0.0.1:12188', '', '', '', '0', '登录成功！', '2022-07-07 17:02:13');
INSERT INTO `sys_logininfor` VALUES ('2001', 'admin', '/127.0.0.1:12188', '', '', '', '1', '连接异常', '2022-07-07 17:02:13');
INSERT INTO `sys_logininfor` VALUES ('2002', 'admin', '/127.0.0.1:12188', '', '', '', '1', '退出登录', '2022-07-07 17:02:13');
INSERT INTO `sys_logininfor` VALUES ('2003', 'admin', '/127.0.0.1:12188', '', '', '', '1', '连接异常', '2022-07-07 17:02:13');
INSERT INTO `sys_logininfor` VALUES ('2004', 'admin', '/127.0.0.1:3429', '', '', '', '0', '登录成功！', '2022-07-07 17:08:39');
INSERT INTO `sys_logininfor` VALUES ('2005', 'admin', '/127.0.0.1:3429', '', '', '', '1', '连接异常', '2022-07-07 17:08:39');
INSERT INTO `sys_logininfor` VALUES ('2006', 'admin', '/127.0.0.1:3429', '', '', '', '1', '退出登录', '2022-07-07 17:08:39');
INSERT INTO `sys_logininfor` VALUES ('2007', 'admin', '/127.0.0.1:3429', '', '', '', '1', '连接异常', '2022-07-07 17:08:39');
INSERT INTO `sys_logininfor` VALUES ('2008', 'admin', '/127.0.0.1:3639', '', '', '', '0', '登录成功！', '2022-07-07 17:12:16');
INSERT INTO `sys_logininfor` VALUES ('2009', 'admin', '/127.0.0.1:3639', '', '', '', '1', '连接异常', '2022-07-07 17:12:16');
INSERT INTO `sys_logininfor` VALUES ('2010', 'admin', '/127.0.0.1:3639', '', '', '', '1', '退出登录', '2022-07-07 17:12:16');
INSERT INTO `sys_logininfor` VALUES ('2011', 'admin', '/127.0.0.1:3639', '', '', '', '1', '连接异常', '2022-07-07 17:12:16');
INSERT INTO `sys_logininfor` VALUES ('2012', 'admin', '/127.0.0.1:3979', '', '', '', '0', '登录成功！', '2022-07-07 17:18:43');
INSERT INTO `sys_logininfor` VALUES ('2013', 'admin', '/127.0.0.1:3979', '', '', '', '1', '连接异常', '2022-07-07 17:18:43');
INSERT INTO `sys_logininfor` VALUES ('2014', 'admin', '/127.0.0.1:3979', '', '', '', '1', '退出登录', '2022-07-07 17:18:43');
INSERT INTO `sys_logininfor` VALUES ('2015', 'admin', '/127.0.0.1:3979', '', '', '', '1', '连接异常', '2022-07-07 17:18:43');
INSERT INTO `sys_logininfor` VALUES ('2016', 'admin', '/127.0.0.1:4055', '', '', '', '0', '登录成功！', '2022-07-07 17:19:59');
INSERT INTO `sys_logininfor` VALUES ('2017', 'admin', '/127.0.0.1:4055', '', '', '', '1', '连接异常', '2022-07-07 17:19:59');
INSERT INTO `sys_logininfor` VALUES ('2018', 'admin', '/127.0.0.1:4055', '', '', '', '1', '退出登录', '2022-07-07 17:19:59');
INSERT INTO `sys_logininfor` VALUES ('2019', 'admin', '/127.0.0.1:4055', '', '', '', '1', '连接异常', '2022-07-07 17:19:59');
INSERT INTO `sys_logininfor` VALUES ('2020', 'admin', '/127.0.0.1:4134', '', '', '', '0', '登录成功！', '2022-07-07 17:21:23');
INSERT INTO `sys_logininfor` VALUES ('2021', 'admin', '/127.0.0.1:9586', '', '', '', '0', '登录成功！', '2022-07-08 08:52:11');
INSERT INTO `sys_logininfor` VALUES ('2022', 'admin', '/127.0.0.1:9586', '', '', '', '1', '连接异常', '2022-07-08 08:52:11');
INSERT INTO `sys_logininfor` VALUES ('2023', 'admin', '/127.0.0.1:9586', '', '', '', '1', '退出登录', '2022-07-08 08:52:11');
INSERT INTO `sys_logininfor` VALUES ('2024', 'admin', '/127.0.0.1:9586', '', '', '', '1', '连接异常', '2022-07-08 08:52:11');
INSERT INTO `sys_logininfor` VALUES ('2025', 'admin', '/127.0.0.1:10266', '', '', '', '0', '登录成功！', '2022-07-08 09:09:24');
INSERT INTO `sys_logininfor` VALUES ('2026', 'admin', '/127.0.0.1:10266', '', '', '', '1', '连接异常', '2022-07-08 09:09:24');
INSERT INTO `sys_logininfor` VALUES ('2027', 'admin', '/127.0.0.1:10266', '', '', '', '1', '退出登录', '2022-07-08 09:09:24');
INSERT INTO `sys_logininfor` VALUES ('2028', 'admin', '/127.0.0.1:10266', '', '', '', '1', '连接异常', '2022-07-08 09:09:24');
INSERT INTO `sys_logininfor` VALUES ('2029', 'admin', '/127.0.0.1:4902', '', '', '', '0', '登录成功！', '2022-07-08 09:25:33');
INSERT INTO `sys_logininfor` VALUES ('2030', 'admin', '/127.0.0.1:4902', '', '', '', '1', '退出登录', '2022-07-08 09:25:33');
INSERT INTO `sys_logininfor` VALUES ('2031', 'admin', '/127.0.0.1:5793', '', '', '', '0', '登录成功！', '2022-07-08 09:44:55');
INSERT INTO `sys_logininfor` VALUES ('2032', 'admin', '/127.0.0.1:5793', '', '', '', '1', '连接异常', '2022-07-08 09:44:55');
INSERT INTO `sys_logininfor` VALUES ('2033', 'admin', '/127.0.0.1:5793', '', '', '', '1', '退出登录', '2022-07-08 09:44:55');
INSERT INTO `sys_logininfor` VALUES ('2034', 'admin', '/127.0.0.1:5793', '', '', '', '1', '连接异常', '2022-07-08 09:44:55');
INSERT INTO `sys_logininfor` VALUES ('2035', 'admin', '/127.0.0.1:6147', '', '', '', '0', '登录成功！', '2022-07-08 09:54:08');
INSERT INTO `sys_logininfor` VALUES ('2036', 'admin', '/127.0.0.1:6147', '', '', '', '1', '连接异常', '2022-07-08 09:54:08');
INSERT INTO `sys_logininfor` VALUES ('2037', 'admin', '/127.0.0.1:6147', '', '', '', '1', '退出登录', '2022-07-08 09:54:08');
INSERT INTO `sys_logininfor` VALUES ('2038', 'admin', '/127.0.0.1:6147', '', '', '', '1', '连接异常', '2022-07-08 09:54:08');
INSERT INTO `sys_logininfor` VALUES ('2039', 'admin', '/127.0.0.1:6273', '', '', '', '0', '登录成功！', '2022-07-08 09:57:30');
INSERT INTO `sys_logininfor` VALUES ('2040', 'admin', '/127.0.0.1:6273', '', '', '', '1', '连接异常', '2022-07-08 09:57:30');
INSERT INTO `sys_logininfor` VALUES ('2041', 'admin', '/127.0.0.1:6273', '', '', '', '1', '退出登录', '2022-07-08 09:57:30');
INSERT INTO `sys_logininfor` VALUES ('2042', 'admin', '/127.0.0.1:6273', '', '', '', '1', '连接异常', '2022-07-08 09:57:30');
INSERT INTO `sys_logininfor` VALUES ('2043', 'admin', '/127.0.0.1:6345', '', '', '', '0', '登录成功！', '2022-07-08 09:59:28');
INSERT INTO `sys_logininfor` VALUES ('2044', 'admin', '/127.0.0.1:6345', '', '', '', '1', '连接异常', '2022-07-08 09:59:28');
INSERT INTO `sys_logininfor` VALUES ('2045', 'admin', '/127.0.0.1:6345', '', '', '', '1', '退出登录', '2022-07-08 09:59:28');
INSERT INTO `sys_logininfor` VALUES ('2046', 'admin', '/127.0.0.1:6345', '', '', '', '1', '连接异常', '2022-07-08 09:59:28');
INSERT INTO `sys_logininfor` VALUES ('2047', 'admin', '/127.0.0.1:6436', '', '', '', '0', '登录成功！', '2022-07-08 10:01:19');
INSERT INTO `sys_logininfor` VALUES ('2048', 'admin', '/127.0.0.1:6436', '', '', '', '1', '连接异常', '2022-07-08 10:01:19');
INSERT INTO `sys_logininfor` VALUES ('2049', 'admin', '/127.0.0.1:6436', '', '', '', '1', '退出登录', '2022-07-08 10:01:19');
INSERT INTO `sys_logininfor` VALUES ('2050', 'admin', '/127.0.0.1:6436', '', '', '', '1', '连接异常', '2022-07-08 10:01:19');
INSERT INTO `sys_logininfor` VALUES ('2051', 'admin', '/127.0.0.1:3588', '', '', '', '0', '登录成功！', '2022-07-08 10:31:33');
INSERT INTO `sys_logininfor` VALUES ('2052', 'admin', '/127.0.0.1:3588', '', '', '', '1', '连接异常', '2022-07-08 10:31:33');
INSERT INTO `sys_logininfor` VALUES ('2053', 'admin', '/127.0.0.1:3588', '', '', '', '1', '退出登录', '2022-07-08 10:31:33');
INSERT INTO `sys_logininfor` VALUES ('2054', 'admin', '/127.0.0.1:3588', '', '', '', '1', '连接异常', '2022-07-08 10:31:33');
INSERT INTO `sys_logininfor` VALUES ('2055', 'admin', '/127.0.0.1:4036', '', '', '', '0', '登录成功！', '2022-07-08 10:38:43');
INSERT INTO `sys_logininfor` VALUES ('2056', 'admin', '/127.0.0.1:4036', '', '', '', '1', '连接异常', '2022-07-08 10:38:43');
INSERT INTO `sys_logininfor` VALUES ('2057', 'admin', '/127.0.0.1:4036', '', '', '', '1', '退出登录', '2022-07-08 10:38:43');
INSERT INTO `sys_logininfor` VALUES ('2058', 'admin', '/127.0.0.1:4036', '', '', '', '1', '连接异常', '2022-07-08 10:38:43');
INSERT INTO `sys_logininfor` VALUES ('2059', 'admin', '/127.0.0.1:4091', '', '', '', '0', '登录成功！', '2022-07-08 10:39:41');
INSERT INTO `sys_logininfor` VALUES ('2060', 'admin', '/127.0.0.1:4091', '', '', '', '1', '连接异常', '2022-07-08 10:39:41');
INSERT INTO `sys_logininfor` VALUES ('2061', 'admin', '/127.0.0.1:4091', '', '', '', '1', '退出登录', '2022-07-08 10:39:41');
INSERT INTO `sys_logininfor` VALUES ('2062', 'admin', '/127.0.0.1:4091', '', '', '', '1', '连接异常', '2022-07-08 10:39:41');
INSERT INTO `sys_logininfor` VALUES ('2063', 'admin', '/127.0.0.1:4117', '', '', '', '0', '登录成功！', '2022-07-08 10:40:08');
INSERT INTO `sys_logininfor` VALUES ('2064', 'admin', '/127.0.0.1:4117', '', '', '', '1', '连接异常', '2022-07-08 10:40:08');
INSERT INTO `sys_logininfor` VALUES ('2065', 'admin', '/127.0.0.1:4117', '', '', '', '1', '退出登录', '2022-07-08 10:40:08');
INSERT INTO `sys_logininfor` VALUES ('2066', 'admin', '/127.0.0.1:4117', '', '', '', '1', '连接异常', '2022-07-08 10:40:08');
INSERT INTO `sys_logininfor` VALUES ('2067', 'admin', '/127.0.0.1:4464', '', '', '', '0', '登录成功！', '2022-07-08 10:49:18');
INSERT INTO `sys_logininfor` VALUES ('2068', 'admin', '/127.0.0.1:4464', '', '', '', '1', '连接异常', '2022-07-08 10:49:18');
INSERT INTO `sys_logininfor` VALUES ('2069', 'admin', '/127.0.0.1:4464', '', '', '', '1', '退出登录', '2022-07-08 10:49:18');
INSERT INTO `sys_logininfor` VALUES ('2070', 'admin', '/127.0.0.1:4464', '', '', '', '1', '连接异常', '2022-07-08 10:49:18');
INSERT INTO `sys_logininfor` VALUES ('2071', 'admin', '/127.0.0.1:4746', '', '', '', '0', '登录成功！', '2022-07-08 10:55:41');
INSERT INTO `sys_logininfor` VALUES ('2072', 'admin', '/127.0.0.1:4746', '', '', '', '1', '连接异常', '2022-07-08 10:55:41');
INSERT INTO `sys_logininfor` VALUES ('2073', 'admin', '/127.0.0.1:4746', '', '', '', '1', '退出登录', '2022-07-08 10:55:41');
INSERT INTO `sys_logininfor` VALUES ('2074', 'admin', '/127.0.0.1:4746', '', '', '', '1', '连接异常', '2022-07-08 10:55:41');
INSERT INTO `sys_logininfor` VALUES ('2075', 'admin', '/127.0.0.1:4904', '', '', '', '0', '登录成功！', '2022-07-08 10:59:42');
INSERT INTO `sys_logininfor` VALUES ('2076', 'admin', '/127.0.0.1:4904', '', '', '', '1', '连接异常', '2022-07-08 10:59:42');
INSERT INTO `sys_logininfor` VALUES ('2077', 'admin', '/127.0.0.1:4904', '', '', '', '1', '退出登录', '2022-07-08 10:59:42');
INSERT INTO `sys_logininfor` VALUES ('2078', 'admin', '/127.0.0.1:4904', '', '', '', '1', '连接异常', '2022-07-08 10:59:42');
INSERT INTO `sys_logininfor` VALUES ('2079', 'admin', '/127.0.0.1:5384', '', '', '', '0', '登录成功！', '2022-07-08 11:12:43');
INSERT INTO `sys_logininfor` VALUES ('2080', 'admin', '/127.0.0.1:5384', '', '', '', '1', '退出登录', '2022-07-08 11:12:43');
INSERT INTO `sys_logininfor` VALUES ('2081', 'admin', '/127.0.0.1:8347', '', '', '', '0', '登录成功！', '2022-07-08 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2082', 'admin', '/127.0.0.1:8347', '', '', '', '1', '连接异常', '2022-07-08 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2083', 'admin', '/127.0.0.1:8347', '', '', '', '1', '退出登录', '2022-07-08 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2084', 'admin', '/127.0.0.1:8347', '', '', '', '1', '连接异常', '2022-07-08 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2085', 'admin', '/127.0.0.1:9866', '', '', '', '0', '登录成功！', '2022-07-08 16:50:05');
INSERT INTO `sys_logininfor` VALUES ('2086', 'admin', '/127.0.0.1:9866', '', '', '', '1', '连接异常', '2022-07-08 16:50:05');
INSERT INTO `sys_logininfor` VALUES ('2087', 'admin', '/127.0.0.1:9866', '', '', '', '1', '退出登录', '2022-07-08 16:50:05');
INSERT INTO `sys_logininfor` VALUES ('2088', 'admin', '/127.0.0.1:9866', '', '', '', '1', '连接异常', '2022-07-08 16:50:05');
INSERT INTO `sys_logininfor` VALUES ('2089', 'admin', '/127.0.0.1:10783', '', '', '', '0', '登录成功！', '2022-07-08 17:04:59');
INSERT INTO `sys_logininfor` VALUES ('2090', 'admin', '/127.0.0.1:10783', '', '', '', '1', '连接异常', '2022-07-08 17:04:59');
INSERT INTO `sys_logininfor` VALUES ('2091', 'admin', '/127.0.0.1:10783', '', '', '', '1', '退出登录', '2022-07-08 17:04:59');
INSERT INTO `sys_logininfor` VALUES ('2092', 'admin', '/127.0.0.1:10783', '', '', '', '1', '连接异常', '2022-07-08 17:04:59');
INSERT INTO `sys_logininfor` VALUES ('2093', 'admin', '/127.0.0.1:10895', '', '', '', '0', '登录成功！', '2022-07-08 17:07:07');
INSERT INTO `sys_logininfor` VALUES ('2094', 'admin', '/127.0.0.1:10895', '', '', '', '1', '连接异常', '2022-07-08 17:07:07');
INSERT INTO `sys_logininfor` VALUES ('2095', 'admin', '/127.0.0.1:10895', '', '', '', '1', '退出登录', '2022-07-08 17:07:07');
INSERT INTO `sys_logininfor` VALUES ('2096', 'admin', '/127.0.0.1:10895', '', '', '', '1', '连接异常', '2022-07-08 17:07:07');
INSERT INTO `sys_logininfor` VALUES ('2097', 'admin', '/127.0.0.1:10925', '', '', '', '0', '登录成功！', '2022-07-08 17:07:37');
INSERT INTO `sys_logininfor` VALUES ('2098', 'admin', '/127.0.0.1:10925', '', '', '', '1', '连接异常', '2022-07-08 17:07:37');
INSERT INTO `sys_logininfor` VALUES ('2099', 'admin', '/127.0.0.1:10925', '', '', '', '1', '退出登录', '2022-07-08 17:07:37');
INSERT INTO `sys_logininfor` VALUES ('2100', 'admin', '/127.0.0.1:10925', '', '', '', '1', '连接异常', '2022-07-08 17:07:37');
INSERT INTO `sys_logininfor` VALUES ('2101', 'admin', '/127.0.0.1:10966', '', '', '', '0', '登录成功！', '2022-07-08 17:08:16');
INSERT INTO `sys_logininfor` VALUES ('2102', 'admin', '/127.0.0.1:10966', '', '', '', '1', '连接异常', '2022-07-08 17:08:16');
INSERT INTO `sys_logininfor` VALUES ('2103', 'admin', '/127.0.0.1:10966', '', '', '', '1', '退出登录', '2022-07-08 17:08:16');
INSERT INTO `sys_logininfor` VALUES ('2104', 'admin', '/127.0.0.1:10966', '', '', '', '1', '连接异常', '2022-07-08 17:08:16');
INSERT INTO `sys_logininfor` VALUES ('2105', 'admin', '/127.0.0.1:10779', '', '', '', '0', '登录成功！', '2022-07-08 17:11:23');
INSERT INTO `sys_logininfor` VALUES ('2106', 'admin', '/127.0.0.1:10779', '', '', '', '1', '连接异常', '2022-07-08 17:11:23');
INSERT INTO `sys_logininfor` VALUES ('2107', 'admin', '/127.0.0.1:10779', '', '', '', '1', '退出登录', '2022-07-08 17:11:23');
INSERT INTO `sys_logininfor` VALUES ('2108', 'admin', '/127.0.0.1:10779', '', '', '', '1', '连接异常', '2022-07-08 17:11:23');
INSERT INTO `sys_logininfor` VALUES ('2109', 'admin', '/127.0.0.1:10862', '', '', '', '0', '登录成功！', '2022-07-08 17:12:23');
INSERT INTO `sys_logininfor` VALUES ('2110', 'admin', '/127.0.0.1:10862', '', '', '', '1', '连接异常', '2022-07-08 17:12:23');
INSERT INTO `sys_logininfor` VALUES ('2111', 'admin', '/127.0.0.1:10862', '', '', '', '1', '退出登录', '2022-07-08 17:12:23');
INSERT INTO `sys_logininfor` VALUES ('2112', 'admin', '/127.0.0.1:10862', '', '', '', '1', '连接异常', '2022-07-08 17:12:23');
INSERT INTO `sys_logininfor` VALUES ('2113', 'admin', '/127.0.0.1:10892', '', '', '', '0', '登录成功！', '2022-07-08 17:12:53');
INSERT INTO `sys_logininfor` VALUES ('2114', 'admin', '/127.0.0.1:10892', '', '', '', '1', '连接异常', '2022-07-08 17:12:53');
INSERT INTO `sys_logininfor` VALUES ('2115', 'admin', '/127.0.0.1:10892', '', '', '', '1', '退出登录', '2022-07-08 17:12:53');
INSERT INTO `sys_logininfor` VALUES ('2116', 'admin', '/127.0.0.1:10892', '', '', '', '1', '连接异常', '2022-07-08 17:12:53');
INSERT INTO `sys_logininfor` VALUES ('2117', 'admin', '/127.0.0.1:11352', '', '', '', '0', '登录成功！', '2022-07-08 17:22:54');
INSERT INTO `sys_logininfor` VALUES ('2118', 'admin', '/127.0.0.1:11352', '', '', '', '1', '连接异常', '2022-07-08 17:22:54');
INSERT INTO `sys_logininfor` VALUES ('2119', 'admin', '/127.0.0.1:11352', '', '', '', '1', '退出登录', '2022-07-08 17:22:54');
INSERT INTO `sys_logininfor` VALUES ('2120', 'admin', '/127.0.0.1:11352', '', '', '', '1', '连接异常', '2022-07-08 17:22:54');
INSERT INTO `sys_logininfor` VALUES ('2121', 'admin', '/127.0.0.1:11495', '', '', '', '0', '登录成功！', '2022-07-08 17:25:25');
INSERT INTO `sys_logininfor` VALUES ('2122', 'admin', '/127.0.0.1:11495', '', '', '', '1', '退出登录', '2022-07-08 17:25:25');
INSERT INTO `sys_logininfor` VALUES ('2123', 'admin', '/127.0.0.1:12059', '', '', '', '0', '登录成功！', '2022-07-11 08:53:13');
INSERT INTO `sys_logininfor` VALUES ('2124', 'admin', '/127.0.0.1:12059', '', '', '', '1', '连接异常', '2022-07-11 08:53:13');
INSERT INTO `sys_logininfor` VALUES ('2125', 'admin', '/127.0.0.1:12059', '', '', '', '1', '退出登录', '2022-07-11 08:53:13');
INSERT INTO `sys_logininfor` VALUES ('2126', 'admin', '/127.0.0.1:12059', '', '', '', '1', '连接异常', '2022-07-11 08:53:13');
INSERT INTO `sys_logininfor` VALUES ('2127', 'admin', '/127.0.0.1:13914', '', '', '', '0', '登录成功！', '2022-07-11 10:49:56');
INSERT INTO `sys_logininfor` VALUES ('2128', 'admin', '/127.0.0.1:13914', '', '', '', '1', '连接异常', '2022-07-11 10:49:56');
INSERT INTO `sys_logininfor` VALUES ('2129', 'admin', '/127.0.0.1:13914', '', '', '', '1', '退出登录', '2022-07-11 10:49:56');
INSERT INTO `sys_logininfor` VALUES ('2130', 'admin', '/127.0.0.1:13914', '', '', '', '1', '连接异常', '2022-07-11 10:49:56');
INSERT INTO `sys_logininfor` VALUES ('2131', 'admin', '/127.0.0.1:13979', '', '', '', '0', '登录成功！', '2022-07-11 10:51:42');
INSERT INTO `sys_logininfor` VALUES ('2132', 'admin', '/127.0.0.1:13979', '', '', '', '1', '连接异常', '2022-07-11 10:51:42');
INSERT INTO `sys_logininfor` VALUES ('2133', 'admin', '/127.0.0.1:13979', '', '', '', '1', '退出登录', '2022-07-11 10:51:42');
INSERT INTO `sys_logininfor` VALUES ('2134', 'admin', '/127.0.0.1:13979', '', '', '', '1', '连接异常', '2022-07-11 10:51:42');
INSERT INTO `sys_logininfor` VALUES ('2135', 'admin', '/127.0.0.1:14415', '', '', '', '0', '登录成功！', '2022-07-11 11:04:08');
INSERT INTO `sys_logininfor` VALUES ('2136', 'admin', '/127.0.0.1:14415', '', '', '', '1', '连接异常', '2022-07-11 11:04:08');
INSERT INTO `sys_logininfor` VALUES ('2137', 'admin', '/127.0.0.1:14415', '', '', '', '1', '退出登录', '2022-07-11 11:04:08');
INSERT INTO `sys_logininfor` VALUES ('2138', 'admin', '/127.0.0.1:14415', '', '', '', '1', '连接异常', '2022-07-11 11:04:08');
INSERT INTO `sys_logininfor` VALUES ('2139', 'admin', '/127.0.0.1:14433', '', '', '', '0', '登录成功！', '2022-07-11 11:04:45');
INSERT INTO `sys_logininfor` VALUES ('2140', 'admin', '/127.0.0.1:14433', '', '', '', '1', '连接异常', '2022-07-11 11:04:45');
INSERT INTO `sys_logininfor` VALUES ('2141', 'admin', '/127.0.0.1:14433', '', '', '', '1', '退出登录', '2022-07-11 11:04:45');
INSERT INTO `sys_logininfor` VALUES ('2142', 'admin', '/127.0.0.1:14433', '', '', '', '1', '连接异常', '2022-07-11 11:04:45');
INSERT INTO `sys_logininfor` VALUES ('2143', 'admin', '/127.0.0.1:14510', '', '', '', '0', '登录成功！', '2022-07-11 11:07:32');
INSERT INTO `sys_logininfor` VALUES ('2144', 'admin', '/127.0.0.1:14510', '', '', '', '1', '连接异常', '2022-07-11 11:07:32');
INSERT INTO `sys_logininfor` VALUES ('2145', 'admin', '/127.0.0.1:14510', '', '', '', '1', '退出登录', '2022-07-11 11:07:32');
INSERT INTO `sys_logininfor` VALUES ('2146', 'admin', '/127.0.0.1:14510', '', '', '', '1', '连接异常', '2022-07-11 11:07:32');
INSERT INTO `sys_logininfor` VALUES ('2147', 'admin', '/127.0.0.1:14561', '', '', '', '0', '登录成功！', '2022-07-11 11:09:14');
INSERT INTO `sys_logininfor` VALUES ('2148', 'admin', '/127.0.0.1:14561', '', '', '', '1', '连接异常', '2022-07-11 11:09:14');
INSERT INTO `sys_logininfor` VALUES ('2149', 'admin', '/127.0.0.1:14561', '', '', '', '1', '退出登录', '2022-07-11 11:09:14');
INSERT INTO `sys_logininfor` VALUES ('2150', 'admin', '/127.0.0.1:14561', '', '', '', '1', '连接异常', '2022-07-11 11:09:14');
INSERT INTO `sys_logininfor` VALUES ('2151', 'admin', '/127.0.0.1:14910', '', '', '', '0', '登录成功！', '2022-07-11 11:18:14');
INSERT INTO `sys_logininfor` VALUES ('2152', 'admin', '/127.0.0.1:14910', '', '', '', '1', '连接异常', '2022-07-11 11:18:14');
INSERT INTO `sys_logininfor` VALUES ('2153', 'admin', '/127.0.0.1:14910', '', '', '', '1', '退出登录', '2022-07-11 11:18:14');
INSERT INTO `sys_logininfor` VALUES ('2154', 'admin', '/127.0.0.1:14910', '', '', '', '1', '连接异常', '2022-07-11 11:18:14');
INSERT INTO `sys_logininfor` VALUES ('2155', 'admin', '/127.0.0.1:14972', '', '', '', '0', '登录成功！', '2022-07-11 11:19:56');
INSERT INTO `sys_logininfor` VALUES ('2156', 'admin', '/127.0.0.1:14972', '', '', '', '1', '连接异常', '2022-07-11 11:19:56');
INSERT INTO `sys_logininfor` VALUES ('2157', 'admin', '/127.0.0.1:14972', '', '', '', '1', '退出登录', '2022-07-11 11:19:56');
INSERT INTO `sys_logininfor` VALUES ('2158', 'admin', '/127.0.0.1:14972', '', '', '', '1', '连接异常', '2022-07-11 11:19:56');
INSERT INTO `sys_logininfor` VALUES ('2159', 'admin', '/127.0.0.1:1131', '', '', '', '0', '登录成功！', '2022-07-11 11:24:36');
INSERT INTO `sys_logininfor` VALUES ('2160', 'admin', '/127.0.0.1:1131', '', '', '', '1', '连接异常', '2022-07-11 11:24:36');
INSERT INTO `sys_logininfor` VALUES ('2161', 'admin', '/127.0.0.1:1131', '', '', '', '1', '退出登录', '2022-07-11 11:24:36');
INSERT INTO `sys_logininfor` VALUES ('2162', 'admin', '/127.0.0.1:1131', '', '', '', '1', '连接异常', '2022-07-11 11:24:36');
INSERT INTO `sys_logininfor` VALUES ('2163', 'admin', '/127.0.0.1:1300', '', '', '', '0', '登录成功！', '2022-07-11 11:30:55');
INSERT INTO `sys_logininfor` VALUES ('2164', 'admin', '/127.0.0.1:1300', '', '', '', '1', '连接异常', '2022-07-11 11:30:55');
INSERT INTO `sys_logininfor` VALUES ('2165', 'admin', '/127.0.0.1:1300', '', '', '', '1', '退出登录', '2022-07-11 11:30:55');
INSERT INTO `sys_logininfor` VALUES ('2166', 'admin', '/127.0.0.1:1300', '', '', '', '1', '连接异常', '2022-07-11 11:30:55');
INSERT INTO `sys_logininfor` VALUES ('2167', 'admin', '/127.0.0.1:1554', '', '', '', '0', '登录成功！', '2022-07-11 11:40:44');
INSERT INTO `sys_logininfor` VALUES ('2168', 'admin', '/127.0.0.1:1554', '', '', '', '1', '连接异常', '2022-07-11 11:40:44');
INSERT INTO `sys_logininfor` VALUES ('2169', 'admin', '/127.0.0.1:1554', '', '', '', '1', '退出登录', '2022-07-11 11:40:44');
INSERT INTO `sys_logininfor` VALUES ('2170', 'admin', '/127.0.0.1:1554', '', '', '', '1', '连接异常', '2022-07-11 11:40:44');
INSERT INTO `sys_logininfor` VALUES ('2171', 'admin', '/127.0.0.1:1693', '', '', '', '0', '登录成功！', '2022-07-11 11:45:44');
INSERT INTO `sys_logininfor` VALUES ('2172', 'admin', '/127.0.0.1:1693', '', '', '', '1', '连接异常', '2022-07-11 11:45:44');
INSERT INTO `sys_logininfor` VALUES ('2173', 'admin', '/127.0.0.1:1693', '', '', '', '1', '退出登录', '2022-07-11 11:45:44');
INSERT INTO `sys_logininfor` VALUES ('2174', 'admin', '/127.0.0.1:1693', '', '', '', '1', '连接异常', '2022-07-11 11:45:44');
INSERT INTO `sys_logininfor` VALUES ('2175', 'admin', '/127.0.0.1:1748', '', '', '', '0', '登录成功！', '2022-07-11 11:47:36');
INSERT INTO `sys_logininfor` VALUES ('2176', 'admin', '/127.0.0.1:1748', '', '', '', '1', '连接异常', '2022-07-11 11:47:36');
INSERT INTO `sys_logininfor` VALUES ('2177', 'admin', '/127.0.0.1:1748', '', '', '', '1', '退出登录', '2022-07-11 11:47:36');
INSERT INTO `sys_logininfor` VALUES ('2178', 'admin', '/127.0.0.1:1748', '', '', '', '1', '连接异常', '2022-07-11 11:47:36');
INSERT INTO `sys_logininfor` VALUES ('2179', 'admin', '/127.0.0.1:5600', '', '', '', '0', '登录成功！', '2022-07-11 13:20:56');
INSERT INTO `sys_logininfor` VALUES ('2180', 'admin', '/127.0.0.1:5600', '', '', '', '1', '连接异常', '2022-07-11 13:20:56');
INSERT INTO `sys_logininfor` VALUES ('2181', 'admin', '/127.0.0.1:5600', '', '', '', '1', '退出登录', '2022-07-11 13:20:56');
INSERT INTO `sys_logininfor` VALUES ('2182', 'admin', '/127.0.0.1:5600', '', '', '', '1', '连接异常', '2022-07-11 13:20:56');
INSERT INTO `sys_logininfor` VALUES ('2183', 'admin', '/127.0.0.1:6077', '', '', '', '0', '登录成功！', '2022-07-11 13:38:20');
INSERT INTO `sys_logininfor` VALUES ('2184', 'admin', '/127.0.0.1:6077', '', '', '', '1', '连接异常', '2022-07-11 13:38:20');
INSERT INTO `sys_logininfor` VALUES ('2185', 'admin', '/127.0.0.1:6077', '', '', '', '1', '退出登录', '2022-07-11 13:38:20');
INSERT INTO `sys_logininfor` VALUES ('2186', 'admin', '/127.0.0.1:6077', '', '', '', '1', '连接异常', '2022-07-11 13:38:20');
INSERT INTO `sys_logininfor` VALUES ('2187', 'admin', '/127.0.0.1:6265', '', '', '', '0', '登录成功！', '2022-07-11 13:45:04');
INSERT INTO `sys_logininfor` VALUES ('2188', 'admin', '/127.0.0.1:6265', '', '', '', '1', '连接异常', '2022-07-11 13:45:04');
INSERT INTO `sys_logininfor` VALUES ('2189', 'admin', '/127.0.0.1:6265', '', '', '', '1', '退出登录', '2022-07-11 13:45:04');
INSERT INTO `sys_logininfor` VALUES ('2190', 'admin', '/127.0.0.1:6265', '', '', '', '1', '连接异常', '2022-07-11 13:45:04');
INSERT INTO `sys_logininfor` VALUES ('2191', 'admin', '/127.0.0.1:6520', '', '', '', '0', '登录成功！', '2022-07-11 13:53:45');
INSERT INTO `sys_logininfor` VALUES ('2192', 'admin', '/127.0.0.1:6520', '', '', '', '1', '连接异常', '2022-07-11 13:53:45');
INSERT INTO `sys_logininfor` VALUES ('2193', 'admin', '/127.0.0.1:6520', '', '', '', '1', '退出登录', '2022-07-11 13:53:45');
INSERT INTO `sys_logininfor` VALUES ('2194', 'admin', '/127.0.0.1:6520', '', '', '', '1', '连接异常', '2022-07-11 13:53:45');
INSERT INTO `sys_logininfor` VALUES ('2195', 'admin', '/127.0.0.1:6705', '', '', '', '0', '登录成功！', '2022-07-11 14:01:47');
INSERT INTO `sys_logininfor` VALUES ('2196', 'admin', '/127.0.0.1:6705', '', '', '', '1', '连接异常', '2022-07-11 14:01:47');
INSERT INTO `sys_logininfor` VALUES ('2197', 'admin', '/127.0.0.1:6705', '', '', '', '1', '退出登录', '2022-07-11 14:01:47');
INSERT INTO `sys_logininfor` VALUES ('2198', 'admin', '/127.0.0.1:6705', '', '', '', '1', '连接异常', '2022-07-11 14:01:47');
INSERT INTO `sys_logininfor` VALUES ('2199', 'admin', '/127.0.0.1:7575', '', '', '', '0', '登录成功！', '2022-07-11 14:28:00');
INSERT INTO `sys_logininfor` VALUES ('2200', 'admin', '/127.0.0.1:7575', '', '', '', '1', '退出登录', '2022-07-11 14:28:00');
INSERT INTO `sys_logininfor` VALUES ('2201', 'admin', '/127.0.0.1:2945', '', '', '', '0', '登录成功！', '2022-07-11 15:55:02');
INSERT INTO `sys_logininfor` VALUES ('2202', 'admin', '/127.0.0.1:7817', '', '', '', '0', '登录成功！', '2022-07-11 16:32:49');
INSERT INTO `sys_logininfor` VALUES ('2203', 'admin', '/127.0.0.1:7817', '', '', '', '1', '连接异常', '2022-07-11 16:32:49');
INSERT INTO `sys_logininfor` VALUES ('2204', 'admin', '/127.0.0.1:7817', '', '', '', '1', '退出登录', '2022-07-11 16:32:49');
INSERT INTO `sys_logininfor` VALUES ('2205', 'admin', '/127.0.0.1:7817', '', '', '', '1', '连接异常', '2022-07-11 16:32:49');
INSERT INTO `sys_logininfor` VALUES ('2206', 'admin', '/127.0.0.1:7927', '', '', '', '0', '登录成功！', '2022-07-11 16:34:01');
INSERT INTO `sys_logininfor` VALUES ('2207', 'admin', '/127.0.0.1:7927', '', '', '', '1', '连接异常', '2022-07-11 16:34:01');
INSERT INTO `sys_logininfor` VALUES ('2208', 'admin', '/127.0.0.1:7927', '', '', '', '1', '退出登录', '2022-07-11 16:34:01');
INSERT INTO `sys_logininfor` VALUES ('2209', 'admin', '/127.0.0.1:7927', '', '', '', '1', '连接异常', '2022-07-11 16:34:01');
INSERT INTO `sys_logininfor` VALUES ('2210', 'admin', '/127.0.0.1:6119', '', '', '', '0', '登录成功！', '2022-07-11 16:54:13');
INSERT INTO `sys_logininfor` VALUES ('2211', 'admin', '/127.0.0.1:6119', '', '', '', '1', '连接异常', '2022-07-11 16:54:13');
INSERT INTO `sys_logininfor` VALUES ('2212', 'admin', '/127.0.0.1:6119', '', '', '', '1', '退出登录', '2022-07-11 16:54:13');
INSERT INTO `sys_logininfor` VALUES ('2213', 'admin', '/127.0.0.1:6119', '', '', '', '1', '连接异常', '2022-07-11 16:54:13');
INSERT INTO `sys_logininfor` VALUES ('2214', 'admin', '/127.0.0.1:7176', '', '', '', '0', '登录成功！', '2022-07-11 17:05:10');
INSERT INTO `sys_logininfor` VALUES ('2215', 'admin', '/127.0.0.1:7176', '', '', '', '1', '连接异常', '2022-07-11 17:05:10');
INSERT INTO `sys_logininfor` VALUES ('2216', 'admin', '/127.0.0.1:7176', '', '', '', '1', '退出登录', '2022-07-11 17:05:10');
INSERT INTO `sys_logininfor` VALUES ('2217', 'admin', '/127.0.0.1:7176', '', '', '', '1', '连接异常', '2022-07-11 17:05:10');
INSERT INTO `sys_logininfor` VALUES ('2218', 'admin', '/127.0.0.1:8831', '', '', '', '0', '登录成功！', '2022-07-11 17:26:09');
INSERT INTO `sys_logininfor` VALUES ('2219', 'admin', '/127.0.0.1:8831', '', '', '', '1', '连接异常', '2022-07-11 17:26:09');
INSERT INTO `sys_logininfor` VALUES ('2220', 'admin', '/127.0.0.1:8831', '', '', '', '1', '退出登录', '2022-07-11 17:26:09');
INSERT INTO `sys_logininfor` VALUES ('2221', 'admin', '/127.0.0.1:8831', '', '', '', '1', '连接异常', '2022-07-11 17:26:09');
INSERT INTO `sys_logininfor` VALUES ('2222', 'admin', '/127.0.0.1:8934', '', '', '', '0', '登录成功！', '2022-07-11 17:27:07');
INSERT INTO `sys_logininfor` VALUES ('2223', 'admin', '/127.0.0.1:8934', '', '', '', '1', '连接异常', '2022-07-11 17:27:07');
INSERT INTO `sys_logininfor` VALUES ('2224', 'admin', '/127.0.0.1:8934', '', '', '', '1', '退出登录', '2022-07-11 17:27:07');
INSERT INTO `sys_logininfor` VALUES ('2225', 'admin', '/127.0.0.1:8934', '', '', '', '1', '连接异常', '2022-07-11 17:27:07');
INSERT INTO `sys_logininfor` VALUES ('2226', 'admin', '/127.0.0.1:9157', '', '', '', '0', '登录成功！', '2022-07-11 17:29:26');
INSERT INTO `sys_logininfor` VALUES ('2227', 'admin', '/127.0.0.1:13283', '', '', '', '0', '登录成功！', '2022-07-12 08:58:01');
INSERT INTO `sys_logininfor` VALUES ('2228', 'admin', '/127.0.0.1:13283', '', '', '', '1', '连接异常', '2022-07-12 08:58:01');
INSERT INTO `sys_logininfor` VALUES ('2229', 'admin', '/127.0.0.1:13283', '', '', '', '1', '退出登录', '2022-07-12 08:58:01');
INSERT INTO `sys_logininfor` VALUES ('2230', 'admin', '/127.0.0.1:13283', '', '', '', '1', '连接异常', '2022-07-12 08:58:01');
INSERT INTO `sys_logininfor` VALUES ('2231', 'admin', '/127.0.0.1:13320', '', '', '', '0', '登录成功！', '2022-07-12 08:59:41');
INSERT INTO `sys_logininfor` VALUES ('2232', 'admin', '/127.0.0.1:13320', '', '', '', '1', '连接异常', '2022-07-12 08:59:41');
INSERT INTO `sys_logininfor` VALUES ('2233', 'admin', '/127.0.0.1:13320', '', '', '', '1', '退出登录', '2022-07-12 08:59:41');
INSERT INTO `sys_logininfor` VALUES ('2234', 'admin', '/127.0.0.1:13320', '', '', '', '1', '连接异常', '2022-07-12 08:59:41');
INSERT INTO `sys_logininfor` VALUES ('2235', 'admin', '/127.0.0.1:7755', '', '', '', '0', '登录成功！', '2022-07-12 10:16:41');
INSERT INTO `sys_logininfor` VALUES ('2236', 'admin', '/127.0.0.1:7755', '', '', '', '1', '连接异常', '2022-07-12 10:16:41');
INSERT INTO `sys_logininfor` VALUES ('2237', 'admin', '/127.0.0.1:7755', '', '', '', '1', '退出登录', '2022-07-12 10:16:41');
INSERT INTO `sys_logininfor` VALUES ('2238', 'admin', '/127.0.0.1:7755', '', '', '', '1', '连接异常', '2022-07-12 10:16:41');
INSERT INTO `sys_logininfor` VALUES ('2239', 'admin', '/127.0.0.1:8039', '', '', '', '0', '登录成功！', '2022-07-12 10:33:12');
INSERT INTO `sys_logininfor` VALUES ('2240', 'admin', '/127.0.0.1:8039', '', '', '', '1', '连接异常', '2022-07-12 10:33:12');
INSERT INTO `sys_logininfor` VALUES ('2241', 'admin', '/127.0.0.1:8039', '', '', '', '1', '退出登录', '2022-07-12 10:33:12');
INSERT INTO `sys_logininfor` VALUES ('2242', 'admin', '/127.0.0.1:8039', '', '', '', '1', '连接异常', '2022-07-12 10:33:12');
INSERT INTO `sys_logininfor` VALUES ('2243', 'admin', '/127.0.0.1:8057', '', '', '', '0', '登录成功！', '2022-07-12 10:34:01');
INSERT INTO `sys_logininfor` VALUES ('2244', 'admin', '/127.0.0.1:8057', '', '', '', '1', '连接异常', '2022-07-12 10:34:01');
INSERT INTO `sys_logininfor` VALUES ('2245', 'admin', '/127.0.0.1:8057', '', '', '', '1', '退出登录', '2022-07-12 10:34:01');
INSERT INTO `sys_logininfor` VALUES ('2246', 'admin', '/127.0.0.1:8057', '', '', '', '1', '连接异常', '2022-07-12 10:34:01');
INSERT INTO `sys_logininfor` VALUES ('2247', 'admin', '/127.0.0.1:8106', '', '', '', '0', '登录成功！', '2022-07-12 10:35:56');
INSERT INTO `sys_logininfor` VALUES ('2248', 'admin', '/127.0.0.1:8106', '', '', '', '1', '连接异常', '2022-07-12 10:35:56');
INSERT INTO `sys_logininfor` VALUES ('2249', 'admin', '/127.0.0.1:8106', '', '', '', '1', '退出登录', '2022-07-12 10:35:56');
INSERT INTO `sys_logininfor` VALUES ('2250', 'admin', '/127.0.0.1:8106', '', '', '', '1', '连接异常', '2022-07-12 10:35:56');
INSERT INTO `sys_logininfor` VALUES ('2251', 'admin', '/127.0.0.1:8441', '', '', '', '0', '登录成功！', '2022-07-12 10:44:18');
INSERT INTO `sys_logininfor` VALUES ('2252', 'admin', '/127.0.0.1:8441', '', '', '', '1', '连接异常', '2022-07-12 10:44:18');
INSERT INTO `sys_logininfor` VALUES ('2253', 'admin', '/127.0.0.1:8441', '', '', '', '1', '退出登录', '2022-07-12 10:44:18');
INSERT INTO `sys_logininfor` VALUES ('2254', 'admin', '/127.0.0.1:8441', '', '', '', '1', '连接异常', '2022-07-12 10:44:18');
INSERT INTO `sys_logininfor` VALUES ('2255', 'admin', '/127.0.0.1:8940', '', '', '', '0', '登录成功！', '2022-07-12 11:02:52');
INSERT INTO `sys_logininfor` VALUES ('2256', 'admin', '/127.0.0.1:8940', '', '', '', '1', '连接异常', '2022-07-12 11:02:52');
INSERT INTO `sys_logininfor` VALUES ('2257', 'admin', '/127.0.0.1:8940', '', '', '', '1', '退出登录', '2022-07-12 11:02:52');
INSERT INTO `sys_logininfor` VALUES ('2258', 'admin', '/127.0.0.1:8940', '', '', '', '1', '连接异常', '2022-07-12 11:02:52');
INSERT INTO `sys_logininfor` VALUES ('2259', 'admin', '/127.0.0.1:10126', '', '', '', '0', '登录成功！', '2022-07-12 11:14:16');
INSERT INTO `sys_logininfor` VALUES ('2260', 'admin', '/127.0.0.1:10126', '', '', '', '1', '连接异常', '2022-07-12 11:14:16');
INSERT INTO `sys_logininfor` VALUES ('2261', 'admin', '/127.0.0.1:10126', '', '', '', '1', '退出登录', '2022-07-12 11:14:16');
INSERT INTO `sys_logininfor` VALUES ('2262', 'admin', '/127.0.0.1:10126', '', '', '', '1', '连接异常', '2022-07-12 11:14:16');
INSERT INTO `sys_logininfor` VALUES ('2263', 'admin', '/127.0.0.1:10291', '', '', '', '0', '登录成功！', '2022-07-12 11:23:20');
INSERT INTO `sys_logininfor` VALUES ('2264', 'admin', '/127.0.0.1:10291', '', '', '', '1', '连接异常', '2022-07-12 11:23:20');
INSERT INTO `sys_logininfor` VALUES ('2265', 'admin', '/127.0.0.1:10291', '', '', '', '1', '退出登录', '2022-07-12 11:23:20');
INSERT INTO `sys_logininfor` VALUES ('2266', 'admin', '/127.0.0.1:10291', '', '', '', '1', '连接异常', '2022-07-12 11:23:20');
INSERT INTO `sys_logininfor` VALUES ('2267', 'admin', '/127.0.0.1:10334', '', '', '', '0', '登录成功！', '2022-07-12 11:25:16');
INSERT INTO `sys_logininfor` VALUES ('2268', 'admin', '/127.0.0.1:10415', '', '', '', '0', '登录成功！', '2022-07-12 11:28:47');
INSERT INTO `sys_logininfor` VALUES ('2269', 'admin', '/127.0.0.1:10415', '', '', '', '1', '连接异常', '2022-07-12 11:28:47');
INSERT INTO `sys_logininfor` VALUES ('2270', 'admin', '/127.0.0.1:10415', '', '', '', '1', '退出登录', '2022-07-12 11:28:47');
INSERT INTO `sys_logininfor` VALUES ('2271', 'admin', '/127.0.0.1:10415', '', '', '', '1', '连接异常', '2022-07-12 11:28:47');
INSERT INTO `sys_logininfor` VALUES ('2272', 'admin', '/127.0.0.1:10481', '', '', '', '0', '登录成功！', '2022-07-12 11:35:18');
INSERT INTO `sys_logininfor` VALUES ('2273', 'admin', '/127.0.0.1:10481', '', '', '', '1', '连接异常', '2022-07-12 11:35:18');
INSERT INTO `sys_logininfor` VALUES ('2274', 'admin', '/127.0.0.1:10481', '', '', '', '1', '退出登录', '2022-07-12 11:35:18');
INSERT INTO `sys_logininfor` VALUES ('2275', 'admin', '/127.0.0.1:10481', '', '', '', '1', '连接异常', '2022-07-12 11:35:18');
INSERT INTO `sys_logininfor` VALUES ('2276', 'admin', '/127.0.0.1:10605', '', '', '', '0', '登录成功！', '2022-07-12 11:42:41');
INSERT INTO `sys_logininfor` VALUES ('2277', 'admin', '/127.0.0.1:10605', '', '', '', '1', '连接异常', '2022-07-12 11:42:41');
INSERT INTO `sys_logininfor` VALUES ('2278', 'admin', '/127.0.0.1:10605', '', '', '', '1', '退出登录', '2022-07-12 11:42:41');
INSERT INTO `sys_logininfor` VALUES ('2279', 'admin', '/127.0.0.1:10605', '', '', '', '1', '连接异常', '2022-07-12 11:42:41');
INSERT INTO `sys_logininfor` VALUES ('2280', 'admin', '/127.0.0.1:10631', '', '', '', '0', '登录成功！', '2022-07-12 11:45:40');
INSERT INTO `sys_logininfor` VALUES ('2281', 'admin', '/127.0.0.1:10631', '', '', '', '1', '连接异常', '2022-07-12 11:45:40');
INSERT INTO `sys_logininfor` VALUES ('2282', 'admin', '/127.0.0.1:10631', '', '', '', '1', '退出登录', '2022-07-12 11:45:40');
INSERT INTO `sys_logininfor` VALUES ('2283', 'admin', '/127.0.0.1:10631', '', '', '', '1', '连接异常', '2022-07-12 11:45:40');
INSERT INTO `sys_logininfor` VALUES ('2284', 'admin', '/127.0.0.1:11724', '', '', '', '0', '登录成功！', '2022-07-12 13:20:22');
INSERT INTO `sys_logininfor` VALUES ('2285', 'admin', '/127.0.0.1:11724', '', '', '', '1', '连接异常', '2022-07-12 13:20:22');
INSERT INTO `sys_logininfor` VALUES ('2286', 'admin', '/127.0.0.1:11724', '', '', '', '1', '退出登录', '2022-07-12 13:20:22');
INSERT INTO `sys_logininfor` VALUES ('2287', 'admin', '/127.0.0.1:11724', '', '', '', '1', '连接异常', '2022-07-12 13:20:22');
INSERT INTO `sys_logininfor` VALUES ('2288', 'admin', '/127.0.0.1:12372', '', '', '', '0', '登录成功！', '2022-07-12 13:39:44');
INSERT INTO `sys_logininfor` VALUES ('2289', 'admin', '/127.0.0.1:12372', '', '', '', '1', '退出登录', '2022-07-12 13:39:44');
INSERT INTO `sys_logininfor` VALUES ('2290', 'admin', '/127.0.0.1:12399', '', '', '', '0', '登录成功！', '2022-07-12 13:42:20');
INSERT INTO `sys_logininfor` VALUES ('2291', 'admin', '/127.0.0.1:12399', '', '', '', '1', '连接异常', '2022-07-12 13:42:20');
INSERT INTO `sys_logininfor` VALUES ('2292', 'admin', '/127.0.0.1:12399', '', '', '', '1', '退出登录', '2022-07-12 13:42:20');
INSERT INTO `sys_logininfor` VALUES ('2293', 'admin', '/127.0.0.1:12399', '', '', '', '1', '连接异常', '2022-07-12 13:42:20');
INSERT INTO `sys_logininfor` VALUES ('2294', 'admin', '/127.0.0.1:12589', '', '', '', '0', '登录成功！', '2022-07-12 13:47:12');
INSERT INTO `sys_logininfor` VALUES ('2295', 'admin', '/127.0.0.1:12589', '', '', '', '1', '连接异常', '2022-07-12 13:47:12');
INSERT INTO `sys_logininfor` VALUES ('2296', 'admin', '/127.0.0.1:12589', '', '', '', '1', '退出登录', '2022-07-12 13:47:12');
INSERT INTO `sys_logininfor` VALUES ('2297', 'admin', '/127.0.0.1:12589', '', '', '', '1', '连接异常', '2022-07-12 13:47:12');
INSERT INTO `sys_logininfor` VALUES ('2298', 'admin', '/127.0.0.1:12618', '', '', '', '0', '登录成功！', '2022-07-12 13:48:57');
INSERT INTO `sys_logininfor` VALUES ('2299', 'admin', '/127.0.0.1:12618', '', '', '', '1', '连接异常', '2022-07-12 13:48:57');
INSERT INTO `sys_logininfor` VALUES ('2300', 'admin', '/127.0.0.1:12618', '', '', '', '1', '退出登录', '2022-07-12 13:48:57');
INSERT INTO `sys_logininfor` VALUES ('2301', 'admin', '/127.0.0.1:12618', '', '', '', '1', '连接异常', '2022-07-12 13:48:57');
INSERT INTO `sys_logininfor` VALUES ('2302', 'admin', '/127.0.0.1:13319', '', '', '', '0', '登录成功！', '2022-07-12 13:59:04');
INSERT INTO `sys_logininfor` VALUES ('2303', 'admin', '/127.0.0.1:13319', '', '', '', '1', '退出登录', '2022-07-12 13:59:04');
INSERT INTO `sys_logininfor` VALUES ('2304', 'admin', '/127.0.0.1:13378', '', '', '', '0', '登录成功！', '2022-07-12 14:01:01');
INSERT INTO `sys_logininfor` VALUES ('2305', 'admin', '/127.0.0.1:13378', '', '', '', '1', '连接异常', '2022-07-12 14:01:01');
INSERT INTO `sys_logininfor` VALUES ('2306', 'admin', '/127.0.0.1:13378', '', '', '', '1', '退出登录', '2022-07-12 14:01:01');
INSERT INTO `sys_logininfor` VALUES ('2307', 'admin', '/127.0.0.1:13378', '', '', '', '1', '连接异常', '2022-07-12 14:01:01');
INSERT INTO `sys_logininfor` VALUES ('2308', 'admin', '/127.0.0.1:13447', '', '', '', '0', '登录成功！', '2022-07-12 14:04:25');
INSERT INTO `sys_logininfor` VALUES ('2309', 'admin', '/127.0.0.1:13447', '', '', '', '1', '连接异常', '2022-07-12 14:04:25');
INSERT INTO `sys_logininfor` VALUES ('2310', 'admin', '/127.0.0.1:13447', '', '', '', '1', '退出登录', '2022-07-12 14:04:25');
INSERT INTO `sys_logininfor` VALUES ('2311', 'admin', '/127.0.0.1:13447', '', '', '', '1', '连接异常', '2022-07-12 14:04:25');
INSERT INTO `sys_logininfor` VALUES ('2312', 'admin', '/127.0.0.1:13462', '', '', '', '0', '登录成功！', '2022-07-12 14:04:58');
INSERT INTO `sys_logininfor` VALUES ('2313', 'admin', '/127.0.0.1:13462', '', '', '', '1', '连接异常', '2022-07-12 14:04:58');
INSERT INTO `sys_logininfor` VALUES ('2314', 'admin', '/127.0.0.1:13462', '', '', '', '1', '退出登录', '2022-07-12 14:04:58');
INSERT INTO `sys_logininfor` VALUES ('2315', 'admin', '/127.0.0.1:13462', '', '', '', '1', '连接异常', '2022-07-12 14:04:58');
INSERT INTO `sys_logininfor` VALUES ('2316', 'admin', '/127.0.0.1:13625', '', '', '', '0', '登录成功！', '2022-07-12 14:12:16');
INSERT INTO `sys_logininfor` VALUES ('2317', 'admin', '/127.0.0.1:13625', '', '', '', '1', '退出登录', '2022-07-12 14:12:16');
INSERT INTO `sys_logininfor` VALUES ('2318', 'admin', '/127.0.0.1:13644', '', '', '', '0', '登录成功！', '2022-07-12 14:13:06');
INSERT INTO `sys_logininfor` VALUES ('2319', 'admin', '/127.0.0.1:13669', '', '', '', '0', '登录成功！', '2022-07-12 14:13:36');
INSERT INTO `sys_logininfor` VALUES ('2320', 'admin', '/127.0.0.1:13669', '', '', '', '1', '退出登录', '2022-07-12 14:13:36');
INSERT INTO `sys_logininfor` VALUES ('2321', 'admin', '/127.0.0.1:13721', '', '', '', '0', '登录成功！', '2022-07-12 14:15:08');
INSERT INTO `sys_logininfor` VALUES ('2322', 'admin', '/127.0.0.1:13721', '', '', '', '1', '退出登录', '2022-07-12 14:15:08');
INSERT INTO `sys_logininfor` VALUES ('2323', 'admin', '/127.0.0.1:13743', '', '', '', '0', '登录成功！', '2022-07-12 14:16:08');
INSERT INTO `sys_logininfor` VALUES ('2324', 'admin', '/127.0.0.1:13743', '', '', '', '1', '退出登录', '2022-07-12 14:16:08');
INSERT INTO `sys_logininfor` VALUES ('2325', 'admin', '/127.0.0.1:13832', '', '', '', '0', '登录成功！', '2022-07-12 14:19:03');
INSERT INTO `sys_logininfor` VALUES ('2326', 'admin', '/127.0.0.1:13832', '', '', '', '1', '连接异常', '2022-07-12 14:19:03');
INSERT INTO `sys_logininfor` VALUES ('2327', 'admin', '/127.0.0.1:13832', '', '', '', '1', '退出登录', '2022-07-12 14:19:03');
INSERT INTO `sys_logininfor` VALUES ('2328', 'admin', '/127.0.0.1:13832', '', '', '', '1', '连接异常', '2022-07-12 14:19:03');
INSERT INTO `sys_logininfor` VALUES ('2329', 'admin', '/127.0.0.1:13857', '', '', '', '0', '登录成功！', '2022-07-12 14:19:54');
INSERT INTO `sys_logininfor` VALUES ('2330', 'admin', '/127.0.0.1:13857', '', '', '', '1', '连接异常', '2022-07-12 14:19:54');
INSERT INTO `sys_logininfor` VALUES ('2331', 'admin', '/127.0.0.1:13857', '', '', '', '1', '退出登录', '2022-07-12 14:19:54');
INSERT INTO `sys_logininfor` VALUES ('2332', 'admin', '/127.0.0.1:13857', '', '', '', '1', '连接异常', '2022-07-12 14:19:54');
INSERT INTO `sys_logininfor` VALUES ('2333', 'admin', '/127.0.0.1:13890', '', '', '', '0', '登录成功！', '2022-07-12 14:20:49');
INSERT INTO `sys_logininfor` VALUES ('2334', 'admin', '/127.0.0.1:13890', '', '', '', '1', '退出登录', '2022-07-12 14:20:49');
INSERT INTO `sys_logininfor` VALUES ('2335', 'admin', '/127.0.0.1:13907', '', '', '', '0', '登录成功！', '2022-07-12 14:21:28');
INSERT INTO `sys_logininfor` VALUES ('2336', 'admin', '/127.0.0.1:13907', '', '', '', '1', '连接异常', '2022-07-12 14:21:28');
INSERT INTO `sys_logininfor` VALUES ('2337', 'admin', '/127.0.0.1:13907', '', '', '', '1', '退出登录', '2022-07-12 14:21:28');
INSERT INTO `sys_logininfor` VALUES ('2338', 'admin', '/127.0.0.1:13907', '', '', '', '1', '连接异常', '2022-07-12 14:21:28');
INSERT INTO `sys_logininfor` VALUES ('2339', 'admin', '/127.0.0.1:13917', '', '', '', '0', '登录成功！', '2022-07-12 14:21:39');
INSERT INTO `sys_logininfor` VALUES ('2340', 'admin', '/127.0.0.1:13917', '', '', '', '1', '连接异常', '2022-07-12 14:21:39');
INSERT INTO `sys_logininfor` VALUES ('2341', 'admin', '/127.0.0.1:13917', '', '', '', '1', '退出登录', '2022-07-12 14:21:39');
INSERT INTO `sys_logininfor` VALUES ('2342', 'admin', '/127.0.0.1:13917', '', '', '', '1', '连接异常', '2022-07-12 14:21:39');
INSERT INTO `sys_logininfor` VALUES ('2343', 'admin', '/127.0.0.1:14093', '', '', '', '0', '登录成功！', '2022-07-12 14:24:54');
INSERT INTO `sys_logininfor` VALUES ('2344', 'admin', '/127.0.0.1:14093', '', '', '', '1', '连接异常', '2022-07-12 14:24:54');
INSERT INTO `sys_logininfor` VALUES ('2345', 'admin', '/127.0.0.1:14093', '', '', '', '1', '退出登录', '2022-07-12 14:24:54');
INSERT INTO `sys_logininfor` VALUES ('2346', 'admin', '/127.0.0.1:14093', '', '', '', '1', '连接异常', '2022-07-12 14:24:54');
INSERT INTO `sys_logininfor` VALUES ('2347', 'admin', '/127.0.0.1:14112', '', '', '', '0', '登录成功！', '2022-07-12 14:25:19');
INSERT INTO `sys_logininfor` VALUES ('2348', 'admin', '/127.0.0.1:14112', '', '', '', '1', '连接异常', '2022-07-12 14:25:19');
INSERT INTO `sys_logininfor` VALUES ('2349', 'admin', '/127.0.0.1:14112', '', '', '', '1', '退出登录', '2022-07-12 14:25:19');
INSERT INTO `sys_logininfor` VALUES ('2350', 'admin', '/127.0.0.1:14112', '', '', '', '1', '连接异常', '2022-07-12 14:25:19');
INSERT INTO `sys_logininfor` VALUES ('2351', 'admin', '/127.0.0.1:14126', '', '', '', '0', '登录成功！', '2022-07-12 14:25:42');
INSERT INTO `sys_logininfor` VALUES ('2352', 'admin', '/127.0.0.1:14126', '', '', '', '1', '连接异常', '2022-07-12 14:25:42');
INSERT INTO `sys_logininfor` VALUES ('2353', 'admin', '/127.0.0.1:14126', '', '', '', '1', '退出登录', '2022-07-12 14:25:42');
INSERT INTO `sys_logininfor` VALUES ('2354', 'admin', '/127.0.0.1:14126', '', '', '', '1', '连接异常', '2022-07-12 14:25:42');
INSERT INTO `sys_logininfor` VALUES ('2355', 'admin', '/127.0.0.1:14144', '', '', '', '0', '登录成功！', '2022-07-12 14:26:12');
INSERT INTO `sys_logininfor` VALUES ('2356', 'admin', '/127.0.0.1:14144', '', '', '', '1', '连接异常', '2022-07-12 14:26:12');
INSERT INTO `sys_logininfor` VALUES ('2357', 'admin', '/127.0.0.1:14144', '', '', '', '1', '退出登录', '2022-07-12 14:26:12');
INSERT INTO `sys_logininfor` VALUES ('2358', 'admin', '/127.0.0.1:14144', '', '', '', '1', '连接异常', '2022-07-12 14:26:12');
INSERT INTO `sys_logininfor` VALUES ('2359', 'admin', '/127.0.0.1:14178', '', '', '', '0', '登录成功！', '2022-07-12 14:27:20');
INSERT INTO `sys_logininfor` VALUES ('2360', 'admin', '/127.0.0.1:14178', '', '', '', '1', '连接异常', '2022-07-12 14:27:20');
INSERT INTO `sys_logininfor` VALUES ('2361', 'admin', '/127.0.0.1:14178', '', '', '', '1', '退出登录', '2022-07-12 14:27:20');
INSERT INTO `sys_logininfor` VALUES ('2362', 'admin', '/127.0.0.1:14178', '', '', '', '1', '连接异常', '2022-07-12 14:27:20');
INSERT INTO `sys_logininfor` VALUES ('2363', 'admin', '/127.0.0.1:14217', '', '', '', '0', '登录成功！', '2022-07-12 14:28:52');
INSERT INTO `sys_logininfor` VALUES ('2364', 'admin', '/127.0.0.1:14217', '', '', '', '1', '连接异常', '2022-07-12 14:28:52');
INSERT INTO `sys_logininfor` VALUES ('2365', 'admin', '/127.0.0.1:14217', '', '', '', '1', '退出登录', '2022-07-12 14:28:52');
INSERT INTO `sys_logininfor` VALUES ('2366', 'admin', '/127.0.0.1:14217', '', '', '', '1', '连接异常', '2022-07-12 14:28:52');
INSERT INTO `sys_logininfor` VALUES ('2367', 'admin', '/127.0.0.1:2997', '', '', '', '0', '登录成功！', '2022-07-12 15:35:31');
INSERT INTO `sys_logininfor` VALUES ('2368', 'admin', '/127.0.0.1:2997', '', '', '', '1', '连接异常', '2022-07-12 15:35:31');
INSERT INTO `sys_logininfor` VALUES ('2369', 'admin', '/127.0.0.1:2997', '', '', '', '1', '退出登录', '2022-07-12 15:35:31');
INSERT INTO `sys_logininfor` VALUES ('2370', 'admin', '/127.0.0.1:2997', '', '', '', '1', '连接异常', '2022-07-12 15:35:31');
INSERT INTO `sys_logininfor` VALUES ('2371', 'admin', '/127.0.0.1:3157', '', '', '', '0', '登录成功！', '2022-07-12 15:45:16');
INSERT INTO `sys_logininfor` VALUES ('2372', 'admin', '/127.0.0.1:3157', '', '', '', '1', '退出登录', '2022-07-12 15:45:16');
INSERT INTO `sys_logininfor` VALUES ('2373', 'admin', '/127.0.0.1:3168', '', '', '', '0', '登录成功！', '2022-07-12 15:46:13');
INSERT INTO `sys_logininfor` VALUES ('2374', 'admin', '/127.0.0.1:3168', '', '', '', '1', '退出登录', '2022-07-12 15:46:13');
INSERT INTO `sys_logininfor` VALUES ('2375', 'admin', '/127.0.0.1:3180', '', '', '', '0', '登录成功！', '2022-07-12 15:46:31');
INSERT INTO `sys_logininfor` VALUES ('2376', 'admin', '/127.0.0.1:3180', '', '', '', '1', '退出登录', '2022-07-12 15:46:31');
INSERT INTO `sys_logininfor` VALUES ('2377', 'admin', '/127.0.0.1:3194', '', '', '', '0', '登录成功！', '2022-07-12 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('2378', 'admin', '/127.0.0.1:3194', '', '', '', '1', '连接异常', '2022-07-12 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('2379', 'admin', '/127.0.0.1:3194', '', '', '', '1', '退出登录', '2022-07-12 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('2380', 'admin', '/127.0.0.1:3194', '', '', '', '1', '连接异常', '2022-07-12 15:47:00');
INSERT INTO `sys_logininfor` VALUES ('2381', 'admin', '/127.0.0.1:3297', '', '', '', '0', '登录成功！', '2022-07-12 15:51:44');
INSERT INTO `sys_logininfor` VALUES ('2382', 'admin', '/127.0.0.1:3297', '', '', '', '1', '连接异常', '2022-07-12 15:51:44');
INSERT INTO `sys_logininfor` VALUES ('2383', 'admin', '/127.0.0.1:3297', '', '', '', '1', '退出登录', '2022-07-12 15:51:44');
INSERT INTO `sys_logininfor` VALUES ('2384', 'admin', '/127.0.0.1:3297', '', '', '', '1', '连接异常', '2022-07-12 15:51:44');
INSERT INTO `sys_logininfor` VALUES ('2385', 'admin', '/127.0.0.1:3380', '', '', '', '0', '登录成功！', '2022-07-12 15:53:18');
INSERT INTO `sys_logininfor` VALUES ('2386', 'admin', '/127.0.0.1:3380', '', '', '', '1', '连接异常', '2022-07-12 15:53:18');
INSERT INTO `sys_logininfor` VALUES ('2387', 'admin', '/127.0.0.1:3380', '', '', '', '1', '退出登录', '2022-07-12 15:53:18');
INSERT INTO `sys_logininfor` VALUES ('2388', 'admin', '/127.0.0.1:3380', '', '', '', '1', '连接异常', '2022-07-12 15:53:18');
INSERT INTO `sys_logininfor` VALUES ('2389', 'admin', '/127.0.0.1:3415', '', '', '', '0', '登录成功！', '2022-07-12 15:55:46');
INSERT INTO `sys_logininfor` VALUES ('2390', 'admin', '/127.0.0.1:3415', '', '', '', '1', '退出登录', '2022-07-12 15:55:46');
INSERT INTO `sys_logininfor` VALUES ('2391', 'admin', '/127.0.0.1:3515', '', '', '', '0', '登录成功！', '2022-07-12 15:57:49');
INSERT INTO `sys_logininfor` VALUES ('2392', 'admin', '/127.0.0.1:3515', '', '', '', '1', '连接异常', '2022-07-12 15:57:49');
INSERT INTO `sys_logininfor` VALUES ('2393', 'admin', '/127.0.0.1:3515', '', '', '', '1', '退出登录', '2022-07-12 15:57:49');
INSERT INTO `sys_logininfor` VALUES ('2394', 'admin', '/127.0.0.1:3515', '', '', '', '1', '连接异常', '2022-07-12 15:57:49');
INSERT INTO `sys_logininfor` VALUES ('2395', 'admin', '/127.0.0.1:7508', '', '', '', '0', '登录成功！', '2022-07-12 17:28:17');
INSERT INTO `sys_logininfor` VALUES ('2396', 'admin', '/127.0.0.1:7508', '', '', '', '1', '退出登录', '2022-07-12 17:28:17');
INSERT INTO `sys_logininfor` VALUES ('2397', 'admin', '/127.0.0.1:10878', '', '', '', '0', '登录成功！', '2022-07-13 09:35:25');
INSERT INTO `sys_logininfor` VALUES ('2398', 'admin', '/127.0.0.1:10878', '', '', '', '1', '连接异常', '2022-07-13 09:35:25');
INSERT INTO `sys_logininfor` VALUES ('2399', 'admin', '/127.0.0.1:10878', '', '', '', '1', '退出登录', '2022-07-13 09:35:25');
INSERT INTO `sys_logininfor` VALUES ('2400', 'admin', '/127.0.0.1:10878', '', '', '', '1', '连接异常', '2022-07-13 09:35:25');
INSERT INTO `sys_logininfor` VALUES ('2401', 'admin', '/127.0.0.1:11806', '', '', '', '0', '登录成功！', '2022-07-13 09:45:38');
INSERT INTO `sys_logininfor` VALUES ('2402', 'admin', '/127.0.0.1:11806', '', '', '', '1', '连接异常', '2022-07-13 09:45:38');
INSERT INTO `sys_logininfor` VALUES ('2403', 'admin', '/127.0.0.1:11806', '', '', '', '1', '退出登录', '2022-07-13 09:45:38');
INSERT INTO `sys_logininfor` VALUES ('2404', 'admin', '/127.0.0.1:11806', '', '', '', '1', '连接异常', '2022-07-13 09:45:38');
INSERT INTO `sys_logininfor` VALUES ('2405', 'admin', '/127.0.0.1:11823', '', '', '', '0', '登录成功！', '2022-07-13 09:46:11');
INSERT INTO `sys_logininfor` VALUES ('2406', 'admin', '/127.0.0.1:11823', '', '', '', '1', '连接异常', '2022-07-13 09:46:11');
INSERT INTO `sys_logininfor` VALUES ('2407', 'admin', '/127.0.0.1:11823', '', '', '', '1', '退出登录', '2022-07-13 09:46:11');
INSERT INTO `sys_logininfor` VALUES ('2408', 'admin', '/127.0.0.1:11823', '', '', '', '1', '连接异常', '2022-07-13 09:46:11');
INSERT INTO `sys_logininfor` VALUES ('2409', 'admin', '/127.0.0.1:11846', '', '', '', '0', '登录成功！', '2022-07-13 09:46:43');
INSERT INTO `sys_logininfor` VALUES ('2410', 'admin', '/127.0.0.1:11846', '', '', '', '1', '连接异常', '2022-07-13 09:46:43');
INSERT INTO `sys_logininfor` VALUES ('2411', 'admin', '/127.0.0.1:11846', '', '', '', '1', '退出登录', '2022-07-13 09:46:43');
INSERT INTO `sys_logininfor` VALUES ('2412', 'admin', '/127.0.0.1:11846', '', '', '', '1', '连接异常', '2022-07-13 09:46:43');
INSERT INTO `sys_logininfor` VALUES ('2413', 'admin', '/127.0.0.1:8095', '', '', '', '0', '登录成功！', '2022-07-13 10:19:23');
INSERT INTO `sys_logininfor` VALUES ('2414', 'admin', '/127.0.0.1:8095', '', '', '', '1', '连接异常', '2022-07-13 10:19:23');
INSERT INTO `sys_logininfor` VALUES ('2415', 'admin', '/127.0.0.1:8095', '', '', '', '1', '退出登录', '2022-07-13 10:19:23');
INSERT INTO `sys_logininfor` VALUES ('2416', 'admin', '/127.0.0.1:8095', '', '', '', '1', '连接异常', '2022-07-13 10:19:23');
INSERT INTO `sys_logininfor` VALUES ('2417', 'admin', '/127.0.0.1:7412', '', '', '', '0', '登录成功！', '2022-07-13 10:20:24');
INSERT INTO `sys_logininfor` VALUES ('2418', 'admin', '/127.0.0.1:7412', '', '', '', '1', '连接异常', '2022-07-13 10:20:24');
INSERT INTO `sys_logininfor` VALUES ('2419', 'admin', '/127.0.0.1:7412', '', '', '', '1', '退出登录', '2022-07-13 10:20:24');
INSERT INTO `sys_logininfor` VALUES ('2420', 'admin', '/127.0.0.1:7412', '', '', '', '1', '连接异常', '2022-07-13 10:20:24');
INSERT INTO `sys_logininfor` VALUES ('2421', 'admin', '/127.0.0.1:7450', '', '', '', '0', '登录成功！', '2022-07-13 10:21:47');
INSERT INTO `sys_logininfor` VALUES ('2422', 'admin', '/127.0.0.1:7450', '', '', '', '1', '连接异常', '2022-07-13 10:21:47');
INSERT INTO `sys_logininfor` VALUES ('2423', 'admin', '/127.0.0.1:7450', '', '', '', '1', '退出登录', '2022-07-13 10:21:47');
INSERT INTO `sys_logininfor` VALUES ('2424', 'admin', '/127.0.0.1:7450', '', '', '', '1', '连接异常', '2022-07-13 10:21:47');
INSERT INTO `sys_logininfor` VALUES ('2425', 'admin', '/127.0.0.1:7475', '', '', '', '0', '登录成功！', '2022-07-13 10:22:54');
INSERT INTO `sys_logininfor` VALUES ('2426', 'admin', '/127.0.0.1:7475', '', '', '', '1', '连接异常', '2022-07-13 10:22:54');
INSERT INTO `sys_logininfor` VALUES ('2427', 'admin', '/127.0.0.1:7475', '', '', '', '1', '退出登录', '2022-07-13 10:22:54');
INSERT INTO `sys_logininfor` VALUES ('2428', 'admin', '/127.0.0.1:7475', '', '', '', '1', '连接异常', '2022-07-13 10:22:54');
INSERT INTO `sys_logininfor` VALUES ('2429', 'admin', '/127.0.0.1:2984', '', '', '', '0', '登录成功！', '2022-07-13 10:27:05');
INSERT INTO `sys_logininfor` VALUES ('2430', 'admin', '/127.0.0.1:2984', '', '', '', '1', '退出登录', '2022-07-13 10:27:05');
INSERT INTO `sys_logininfor` VALUES ('2431', 'admin', '/127.0.0.1:4962', '', '', '', '0', '登录成功！', '2022-07-13 14:02:12');
INSERT INTO `sys_logininfor` VALUES ('2432', 'admin', '/127.0.0.1:4962', '', '', '', '1', '退出登录', '2022-07-13 14:02:12');
INSERT INTO `sys_logininfor` VALUES ('2433', 'admin', '/127.0.0.1:3014', '', '', '', '0', '登录成功！', '2022-07-13 17:25:28');
INSERT INTO `sys_logininfor` VALUES ('2434', 'admin', '/127.0.0.1:3014', '', '', '', '1', '退出登录', '2022-07-13 17:25:28');
INSERT INTO `sys_logininfor` VALUES ('2435', 'admin', '/127.0.0.1:14878', '', '', '', '0', '登录成功！', '2022-07-14 09:46:34');
INSERT INTO `sys_logininfor` VALUES ('2436', 'admin', '/127.0.0.1:14878', '', '', '', '1', '连接异常', '2022-07-14 09:46:34');
INSERT INTO `sys_logininfor` VALUES ('2437', 'admin', '/127.0.0.1:14878', '', '', '', '1', '退出登录', '2022-07-14 09:46:34');
INSERT INTO `sys_logininfor` VALUES ('2438', 'admin', '/127.0.0.1:14878', '', '', '', '1', '连接异常', '2022-07-14 09:46:34');
INSERT INTO `sys_logininfor` VALUES ('2439', 'admin', '/127.0.0.1:1362', '', '', '', '0', '登录成功！', '2022-07-14 09:53:30');
INSERT INTO `sys_logininfor` VALUES ('2440', 'admin', '/127.0.0.1:1362', '', '', '', '1', '连接异常', '2022-07-14 09:53:30');
INSERT INTO `sys_logininfor` VALUES ('2441', 'admin', '/127.0.0.1:1362', '', '', '', '1', '退出登录', '2022-07-14 09:53:30');
INSERT INTO `sys_logininfor` VALUES ('2442', 'admin', '/127.0.0.1:1362', '', '', '', '1', '连接异常', '2022-07-14 09:53:30');
INSERT INTO `sys_logininfor` VALUES ('2443', 'admin', '/127.0.0.1:1551', '', '', '', '0', '登录成功！', '2022-07-14 09:56:16');
INSERT INTO `sys_logininfor` VALUES ('2444', 'admin', '/127.0.0.1:1551', '', '', '', '1', '连接异常', '2022-07-14 09:56:16');
INSERT INTO `sys_logininfor` VALUES ('2445', 'admin', '/127.0.0.1:1551', '', '', '', '1', '退出登录', '2022-07-14 09:56:16');
INSERT INTO `sys_logininfor` VALUES ('2446', 'admin', '/127.0.0.1:1551', '', '', '', '1', '连接异常', '2022-07-14 09:56:16');
INSERT INTO `sys_logininfor` VALUES ('2447', 'admin', '/127.0.0.1:8378', '', '', '', '0', '登录成功！', '2022-07-14 11:33:42');
INSERT INTO `sys_logininfor` VALUES ('2448', 'admin', '/127.0.0.1:8378', '', '', '', '1', '连接异常', '2022-07-14 11:33:42');
INSERT INTO `sys_logininfor` VALUES ('2449', 'admin', '/127.0.0.1:8378', '', '', '', '1', '退出登录', '2022-07-14 11:33:42');
INSERT INTO `sys_logininfor` VALUES ('2450', 'admin', '/127.0.0.1:8378', '', '', '', '1', '连接异常', '2022-07-14 11:33:42');
INSERT INTO `sys_logininfor` VALUES ('2451', 'admin', '/127.0.0.1:8488', '', '', '', '0', '登录成功！', '2022-07-14 11:35:09');
INSERT INTO `sys_logininfor` VALUES ('2452', 'admin', '/127.0.0.1:8488', '', '', '', '1', '连接异常', '2022-07-14 11:35:09');
INSERT INTO `sys_logininfor` VALUES ('2453', 'admin', '/127.0.0.1:8488', '', '', '', '1', '退出登录', '2022-07-14 11:35:09');
INSERT INTO `sys_logininfor` VALUES ('2454', 'admin', '/127.0.0.1:8488', '', '', '', '1', '连接异常', '2022-07-14 11:35:09');
INSERT INTO `sys_logininfor` VALUES ('2455', 'admin', '/127.0.0.1:12893', '', '', '', '0', '登录成功！', '2022-07-14 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('2456', 'admin', '/127.0.0.1:12893', '', '', '', '1', '连接异常', '2022-07-14 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('2457', 'admin', '/127.0.0.1:12893', '', '', '', '1', '退出登录', '2022-07-14 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('2458', 'admin', '/127.0.0.1:12893', '', '', '', '1', '连接异常', '2022-07-14 15:00:10');
INSERT INTO `sys_logininfor` VALUES ('2459', 'admin', '/127.0.0.1:13268', '', '', '', '0', '登录成功！', '2022-07-14 15:07:06');
INSERT INTO `sys_logininfor` VALUES ('2460', 'admin', '/127.0.0.1:13268', '', '', '', '1', '连接异常', '2022-07-14 15:07:06');
INSERT INTO `sys_logininfor` VALUES ('2461', 'admin', '/127.0.0.1:13268', '', '', '', '1', '退出登录', '2022-07-14 15:07:06');
INSERT INTO `sys_logininfor` VALUES ('2462', 'admin', '/127.0.0.1:13268', '', '', '', '1', '连接异常', '2022-07-14 15:07:06');
INSERT INTO `sys_logininfor` VALUES ('2463', 'admin', '/127.0.0.1:13572', '', '', '', '0', '登录成功！', '2022-07-14 15:10:03');
INSERT INTO `sys_logininfor` VALUES ('2464', 'admin', '/127.0.0.1:13572', '', '', '', '1', '退出登录', '2022-07-14 15:10:03');
INSERT INTO `sys_logininfor` VALUES ('2465', 'admin', '/127.0.0.1:1082', '', '', '', '0', '登录成功！', '2022-07-14 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('2466', 'admin', '/127.0.0.1:1082', '', '', '', '1', '连接异常', '2022-07-14 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('2467', 'admin', '/127.0.0.1:1082', '', '', '', '1', '退出登录', '2022-07-14 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('2468', 'admin', '/127.0.0.1:1082', '', '', '', '1', '连接异常', '2022-07-14 15:31:05');
INSERT INTO `sys_logininfor` VALUES ('2469', 'admin', '/127.0.0.1:1226', '', '', '', '0', '登录成功！', '2022-07-14 15:33:03');
INSERT INTO `sys_logininfor` VALUES ('2470', 'admin', '/127.0.0.1:1226', '', '', '', '1', '连接异常', '2022-07-14 15:33:03');
INSERT INTO `sys_logininfor` VALUES ('2471', 'admin', '/127.0.0.1:1226', '', '', '', '1', '退出登录', '2022-07-14 15:33:03');
INSERT INTO `sys_logininfor` VALUES ('2472', 'admin', '/127.0.0.1:1226', '', '', '', '1', '连接异常', '2022-07-14 15:33:03');
INSERT INTO `sys_logininfor` VALUES ('2473', 'admin', '/127.0.0.1:13423', '', '', '', '0', '登录成功！', '2022-07-18 10:36:34');
INSERT INTO `sys_logininfor` VALUES ('2474', 'admin', '/127.0.0.1:13545', '', '', '', '0', '登录成功！', '2022-07-18 10:40:25');
INSERT INTO `sys_logininfor` VALUES ('2475', 'admin', '/127.0.0.1:14567', '', '', '', '0', '登录成功！', '2022-07-18 11:06:25');
INSERT INTO `sys_logininfor` VALUES ('2476', 'admin', '/127.0.0.1:1180', '', '', '', '0', '登录成功！', '2022-07-18 11:25:14');
INSERT INTO `sys_logininfor` VALUES ('2477', 'admin', '/127.0.0.1:1180', '', '', '', '1', '退出登录', '2022-07-18 11:25:14');
INSERT INTO `sys_logininfor` VALUES ('2478', 'admin', '/127.0.0.1:3308', '', '', '', '0', '登录成功！', '2022-07-18 13:28:05');
INSERT INTO `sys_logininfor` VALUES ('2479', 'admin', '/127.0.0.1:3308', '', '', '', '1', '退出登录', '2022-07-18 13:28:05');
INSERT INTO `sys_logininfor` VALUES ('2480', 'admin', '/127.0.0.1:8151', '', '', '', '0', '登录成功！', '2022-07-18 15:13:49');
INSERT INTO `sys_logininfor` VALUES ('2481', 'admin', '/127.0.0.1:8151', '', '', '', '1', '退出登录', '2022-07-18 15:13:49');
INSERT INTO `sys_logininfor` VALUES ('2482', 'admin', '/127.0.0.1:8646', '', '', '', '0', '登录成功！', '2022-07-18 15:25:51');
INSERT INTO `sys_logininfor` VALUES ('2483', 'admin', '/127.0.0.1:8646', '', '', '', '1', '退出登录', '2022-07-18 15:25:51');
INSERT INTO `sys_logininfor` VALUES ('2484', 'admin', '/127.0.0.1:8973', '', '', '', '0', '登录成功！', '2022-07-18 15:34:46');
INSERT INTO `sys_logininfor` VALUES ('2485', 'admin', '/127.0.0.1:8973', '', '', '', '1', '连接异常', '2022-07-18 15:34:46');
INSERT INTO `sys_logininfor` VALUES ('2486', 'admin', '/127.0.0.1:8973', '', '', '', '1', '退出登录', '2022-07-18 15:34:46');
INSERT INTO `sys_logininfor` VALUES ('2487', 'admin', '/127.0.0.1:8973', '', '', '', '1', '连接异常', '2022-07-18 15:34:46');
INSERT INTO `sys_logininfor` VALUES ('2488', 'admin', '/127.0.0.1:9011', '', '', '', '0', '登录成功！', '2022-07-18 15:35:46');
INSERT INTO `sys_logininfor` VALUES ('2489', 'admin', '/127.0.0.1:9011', '', '', '', '1', '退出登录', '2022-07-18 15:35:46');
INSERT INTO `sys_logininfor` VALUES ('2490', 'admin', '/127.0.0.1:14004', '', '', '', '0', '登录成功！', '2022-07-18 16:20:54');
INSERT INTO `sys_logininfor` VALUES ('2491', 'admin', '/127.0.0.1:14004', '', '', '', '1', '连接异常', '2022-07-18 16:20:54');
INSERT INTO `sys_logininfor` VALUES ('2492', 'admin', '/127.0.0.1:14004', '', '', '', '1', '退出登录', '2022-07-18 16:20:54');
INSERT INTO `sys_logininfor` VALUES ('2493', 'admin', '/127.0.0.1:14004', '', '', '', '1', '连接异常', '2022-07-18 16:20:54');
INSERT INTO `sys_logininfor` VALUES ('2494', 'admin', '/127.0.0.1:6536', '', '', '', '0', '登录成功！', '2022-07-19 13:44:29');
INSERT INTO `sys_logininfor` VALUES ('2495', 'admin', '/127.0.0.1:6536', '', '', '', '1', '连接异常', '2022-07-19 13:44:29');
INSERT INTO `sys_logininfor` VALUES ('2496', 'admin', '/127.0.0.1:6536', '', '', '', '1', '退出登录', '2022-07-19 13:44:29');
INSERT INTO `sys_logininfor` VALUES ('2497', 'admin', '/127.0.0.1:6536', '', '', '', '1', '连接异常', '2022-07-19 13:44:29');
INSERT INTO `sys_logininfor` VALUES ('2498', 'admin', '/127.0.0.1:7807', '', '', '', '0', '登录成功！', '2022-07-19 14:01:04');
INSERT INTO `sys_logininfor` VALUES ('2499', 'admin', '/127.0.0.1:7807', '', '', '', '1', '退出登录', '2022-07-19 14:01:04');
INSERT INTO `sys_logininfor` VALUES ('2500', 'admin', '/127.0.0.1:8492', '', '', '', '0', '登录成功！', '2022-07-19 14:10:05');
INSERT INTO `sys_logininfor` VALUES ('2501', 'admin', '/127.0.0.1:8492', '', '', '', '1', '退出登录', '2022-07-19 14:10:05');
INSERT INTO `sys_logininfor` VALUES ('2502', 'admin', '/127.0.0.1:8662', '', '', '', '0', '登录成功！', '2022-07-19 14:12:48');
INSERT INTO `sys_logininfor` VALUES ('2503', 'admin', '/127.0.0.1:11212', '', '', '', '0', '登录成功！', '2022-07-19 14:45:47');
INSERT INTO `sys_logininfor` VALUES ('2504', 'admin', '/127.0.0.1:11212', '', '', '', '1', '连接异常', '2022-07-19 14:45:47');
INSERT INTO `sys_logininfor` VALUES ('2505', 'admin', '/127.0.0.1:11212', '', '', '', '1', '退出登录', '2022-07-19 14:45:47');
INSERT INTO `sys_logininfor` VALUES ('2506', 'admin', '/127.0.0.1:11212', '', '', '', '1', '连接异常', '2022-07-19 14:45:47');
INSERT INTO `sys_logininfor` VALUES ('2507', 'admin', '/127.0.0.1:11727', '', '', '', '0', '登录成功！', '2022-07-19 15:01:32');
INSERT INTO `sys_logininfor` VALUES ('2508', 'admin', '/127.0.0.1:11727', '', '', '', '1', '连接异常', '2022-07-19 15:01:32');
INSERT INTO `sys_logininfor` VALUES ('2509', 'admin', '/127.0.0.1:11727', '', '', '', '1', '退出登录', '2022-07-19 15:01:32');
INSERT INTO `sys_logininfor` VALUES ('2510', 'admin', '/127.0.0.1:11727', '', '', '', '1', '连接异常', '2022-07-19 15:01:32');
INSERT INTO `sys_logininfor` VALUES ('2511', 'admin', '/127.0.0.1:12131', '', '', '', '0', '登录成功！', '2022-07-19 15:11:07');
INSERT INTO `sys_logininfor` VALUES ('2512', 'admin', '/127.0.0.1:12131', '', '', '', '1', '连接异常', '2022-07-19 15:11:07');
INSERT INTO `sys_logininfor` VALUES ('2513', 'admin', '/127.0.0.1:12131', '', '', '', '1', '退出登录', '2022-07-19 15:11:07');
INSERT INTO `sys_logininfor` VALUES ('2514', 'admin', '/127.0.0.1:12131', '', '', '', '1', '连接异常', '2022-07-19 15:11:07');
INSERT INTO `sys_logininfor` VALUES ('2515', 'admin', '/127.0.0.1:12228', '', '', '', '0', '登录成功！', '2022-07-19 15:14:01');
INSERT INTO `sys_logininfor` VALUES ('2516', 'admin', '/127.0.0.1:12228', '', '', '', '1', '连接异常', '2022-07-19 15:14:01');
INSERT INTO `sys_logininfor` VALUES ('2517', 'admin', '/127.0.0.1:12228', '', '', '', '1', '退出登录', '2022-07-19 15:14:01');
INSERT INTO `sys_logininfor` VALUES ('2518', 'admin', '/127.0.0.1:12228', '', '', '', '1', '连接异常', '2022-07-19 15:14:01');
INSERT INTO `sys_logininfor` VALUES ('2519', 'admin', '/127.0.0.1:12444', '', '', '', '0', '登录成功！', '2022-07-19 15:19:36');
INSERT INTO `sys_logininfor` VALUES ('2520', 'admin', '/127.0.0.1:12444', '', '', '', '1', '连接异常', '2022-07-19 15:19:36');
INSERT INTO `sys_logininfor` VALUES ('2521', 'admin', '/127.0.0.1:12444', '', '', '', '1', '退出登录', '2022-07-19 15:19:36');
INSERT INTO `sys_logininfor` VALUES ('2522', 'admin', '/127.0.0.1:12444', '', '', '', '1', '连接异常', '2022-07-19 15:19:36');
INSERT INTO `sys_logininfor` VALUES ('2523', 'admin', '/127.0.0.1:12761', '', '', '', '0', '登录成功！', '2022-07-19 15:25:04');
INSERT INTO `sys_logininfor` VALUES ('2524', 'admin', '/127.0.0.1:12761', '', '', '', '1', '连接异常', '2022-07-19 15:25:04');
INSERT INTO `sys_logininfor` VALUES ('2525', 'admin', '/127.0.0.1:12761', '', '', '', '1', '退出登录', '2022-07-19 15:25:04');
INSERT INTO `sys_logininfor` VALUES ('2526', 'admin', '/127.0.0.1:12761', '', '', '', '1', '连接异常', '2022-07-19 15:25:04');
INSERT INTO `sys_logininfor` VALUES ('2527', 'admin', '/127.0.0.1:12895', '', '', '', '0', '登录成功！', '2022-07-19 15:28:28');
INSERT INTO `sys_logininfor` VALUES ('2528', 'admin', '/127.0.0.1:12895', '', '', '', '1', '连接异常', '2022-07-19 15:28:28');
INSERT INTO `sys_logininfor` VALUES ('2529', 'admin', '/127.0.0.1:12895', '', '', '', '1', '退出登录', '2022-07-19 15:28:28');
INSERT INTO `sys_logininfor` VALUES ('2530', 'admin', '/127.0.0.1:12895', '', '', '', '1', '连接异常', '2022-07-19 15:28:28');
INSERT INTO `sys_logininfor` VALUES ('2531', 'admin', '/127.0.0.1:13009', '', '', '', '0', '登录成功！', '2022-07-19 15:32:21');
INSERT INTO `sys_logininfor` VALUES ('2532', 'admin', '/127.0.0.1:13009', '', '', '', '1', '连接异常', '2022-07-19 15:32:21');
INSERT INTO `sys_logininfor` VALUES ('2533', 'admin', '/127.0.0.1:13009', '', '', '', '1', '退出登录', '2022-07-19 15:32:21');
INSERT INTO `sys_logininfor` VALUES ('2534', 'admin', '/127.0.0.1:13009', '', '', '', '1', '连接异常', '2022-07-19 15:32:21');
INSERT INTO `sys_logininfor` VALUES ('2535', 'admin', '/127.0.0.1:13204', '', '', '', '0', '登录成功！', '2022-07-19 15:37:38');
INSERT INTO `sys_logininfor` VALUES ('2536', 'admin', '/127.0.0.1:13204', '', '', '', '1', '连接异常', '2022-07-19 15:37:38');
INSERT INTO `sys_logininfor` VALUES ('2537', 'admin', '/127.0.0.1:13204', '', '', '', '1', '退出登录', '2022-07-19 15:37:38');
INSERT INTO `sys_logininfor` VALUES ('2538', 'admin', '/127.0.0.1:13204', '', '', '', '1', '连接异常', '2022-07-19 15:37:38');
INSERT INTO `sys_logininfor` VALUES ('2539', 'admin', '/127.0.0.1:13363', '', '', '', '0', '登录成功！', '2022-07-19 15:42:34');
INSERT INTO `sys_logininfor` VALUES ('2540', 'admin', '/127.0.0.1:13363', '', '', '', '1', '连接异常', '2022-07-19 15:42:34');
INSERT INTO `sys_logininfor` VALUES ('2541', 'admin', '/127.0.0.1:13363', '', '', '', '1', '退出登录', '2022-07-19 15:42:34');
INSERT INTO `sys_logininfor` VALUES ('2542', 'admin', '/127.0.0.1:13363', '', '', '', '1', '连接异常', '2022-07-19 15:42:34');
INSERT INTO `sys_logininfor` VALUES ('2543', 'admin', '/127.0.0.1:13558', '', '', '', '0', '登录成功！', '2022-07-19 15:48:18');
INSERT INTO `sys_logininfor` VALUES ('2544', 'admin', '/127.0.0.1:13558', '', '', '', '1', '连接异常', '2022-07-19 15:48:18');
INSERT INTO `sys_logininfor` VALUES ('2545', 'admin', '/127.0.0.1:13558', '', '', '', '1', '退出登录', '2022-07-19 15:48:18');
INSERT INTO `sys_logininfor` VALUES ('2546', 'admin', '/127.0.0.1:13558', '', '', '', '1', '连接异常', '2022-07-19 15:48:18');
INSERT INTO `sys_logininfor` VALUES ('2547', 'admin', '/127.0.0.1:13819', '', '', '', '0', '登录成功！', '2022-07-19 15:56:32');
INSERT INTO `sys_logininfor` VALUES ('2548', 'admin', '/127.0.0.1:13819', '', '', '', '1', '连接异常', '2022-07-19 15:56:32');
INSERT INTO `sys_logininfor` VALUES ('2549', 'admin', '/127.0.0.1:13819', '', '', '', '1', '退出登录', '2022-07-19 15:56:32');
INSERT INTO `sys_logininfor` VALUES ('2550', 'admin', '/127.0.0.1:13819', '', '', '', '1', '连接异常', '2022-07-19 15:56:32');
INSERT INTO `sys_logininfor` VALUES ('2551', 'admin', '/127.0.0.1:14216', '', '', '', '0', '登录成功！', '2022-07-19 16:04:39');
INSERT INTO `sys_logininfor` VALUES ('2552', 'admin', '/127.0.0.1:14216', '', '', '', '1', '连接异常', '2022-07-19 16:04:39');
INSERT INTO `sys_logininfor` VALUES ('2553', 'admin', '/127.0.0.1:14216', '', '', '', '1', '退出登录', '2022-07-19 16:04:39');
INSERT INTO `sys_logininfor` VALUES ('2554', 'admin', '/127.0.0.1:14216', '', '', '', '1', '连接异常', '2022-07-19 16:04:39');
INSERT INTO `sys_logininfor` VALUES ('2555', 'admin', '/127.0.0.1:14373', '', '', '', '0', '登录成功！', '2022-07-19 16:09:05');
INSERT INTO `sys_logininfor` VALUES ('2556', 'admin', '/127.0.0.1:14373', '', '', '', '1', '连接异常', '2022-07-19 16:09:05');
INSERT INTO `sys_logininfor` VALUES ('2557', 'admin', '/127.0.0.1:14373', '', '', '', '1', '退出登录', '2022-07-19 16:09:05');
INSERT INTO `sys_logininfor` VALUES ('2558', 'admin', '/127.0.0.1:14373', '', '', '', '1', '连接异常', '2022-07-19 16:09:05');
INSERT INTO `sys_logininfor` VALUES ('2559', 'admin', '/127.0.0.1:14500', '', '', '', '0', '登录成功！', '2022-07-19 16:12:47');
INSERT INTO `sys_logininfor` VALUES ('2560', 'admin', '/127.0.0.1:14500', '', '', '', '1', '连接异常', '2022-07-19 16:12:47');
INSERT INTO `sys_logininfor` VALUES ('2561', 'admin', '/127.0.0.1:14500', '', '', '', '1', '退出登录', '2022-07-19 16:12:47');
INSERT INTO `sys_logininfor` VALUES ('2562', 'admin', '/127.0.0.1:14500', '', '', '', '1', '连接异常', '2022-07-19 16:12:47');
INSERT INTO `sys_logininfor` VALUES ('2563', 'admin', '/127.0.0.1:14574', '', '', '', '0', '登录成功！', '2022-07-19 16:15:05');
INSERT INTO `sys_logininfor` VALUES ('2564', 'admin', '/127.0.0.1:14574', '', '', '', '1', '连接异常', '2022-07-19 16:15:05');
INSERT INTO `sys_logininfor` VALUES ('2565', 'admin', '/127.0.0.1:14574', '', '', '', '1', '退出登录', '2022-07-19 16:15:05');
INSERT INTO `sys_logininfor` VALUES ('2566', 'admin', '/127.0.0.1:14574', '', '', '', '1', '连接异常', '2022-07-19 16:15:05');
INSERT INTO `sys_logininfor` VALUES ('2567', 'admin', '/127.0.0.1:14620', '', '', '', '0', '登录成功！', '2022-07-19 16:16:09');
INSERT INTO `sys_logininfor` VALUES ('2568', 'admin', '/127.0.0.1:14620', '', '', '', '1', '连接异常', '2022-07-19 16:16:09');
INSERT INTO `sys_logininfor` VALUES ('2569', 'admin', '/127.0.0.1:14620', '', '', '', '1', '退出登录', '2022-07-19 16:16:09');
INSERT INTO `sys_logininfor` VALUES ('2570', 'admin', '/127.0.0.1:14620', '', '', '', '1', '连接异常', '2022-07-19 16:16:09');
INSERT INTO `sys_logininfor` VALUES ('2571', 'admin', '/127.0.0.1:14659', '', '', '', '0', '登录成功！', '2022-07-19 16:17:01');
INSERT INTO `sys_logininfor` VALUES ('2572', 'admin', '/127.0.0.1:14659', '', '', '', '1', '退出登录', '2022-07-19 16:17:01');
INSERT INTO `sys_logininfor` VALUES ('2573', 'admin', '/127.0.0.1:14683', '', '', '', '0', '登录成功！', '2022-07-19 16:17:45');
INSERT INTO `sys_logininfor` VALUES ('2574', 'admin', '/127.0.0.1:14683', '', '', '', '1', '连接异常', '2022-07-19 16:17:45');
INSERT INTO `sys_logininfor` VALUES ('2575', 'admin', '/127.0.0.1:14683', '', '', '', '1', '退出登录', '2022-07-19 16:17:45');
INSERT INTO `sys_logininfor` VALUES ('2576', 'admin', '/127.0.0.1:14683', '', '', '', '1', '连接异常', '2022-07-19 16:17:45');
INSERT INTO `sys_logininfor` VALUES ('2577', 'admin', '/127.0.0.1:14858', '', '', '', '0', '登录成功！', '2022-07-19 16:21:23');
INSERT INTO `sys_logininfor` VALUES ('2578', 'admin', '/127.0.0.1:14858', '', '', '', '1', '连接异常', '2022-07-19 16:21:23');
INSERT INTO `sys_logininfor` VALUES ('2579', 'admin', '/127.0.0.1:14858', '', '', '', '1', '退出登录', '2022-07-19 16:21:23');
INSERT INTO `sys_logininfor` VALUES ('2580', 'admin', '/127.0.0.1:14858', '', '', '', '1', '连接异常', '2022-07-19 16:21:23');
INSERT INTO `sys_logininfor` VALUES ('2581', 'admin', '/127.0.0.1:14926', '', '', '', '0', '登录成功！', '2022-07-19 16:23:28');
INSERT INTO `sys_logininfor` VALUES ('2582', 'admin', '/127.0.0.1:14926', '', '', '', '1', '退出登录', '2022-07-19 16:23:28');
INSERT INTO `sys_logininfor` VALUES ('2583', 'admin', '/127.0.0.1:14963', '', '', '', '0', '登录成功！', '2022-07-19 16:24:24');
INSERT INTO `sys_logininfor` VALUES ('2584', 'admin', '/127.0.0.1:14963', '', '', '', '1', '连接异常', '2022-07-19 16:24:24');
INSERT INTO `sys_logininfor` VALUES ('2585', 'admin', '/127.0.0.1:14963', '', '', '', '1', '退出登录', '2022-07-19 16:24:24');
INSERT INTO `sys_logininfor` VALUES ('2586', 'admin', '/127.0.0.1:14963', '', '', '', '1', '连接异常', '2022-07-19 16:24:24');
INSERT INTO `sys_logininfor` VALUES ('2587', 'admin', '/127.0.0.1:1125', '', '', '', '0', '登录成功！', '2022-07-19 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2588', 'admin', '/127.0.0.1:1125', '', '', '', '1', '连接异常', '2022-07-19 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2589', 'admin', '/127.0.0.1:1125', '', '', '', '1', '退出登录', '2022-07-19 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2590', 'admin', '/127.0.0.1:1125', '', '', '', '1', '连接异常', '2022-07-19 16:27:35');
INSERT INTO `sys_logininfor` VALUES ('2591', 'admin', '/127.0.0.1:1589', '', '', '', '0', '登录成功！', '2022-07-19 16:40:24');
INSERT INTO `sys_logininfor` VALUES ('2592', 'admin', '/127.0.0.1:1589', '', '', '', '1', '连接异常', '2022-07-19 16:40:24');
INSERT INTO `sys_logininfor` VALUES ('2593', 'admin', '/127.0.0.1:1589', '', '', '', '1', '退出登录', '2022-07-19 16:40:24');
INSERT INTO `sys_logininfor` VALUES ('2594', 'admin', '/127.0.0.1:1589', '', '', '', '1', '连接异常', '2022-07-19 16:40:24');
INSERT INTO `sys_logininfor` VALUES ('2595', 'admin', '/127.0.0.1:1696', '', '', '', '0', '登录成功！', '2022-07-19 16:43:21');
INSERT INTO `sys_logininfor` VALUES ('2596', 'admin', '/127.0.0.1:1696', '', '', '', '1', '连接异常', '2022-07-19 16:43:21');
INSERT INTO `sys_logininfor` VALUES ('2597', 'admin', '/127.0.0.1:1696', '', '', '', '1', '退出登录', '2022-07-19 16:43:21');
INSERT INTO `sys_logininfor` VALUES ('2598', 'admin', '/127.0.0.1:1696', '', '', '', '1', '连接异常', '2022-07-19 16:43:21');
INSERT INTO `sys_logininfor` VALUES ('2599', 'admin', '/127.0.0.1:1727', '', '', '', '0', '登录成功！', '2022-07-19 16:44:09');
INSERT INTO `sys_logininfor` VALUES ('2600', 'admin', '/127.0.0.1:1727', '', '', '', '1', '连接异常', '2022-07-19 16:44:09');
INSERT INTO `sys_logininfor` VALUES ('2601', 'admin', '/127.0.0.1:1727', '', '', '', '1', '退出登录', '2022-07-19 16:44:09');
INSERT INTO `sys_logininfor` VALUES ('2602', 'admin', '/127.0.0.1:1727', '', '', '', '1', '连接异常', '2022-07-19 16:44:09');
INSERT INTO `sys_logininfor` VALUES ('2603', 'admin', '/127.0.0.1:1767', '', '', '', '0', '登录成功！', '2022-07-19 16:45:13');
INSERT INTO `sys_logininfor` VALUES ('2604', 'admin', '/127.0.0.1:1767', '', '', '', '1', '连接异常', '2022-07-19 16:45:13');
INSERT INTO `sys_logininfor` VALUES ('2605', 'admin', '/127.0.0.1:1767', '', '', '', '1', '退出登录', '2022-07-19 16:45:13');
INSERT INTO `sys_logininfor` VALUES ('2606', 'admin', '/127.0.0.1:1767', '', '', '', '1', '连接异常', '2022-07-19 16:45:13');
INSERT INTO `sys_logininfor` VALUES ('2607', 'admin', '/127.0.0.1:1851', '', '', '', '0', '登录成功！', '2022-07-19 16:47:36');
INSERT INTO `sys_logininfor` VALUES ('2608', 'admin', '/127.0.0.1:1851', '', '', '', '1', '退出登录', '2022-07-19 16:47:36');
INSERT INTO `sys_logininfor` VALUES ('2609', 'admin', '/127.0.0.1:6396', '', '', '', '0', '登录成功！', '2022-07-19 17:12:14');
INSERT INTO `sys_logininfor` VALUES ('2610', 'admin', '/127.0.0.1:6643', '', '', '', '0', '登录成功！', '2022-07-20 08:45:34');
INSERT INTO `sys_logininfor` VALUES ('2611', 'admin', '/127.0.0.1:6643', '', '', '', '1', '退出登录', '2022-07-20 08:45:34');
INSERT INTO `sys_logininfor` VALUES ('2612', 'admin', '/127.0.0.1:8065', '', '', '', '0', '登录成功！', '2022-07-20 09:19:29');
INSERT INTO `sys_logininfor` VALUES ('2613', 'admin', '/127.0.0.1:8065', '', '', '', '1', '连接异常', '2022-07-20 09:19:29');
INSERT INTO `sys_logininfor` VALUES ('2614', 'admin', '/127.0.0.1:8065', '', '', '', '1', '退出登录', '2022-07-20 09:19:29');
INSERT INTO `sys_logininfor` VALUES ('2615', 'admin', '/127.0.0.1:8065', '', '', '', '1', '连接异常', '2022-07-20 09:19:29');
INSERT INTO `sys_logininfor` VALUES ('2616', 'admin', '/127.0.0.1:8690', '', '', '', '0', '登录成功！', '2022-07-20 09:28:25');
INSERT INTO `sys_logininfor` VALUES ('2617', 'admin', '/127.0.0.1:8690', '', '', '', '1', '连接异常', '2022-07-20 09:28:25');
INSERT INTO `sys_logininfor` VALUES ('2618', 'admin', '/127.0.0.1:8690', '', '', '', '1', '退出登录', '2022-07-20 09:28:25');
INSERT INTO `sys_logininfor` VALUES ('2619', 'admin', '/127.0.0.1:8690', '', '', '', '1', '连接异常', '2022-07-20 09:28:25');
INSERT INTO `sys_logininfor` VALUES ('2620', 'admin', '/127.0.0.1:8860', '', '', '', '0', '登录成功！', '2022-07-20 09:32:22');
INSERT INTO `sys_logininfor` VALUES ('2621', 'admin', '/127.0.0.1:8860', '', '', '', '1', '连接异常', '2022-07-20 09:32:22');
INSERT INTO `sys_logininfor` VALUES ('2622', 'admin', '/127.0.0.1:8860', '', '', '', '1', '退出登录', '2022-07-20 09:32:22');
INSERT INTO `sys_logininfor` VALUES ('2623', 'admin', '/127.0.0.1:8860', '', '', '', '1', '连接异常', '2022-07-20 09:32:22');
INSERT INTO `sys_logininfor` VALUES ('2624', 'admin', '/127.0.0.1:8959', '', '', '', '0', '登录成功！', '2022-07-20 09:34:16');
INSERT INTO `sys_logininfor` VALUES ('2625', 'admin', '/127.0.0.1:8959', '', '', '', '1', '连接异常', '2022-07-20 09:34:16');
INSERT INTO `sys_logininfor` VALUES ('2626', 'admin', '/127.0.0.1:8959', '', '', '', '1', '退出登录', '2022-07-20 09:34:16');
INSERT INTO `sys_logininfor` VALUES ('2627', 'admin', '/127.0.0.1:8959', '', '', '', '1', '连接异常', '2022-07-20 09:34:16');
INSERT INTO `sys_logininfor` VALUES ('2628', 'admin', '/127.0.0.1:1532', '', '', '', '0', '登录成功！', '2022-07-20 09:37:49');
INSERT INTO `sys_logininfor` VALUES ('2629', 'admin', '/127.0.0.1:1532', '', '', '', '1', '连接异常', '2022-07-20 09:37:49');
INSERT INTO `sys_logininfor` VALUES ('2630', 'admin', '/127.0.0.1:1532', '', '', '', '1', '退出登录', '2022-07-20 09:37:49');
INSERT INTO `sys_logininfor` VALUES ('2631', 'admin', '/127.0.0.1:1532', '', '', '', '1', '连接异常', '2022-07-20 09:37:49');
INSERT INTO `sys_logininfor` VALUES ('2632', 'admin', '/127.0.0.1:8704', '', '', '', '0', '登录成功！', '2022-07-20 09:45:54');
INSERT INTO `sys_logininfor` VALUES ('2633', 'admin', '/127.0.0.1:7863', '', '', '', '0', '登录成功！', '2022-07-20 10:06:18');
INSERT INTO `sys_logininfor` VALUES ('2634', 'admin', '/127.0.0.1:7863', '', '', '', '1', '连接异常', '2022-07-20 10:06:18');
INSERT INTO `sys_logininfor` VALUES ('2635', 'admin', '/127.0.0.1:7863', '', '', '', '1', '退出登录', '2022-07-20 10:06:18');
INSERT INTO `sys_logininfor` VALUES ('2636', 'admin', '/127.0.0.1:7863', '', '', '', '1', '连接异常', '2022-07-20 10:06:18');
INSERT INTO `sys_logininfor` VALUES ('2637', 'admin', '/127.0.0.1:2652', '', '', '', '0', '登录成功！', '2022-07-20 10:10:33');
INSERT INTO `sys_logininfor` VALUES ('2638', 'admin', '/127.0.0.1:2652', '', '', '', '1', '连接异常', '2022-07-20 10:10:33');
INSERT INTO `sys_logininfor` VALUES ('2639', 'admin', '/127.0.0.1:2652', '', '', '', '1', '退出登录', '2022-07-20 10:10:33');
INSERT INTO `sys_logininfor` VALUES ('2640', 'admin', '/127.0.0.1:2652', '', '', '', '1', '连接异常', '2022-07-20 10:10:33');
INSERT INTO `sys_logininfor` VALUES ('2641', 'admin', '/127.0.0.1:12826', '', '', '', '0', '登录成功！', '2022-07-20 10:10:59');
INSERT INTO `sys_logininfor` VALUES ('2642', 'admin', '/127.0.0.1:12826', '', '', '', '1', '连接异常', '2022-07-20 10:10:59');
INSERT INTO `sys_logininfor` VALUES ('2643', 'admin', '/127.0.0.1:12826', '', '', '', '1', '退出登录', '2022-07-20 10:10:59');
INSERT INTO `sys_logininfor` VALUES ('2644', 'admin', '/127.0.0.1:12826', '', '', '', '1', '连接异常', '2022-07-20 10:10:59');
INSERT INTO `sys_logininfor` VALUES ('2645', 'admin', '/127.0.0.1:12924', '', '', '', '0', '登录成功！', '2022-07-20 10:13:08');
INSERT INTO `sys_logininfor` VALUES ('2646', 'admin', '/127.0.0.1:12924', '', '', '', '1', '连接异常', '2022-07-20 10:13:08');
INSERT INTO `sys_logininfor` VALUES ('2647', 'admin', '/127.0.0.1:12924', '', '', '', '1', '退出登录', '2022-07-20 10:13:08');
INSERT INTO `sys_logininfor` VALUES ('2648', 'admin', '/127.0.0.1:12924', '', '', '', '1', '连接异常', '2022-07-20 10:13:08');
INSERT INTO `sys_logininfor` VALUES ('2649', 'admin', '/127.0.0.1:2214', '', '', '', '0', '登录成功！', '2022-07-20 10:15:38');
INSERT INTO `sys_logininfor` VALUES ('2650', 'admin', '/127.0.0.1:2214', '', '', '', '1', '连接异常', '2022-07-20 10:15:38');
INSERT INTO `sys_logininfor` VALUES ('2651', 'admin', '/127.0.0.1:2214', '', '', '', '1', '退出登录', '2022-07-20 10:15:38');
INSERT INTO `sys_logininfor` VALUES ('2652', 'admin', '/127.0.0.1:2214', '', '', '', '1', '连接异常', '2022-07-20 10:15:38');
INSERT INTO `sys_logininfor` VALUES ('2653', 'admin', '/127.0.0.1:14499', '', '', '', '0', '登录成功！', '2022-07-20 10:18:34');
INSERT INTO `sys_logininfor` VALUES ('2654', 'admin', '/127.0.0.1:14499', '', '', '', '1', '连接异常', '2022-07-20 10:18:34');
INSERT INTO `sys_logininfor` VALUES ('2655', 'admin', '/127.0.0.1:14499', '', '', '', '1', '退出登录', '2022-07-20 10:18:34');
INSERT INTO `sys_logininfor` VALUES ('2656', 'admin', '/127.0.0.1:14499', '', '', '', '1', '连接异常', '2022-07-20 10:18:34');
INSERT INTO `sys_logininfor` VALUES ('2657', 'admin', '/127.0.0.1:14521', '', '', '', '0', '登录成功！', '2022-07-20 10:18:59');
INSERT INTO `sys_logininfor` VALUES ('2658', 'admin', '/127.0.0.1:14521', '', '', '', '1', '连接异常', '2022-07-20 10:18:59');
INSERT INTO `sys_logininfor` VALUES ('2659', 'admin', '/127.0.0.1:14521', '', '', '', '1', '退出登录', '2022-07-20 10:18:59');
INSERT INTO `sys_logininfor` VALUES ('2660', 'admin', '/127.0.0.1:14521', '', '', '', '1', '连接异常', '2022-07-20 10:18:59');
INSERT INTO `sys_logininfor` VALUES ('2661', 'admin', '/127.0.0.1:14546', '', '', '', '0', '登录成功！', '2022-07-20 10:19:19');
INSERT INTO `sys_logininfor` VALUES ('2662', 'admin', '/127.0.0.1:14546', '', '', '', '1', '连接异常', '2022-07-20 10:19:19');
INSERT INTO `sys_logininfor` VALUES ('2663', 'admin', '/127.0.0.1:14546', '', '', '', '1', '退出登录', '2022-07-20 10:19:19');
INSERT INTO `sys_logininfor` VALUES ('2664', 'admin', '/127.0.0.1:14546', '', '', '', '1', '连接异常', '2022-07-20 10:19:19');
INSERT INTO `sys_logininfor` VALUES ('2665', 'admin', '/127.0.0.1:4523', '', '', '', '0', '登录成功！', '2022-07-20 10:19:48');
INSERT INTO `sys_logininfor` VALUES ('2666', 'admin', '/127.0.0.1:4523', '', '', '', '1', '连接异常', '2022-07-20 10:19:48');
INSERT INTO `sys_logininfor` VALUES ('2667', 'admin', '/127.0.0.1:4523', '', '', '', '1', '退出登录', '2022-07-20 10:19:48');
INSERT INTO `sys_logininfor` VALUES ('2668', 'admin', '/127.0.0.1:4523', '', '', '', '1', '连接异常', '2022-07-20 10:19:48');
INSERT INTO `sys_logininfor` VALUES ('2669', 'admin', '/127.0.0.1:4563', '', '', '', '0', '登录成功！', '2022-07-20 10:20:41');
INSERT INTO `sys_logininfor` VALUES ('2670', 'admin', '/127.0.0.1:4563', '', '', '', '1', '连接异常', '2022-07-20 10:20:41');
INSERT INTO `sys_logininfor` VALUES ('2671', 'admin', '/127.0.0.1:4563', '', '', '', '1', '退出登录', '2022-07-20 10:20:41');
INSERT INTO `sys_logininfor` VALUES ('2672', 'admin', '/127.0.0.1:4563', '', '', '', '1', '连接异常', '2022-07-20 10:20:41');
INSERT INTO `sys_logininfor` VALUES ('2673', 'admin', '/127.0.0.1:4614', '', '', '', '0', '登录成功！', '2022-07-20 10:21:51');
INSERT INTO `sys_logininfor` VALUES ('2674', 'admin', '/127.0.0.1:4614', '', '', '', '1', '连接异常', '2022-07-20 10:21:51');
INSERT INTO `sys_logininfor` VALUES ('2675', 'admin', '/127.0.0.1:4614', '', '', '', '1', '退出登录', '2022-07-20 10:21:51');
INSERT INTO `sys_logininfor` VALUES ('2676', 'admin', '/127.0.0.1:4614', '', '', '', '1', '连接异常', '2022-07-20 10:21:51');
INSERT INTO `sys_logininfor` VALUES ('2677', 'admin', '/127.0.0.1:10218', '', '', '', '0', '登录成功！', '2022-07-20 10:22:29');
INSERT INTO `sys_logininfor` VALUES ('2678', 'admin', '/127.0.0.1:10218', '', '', '', '1', '连接异常', '2022-07-20 10:22:29');
INSERT INTO `sys_logininfor` VALUES ('2679', 'admin', '/127.0.0.1:10218', '', '', '', '1', '退出登录', '2022-07-20 10:22:29');
INSERT INTO `sys_logininfor` VALUES ('2680', 'admin', '/127.0.0.1:10218', '', '', '', '1', '连接异常', '2022-07-20 10:22:29');
INSERT INTO `sys_logininfor` VALUES ('2681', 'admin', '/127.0.0.1:10252', '', '', '', '0', '登录成功！', '2022-07-20 10:23:06');
INSERT INTO `sys_logininfor` VALUES ('2682', 'admin', '/127.0.0.1:10252', '', '', '', '1', '连接异常', '2022-07-20 10:23:06');
INSERT INTO `sys_logininfor` VALUES ('2683', 'admin', '/127.0.0.1:10252', '', '', '', '1', '退出登录', '2022-07-20 10:23:06');
INSERT INTO `sys_logininfor` VALUES ('2684', 'admin', '/127.0.0.1:10252', '', '', '', '1', '连接异常', '2022-07-20 10:23:06');
INSERT INTO `sys_logininfor` VALUES ('2685', 'admin', '/127.0.0.1:10292', '', '', '', '0', '登录成功！', '2022-07-20 10:24:04');
INSERT INTO `sys_logininfor` VALUES ('2686', 'admin', '/127.0.0.1:10292', '', '', '', '1', '连接异常', '2022-07-20 10:24:04');
INSERT INTO `sys_logininfor` VALUES ('2687', 'admin', '/127.0.0.1:10292', '', '', '', '1', '退出登录', '2022-07-20 10:24:04');
INSERT INTO `sys_logininfor` VALUES ('2688', 'admin', '/127.0.0.1:10292', '', '', '', '1', '连接异常', '2022-07-20 10:24:04');
INSERT INTO `sys_logininfor` VALUES ('2689', 'admin', '/127.0.0.1:10694', '', '', '', '0', '登录成功！', '2022-07-20 10:28:22');
INSERT INTO `sys_logininfor` VALUES ('2690', 'admin', '/127.0.0.1:10694', '', '', '', '1', '退出登录', '2022-07-20 10:28:22');
INSERT INTO `sys_logininfor` VALUES ('2691', 'admin', '/127.0.0.1:7362', '', '', '', '0', '登录成功！', '2022-07-20 10:32:48');
