package com.tcoding.server.service;

import com.tcoding.core.entity.CustomerSearchItem;
import com.tcoding.server.mapper.CustomerSearchItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/10 10:07
 * @description
 **/
@Service
public class CustomerSearchItemService {

    @Autowired
    private CustomerSearchItemMapper customerSearchItemMapper;

    public List<CustomerSearchItem> queryByProgrammeId(Integer programmeId){
        return customerSearchItemMapper.queryByProgrammeId(programmeId);
    }

    @Transactional(rollbackFor = Exception.class)
    public int save(List<CustomerSearchItem> customerSearchItems){
        if(customerSearchItems.isEmpty()){
            return 0;
        }
        this.deleteByProgrammeId(customerSearchItems.get(0).getProgrammeId());

        for (CustomerSearchItem customerSearchItem : customerSearchItems) {
            customerSearchItemMapper.save(customerSearchItem);
        }
        return customerSearchItems.size();
    }

    public int delete(Integer itemId){
        return customerSearchItemMapper.delete(itemId);
    }

    public int deleteByProgrammeId(Integer programmeId){
        return customerSearchItemMapper.deleteByProgrammeId(programmeId);
    }

    public int update(CustomerSearchItem customerSearchItem){
        return customerSearchItemMapper.update(customerSearchItem);
    }
}
