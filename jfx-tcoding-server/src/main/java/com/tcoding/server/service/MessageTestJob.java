package com.tcoding.server.service;

import com.tcoding.server.websocket.WebSocketMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

/**
 * @author 唐全成
 * @Date: 2022/6/13 10:53
 * @description
 **/
@Component
public class MessageTestJob {
    @Autowired
    private WebSocketMessageHandler webSocketMessageHandler;


    @Scheduled(cron = "0/10 * * * * ?")
    public void testMessage(){
        //将内容转换为 TextMessage
        TextMessage textMessage = new TextMessage("测试消息"+System.currentTimeMillis());
        webSocketMessageHandler.sendMessageToUsers(textMessage);
    }


}
