package com.tcoding.server.service;

import com.tcoding.core.entity.CustomerSearchProgramme;
import com.tcoding.server.mapper.CustomerSearchProgrammeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/8 11:17
 * @description
 **/
@Service
public class CustomerSearchProgrammeService {

    @Autowired
    private CustomerSearchProgrammeMapper customerSearchProgrammeMapper;


    public List<CustomerSearchProgramme> queryByUserId(Integer userId,String realm){
     return customerSearchProgrammeMapper.queryByUserId(userId,realm);
    }

    public Integer save(CustomerSearchProgramme customerSearchProgramme){
        customerSearchProgrammeMapper.save(customerSearchProgramme);
        return customerSearchProgramme.getId();
    }

    public Integer delete(Integer id){
        return customerSearchProgrammeMapper.delete(id);
    }

    public Integer update(CustomerSearchProgramme customerSearchProgramme){
        customerSearchProgrammeMapper.update(customerSearchProgramme);
        return customerSearchProgramme.getId();
    }

}
