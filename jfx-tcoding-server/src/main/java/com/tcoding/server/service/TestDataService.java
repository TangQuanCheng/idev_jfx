package com.tcoding.server.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tcoding.core.entity.TestData;
import com.tcoding.core.msg.TableResultResponse;
import com.tcoding.core.util.Query;
import com.tcoding.server.mapper.TestDataMapper;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/7/21 14:22
 * @description
 **/
@Service
public class TestDataService {
    @Autowired
    private TestDataMapper testDataMapper;

    public int addData(){
        int result = 0;
        for(int i=0;i<1000;i++){
            TestData testData = TestData.builder().col1("random_1_data_"+ RandomUtils.nextInt(10000,100000))
                    .col2("random_2_data_"+ RandomUtils.nextInt(10000,100000))
                    .col3("random_3_data_"+ RandomUtils.nextInt(10000,100000))
                    .col4("random_4_data_"+ RandomUtils.nextInt(10000,100000))
                    .col5("random_5_data_"+ RandomUtils.nextInt(10000,100000))
                    .col6("random_6_data_"+ RandomUtils.nextInt(10000,100000))
                    .col7("random_7_data_"+ RandomUtils.nextInt(10000,100000))
                    .col8("random_8_data_"+ RandomUtils.nextInt(10000,100000))
                    .col9("random_9_data_"+ RandomUtils.nextInt(10000,100000))
                    .col10("random_10_data_"+ RandomUtils.nextInt(10000,100000))
                    .col11("random_11_data_"+ RandomUtils.nextInt(10000,100000))
                    .col12("random_12_data_"+ RandomUtils.nextInt(10000,100000))
                    .col13("random_13_data_"+ RandomUtils.nextInt(10000,100000))
                    .col14("random_14_data_"+ RandomUtils.nextInt(10000,100000))
                    .col15("random_15_data_"+ RandomUtils.nextInt(10000,100000))
                    .col16("random_16_data_"+ RandomUtils.nextInt(10000,100000))
                    .col17("random_17_data_"+ RandomUtils.nextInt(10000,100000))
                    .col18("random_18_data_"+ RandomUtils.nextInt(10000,100000))
                    .col19("random_19_data_"+ RandomUtils.nextInt(10000,100000))
                    .col20("random_20_data_"+ RandomUtils.nextInt(10000,100000))
                    .col21("random_21_data_"+ RandomUtils.nextInt(10000,100000))
                    .col22("random_22_data_"+ RandomUtils.nextInt(10000,100000))
                    .col23("random_23_data_"+ RandomUtils.nextInt(10000,100000))
                    .col24("random_24_data_"+ RandomUtils.nextInt(10000,100000))
                    .col25("random_25_data_"+ RandomUtils.nextInt(10000,100000))
                    .col26("random_26_data_"+ RandomUtils.nextInt(10000,100000))
                    .col27("random_27_data_"+ RandomUtils.nextInt(10000,100000))
                    .col28("random_28_data_"+ RandomUtils.nextInt(10000,100000))
                    .col29("random_29_data_"+ RandomUtils.nextInt(10000,100000))
                    .col30("random_30_data_"+ RandomUtils.nextInt(10000,100000))
                    .build();
            int i1 = testDataMapper.saveData(testData);
            result+=i1;
        }
        return result;
    }

    public List<TestData> getAllData(){
        return testDataMapper.queryAll();
    }



    public TableResultResponse<TestData> getPage(Map<String, Object> params){
        Query query = new Query(params);
        Page<Object> page = PageHelper.startPage(query.getPage(), query.getLimit());
        List<TestData> testData = testDataMapper.queryPage();
        return new TableResultResponse<>(page.getTotal(),testData);
    }
}
