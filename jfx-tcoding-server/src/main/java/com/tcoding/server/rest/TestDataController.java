package com.tcoding.server.rest;

import com.github.pagehelper.Page;
import com.tcoding.core.entity.TestData;
import com.tcoding.core.msg.TableResultResponse;
import com.tcoding.server.service.TestDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/7/21 14:29
 * @description
 **/
@RequestMapping("testData")
@RestController
public class TestDataController {

    @Autowired
    private TestDataService testDataService;

    @RequestMapping("addData")
    public int addData(){
        return testDataService.addData();
    }

    @RequestMapping("queryAll")
    public List<TestData> queryAll(){
        return testDataService.getAllData();
    }

    @RequestMapping("queryPage")
    public TableResultResponse<TestData> queryPage(@RequestBody Map<String, Object> params){
        return testDataService.getPage(params);
    }
}
