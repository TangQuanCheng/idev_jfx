package com.tcoding.server.rest;

import com.tcoding.core.entity.CustomerSearchProgramme;
import com.tcoding.core.jwt.IJWTInfo;
import com.tcoding.server.service.CustomerSearchProgrammeService;
import com.tcoding.server.service.security.auth.AuthService;
import com.tcoding.server.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/8 11:22
 * @description
 **/
@RequestMapping("customerSearchProgramme")
@RestController
public class CustomerSearchProgrammeController {

    @Autowired
    private CustomerSearchProgrammeService customerSearchProgrammeService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping("list/{actionRealm}")
    public List<CustomerSearchProgramme> queryByUserId(HttpServletRequest request,@PathVariable String actionRealm) throws Exception {
        String token = request.getHeader("authorization");
        IJWTInfo infoFromToken = jwtTokenUtil.getInfoFromToken(token);
        return customerSearchProgrammeService.queryByUserId(Integer.valueOf(infoFromToken.getId()),actionRealm);
    }

    @RequestMapping("save")
    public Integer save(@RequestBody CustomerSearchProgramme customerSearchProgramme,HttpServletRequest request) throws Exception {
        String token = request.getHeader("authorization");
        IJWTInfo infoFromToken = jwtTokenUtil.getInfoFromToken(token);
        customerSearchProgramme.setUserId(Integer.valueOf(infoFromToken.getId()));
        return customerSearchProgrammeService.save(customerSearchProgramme);
    }

    @RequestMapping("delete/{id}")
    public Integer delete(@PathVariable Integer id){
        return customerSearchProgrammeService.delete(id);
    }

    @RequestMapping("update")
    public Integer update(@RequestBody CustomerSearchProgramme customerSearchProgramme){
        return customerSearchProgrammeService.update(customerSearchProgramme);
    }
}
