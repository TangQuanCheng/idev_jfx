package com.tcoding.server.rest;

import com.tcoding.core.entity.CustomerSearchItem;
import com.tcoding.server.service.CustomerSearchItemService;
import com.tcoding.server.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/8 11:22
 * @description
 **/
@RequestMapping("customerSearchItem")
@RestController
public class CustomerSearchItemController {

    @Autowired
    private CustomerSearchItemService customerSearchItemService;

    @RequestMapping("list/{programmeId}")
    public List<CustomerSearchItem> queryByUserId(HttpServletRequest request,@PathVariable Integer programmeId) throws Exception {
        return customerSearchItemService.queryByProgrammeId(programmeId);
    }

    @RequestMapping("save")
    public Integer save(@RequestBody List<CustomerSearchItem> customerSearchItems) throws Exception {
        return customerSearchItemService.save(customerSearchItems);
    }

    @RequestMapping("delete/{id}")
    public Integer delete(@PathVariable Integer id){
        return customerSearchItemService.delete(id);
    }

    @RequestMapping("update")
    public Integer update(@RequestBody CustomerSearchItem customerSearchItem){
        return customerSearchItemService.update(customerSearchItem);
    }
}
