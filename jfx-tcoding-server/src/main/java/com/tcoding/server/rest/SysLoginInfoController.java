package com.tcoding.server.rest;

import com.tcoding.core.entity.log.SysLoginInfor;
import com.tcoding.core.msg.TableResultResponse;
import com.tcoding.server.service.log.SysLoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description:
 * @className: SysLoginInfoController
 * @author: liwen
 * @date: 2021/1/4 13:21
 */
@RestController()
@RequestMapping("log/login")
public class SysLoginInfoController {

    @Autowired
    private SysLoginInfoService sysLoginInfoService;

    @GetMapping("/list")
    @ResponseBody
    private TableResultResponse<SysLoginInfor> getSysloginInforList(@RequestParam Map<String, Object> params) {
        return sysLoginInfoService.getPageList(params);
    }
}
