package com.tcoding.server.mapper;

import com.tcoding.core.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/6/13 13:44
 * @description
 **/
@Mapper
@Repository
public interface MessageMapper {

    List<Message> queryAll();

}
