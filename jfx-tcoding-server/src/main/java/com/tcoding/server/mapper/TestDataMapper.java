package com.tcoding.server.mapper;

import com.tcoding.core.entity.TestData;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/**
 * @author 唐全成
 * @Date: 2022/7/21 14:04
 * @description
 **/
public interface TestDataMapper {

    @Insert("insert into test_bigdata (" +
            "col1,col2,col3,col4,col5,col6,col7,col8,col9,col10," +
            "col11,col12,col13,col14,col15,col16,col17,col18,col19,col20," +
            "col21,col22,col23,col24,col25,col26,col27,col28,col29,col30) " +
            "values" +
            "(#{testData.col1},#{testData.col2},#{testData.col3},#{testData.col4},#{testData.col5},#{testData.col6}," +
            "#{testData.col7},#{testData.col8},#{testData.col9},#{testData.col10},#{testData.col11},#{testData.col12}," +
            "#{testData.col13},#{testData.col14},#{testData.col15},#{testData.col16},#{testData.col17},#{testData.col18}," +
            "#{testData.col19},#{testData.col20},#{testData.col21},#{testData.col22},#{testData.col23},#{testData.col24}," +
            "#{testData.col25},#{testData.col26},#{testData.col27},#{testData.col28},#{testData.col29},#{testData.col30})")
    int saveData(@Param("testData") TestData testData);

    @Select("select * from test_bigdata ")
    List<TestData> queryAll();

    @Select("select * from test_bigdata  ")
    List<TestData> queryPage();
}
