package com.tcoding.server.mapper;

import com.tcoding.core.entity.CustomerSearchProgramme;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/8 10:48
 * @description
 **/
@Mapper
public interface CustomerSearchProgrammeMapper {


    /**
     * 查询
     * @param userId
     * @return
     */
    @Select("select id,realm_name as actionRealm,parent_id as parentId, title,user_id as userId, create_time as createTime,level from customer_search_programme " +
            "where user_id = #{userId} and realm_name  = #{realm}")
    List<CustomerSearchProgramme> queryByUserId(@Param("userId") Integer userId,@Param("realm") String realm);

    /**
     * 修改
     * @param customerSearchProgramme
     * @return
     */
    @Update("update customer_search_programme set title = #{customerSearchProgramme.title} where id = #{customerSearchProgramme.id}")
    int update(@Param("customerSearchProgramme") CustomerSearchProgramme customerSearchProgramme);

    /**
     * 删除
     * @param id
     * @return
     */
    @Delete("delete from customer_search_programme where id= #{id}")
    int delete(Integer id);

    /**
     * 保存
     * @param customerSearchProgramme
     * @return
     */
    @Insert("insert into customer_search_programme (title,realm_name,parent_id, user_id , create_time ,level) " +
            "values (#{customerSearchProgramme.title},#{customerSearchProgramme.actionRealm},#{customerSearchProgramme.parentId},#{customerSearchProgramme.userId},now(),#{customerSearchProgramme.level}) ")
    @SelectKey(before = false, keyProperty = "customerSearchProgramme.id", resultType = Integer.class, statement =
            "SELECT LAST_INSERT_ID()")
    int save(@Param("customerSearchProgramme") CustomerSearchProgramme customerSearchProgramme);
}
