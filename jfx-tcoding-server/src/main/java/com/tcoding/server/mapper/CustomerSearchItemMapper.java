package com.tcoding.server.mapper;

import com.tcoding.core.entity.CustomerSearchItem;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/10 9:09
 * @description
 **/
@Mapper
public interface CustomerSearchItemMapper {

    @Select("select id," +
            " programme_id as programmeId ," +
            " item_index as itemIndex," +
            " group_tag as groupTag," +
            " item_name as itemName," +
            " action," +
            " value," +
            " logic " +
            " from customer_search_item " +
            " where programme_id = #{programmeId}")
    List<CustomerSearchItem> queryByProgrammeId(Integer programmeId);

    @Insert("insert into customer_search_item (programme_id,item_index,group_tag,item_name,action,value,logic) " +
            "values(#{customerSearchItem.programmeId},#{customerSearchItem.itemIndex},#{customerSearchItem.groupTag}," +
            "#{customerSearchItem.itemName},#{customerSearchItem.action},#{customerSearchItem.value},#{customerSearchItem.logic})")
    @SelectKey(before = false, keyProperty = "customerSearchItem.id", resultType = Integer.class, statement =
            "SELECT LAST_INSERT_ID()")
    int save(@Param("customerSearchItem") CustomerSearchItem customerSearchItem);

    @Delete("delete from customer_search_item where id =#{itemId}")
    int delete(Integer itemId);

    @Delete("delete from customer_search_item where programme_id =#{programmeId}")
    int deleteByProgrammeId(Integer programmeId);

    @Update("update customer_search_item set group_tag = #{customerSearchItem.groupTag}, item_name = #{customerSearchItem.itemName}," +
            " action = #{customerSearchItem.action}, value = #{customerSearchItem.value}, logic = #{customerSearchItem.logic}" +
            " where id = #{customerSearchItem.id} ")
    int update(CustomerSearchItem customerSearchItem);

}
