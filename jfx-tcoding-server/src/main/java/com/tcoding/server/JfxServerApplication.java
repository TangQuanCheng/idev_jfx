package com.tcoding.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@MapperScan("com.tcoding.server.mapper")
@SpringBootApplication(exclude= SecurityAutoConfiguration.class)
@EnableScheduling
public class JfxServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JfxServerApplication.class, args);
    }

}
