/*
 * Copyright (c) 2016 JFoenix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.tcoding.client.editor;

import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;

/**
 * <h1>Text field cell editor</h1>
 * this an example of the cell editor, it creates a JFXTextField node to
 * allow the user to edit the cell value
 * <p>
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2016-03-09
 */
public abstract class ComboBoxEditorBaseback<T> implements EditorNodeBuilder<T> {

    protected ComboBox<T> comboBox;
    protected ObservableList<T> items;

    @Override
    public void startEdit() {
        T value = comboBox.getValue();

        int index = 0;
        for(int i=0;i<items.size();i++){
            if(items.get(i).equals(value)){
                index = i;
                break;
            }
        }

        int finalIndex = index;
        Platform.runLater(() -> {
            comboBox.getSelectionModel().select(finalIndex);
        });
    }

    @Override
    public void cancelEdit() {

    }

    @Override
    public void updateItem(T item, boolean empty) {
        Platform.runLater(() -> {
            comboBox.getSelectionModel().select(item);
        });
    }

    @Override
    public Region createNode(T value, EventHandler<KeyEvent> keyEventsHandler, ChangeListener<Boolean> focusChangeListener) {
        comboBox =  new ComboBox<T>(items);
        if(value!=null){
            comboBox.setValue(value);
        }
        comboBox.getStyleClass().add("cf-combo-box");
        comboBox.setOnKeyPressed(keyEventsHandler);
        comboBox.focusedProperty().addListener(focusChangeListener);
        return comboBox;
    }

    @Override
    public void setValue(T value) {
        comboBox.setValue(value);
    }

    @Override
    public void validateValue() throws Exception {
        if (comboBox.getValue()==null) {
            throw new Exception("Invalid value");
        }
    }

    @Override
    public void nullEditorNode() {
        comboBox = null;
    }
}
