package com.tcoding.client.extend.jfoenix;

import com.jfoenix.controls.cells.editors.TextFieldEditorBase;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;

/**
 * @author 唐全成
 * @Date: 2022/9/1 17:13
 * @description
 **/
public class MyTextFieldEditorBuilder extends TextFieldEditorBase<String> {

    public MyTextFieldEditorBuilder(ValidatorBase... validators) {
        super(validators);
    }

    @Override
    public String getValue() {
        return this.textField.getText();
    }
}
