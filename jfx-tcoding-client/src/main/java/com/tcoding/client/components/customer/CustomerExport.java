package com.tcoding.client.components.customer;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.svg.SVGGlyph;
import com.tcoding.client.config.AppConfig;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.utils.SvgGraphicUtil;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author 唐全成
 * @Date: 2022/8/3 13:42
 * @description
 **/
public abstract class CustomerExport<T> {


    private Properties appConfig;


    public void initComponent(Pane buttonPane, TableView<T> tableView,String fileName){
        JFXButton button = new JFXButton();
        button.setText("导出");
        button.getStyleClass().add("cf-primary-but");
        SVGGlyph svgGlyph = SvgGraphicUtil.buildSvgGraphic("download", 12, Color.WHITE);
        button.setGraphic(svgGlyph);
        buttonPane.getChildren().add(button);
        appConfig = AppConfig.getProperties();
        button.setOnAction((e)->{
            ApplicationStore.getProgressBar().setVisible(true);
            ApplicationStore.EXECUTOR_SERVICE.execute(()->{
                button.setDisable(true);
                Workbook workbook = new HSSFWorkbook();
                Sheet spreadsheet = workbook.createSheet("sample");
                Row row = spreadsheet.createRow(0);
                for (int j = 0; j < tableView.getColumns().size(); j++) {
                    row.createCell(j).setCellValue(tableView.getColumns().get(j).getText());
                }

                for (int i = 0; i < tableView.getItems().size(); i++) {
                    row = spreadsheet.createRow(i + 1);
                    for (int j = 0; j < tableView.getColumns().size(); j++) {
                        if(tableView.getColumns().get(j).getCellData(i) != null) {
                            row.createCell(j).setCellValue(tableView.getColumns().get(j).getCellData(i).toString());
                        }
                        else {
                            row.createCell(j).setCellValue("");
                        }
                    }
                }

                FileOutputStream fileOut = null;
                try {
                    fileOut = new FileOutputStream(appConfig.getProperty("fileDownloadPath")+fileName+System.currentTimeMillis()+".xls");
                    workbook.write(fileOut);
                    fileOut.close();
                    System.out.println("导出数据量："+tableView.getItems().size());
                } catch (IOException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }finally {
                    button.setDisable(false);
                    ApplicationStore.getProgressBar().setVisible(false);
                    this.endExport();
                }

            });

        });
    }

    /**
     * 开始导出
     */
    public abstract void startExport();

    /**
     * 结束导出
     */
    public abstract void endExport();

}
