package com.tcoding.client.components.customer;

import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.tcoding.client.event.CustomerEvent;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.utils.SvgGraphicUtil;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;

import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * @author 唐全成
 * @Date: 2022/8/9 11:12
 * @description 表格右键
 **/
public class CustomerTableContextMenu<T> extends ContextMenu {

    private TableView<T> registeredTable;

    private EventHandler<CustomerEvent<T>> remove;

    private EventHandler<CustomerEvent<T>> update;

    private JFXSnackbar jfxSnackbar;

    public void initComponent(TableView<T> tableView ,
                              EventHandler<CustomerEvent<T>> removeHandler,
                              EventHandler<CustomerEvent<T>> updateHandler,
                              JFXSnackbar snackbar){
        registeredTable =tableView;
        remove = removeHandler;
        update = updateHandler;
        jfxSnackbar = snackbar;

        ResourceBundle resourceBundle = ApplicationStore.getResourceBundle();

        MenuItem copy = new MenuItem(resourceBundle.getString("copy"));
        MenuItem remove = new MenuItem(resourceBundle.getString("delete"));
        MenuItem update = new MenuItem(resourceBundle.getString("update"));


        copy.setGraphic(SvgGraphicUtil.buildSvgGraphic("copy2",12, Color.GRAY));
        remove.setGraphic(SvgGraphicUtil.buildSvgGraphic("trash",12, Color.GRAY));
        update.setGraphic(SvgGraphicUtil.buildSvgGraphic("pencil",12, Color.GRAY));

        copy.setOnAction(event -> actionFire(event,1));
        remove.setOnAction(event -> actionFire(event,2));
        update.setOnAction(event -> actionFire(event,3));

        getItems().add(copy);
        getItems().add(remove);
        getItems().add(update);
        tableView.setContextMenu(this);

    }

    private void actionFire(ActionEvent event, int actionId){

        ObservableList<T> selectedItems = registeredTable.getSelectionModel().getSelectedItems();

        switch (actionId){
            case 1:
                jfxSnackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout(selectedItems.toString())));
                break;
            case 2:
                remove.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                    put("data", selectedItems.toString());
                }}));
                break;
            case 3:
                if(selectedItems.size()>1){
                    jfxSnackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("请选择单条数据进行修改")));
                    break;
                }
                update.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                    put("data", selectedItems.toString());
                }}));
                break;
            default:
        }

    }




}
