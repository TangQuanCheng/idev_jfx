package com.tcoding.client.components;

import com.jfoenix.controls.*;
import com.jfoenix.controls.JFXPopup.PopupHPosition;
import com.jfoenix.controls.JFXPopup.PopupVPosition;
import com.jfoenix.controls.JFXRippler.RipplerMask;
import com.jfoenix.controls.JFXRippler.RipplerPos;
import com.tcoding.client.components.extend.ComboGridConfig;
import com.tcoding.client.components.extend.ComboGridTableColumn;
import com.tcoding.client.components.extend.JFXComboGrid;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PopupDemo extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        JFXHamburger show = new JFXHamburger();
        show.setPadding(new Insets(10, 5, 10, 5));
        JFXRippler rippler = new JFXRippler(show, RipplerMask.CIRCLE, RipplerPos.BACK);

        JFXListView<Label> list = new JFXListView<>();
        for (int i = 1; i < 5; i++) {
            list.getItems().add(new Label("Item " + i));
        }

        AnchorPane container = new AnchorPane();
        container.getChildren().add(rippler);
        AnchorPane.setLeftAnchor(rippler, 200.0);
        AnchorPane.setTopAnchor(rippler, 210.0);

        StackPane main = new StackPane();
        main.getChildren().add(container);

        LinkedList<ComboGridTableColumn> columns = new LinkedList<ComboGridTableColumn>(){{
            add(new ComboGridTableColumn("船名","shipName",160.0));
            add(new ComboGridTableColumn("船号","shipCode",180.0));
        }};

        ComboGridConfig comboGridConfig = new ComboGridConfig();
        comboGridConfig.setPlaceholder("测试22");
        comboGridConfig.setValue("2");
        comboGridConfig.setColumns(columns);
        comboGridConfig.setTableTextColumn("shipName");
        comboGridConfig.setTableValueColumn("shipCode");
//        JFXComboGrid comboGrid = new JFXComboGrid(comboGridConfig);
//        comboGrid.getItems().add(new HashMap<String,Object>(4){{
//            put("shipName","公主");
//            put("shipCode","princess");
//        }});
//        comboGrid.getItems().add(new HashMap<String,Object>(4){{
//            put("shipName","王子");
//            put("shipCode","prince");
//        }});



//        main.getChildren().add(comboGrid);
        main.setOnMouseClicked(e->{
            System.out.println("aaa");
        });
        JFXPopup popup = new JFXPopup(list);
        rippler.setOnMouseClicked(e -> popup.show(rippler, PopupVPosition.TOP, PopupHPosition.LEFT));

        final Scene scene = new Scene(main, 1800, 800);
        scene.getStylesheets().add(PopupDemo.class.getResource("/css/jfoenix-components.css").toExternalForm());

        primaryStage.setTitle("JFX Popup Demo");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

