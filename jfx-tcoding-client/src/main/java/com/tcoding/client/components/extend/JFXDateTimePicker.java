package com.tcoding.client.components.extend;

import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.kordamp.ikonli.javafx.FontIcon;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/9/8 16:07
 * @description
 **/
@Data
public class JFXDateTimePicker extends HBox {

    private JFXPopup comboPopup;
    private JFXTextField comboTextField;
    private StackPane comboTablePane;
    private JFXDateTimeCalendar calendar;
    private LocalDateTime value;
    private DoubleProperty pickerWidth = new SimpleDoubleProperty();
    private String placeholder;

    public double getPickerWidth() {
        return pickerWidth.get();
    }

    public DoubleProperty pickerWidthProperty() {
        return pickerWidth;
    }

    public void setPickerWidth(double pickerWidth) {
        this.pickerWidth.set(pickerWidth);
    }

    /**
     * Creates an HBox layout with spacing = 0.
     */
    public JFXDateTimePicker() {
        this.init();
    }


    private void init(){
        comboTextField = new JFXTextField();
        super.setPadding(Insets.EMPTY);
        comboTextField.setPromptText(StringUtils.isBlank(placeholder)?"请选择日期":placeholder);
        comboTablePane = new StackPane();
        calendar = new JFXDateTimeCalendar( event -> {
            Map<String, Object> paramMap = event.getParamMap();
            Object data = paramMap.get("data");
            this.value =data==null?null: LocalDateTime.parse(data.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            this.comboTextField.setText(data==null?"":data.toString());
            if(comboPopup.isShowing()){
                comboPopup.hide();
            }
        },event -> {
            if(comboPopup.isShowing()){
                comboPopup.hide();
            }
        });


        comboTablePane.getChildren().add(calendar);
        comboPopup = new JFXPopup(comboTablePane);
        comboTablePane.setPrefWidth(310);
        EventHandler<KeyEvent> keyEventsHandler = t -> {
            if (t.getCode() == KeyCode.BACK_SPACE||t.getCode() == KeyCode.DELETE) {
                comboTextField.setText("");
            }
        };
        comboTextField.setOnKeyPressed(keyEventsHandler);

        this.setSpacing(6.0);
        this.setAlignment(Pos.BOTTOM_CENTER);
        this.getChildren().add(comboTextField);

        FontIcon fontIcon = new FontIcon("far-calendar-alt");
        fontIcon.setIconSize(18);
        fontIcon.setIconColor(Color.GOLDENROD);
        comboPopup.setAutoHide(false);
        fontIcon.setOnMouseClicked(e->{
            if(comboPopup.isShowing()){
                comboPopup.hide();
            }else {
                comboPopup.show(comboTextField, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT,0,30);
                comboTextField.requestFocus();
            }
        });
        comboTextField.focusedProperty().addListener((ob,o,n)->{
            if(!n){
                if(comboPopup.isShowing()){
                    comboPopup.hide();
                }
            }
        });

        this.getChildren().add(fontIcon);
    }

    @Data
    static
    class PickerConfig{
        Integer width;
        String placeholder;

        public PickerConfig() {
        }

        public PickerConfig(Integer width, String placeholder) {
            this.width = width;
            this.placeholder = placeholder;
        }
    }

    public LocalDateTime getValue() {
        return value;
    }

    public void setValue(LocalDateTime value) {
        this.value = value;
        this.comboTextField.setText(value.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }
}
