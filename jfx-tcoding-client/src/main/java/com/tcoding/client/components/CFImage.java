package com.tcoding.client.components;

import com.tcoding.client.utils.CFFXUtils;
import javafx.animation.ScaleTransition;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.util.Duration;

/**
 * @author ChenFei
 * @date 2022/7/9
 * <p>
 * CFImage 图片
 */
public class CFImage extends StackPane {

    private StackPane imagePane = new StackPane();
    private StackPane view = new StackPane();
    private ScaleTransition transition = new ScaleTransition(Duration.millis(300), view);
    //
    private Image image;

    {
        this.getChildren().add(imagePane);
        imagePane.getChildren().add(view);
        this.setMaxWidth(Double.NEGATIVE_INFINITY);
        this.setMaxHeight(Double.NEGATIVE_INFINITY);
        //styleClass
        this.getStyleClass().add("cf-image");
        //动画
        this.hoverProperty().addListener((observableValue, aBoolean, t1) -> {
            transition.stop();
            transition.setToX(t1 ? 1.2 : 1);
            transition.setToY(t1 ? 1.2 : 1);
            transition.play();
        });
    }

    public CFImage(Image image) {
        this.image = image;
        setImageStyle(200, 3);
    }

    /**
     * 设置图片样式
     *
     * @param width
     * @param radius
     */
    public void setImageStyle(double width, double radius) {
        BackgroundSize backgroundSize = new BackgroundSize(200, 200, false, false, false, true);
        BackgroundImage backgroundImage = new BackgroundImage(image, null, null, BackgroundPosition.DEFAULT, backgroundSize);
        Background background = new Background(backgroundImage);
        view.setBackground(background);
        CFFXUtils.setClip(imagePane, radius * 2);//圆角
        view.setPrefHeight(width);
        view.setPrefWidth(width);
    }

}
