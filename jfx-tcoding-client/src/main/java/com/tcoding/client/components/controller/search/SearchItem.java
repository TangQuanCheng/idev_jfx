package com.tcoding.client.components.controller.search;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Data;

/**
 * @author 唐全成
 * @Date: 2022/7/25 13:36
 * @description
 **/
@Data
public final class SearchItem extends RecursiveTreeObject<SearchItem> {

    /**
     * 条目名称
     */
    private StringProperty itemName;
    /**
     * 运算
     */
    private StringProperty action;
    /**
     * 值
     */
    private StringProperty value;
    /**
     * 组标记（开始、结束）
     */
    private StringProperty groupTag;

    /**
     * 逻辑（or/and）
     */
    private StringProperty logic;


    public SearchItem(String itemName, String action, String value,String groupTag,String logic) {
        this.itemName = new SimpleStringProperty(itemName);
        this.action = new SimpleStringProperty(action);
        this.value = new SimpleStringProperty(value);
        this.groupTag = new SimpleStringProperty(groupTag);
        this.logic = new SimpleStringProperty(logic);
    }
}
