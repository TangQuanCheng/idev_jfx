package com.tcoding.client.components;

import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;

/**
 * @author ChenFei
 * @date 2022年6月9日
 * <p>
 * 原生ProgressIndicator样式美化案例
 */
public class CustomerProgressIndicator extends StackPane {

    private Double loadingWidth;

    private Double loadingHeight;

    /**
     * Creates a StackPane layout with default CENTER alignment.
     */
    public CustomerProgressIndicator(Double loadingWidth, Double loadingHeight) {
        this.loadingWidth = loadingWidth;
        this.loadingHeight = loadingHeight;
        ProgressIndicator pb = new ProgressIndicator();
        this.getChildren().add(pb);
        pb.setPrefWidth(loadingWidth);
        pb.setPrefHeight(loadingHeight);
        pb.getStyleClass().add("cf-progress-indicator");
    }


    public Double getLoadingWidth() {
        return loadingWidth;
    }

    public void setLoadingWidth(Double loadingWidth) {
        this.loadingWidth = loadingWidth;
    }

    public Double getLoadingHeight() {
        return loadingHeight;
    }

    public void setLoadingHeight(Double loadingHeight) {
        this.loadingHeight = loadingHeight;
    }
}
