package com.tcoding.client.components.customer;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.svg.SVGGlyph;
import com.tcoding.client.editor.CustomerBoxEditorModel;
import com.tcoding.client.event.CustomerEvent;
import com.tcoding.client.model.CustomerSearchModel;
import com.tcoding.client.components.controller.search.CustomerSearchController;
import com.tcoding.client.components.controller.search.SearchItem;
import com.tcoding.client.utils.SvgGraphicUtil;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.property.ListProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * @author 唐全成
 * @Date: 2022/7/21 16:06
 * @description
 **/
public class CustomerSearch<T> {

    private Pane barPane;

    private Scene scene;

    private Pane mainPane;
    /**
     *
     * @param context
     * @param buttonPane
     * @param rootPane
     * @param tableView
     * @param handler
     */
    public void initComponent(CustomerSearchModel customerSearchModel, ViewFlowContext context, Pane buttonPane, Pane rootPane, TableView<T> tableView, EventHandler<CustomerEvent<T>> handler){
        barPane = buttonPane;
        scene =  buttonPane.getScene();
        JFXButton button = new JFXButton();
        button.setText("高级查询");
        SVGGlyph svgGlyph = SvgGraphicUtil.buildSvgGraphic("sliders", 12, Color.WHITE);
        button.setGraphic(svgGlyph);
        button.getStyleClass().add("cf-success-but");
        buttonPane.getChildren().add(button);
        ObservableList<TableColumn<T, ?>> columns = tableView.getColumns();
        customerSearchModel.getSearchItems().clear();
        for (TableColumn<T, ?> column : columns) {
            String id = column.getId();
            String prop = StringUtils.replace(id,"Column","");
            customerSearchModel.getSearchItems().add(new CustomerBoxEditorModel(column.getText(),prop));
        }

        button.setOnAction(e->{

            Flow flow = new Flow(CustomerSearchController.class);

            JFXDialog dialog =new JFXDialog();
            JFXDialogLayout layout = new JFXDialogLayout();
            layout.setHeading(new Label("高级查询"));
            JFXButton confirmButton = new JFXButton("确定");
            JFXButton cancelButton = new JFXButton("取消");
            confirmButton.getStyleClass().add("cf-success-but");
            cancelButton.getStyleClass().add("cf-but");

            confirmButton.setOnAction(event -> {
                dialog.close();
                ListProperty<SearchItem> searchData = customerSearchModel.getSearchDatas();
                StringBuilder builder = new StringBuilder(100);
                for (SearchItem datum : searchData) {
                    builder.append(datum.getGroupTag().getValue()).append(" ")
                            .append(datum.getItemName().getValue()).append(" ")
                            .append(datum.getAction().getValue()).append(" ")
                            .append("'").append(datum.getValue().getValue()).append("'").append(" ")
                            .append(datum.getLogic().getValue()).append(" ");
                }
                handler.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                    put("data", builder.toString());
                }}));
            });
            cancelButton.setOnAction(event -> dialog.close());

            layout.setActions(cancelButton,confirmButton);
            layout.setPrefWidth(800);
            try {
                Object userData = tableView.getUserData();
                FlowHandler handler1 = flow.createHandler(context);
                handler1.registerInFlowContext("realmName",userData.toString());
                layout.setBody(handler1.start());
            } catch (FlowException flowException) {
                flowException.printStackTrace();
            }
            dialog.setContent(layout);
            dialog.setOverlayClose(false);
            dialog.setDialogContainer((StackPane) rootPane);
            dialog.show();

//            System.out.println("子级触发"+e.toString());
//            HashMap<String, Object> param = new HashMap<>(8);
//            param.put("demo","1");
//            handler.handle(new CustomerSearchEvent<T>(e,param));

        });
    }

}
