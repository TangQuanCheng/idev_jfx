package com.tcoding.client.components.extend;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
public class ComboGridConfig{
    private String placeholder;
    private String text;
    private String value;
    private Double width;
    private String tableTextColumn;
    private String tableValueColumn;
    private Double popWidth;
    private Boolean setInputValue =false;

    private LinkedList<ComboGridTableColumn> columns;

    private List<Map<String,Object>> items;

    private EventHandler<WindowEvent> popupShowing;

    private EventHandler<WindowEvent> popupShown;
}
