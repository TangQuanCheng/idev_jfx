package com.tcoding.client.event;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/7/21 16:58
 * @description
 **/
public class CustomerEvent<T> extends Event {

    private Map<String,Object> paramMap;

    /**
     * Construct a new {@code Event} with the specified event type. The source
     * and target of the event is set to {@code NULL_SOURCE_TARGET}.
     *
     * @param eventType the event type
     */
    public CustomerEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }


    /**
     * Construct a new {@code Event} with the specified event type. The source
     * and target of the event is set to {@code NULL_SOURCE_TARGET}.
     *
     * @param eventType the event type
     */
    public CustomerEvent(EventType<? extends Event> eventType, Map<String, Object> paramMap) {
        super(eventType);
        this.paramMap = paramMap;
    }

    public CustomerEvent(ActionEvent e, HashMap<String, Object> paramMap) {
        super(e.getEventType());
        this.paramMap = paramMap;
    }

    /**
     * Construct a new {@code Event} with the specified event type. The source
     * and target of the event is set to {@code NULL_SOURCE_TARGET}.
     *
     * @param e the event type
     */
    public CustomerEvent(MouseEvent e, Map<String, Object> paramMap) {
        super(e.getEventType());
        this.paramMap = paramMap;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }
}
