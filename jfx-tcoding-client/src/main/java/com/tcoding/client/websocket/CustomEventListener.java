package com.tcoding.client.websocket;

/**
 * @author 唐全成
 * @Date: 2022/6/13 11:40
 * @description
 **/
public interface CustomEventListener<E extends  CustomEvent> {
    /**
     * 处理器
     * @param e
     */
    public void handler(E e) throws ClassNotFoundException;
}
