package com.tcoding.client.websocket;

import com.jfoenix.controls.JFXSnackbar;
import com.tcoding.client.config.AppConfig;
import io.datafx.controller.context.ApplicationContext;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import io.datafx.core.concurrent.ProcessChain;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;
import com.tcoding.client.gui.main.LoginController;
import com.tcoding.client.store.ApplicationStore;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @className: Session
 * @author: liwen
 * @date: 2020/6/24 16:17
 */
@Slf4j
public class WSClient extends WebSocketClient {
    @FXMLViewFlowContext
    private ViewFlowContext context;
    private static WSClient instance;
    private JFXSnackbar snackbar;

    private StackPane stackPane;

    public static synchronized WSClient getInstance() {
        String websocketDomain = AppConfig.getProperties().getProperty("websocketDomain");
        if (instance == null) {
            Map<String, String> httpHeaders = new HashMap<>();
            try {
                instance = new WSClient(new URI("ws://"+websocketDomain+"/websocket/test"));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        return instance;
    }


    public WSClient(URI serverUri) {
        super(serverUri);
    }


    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.err.println(instance.isOpen());
    }

    @Override
    public void onMessage(String s) {
//        log.info("收到消息：{},{}",s,stackPane);
        CustomEventManager.publish(new SocketDataChangeEvent(s));
    }


    @Override
    public void onClose(int code, String reason, boolean remote) {
        instance = null;

        ProcessChain.create().addRunnableInPlatformThread(() -> {
            ApplicationStore.clearPermissionInfo();
            FlowHandler flowHandler= (FlowHandler) ApplicationContext.getInstance().getRegisteredObject("ContentFlowHandler");
            try {
                flowHandler.navigateTo(LoginController.class);
            } catch (VetoException e) {
                e.printStackTrace();
            } catch (FlowException e) {
                e.printStackTrace();
            }
        }).run();


    }

    @Override
    public void onError(Exception ex) {
        instance = null;
    }


}
