package com.tcoding.client.websocket;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 唐全成
 * @Date: 2022/6/13 11:45
 * @description
 **/
@Slf4j
public class SocketEventListener implements CustomEventListener<SocketDataChangeEvent>{
    /**
     * 处理器
     *
     * @param socketDataChangeEvent
     */
    @Override
    public void handler(SocketDataChangeEvent socketDataChangeEvent) {
        log.info("自定义事件处理：{}",socketDataChangeEvent.getTitle());
    }
}
