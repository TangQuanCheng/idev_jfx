package com.tcoding.client.bean;

import com.jfoenix.controls.JFXTooltip;
import com.tcoding.core.entity.TestData;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;


/**
 * @author 唐全成
 * @Date: 2022/8/12 14:41
 * @description
 **/
public class TableTooltipCellFactory<T> {
    public  Callback<TableColumn<T, String>, TableCell<T, String>> buildFactory(){
        return  new Callback<TableColumn<T, String>, TableCell<T, String>>() {
            @Override
            public TableCell<T, String> call(TableColumn param) {
                return new TableCell<T, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            Label label = new Label(item);
                            JFXTooltip.install(label,new JFXTooltip(item));
                            setGraphic(label);
                        }
                    }
                };
            }
        };
    }
}
