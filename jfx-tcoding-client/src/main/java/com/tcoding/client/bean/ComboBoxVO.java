package com.tcoding.client.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 唐全成
 * @Date: 2022/7/8 8:57
 * @description
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComboBoxVO {

    private String value;

    private String title;

    @Override
    public String toString() {
        return this.title;
    }
}
