package com.tcoding.client.privilege.user;

import com.tcoding.client.components.CFImage;
import com.tcoding.client.utils.ImageUtil;
import io.datafx.controller.ViewController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import com.tcoding.client.store.ApplicationStore;
import javafx.scene.layout.StackPane;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author 唐全成
 * @Date: 2022/7/13 10:12
 * @description
 **/
@ViewController(value = "/fxml/user/profile.fxml", title = "用户信息")
public class ProfileController {

    @FXML
    private StackPane root;

    @FXML
    private StackPane avatarPane;

    @FXML
    private Label username;

    @FXML
    private Label description;
    @PostConstruct
    public void init() throws Exception{

        String avatar = ApplicationStore.getAvatar();
        if (StringUtils.isNotBlank(avatar)) {
            Image image;
            File f = null;
            try {
                f = File.createTempFile(UUID.randomUUID().toString(), ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            ImageUtil.GenerateImage(avatar, f);
            if (f == null) {
                image = new Image(this.getClass().getResourceAsStream("/images/default-avatar.png"));
            } else {
                image = new Image(f.toURI().toString());
            }


            CFImage cfImage3 = new CFImage(image);
            cfImage3.setImageStyle(64,32);
            avatarPane.getChildren().add(cfImage3);

        }

        String name = ApplicationStore.getName();
        username.setText("用户名："+name);

    }
}
