package com.tcoding.client.privilege.user;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tcoding.client.model.OnlineUserDataModel;
import com.tcoding.client.request.Request;
import com.tcoding.client.request.feign.admin.UserFeign;
import com.tcoding.client.utils.Pinyin4jUtil;
import com.tcoding.client.utils.SvgGraphicUtil;
import com.tcoding.core.entity.log.SysLoginInfor;
import com.tcoding.core.util.DateUtils;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.core.concurrent.ProcessChain;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.kordamp.ikonli.javafx.FontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;

/**
 * @description:
 * @className: OnlineUserController
 * @author: liwen
 * @date: 2021/1/1 13:24
 */
@ViewController(value = "/fxml/user/onlineuser/online_user.fxml", title = "在线用户")
public class OnlineUserController {
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox contentPane;

    @FXML
    private TextField userNameTextField;
    @FXML
    private TextField ipTextfiled;
    @FXML
    private TableView<SysLoginInfor> tableView;

    @FXML
    private JFXSpinner spinner;
    @FXML
    @ActionTrigger("rest")
    private Button restButton;
    @FXML
    @ActionTrigger("search")
    private Button searchButton;

    @FXML
    private TableColumn<SysLoginInfor, String> numberColumn;
    @FXML
    private TableColumn<SysLoginInfor, String> sessionNumberColumn;
    @FXML
    private TableColumn<SysLoginInfor, String> loginNameColumn;
    @FXML
    private TableColumn<SysLoginInfor, String> ipaddrColumn;
    @FXML
    private TableColumn<SysLoginInfor, String> sessionStatusColumn;
    @FXML
    private TableColumn<SysLoginInfor, Date> loginTimeColumn;
    @FXML
    private TableColumn<SysLoginInfor, String> optColumn;

    @Inject
    private OnlineUserDataModel onlineUserDataModel;

    private JFXSnackbar snackbar;

    @PostConstruct
    private void init() {

        searchButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("search",12, Color.WHITE));
        restButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("refresh",12, Color.GRAY));
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.getStyleClass().add("cf-context-menu");
        MenuItem removeItem = new MenuItem("删除");
        MenuItem editItem = new MenuItem("编辑");
        editItem.setGraphic(new FontIcon("far-edit"));
        //详情
        MenuItem detailItem = new MenuItem("详情");
        MenuItem inItem = new MenuItem("导入");
        MenuItem outItem = new MenuItem("导出");
        //更多
        Menu menu = new Menu("更多...");
        menu.getItems().addAll(new MenuItem("主题1"), new MenuItem("主题2"), new MenuItem("油泼面加蒜。。。"));

        contextMenu.getItems().addAll(removeItem,
                editItem,
                detailItem,
                menu,
                new SeparatorMenuItem(),
                inItem,
                outItem);


        spinner.setVisible(false);
        snackbar = new JFXSnackbar(rootPane);
        snackbar.setPrefWidth(500);
        numberColumn.setCellFactory((col) -> {
            TableCell<SysLoginInfor, String> cell = new TableCell<SysLoginInfor, String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setText(null);
                    this.setGraphic(null);

                    if (!empty) {
                        int rowIndex = this.getIndex() + 1;
                        this.setText(String.valueOf(rowIndex));
                    }
                }
            };
            return cell;
        });
        sessionNumberColumn.setCellValueFactory(new PropertyValueFactory<>("infoId"));
        loginNameColumn.setCellValueFactory(new PropertyValueFactory<>("loginName"));
        ipaddrColumn.setCellValueFactory(new PropertyValueFactory<>("ipaddr"));
        sessionStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        sessionStatusColumn.setCellFactory(col -> {
            TableCell<SysLoginInfor, String> cell = new TableCell<SysLoginInfor, String>() {
                private Label label = new Label("");

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setText(null);
                    this.setGraphic(null);
                    if (!empty) {

                        if (item.equals("0")) {
                            label.setText("在线");
                            label.getStyleClass().add("cf-success-label-b");
                        } else {
                            label.setText("离线");
                            label.getStyleClass().add("cf-danger-label-b");
                        }
                        setGraphic(label);
                    }
                }
            };
            return cell;
         });
        loginTimeColumn.setCellValueFactory(new PropertyValueFactory<>("loginTime"));
        loginTimeColumn.setCellFactory(col -> new TableCell<SysLoginInfor, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);
                if (!empty && item != null) {
                    setText(DateUtils.formatDate(item, DateUtils.DATETIME_FORMAT));
                }
            }
        });

        Callback<TableColumn<SysLoginInfor, String>, TableCell<SysLoginInfor, String>> cellFactory = new Callback<TableColumn<SysLoginInfor, String>, TableCell<SysLoginInfor, String>>() {
            @Override
            public TableCell<SysLoginInfor, String> call(TableColumn param) {

                final TableCell<SysLoginInfor, String> cell = new TableCell<SysLoginInfor, String>() {

                    private final JFXButton retreatBut = new JFXButton("强退");

                    {
                        retreatBut.getStyleClass().add("cf-danger-but");
                        try {
                            retreatBut.setGraphic(SvgGraphicUtil.buildSvgGraphic("user-minus",12, Color.WHITE));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        retreatBut.setOnAction(event -> {

                            tableView.getSelectionModel().select(getIndex());
                            retreat();

                        });

                    }

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(retreatBut);
                        }
                    }
                };
                return cell;
            }
        };

        optColumn.setCellFactory(cellFactory);

        FilteredList<SysLoginInfor> filteredData = new FilteredList<>(onlineUserDataModel.getOnlineUsers(), p -> true);
        tableView.setItems(filteredData);
        userNameTextField.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getLoginName()).contains(val);
            });
        });
        ipTextfiled.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getIpaddr()).contains(val);
            });
        });

        onlineUserDataModel.selectedIndexProperty().bind(tableView.getSelectionModel().selectedIndexProperty());
        tableView.getStyleClass().addAll("cf-table-view", "cf-scroll-bar-style");

//        tableView.setContextMenu(contextMenu);

        query();

    }


    private void query() {

        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    onlineUserDataModel.getOnlineUsers().clear();
                    spinner.setVisible(true);
                    contentPane.setDisable(true);
                })
                .addSupplierInExecutor(
                        () -> Request.connector(UserFeign.class).getOnlineUsers()
                )
                .addConsumerInPlatformThread(result -> {

                    onlineUserDataModel.getOnlineUsers().addAll(result);

                })
                .withFinal(() -> {
                    spinner.setVisible(false);
                    contentPane.setDisable(false);
                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    @ActionMethod("search")
    private void search() {
        query();
    }

    @ActionMethod("rest")
    private void rest() {
        userNameTextField.setText("");
        ipTextfiled.setText("");
        searchButton.fire();
    }

    /**
     * @Description:强退
     * @param: []
     * @return: void
     * @auther: liwen
     * @date: 2021/1/4 4:38 下午
     */
    private void retreat() {
        ProcessChain.create()
                .addSupplierInExecutor(
                        () -> Request.connector(UserFeign.class).retreat(onlineUserDataModel.getOnlineUsers().get(onlineUserDataModel.getSelectedIndex()).getLoginName())
                )
                .addConsumerInPlatformThread(result -> {

                    if (result.isRel() == true) {
                        snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("强退成功！ ")));
                        query();
                    }

                })
                .withFinal(() -> {
                    spinner.setVisible(false);
                    contentPane.setDisable(false);
                })
                .onException(e -> {
                    e.printStackTrace();
                    snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("强退失败！ ")));

                })
                .run();
    }
}

