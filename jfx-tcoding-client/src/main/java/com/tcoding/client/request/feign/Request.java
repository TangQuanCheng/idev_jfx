package com.tcoding.client.request.feign;

import com.netflix.client.ClientFactory;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.BestAvailableRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.ZoneAwareLoadBalancer;
import com.tcoding.client.store.ApplicationStore;
import feign.Feign;
import feign.Logger;
import feign.Retryer;
import feign.codec.Encoder;
import feign.form.FormEncoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.ribbon.LBClient;
import feign.ribbon.LBClientFactory;
import feign.ribbon.RibbonClient;
import feign.slf4j.Slf4jLogger;
import okhttp3.ConnectionPool;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Request {

    private static final Map<String, FeignAPI> CONNECTORS = new ConcurrentHashMap<>();
    private static RibbonClient ribbonClient;

    private static String API_URL = "http://sample-client";
    private final static int CONNECT_TIME_OUT_MILLIS = 1000;
    private final static int READ_TIME_OUT_MILLIS = 60000;
    private static JacksonDecoder jacksonDecoder;
    private static JacksonEncoder jacksonEncoder;
    private static FormEncoder formEncoder;
    private static AppRequestInterceptor requestInterceptor;
    private static AppRequestErrorDecoder requestErrorDecoder;
    private static Slf4jLogger slf4jLogger;

    /**
     * @Description:
     * @param: [connectorClass, readTimeOut->设置超时时间]
     * @return: T
     * @auther: liwen
     * @date: 2019-06-05 14:33
     */
    public static <T extends FeignAPI> T connector(Class<T> connectorClass, int readTimeOut, Encoder encoder,Map<String,String> headers) {
        final String commandConfigKey = connectorClass.getSimpleName() + readTimeOut;

        T t = (T) CONNECTORS.computeIfAbsent(commandConfigKey, k -> Feign.builder()
                .client(ribbonClient())
                .decoder(getJacksonDecoder())
                .encoder(encoder)
                .errorDecoder(getRequestErrorDecoder())
                .requestInterceptor(getRequestInterceptor())
                .logger(getSlf4jLogger())
                .logLevel(Logger.Level.FULL)
                .retryer(Retryer.NEVER_RETRY)
                .requestInterceptor(requestTemplate -> {
                    for (String key : headers.keySet()) {
                        String value = headers.get(key);
                        requestTemplate.header(key, value);
                    }
                })
                .options(new feign.Request.Options(CONNECT_TIME_OUT_MILLIS, readTimeOut))
                .target(connectorClass, API_URL)
        );


        return t;

    }

    /**
     * 提交表单参数（不带自定义头信息）
     * @param connectorClass
     * @param <T>
     * @return
     */
    public static <T extends FeignAPI> T formConnector(Class<T> connectorClass) {
        String token = ApplicationStore.getToken();
        HashMap<String, String> headers = new HashMap<>(4);
        if(StringUtils.isNotBlank(token)){
            headers.put("Authorization",token);
        }
        return connector(connectorClass, READ_TIME_OUT_MILLIS, getFormEncoder(),headers);
    }

    /**
     * 提交表单参数（自定义头信息）
     * @param connectorClass
     * @param headers
     * @param <T>
     * @return
     */
    public static <T extends FeignAPI> T formConnector(Class<T> connectorClass,Map<String,String> headers) {
        return connector(connectorClass, READ_TIME_OUT_MILLIS, getFormEncoder(),headers==null?new HashMap<>(4):headers);
    }

    /**
     * 提交json参数（不带自定义头信息）
     * @param connectorClass
     * @param <T>
     * @return
     */
    public static <T extends FeignAPI> T jsonConnector(Class<T> connectorClass) {
        String token = ApplicationStore.getToken();
        HashMap<String, String> headers = new HashMap<>(4);
        if(StringUtils.isNotBlank(token)){
            headers.put("Authorization",token);
        }
        return connector(connectorClass, READ_TIME_OUT_MILLIS,getJacksonEncoder(),headers);
    }

    /**
     * 提交json参数（带自定义头信息）
     * @param connectorClass
     * @param headers
     * @param <T>
     * @return
     */
    public static <T extends FeignAPI> T jsonConnector(Class<T> connectorClass,Map<String,String> headers) {
        return connector(connectorClass, READ_TIME_OUT_MILLIS,getJacksonEncoder(),headers==null?new HashMap<>(4):headers);
    }
    private static JacksonDecoder getJacksonDecoder() {
        if (jacksonDecoder == null) {
            jacksonDecoder = new JacksonDecoder();
        }
        return jacksonDecoder;
    }


    private static JacksonEncoder getJacksonEncoder() {
        if (jacksonEncoder == null) {
            jacksonEncoder = new JacksonEncoder();
        }
        return jacksonEncoder;
    }

    private static FormEncoder getFormEncoder() {
        if (formEncoder == null) {
            formEncoder = new FormEncoder(getJacksonEncoder());
        }
        return formEncoder;
    }

    private static AppRequestInterceptor getRequestInterceptor() {
        if (requestInterceptor == null) {
            requestInterceptor = new AppRequestInterceptor();
        }
        return requestInterceptor;
    }

    private static AppRequestErrorDecoder getRequestErrorDecoder() {
        if (requestErrorDecoder == null) {
            requestErrorDecoder = new AppRequestErrorDecoder();
        }
        return requestErrorDecoder;
    }

    private static Slf4jLogger getSlf4jLogger() {
        if (slf4jLogger == null) {
            slf4jLogger = new Slf4jLogger();
        }
        return slf4jLogger;
    }

    private static RibbonClient ribbonClient() {

        if (ribbonClient == null) {
            ribbonClient = RibbonClient.builder().delegate(new OkHttpClient(createOkHttpClient())).lbClientFactory(new LBClientFactory() {

                @Override
                public LBClient create(String clientName) {
                    System.err.println("--------------------"+clientName+"--------------------");
                    IClientConfig config = ClientFactory.getNamedConfig(clientName);
                    ILoadBalancer lb = ClientFactory.getNamedLoadBalancer(clientName);

                    ZoneAwareLoadBalancer zb = (ZoneAwareLoadBalancer) lb;
                    zb.setRule(new BestAvailableRule());
                    LBClient lbClient = LBClient.create(lb, config);
                    return lbClient;
                }


            }).build();
        }
        return ribbonClient;
    }

    private static okhttp3.OkHttpClient createOkHttpClient() {
        return new okhttp3.OkHttpClient.Builder().connectionPool(new ConnectionPool())
                .build();
    }
}
