package com.tcoding.client.request.feign.admin;

import com.tcoding.client.request.feign.FeignAPI;
import feign.Param;
import feign.RequestLine;
import com.tcoding.core.vo.GroupTypeVO;

import java.util.List;

/**
 * @description:
 * @className: TestFeign
 * @author: liwen
 * @date: 2020/4/1 17:31
 */
public interface GroupTypeFeign extends FeignAPI {
    @RequestLine("GET /groupType/all")
    List<GroupTypeVO> getAllGroupTypes();

    @RequestLine("POST /groupType")
    Integer addGroupType(GroupTypeVO groupTypeVO);

    @RequestLine("PUT /groupType")
    Integer updateGroupType(GroupTypeVO groupTypeVO);

    @RequestLine("DELETE /groupType/{groupTypeId}")
    Integer deleteGroupTypes(@Param("groupTypeId") int groupTypeId);

}
