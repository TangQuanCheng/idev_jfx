package com.tcoding.client.request.feign.admin;

import com.tcoding.client.request.feign.FeignAPI;
import com.tcoding.core.entity.TestData;
import com.tcoding.core.entity.User;
import com.tcoding.core.entity.log.SysLoginInfor;
import com.tcoding.core.msg.ObjectRestResponse;
import com.tcoding.core.msg.TableResultResponse;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @className: TestFeign
 * @author: liwen
 * @date: 2020/4/1 17:31
 */
public interface TestDataFeign extends FeignAPI {

    @RequestLine("GET /testData/queryAll")
    List<TestData> getAllData();

    @RequestLine("POST /testData/queryPage")
    TableResultResponse<TestData> getPageData(Map<String,Object> params);
}
