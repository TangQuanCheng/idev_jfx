package com.tcoding.client.request.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import com.tcoding.client.store.ApplicationStore;

public class AppRequestInterceptor implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate template) {

        template.header("Authorization", ApplicationStore.getToken());
    }
}
