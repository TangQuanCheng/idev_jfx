package com.tcoding.client.test.controlsfx;

import io.datafx.controller.ViewController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;
import org.controlsfx.control.cell.ColorGridCell;
import org.controlsfx.control.cell.ImageGridCell;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author 唐全成
 * @Date: 2022/9/9 16:49
 * @description
 **/

@ViewController(value = "/fxml/controlsfx/colorGrid.fxml", title = "大数据量测试")
public class ColorGridController {

    @FXML
    private StackPane rootPane;

    private static final String[] FILE_LIST = {
           "anquan.png",
           "baocun.png",
           "biaoqian.png",
           "butongguo.png",
           "danwei.png",
           "gerenziliao.png",
           "liwu.png",
           "mima.png",
           "peizhi.png",
           "pingshen.png",
           "pingtai.png",
           "renwushu.png",
           "renwushubiangeng.png",
           "renzheng.png",
           "rili.png",
           "shenqingshu.png",
           "shipin.png",
           "shouji.png",
           "shouye.png",
           "tijiao.png",
           "tongguo.png",
           "tongzhi.png",
           "touxiang.png",
           "tuihui.png",
           "tuijian.png",
           "tupian.png",
           "wenjianhe.png",
           "wenjianjia.png",
           "xiangmulixiang.png",
           "xiangmushenbao.png",
           "xiaoxi.png",
           "xingshen.png",
           "xunzhang.png",
           "yanshou.png",
           "yasuowenjian.png",
           "yingyong.png",
           "yinhangka.png",
           "youjian.png",
           "zhihangshu.png",
           "zhinan.png",
           "zhongzhishu.png",
           "zhuanjia.png",
           "zhuanti.png"
    };

    @PostConstruct
    public void init(){
        rootPane.getChildren().add(getImageGrid(true));
    }

    private GridView<?> getColorGrid() {
        final ObservableList<Color> list = FXCollections.<Color>observableArrayList();

        GridView<Color> colorGrid = new GridView<>(list);

        colorGrid.setCellFactory(new Callback<GridView<Color>, GridCell<Color>>() {
            @Override public GridCell<Color> call(GridView<Color> arg0) {
                return new ColorGridCell();
            }
        });
        Random r = new Random(System.currentTimeMillis());
        for(int i = 0; i < 500; i++) {
            list.add(new Color(r.nextDouble(), r.nextDouble(), r.nextDouble(), 1.0));
        }
        return colorGrid;
    }

    private GridView<?> getImageGrid( final boolean preserveImageProperties ) {

//        final Image image = new Image("/images/bg4.jpg", 200, 0, true, true);
        final ObservableList<ImageView> list = FXCollections.observableArrayList();

        GridView<ImageView> colorGrid = new GridView<>(list);
        colorGrid.setHorizontalCellSpacing(30);
        colorGrid.setVerticalCellSpacing(30);
        colorGrid.setCellWidth(64);
        colorGrid.setCellHeight(64);
        colorGrid.setCellFactory(arg0 -> new CustomerImageGridCell(preserveImageProperties,e->{
            Map<String, Object> paramMap = e.getParamMap();
            Object data = paramMap.get("data");
            System.out.println("执行"+data.toString());
        }));
        for(int i = 0; i < FILE_LIST.length; i++) {
            Image icon = new Image("/images/icons/"+FILE_LIST[i], 200, 0, true, true);
            ImageView view = new ImageView(icon);
            view.setSmooth(true);
            view.setUserData("Data"+i);
            list.add(view);
        }
        return colorGrid;
    }
}
