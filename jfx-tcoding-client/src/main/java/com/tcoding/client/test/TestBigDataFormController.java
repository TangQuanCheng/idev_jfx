package com.tcoding.client.test;

import com.jfoenix.controls.*;
import com.tcoding.client.gui.feature.FeatureResourceConsumer;
import com.tcoding.client.model.TestBigDataModel;
import com.tcoding.client.test.model.TestDataVO;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import jfxtras.scene.control.LocalDateTimeTextField;
import tornadofx.control.DateTimePicker;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author 唐全成
 * @Date: 2022/8/11 10:06
 * @description
 **/
@ViewController(value = "/fxml/test/big_data_form.fxml", title = "大数据量表单测试")
public class TestBigDataFormController {

    @Inject
    private TestBigDataModel testBigDataModel;
    @ActionHandler
    private FlowActionHandler actionHandler;
    @FXMLViewFlowContext
    private ViewFlowContext flowContext;
    @FXML
    private JFXTextField col1Field;
    @FXML
    private JFXTextField col2Field;
    @FXML
    private JFXTextField col3Field;
    @FXML
    private JFXTextField col4Field;
    @FXML
    private JFXTextField col5Field;
    @FXML
    private JFXTextField col6Field;
    @FXML
    private JFXDatePicker col7Field;
    @FXML
    private JFXColorPicker col8Field;
    @FXML
    private JFXTextField col9Field;
    @FXML
    private JFXTimePicker col10Field;
    @FXML
    private JFXTextField col11Field;
    @FXML
    private JFXTextField col12Field;
    @FXML
    private LocalDateTimeTextField col13Field;
    @FXML
    private DateTimePicker col14Field;
    @FXML
    private JFXTextField col15Field;
    @FXML
    private JFXTextField col16Field;
    @FXML
    private JFXTextField col17Field;
    @FXML
    private JFXTextField col18Field;
    @FXML
    private JFXTextField col19Field;
    @FXML
    private JFXTextField col20Field;
    @FXML
    private JFXTextField col21Field;
    @FXML
    private JFXTextField col22Field;
    @FXML
    private JFXTextField col23Field;
    @FXML
    private JFXTextField col24Field;
    @FXML
    private JFXTextField col25Field;
    @FXML
    private JFXTextField col26Field;
    @FXML
    private JFXTextField col27Field;
    @FXML
    private TextField col28Field;
    @FXML
    private TextField col29Field;
    @FXML
    private TextField col30Field;

    @FXML
    private JFXButton confirmSubmit;

    @FXML
    private JFXButton cancelSubmit;

    private TestDataVO dataVO;
    @Inject
    private FeatureResourceConsumer featureResourceConsumer;
    @PostConstruct
    private void init(){

        SimpleObjectProperty<LocalDate> localDateSimpleObjectProperty = new SimpleObjectProperty<>();
        SimpleObjectProperty<Color> col8ColorObjectProperty = new SimpleObjectProperty<>();
        SimpleObjectProperty<LocalTime> col10LocalTimeObjectProperty = new SimpleObjectProperty<>();

        dataVO = flowContext.getRegisteredObject(TestDataVO.class);
        col1Field.textProperty().bindBidirectional(dataVO.col1Property());
        col2Field.textProperty().bindBidirectional(dataVO.col2Property());
        col3Field.textProperty().bindBidirectional( new SimpleStringProperty(String.valueOf(dataVO.col3Property().get())));
        col4Field.textProperty().bindBidirectional(dataVO.col4Property());
        col5Field.textProperty().bindBidirectional(dataVO.col5Property());
        col6Field.textProperty().bindBidirectional(dataVO.col6Property());
        col7Field.valueProperty().bindBidirectional(localDateSimpleObjectProperty);
        col8Field.valueProperty().bindBidirectional(col8ColorObjectProperty);
        col9Field.textProperty().bindBidirectional(dataVO.col9Property());
        col10Field.valueProperty().bindBidirectional(col10LocalTimeObjectProperty);
        col11Field.textProperty().bindBidirectional(dataVO.col11Property());
        col12Field.textProperty().bindBidirectional(dataVO.col12Property());
        col13Field.textProperty().bindBidirectional(dataVO.col13Property());
        col15Field.textProperty().bindBidirectional(dataVO.col15Property());
        col16Field.textProperty().bindBidirectional(dataVO.col16Property());
        col17Field.textProperty().bindBidirectional(dataVO.col17Property());
        col18Field.textProperty().bindBidirectional(dataVO.col18Property());
        col19Field.textProperty().bindBidirectional(dataVO.col19Property());
        col20Field.textProperty().bindBidirectional(dataVO.col20Property());
        col21Field.textProperty().bindBidirectional(dataVO.col21Property());
        col22Field.textProperty().bindBidirectional(dataVO.col22Property());
        col23Field.textProperty().bindBidirectional(dataVO.col23Property());
        col24Field.textProperty().bindBidirectional(dataVO.col24Property());
        col25Field.textProperty().bindBidirectional(dataVO.col25Property());
        col26Field.textProperty().bindBidirectional(dataVO.col26Property());
        col27Field.textProperty().bindBidirectional(dataVO.col27Property());
        col28Field.textProperty().bindBidirectional(dataVO.col28Property());
        col29Field.textProperty().bindBidirectional(dataVO.col29Property());
        col30Field.textProperty().bindBidirectional(dataVO.col30Property());

        col1Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col1Field.validate();
            }
        });
        col2Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col2Field.validate();
            }
        });
        col3Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col3Field.validate();
            }
        });
        col4Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col4Field.validate();
            }
        });
        col5Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col5Field.validate();
            }
        });
        col6Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col6Field.validate();
            }
        });
        col7Field.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                col7Field.validate();
            }
        });

        confirmSubmit.setOnAction(e->{
            try {
                boolean validate = col1Field.validate();
                boolean validate1 = col2Field.validate();
                boolean validate2 = col3Field.validate();
                boolean validate3 = col4Field.validate();
                boolean validate4 = col5Field.validate();
                boolean validate5 = col6Field.validate();
                boolean validate6 = col7Field.validate();

                if(validate&&validate1&&validate2&&validate3&&validate4&&validate5&&validate6){
                    dataVO.setCol7(localDateSimpleObjectProperty.get().toString());
                    dataVO.setCol8(col8ColorObjectProperty.get().toString());
                    dataVO.setCol10(col10LocalTimeObjectProperty.get().toString());

                    actionHandler.handle("submit");
                }else{
                    System.out.println("表单非法.....");

                }
            } catch (VetoException | FlowException vetoException) {
                vetoException.printStackTrace();
            }
        });
        cancelSubmit.setOnAction(e->{
            try {
                actionHandler.handle("cancel");
            } catch (VetoException | FlowException vetoException) {
                vetoException.printStackTrace();
            }
        });
    }
}
