package com.tcoding.client.test;


import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.DoubleTextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.IntegerTextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import com.tcoding.client.components.controller.search.SearchItem;
import com.tcoding.client.components.customer.CustomerExport;
import com.tcoding.client.components.customer.CustomerPrint;
import com.tcoding.client.components.customer.CustomerSearch;
import com.tcoding.client.components.customer.CustomerTableContextMenu;
import com.tcoding.client.components.extend.ComboGridConfig;
import com.tcoding.client.components.extend.ComboGridTableColumn;
import com.tcoding.client.components.extend.JFXComboGrid;
import com.tcoding.client.components.extend.SnackbarExtend;
import com.tcoding.client.editor.*;
import com.tcoding.client.extend.jfoenix.EnduringEditableTreeTableCell;
import com.tcoding.client.extend.jfoenix.GenericEditableTreeTableComboCell;
import com.tcoding.client.extend.jfoenix.GenericEditableTreeTableComboGridCell;
import com.tcoding.client.extend.jfoenix.MyTextFieldEditorBuilder;
import com.tcoding.client.gui.feature.FeatureResourceConsumer;
import com.tcoding.client.model.CustomerSearchModel;
import com.tcoding.client.model.TestBigDataModel;
import com.tcoding.client.model.TestBigDataVOModel;
import com.tcoding.client.request.Request;
import com.tcoding.client.request.feign.admin.TestDataFeign;
import com.tcoding.client.test.combo.ConstantComboModels;
import com.tcoding.client.test.model.TestDataVO;
import com.tcoding.client.utils.Pinyin4jUtil;
import com.tcoding.client.utils.SvgGraphicUtil;
import com.tcoding.core.entity.TestData;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.table.TableFilter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;

/**
 * @description:
 * @className: OnlineUserController
 * @author: liwen
 * @date: 2021/1/1 13:24
 */
@ViewController(value = "/fxml/test/big_data_jfoenix.fxml", title = "大数据量测试")
public class TestBigDataJfoenixController {
    @FXMLViewFlowContext
    private ViewFlowContext flowContext;
    @ActionHandler
    private FlowActionHandler actionHandler;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox contentPane;
    @FXML
    private TextField userNameTextField;
    @FXML
    private TextField ipTextfiled;
    @FXML
    private JFXComboGrid testComboGrid;
    @FXML
    private JFXTreeTableView<TestDataVO> tableView;
    @FXML
    private JFXTreeTableColumn<TestDataVO,Label> numberColumn;
    @FXML
    private JFXTreeTableColumn<TestDataVO,String> col1Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col2Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, Number> col3Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, CustomerBoxEditorModel> col4Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, LocalDate> col5Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, LocalTime> col6Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col7Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col8Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col9Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, CustomerBoxEditorModel> col10Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col11Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col12Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col13Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col14Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col15Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col16Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col17Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col18Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col19Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col20Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col21Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col22Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col23Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col24Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col25Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col26Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col27Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col28Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col29Column;
    @FXML
    private JFXTreeTableColumn<TestDataVO, String> col30Column;
    private TableFilter<TestDataVO> tableFilter;
    private Integer page = 1;
    private Integer total=0;
    private Map<String,Object> queryParams = new HashMap<>();
    @FXML
    private JFXSpinner spinner;
    @FXML
    @ActionTrigger("rest")
    private Button restButton;
    @FXML
    @ActionTrigger("search")
    private Button searchButton;
    @FXML
    private Button addButton;
    @FXML
    private Button updateButton;
    @FXML
    private HBox gridBar;

    @FXML
    private Label dataCount;

    @FXML
    private Label loadingDuration;

    @Inject
    private TestBigDataVOModel testBigDataModel;

    @Inject
    private CustomerSearchModel customerSearchModel;

    private SnackbarExtend snackbar;

    /**
     * 高级查询
     */
    private CustomerSearch<TestData> customerSearch;
    /**
     * 表格导出
     */
    private CustomerExport<TestData> customerExport;
    /**
     * 表格打印
     */
    private CustomerPrint<TestData> customerPrint;

    /**
     * 右键菜单
     */
    private CustomerTableContextMenu<TestData> contextMenu;

    @Inject
    private FeatureResourceConsumer featureResourceConsumer;

    private  ObservableList<CustomerBoxEditorModel> lifeList;

    private List<Map<String,Object>> col10ComboGridList;

    @PostConstruct
    private void init() {
        LinkedList<ComboGridTableColumn> columns = new LinkedList<ComboGridTableColumn>(){{
            add(new ComboGridTableColumn("船名","shipName",200.0));
            add(new ComboGridTableColumn("船号","shipCode",220.0));
            add(new ComboGridTableColumn("船型","shipType",220.0));
            add(new ComboGridTableColumn("船代","shipRant",220.0));
            add(new ComboGridTableColumn("航次","voyage",220.0));
        }};
        ComboGridConfig comboGridConfig = new ComboGridConfig();
        comboGridConfig.setPlaceholder("选船");
        comboGridConfig.setColumns(columns);
        comboGridConfig.setTableTextColumn("shipName");
        comboGridConfig.setTableValueColumn("shipCode");
        comboGridConfig.setWidth(col10Column.getWidth()-5);
        col10ComboGridList = new ArrayList<Map<String,Object>>(){{
            add(new HashMap<String,Object>(4){{
                put("shipName","公主");
                put("shipCode","princess");
                put("shipType","台风");
                put("shipRant","张三");
                put("voyage","2022090301");
            }});
            add(new HashMap<String,Object>(4){{
                put("shipName","王子");
                put("shipCode","prince");
                put("shipType","台风");
                put("shipRant","李四");
                put("voyage","2022090304");
            }});
        }};
        comboGridConfig.setItems(col10ComboGridList);

        testComboGrid.setConfig(comboGridConfig);

        ObservableList<CustomerBoxEditorModel> innerList = FXCollections.observableArrayList();
        lifeList = new SimpleListProperty<>(innerList);
        lifeList.addAll(ConstantComboModels.LIFE_TYPES);

        dataCount.setText("加载数据量：0");
        loadingDuration.setText("请求耗时：0ms");

        searchButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("search", 12, Color.WHITE));
        restButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("refresh", 12, Color.GRAY));
        spinner.setVisible(false);

        FilteredList<TestDataVO> filteredData = new FilteredList<>(testBigDataModel.getTestDatas(), p -> true);
        tableView.setRoot(new RecursiveTreeItem<>(filteredData, RecursiveTreeObject::getChildren));
        tableView.setShowRoot(false);
        tableView.setEditable(true);

        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        userNameTextField.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol1()).contains(val);
            });
        });
        ipTextfiled.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol2()).contains(val);
            });
        });

        numberColumn.setCellFactory((col) -> new TreeTableCell<TestDataVO, Label>() {
            @Override
            public void updateItem(Label item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);

                if (!empty) {
                    int rowIndex = this.getIndex() + 1;
                    Label label = new Label("" + rowIndex);
                    this.setGraphic(label);
                }
            }
        });
        setupCellValueFactory(col1Column,TestDataVO::col1Property );
        setupCellValueFactory(col2Column,TestDataVO::col2Property );
        setupCellValueFactory(col3Column,TestDataVO::col3Property );
        setupCellValueFactory(col4Column,TestDataVO::col4ComboboxModelProperty);
        setupCellValueFactory(col5Column,TestDataVO::col5LocalDateProperty );
        setupCellValueFactory(col6Column,TestDataVO::col6LocalTimeProperty );
        setupCellValueFactory(col7Column,TestDataVO::col7Property );
        setupCellValueFactory(col8Column, TestDataVO::col8Property);
        setupCellValueFactory(col9Column,TestDataVO::col9Property );
        setupCellValueFactory(col10Column,testDataVO1-> testDataVO1.col10ComboboxModelProperty(col10ComboGridList, comboGridConfig.getTableValueColumn(), comboGridConfig.getTableTextColumn()));
        setupCellValueFactory(col11Column,TestDataVO::col11Property );
        setupCellValueFactory(col12Column,TestDataVO::col12Property );
        setupCellValueFactory(col13Column,TestDataVO::col13Property );
        setupCellValueFactory(col14Column,TestDataVO::col14Property );
        setupCellValueFactory(col15Column,TestDataVO::col15Property );
        setupCellValueFactory(col16Column,TestDataVO::col16Property );
        setupCellValueFactory(col17Column,TestDataVO::col17Property );
        setupCellValueFactory(col18Column,TestDataVO::col18Property );
        setupCellValueFactory(col19Column,TestDataVO::col19Property );
        setupCellValueFactory(col20Column,TestDataVO::col20Property );
        setupCellValueFactory(col21Column,TestDataVO::col21Property );
        setupCellValueFactory(col22Column,TestDataVO::col22Property );
        setupCellValueFactory(col23Column,TestDataVO::col23Property );
        setupCellValueFactory(col24Column,TestDataVO::col24Property );
        setupCellValueFactory(col25Column,TestDataVO::col25Property );
        setupCellValueFactory(col26Column,TestDataVO::col26Property );
        setupCellValueFactory(col27Column,TestDataVO::col27Property );
        setupCellValueFactory(col28Column,TestDataVO::col28Property );
        setupCellValueFactory(col29Column,TestDataVO::col29Property );
        setupCellValueFactory(col30Column,TestDataVO::col30Property );

        MyTextFieldEditorBuilder textFieldEditorBuilder = new MyTextFieldEditorBuilder(new RequiredFieldValidator("此处必填"));

        col1Column.setCellFactory((TreeTableColumn<TestDataVO, String> param) -> new GenericEditableTreeTableCell<>(textFieldEditorBuilder));
        col1Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, String> t) -> {
            t.getTreeTableView()
                    .getTreeItem(t.getTreeTablePosition()
                            .getRow())
                    .getValue().setCol1(t.getNewValue());
                }
        );

        col2Column.setCellFactory((TreeTableColumn<TestDataVO, String> param) -> new GenericEditableTreeTableCell<>(
                new DoubleTextFieldEditorBuilder()));
        col2Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, String> t) -> t.getTreeTableView()
                .getTreeItem(t.getTreeTablePosition()
                        .getRow())
                .getValue().setCol2(t.getNewValue()));

        //整数输入
        col3Column.setCellFactory((TreeTableColumn<TestDataVO, Number> param) -> new GenericEditableTreeTableCell<>(
                new IntegerTextFieldEditorBuilder(new RequiredFieldValidator("仅支持输入整数数字"))));
        col3Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, Number> t) -> t.getTreeTableView()
                .getTreeItem(t.getTreeTablePosition()
                        .getRow())
                .getValue().setCol3(t.getNewValue().intValue()));
        //行内下拉选择示例
        col4Column.setCellFactory(
                (TreeTableColumn<TestDataVO, CustomerBoxEditorModel> param) -> new GenericEditableTreeTableComboCell<TestDataVO, CustomerBoxEditorModel>(
                        new ComboBoxEditorBuilder<>(lifeList),ConstantComboModels.LIFE_TYPES
                )
        );
        col4Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, CustomerBoxEditorModel> t) -> {
                    t.getTreeTableView()
                            .getTreeItem(t.getTreeTablePosition()
                                    .getRow())
                            .getValue().setCol4(t.getNewValue().getValue().toString());
                }
        );
        //行内日期选择示例
        col5Column.setCellFactory(
                (TreeTableColumn<TestDataVO, LocalDate> param) -> new GenericEditableTreeTableCell<TestDataVO, LocalDate>(
                        new DatePickerEditorBuilder<>()
                )
        );
        col5Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, LocalDate> t) -> {
                    t.getTreeTableView()
                            .getTreeItem(t.getTreeTablePosition()
                                    .getRow())
                            .getValue().setLocalDateCol5(t.getNewValue());
                }
        );
        //行内时间选择示例
        col6Column.setCellFactory(
                (TreeTableColumn<TestDataVO, LocalTime> param) -> new GenericEditableTreeTableCell<TestDataVO, LocalTime>(
                        new TimePickerEditorBuilder<>()
                )
        );
        col6Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, LocalTime> t) -> {
                    t.getTreeTableView()
                            .getTreeItem(t.getTreeTablePosition()
                                    .getRow())
                            .getValue().setLocalDateCol6(t.getNewValue());
                }
        );

        col7Column.setCellFactory(
                (TreeTableColumn<TestDataVO, String> param) -> new EnduringEditableTreeTableCell<TestDataVO, String>(
                        new CheckBoxEditorBuilder<>(e->{
                            Map<String, Object> paramMap = e.getParamMap();
                            Boolean data = (Boolean)paramMap.get("data");
                            Integer row =(Integer) paramMap.get("row");
                            tableView.getTreeItem(row).getValue().setCol7(data?"1":"0");
                        })
                )
        );

        col8Column.setCellFactory(
                (TreeTableColumn<TestDataVO, String> param) -> new EnduringEditableTreeTableCell<TestDataVO, String>(
                        new SwitchEditorBuilder<>(e->{
                            Map<String, Object> paramMap = e.getParamMap();
                            Boolean data = (Boolean)paramMap.get("data");
                            Integer row =(Integer) paramMap.get("row");
                            tableView.getTreeItem(row).getValue().setCol8(data?"1":"0");
                        })
                )
        );

        col9Column.setCellFactory(
                (TreeTableColumn<TestDataVO, String> param) -> new EnduringEditableTreeTableCell<TestDataVO, String>(
                        new ColorPickerEditorBuilder<>(e->{
                            Map<String, Object> paramMap = e.getParamMap();
                            String data = (String)paramMap.get("data");
                            Integer row =(Integer) paramMap.get("row");
                            tableView.getTreeItem(row).getValue().setCol9(data);
                        })
                )
        );




        col10Column.setCellFactory(
                (TreeTableColumn<TestDataVO, CustomerBoxEditorModel> param) -> new GenericEditableTreeTableComboCell<TestDataVO, CustomerBoxEditorModel>(
                        new ComboGridEditorBuilder<CustomerBoxEditorModel>(comboGridConfig),col10ComboGridList,comboGridConfig
                )
        );
        col10Column.setOnEditCommit((TreeTableColumn.CellEditEvent<TestDataVO, CustomerBoxEditorModel> t) -> {
                if(t.getNewValue()!=null&&StringUtils.isNotBlank(t.getNewValue().getValue().toString())){
                    t.getTreeTableView()
                            .getTreeItem(t.getTreeTablePosition()
                                    .getRow())
                            .getValue().setCol10(t.getNewValue().getValue().toString());
                }

        });

        tableView.getStyleClass().add("cf-scroll-bar-style");

        testBigDataModel.selectedIndexProperty().bind(tableView.getSelectionModel().selectedIndexProperty());
        query();

        addButton.setOnAction(e-> openEditDialog(true));


        updateButton.setOnAction(e->{
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(testBigDataModel.getSelectedIndex());
            System.out.println(testDataVO.toString());
        });
    }

    private void query() {
        long start = System.currentTimeMillis();
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {

                    String value = testComboGrid.getValue();
                    System.out.println("宣传combogrid值："+value);

                    testBigDataModel.getTestDatas().clear();
                    spinner.setVisible(true);
                    contentPane.setDisable(true);

                    queryParams.put("page",page);
                    queryParams.put("limit",20);

                })
                .addSupplierInExecutor(
                        () -> Request.connector(TestDataFeign.class).getPageData(queryParams)
                )
                .addConsumerInPlatformThread(result -> {
                    List<TestData> datas = result.getDatas();
                    for (TestData testData : datas) {
                        TestDataVO testDataVO = new TestDataVO(testData);
                        testBigDataModel.getTestDatas().add(testDataVO);
                    }
                    dataCount.setText("加载数据量：" + datas.size());
                    long duration = System.currentTimeMillis() - start;
                    loadingDuration.setText("请求耗时：" + duration + "ms");

                })
                .withFinal(() -> {
                    spinner.setVisible(false);
                    contentPane.setDisable(false);
                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    @ActionMethod("search")
    private void search() {
        query();
    }

    @ActionMethod("rest")
    private void rest() throws FlowException, VetoException {
        userNameTextField.setText("");
        ipTextfiled.setText("");

        Notifications notificationBuilder = Notifications.create()
                .title( "通知")
                .text("这里是测试消息")
                .graphic(SvgGraphicUtil.buildSvgGraphic("file-empty", 12, Color.GRAY))
                .hideAfter(Duration.seconds(3))
                .position(Pos.BOTTOM_RIGHT)
                .onAction(e -> System.out.println("Notification clicked on!"))
                .threshold(4,
                        Notifications.create().title("Threshold Notification"));
        notificationBuilder.show();
    }
    private <T> void setupCellValueFactory(JFXTreeTableColumn<TestDataVO, T> column, Function<TestDataVO, ObservableValue<T>> mapper) {
        column.setCellValueFactory((TreeTableColumn.CellDataFeatures<TestDataVO, T> param) -> {
            if (column.validateValue(param)) {
                return mapper.apply(param.getValue().getValue());
            } else {
                return column.getComputedValue(param);
            }
        });
    }
    /**
     * 打开修改窗口
     * @param isAdd 是否是新增
     */
    private void openEditDialog(boolean isAdd){

        Flow flow = new Flow(TestBigDataFormController.class);
        JFXDialog dialog =new JFXDialog();
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label("编辑表单"));
        TestDataVO dataVO =isAdd?new TestDataVO():testBigDataModel.getTestDatas().get(testBigDataModel.getSelectedIndex());
        FlowHandler handler1 = flow.createHandler(flowContext);
        flow.withGlobalAction("submit", (flowHandler, actionId) -> {
            TestData newData = dataVO.convertToData();
            saveData(newData);
            dialog.close();
        });
        flow.withGlobalAction("cancel", (flowHandler, actionId) -> {
            dialog.close();
        });
        try {
            handler1.registerInFlowContext("data",dataVO);
            layout.setBody(handler1.start());
            featureResourceConsumer.consumeResource(handler1.getCurrentViewContext().getController());
        } catch (FlowException flowException) {
            flowException.printStackTrace();
        }
        layout.setPrefWidth(800);
        dialog.setContent(layout);
        dialog.setOverlayClose(false);
        dialog.setDialogContainer(rootPane);
        dialog.show();
    }

    /**
     * 保存数据
     * @param testData
     */
    private void saveData(TestData testData){
        System.out.println(testData);
    }

}

