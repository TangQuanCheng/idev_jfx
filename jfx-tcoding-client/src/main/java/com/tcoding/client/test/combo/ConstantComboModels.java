package com.tcoding.client.test.combo;

import com.tcoding.client.editor.CustomerBoxEditorModel;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/9/2 9:41
 * @description
 **/
public class ConstantComboModels {

    public static final List<CustomerBoxEditorModel> LIFE_TYPES = new ArrayList<CustomerBoxEditorModel>(){{
        add(new CustomerBoxEditorModel("人类","human","cf-primary-label"));
        add(new CustomerBoxEditorModel( "动物","animal","cf-success-label"));
        add(new CustomerBoxEditorModel("植物","plant","cf-info-label"));
        add(new CustomerBoxEditorModel("细菌","bacteria","cf-warn-label"));
        add(new CustomerBoxEditorModel( "病毒","virus","cf-danger-label"));
        add(new CustomerBoxEditorModel( "真菌","fungus","cf-danger-label"));
    }};
    public static CustomerBoxEditorModel getComboModelByValue(String value,List<CustomerBoxEditorModel> list){
        CustomerBoxEditorModel customerBoxEditorModel=null;
        for (CustomerBoxEditorModel item : list) {
            if(StringUtils.equals(item.getValue().toString(),value)||StringUtils.equals(item.getLabel(),value)){
                customerBoxEditorModel = item;
                break;
            }
        }
        return customerBoxEditorModel==null?new CustomerBoxEditorModel("未知",value):customerBoxEditorModel;
    }

    public static CustomerBoxEditorModel getComboGridModelByValue(String value, List<Map<String,Object>> list, String prop,String text){
        CustomerBoxEditorModel customerBoxEditorModel=null;
        for (Map<String,Object> item : list) {
            if(StringUtils.equalsAny(value,item.get(prop).toString(),item.get(text).toString())){
                customerBoxEditorModel = new CustomerBoxEditorModel(item.get(text).toString(),item.get(prop));
                break;
            }
        }
        return customerBoxEditorModel==null?new CustomerBoxEditorModel("未知",value):customerBoxEditorModel;
    }


}
