package com.tcoding.client.test;

import com.jfoenix.controls.JFXRippler;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXToggleButton;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.tcoding.client.components.extend.ComboGridConfig;
import com.tcoding.client.components.extend.ComboGridTableColumn;
import com.tcoding.client.components.extend.JFXComboGrid;
import com.tcoding.client.editor.CustomerBoxEditorModel;
import com.tcoding.client.extend.controlsfx.ComboGridFieldTableCell;
import com.tcoding.client.extend.controlsfx.DatePickerTableCell;
import com.tcoding.client.extend.controlsfx.DateTimePickerTableCell;
import com.tcoding.client.extend.controlsfx.SwitchTableCell;
import com.tcoding.client.model.TestBigDataModel;
import com.tcoding.client.model.TestBigDataVOModel;
import com.tcoding.client.request.Request;
import com.tcoding.client.request.feign.admin.TestDataFeign;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.test.model.TestDataVO;
import com.tcoding.core.entity.TestData;
import io.datafx.controller.ViewController;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.tableview2.FilteredTableColumn;
import org.controlsfx.control.tableview2.FilteredTableView;
import org.controlsfx.control.tableview2.actions.ColumnFixAction;
import org.controlsfx.control.tableview2.actions.RowFixAction;
import org.controlsfx.control.tableview2.cell.ComboBox2TableCell;
import org.controlsfx.control.tableview2.cell.TextField2TableCell;
import org.controlsfx.control.tableview2.filter.filtereditor.SouthFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupNumberFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupStringFilter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;

/**
 * @author 唐全成
 * @Date: 2022/9/13 14:04
 * @description
 **/
@ViewController(value = "/fxml/test/big_data_controlsfx.fxml", title = "大数据量表单测试")
public class TestBigDataControlsfxController {

    @FXML
    private StackPane rootPane;
    @FXML
    private JFXSpinner spinner;
    @FXML
    private JFXComboGrid testComboGrid;
    @FXML
    private Pagination pagination;
    @FXML
    private JFXRippler confirmSelectedRow;
    @FXML
    private FilteredTableView<TestDataVO> filteredTableView;
    @FXML
    private FilteredTableColumn<TestDataVO,String> col1Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col2Column;
    @FXML
    private FilteredTableColumn<TestDataVO, Boolean> col3Column;
    @FXML
    private FilteredTableColumn<TestDataVO, CustomerBoxEditorModel> col4Column;
    @FXML
    private FilteredTableColumn<TestDataVO, LocalDate> col5Column;
    @FXML
    private FilteredTableColumn<TestDataVO, LocalTime> col6Column;
    @FXML
    private FilteredTableColumn<TestDataVO, Boolean> col7Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col8Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col9Column;
    @FXML
    private FilteredTableColumn<TestDataVO, CustomerBoxEditorModel> col10Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col11Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col12Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col13Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col14Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col15Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col16Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col17Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col18Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col19Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col20Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col21Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col22Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col23Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col24Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col25Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col26Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col27Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col28Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col29Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col30Column;

    private SouthFilter<TestDataVO, String> southFilterCol1;
    private SouthFilter<TestDataVO, String> southFilterCol2;
    private SouthFilter<TestDataVO, String> southFilterCol3;
    private SouthFilter<TestDataVO, String> southFilterCol4;
    private SouthFilter<TestDataVO, String> southFilterCol5;
    private SouthFilter<TestDataVO, String> southFilterCol6;
    private SouthFilter<TestDataVO, String> southFilterCol7;
    private SouthFilter<TestDataVO, String> southFilterCol8;
    private SouthFilter<TestDataVO, String> southFilterCol10;
    private SouthFilter<TestDataVO, String> southFilterCol11;
    private SouthFilter<TestDataVO, String> southFilterCol12;
    private SouthFilter<TestDataVO, String> southFilterCol13;
    private SouthFilter<TestDataVO, String> southFilterCol14;
    private SouthFilter<TestDataVO, String> southFilterCol15;
    private SouthFilter<TestDataVO, String> southFilterCol16;
    private SouthFilter<TestDataVO, String> southFilterCol17;
    private SouthFilter<TestDataVO, String> southFilterCol18;
    private SouthFilter<TestDataVO, String> southFilterCol19;
    private SouthFilter<TestDataVO, String> southFilterCol20;
    private SouthFilter<TestDataVO, String> southFilterCol21;
    private SouthFilter<TestDataVO, String> southFilterCol22;
    private SouthFilter<TestDataVO, String> southFilterCol23;
    private SouthFilter<TestDataVO, String> southFilterCol24;
    private SouthFilter<TestDataVO, String> southFilterCol25;
    private SouthFilter<TestDataVO, String> southFilterCol26;
    private SouthFilter<TestDataVO, String> southFilterCol27;
    private SouthFilter<TestDataVO, String> southFilterCol28;
    private SouthFilter<TestDataVO, String> southFilterCol29;
    private SouthFilter<TestDataVO, String> southFilterCol30;
    @FXML
    private AnchorPane anchorPane;
    private Integer page = 1;
    private Integer total=0;
    private Map<String,Object> queryParams = new HashMap<>();
    private SouthFilter<TestDataVO, String> editorColor;
    @Inject
    private TestBigDataVOModel testBigDataModel;
    @FXML
    private JFXToggleButton customSearch;
    @FXML
    private JFXToggleButton showRowIndex;
    @FXML
    private JFXRippler search;
    @PostConstruct
    public void init(){

        LinkedList<ComboGridTableColumn> columns = new LinkedList<ComboGridTableColumn>(){{
            add(new ComboGridTableColumn("船名","shipName",200.0));
            add(new ComboGridTableColumn("船号","shipCode",220.0));
            add(new ComboGridTableColumn("船型","shipType",220.0));
            add(new ComboGridTableColumn("船代","shipRant",220.0));
            add(new ComboGridTableColumn("航次","voyage",220.0));
        }};
        ComboGridConfig comboGridConfig = new ComboGridConfig();
        comboGridConfig.setPlaceholder("选船");
        comboGridConfig.setColumns(columns);
        comboGridConfig.setTableTextColumn("shipName");
        comboGridConfig.setTableValueColumn("shipCode");
        comboGridConfig.setWidth(col10Column.getWidth()-5);
        List<Map<String,Object>> col10ComboGridList = new ArrayList<Map<String,Object>>(){{
            add(new HashMap<String,Object>(4){{
                put("shipName","公主");
                put("shipCode","princess");
                put("shipType","台风");
                put("shipRant","张三");
                put("voyage","2022090301");
            }});
            add(new HashMap<String,Object>(4){{
                put("shipName","王子");
                put("shipCode","prince");
                put("shipType","台风");
                put("shipRant","李四");
                put("voyage","2022090304");
            }});
        }};
        comboGridConfig.setItems(col10ComboGridList);

        testComboGrid.setConfig(comboGridConfig);
        customSearch.selectedProperty().addListener((obs,o,n)-> setupFilter(n));
        showRowIndex.selectedProperty().addListener((obs,o,n)-> filteredTableView.setRowHeaderVisible(n));
        filteredTableView.setTableMenuButtonVisible(true);
        filteredTableView.setColumnFixingEnabled(true);
        filteredTableView.setEditable(true);
        col1Column.setCellValueFactory(p->p.getValue().col1Property());
        col1Column.setCellFactory(TextField2TableCell.forTableColumn());
        setupCellValueFactory(col2Column,TestDataVO::col2Property );
        col2Column.setCellFactory(ComboBox2TableCell.forTableColumn("Name 1", "Name 2", "Name 3", "Name 4"));
        setupCellValueFactory(col3Column,TestDataVO::col3BooleanProperty );
        setupCellValueFactory(col4Column,TestDataVO::col4ComboboxModelProperty);
        setupCellValueFactory(col5Column,TestDataVO::col5LocalDateProperty );
        setupCellValueFactory(col6Column,TestDataVO::col6LocalTimeProperty );
        setupCellValueFactory(col7Column,TestDataVO::col7BooleanProperty );
        col7Column.setCellFactory(CheckBoxTableCell.forTableColumn(col7Column));
        col3Column.setCellFactory(SwitchTableCell.forTableColumn(event -> {
            Map<String, Object> paramMap = event.getParamMap();
            Boolean data = (Boolean)paramMap.get("data");
            Integer row = (Integer) paramMap.get("row");
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol3(data?1:0);
        }));
        setupCellValueFactory(col8Column, TestDataVO::col8Property);
        setupCellValueFactory(col9Column, TestDataVO::col9Property);
        setupCellValueFactory(col11Column,TestDataVO::col11Property );
        setupCellValueFactory(col12Column,TestDataVO::col12Property );
        setupCellValueFactory(col13Column,TestDataVO::col13Property );
        col13Column.setCellFactory(DatePickerTableCell.forTableColumn());
        col13Column.setOnEditCommit(e->{
            int row = e.getTablePosition().getRow();
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol13(e.getNewValue());
        });
        setupCellValueFactory(col14Column,TestDataVO::col14Property );
        col14Column.setCellFactory(DateTimePickerTableCell.forTableColumn());
        col14Column.setOnEditCommit(e->{
            int row = e.getTablePosition().getRow();
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol14(e.getNewValue());
        });

        setupCellValueFactory(col15Column,TestDataVO::col15Property );
        setupCellValueFactory(col16Column,TestDataVO::col16Property );
        setupCellValueFactory(col17Column,TestDataVO::col17Property );
        setupCellValueFactory(col18Column,TestDataVO::col18Property );
        setupCellValueFactory(col19Column,TestDataVO::col19Property );
        setupCellValueFactory(col20Column,TestDataVO::col20Property );
        setupCellValueFactory(col21Column,TestDataVO::col21Property );
        setupCellValueFactory(col22Column,TestDataVO::col22Property );
        setupCellValueFactory(col23Column,TestDataVO::col23Property );
        setupCellValueFactory(col24Column,TestDataVO::col24Property );
        setupCellValueFactory(col25Column,TestDataVO::col25Property );
        setupCellValueFactory(col26Column,TestDataVO::col26Property );
        setupCellValueFactory(col27Column,TestDataVO::col27Property );
        setupCellValueFactory(col28Column,TestDataVO::col28Property );
        setupCellValueFactory(col29Column,TestDataVO::col29Property );
        setupCellValueFactory(col30Column,TestDataVO::col30Property );

        initContextMenu(col1Column,col2Column,col3Column,col4Column,col5Column,col6Column,col7Column,col8Column,col9Column,
                col10Column,col11Column,col12Column,col13Column,col14Column,col15Column, col16Column,col17Column,col18Column,
                col19Column,col20Column,col21Column,col22Column,col23Column,col24Column,col25Column,col26Column,col27Column,
                col28Column,col29Column,col30Column);
        filterAction(col1Column,col2Column,col3Column,col4Column,col5Column,col6Column,col7Column,col8Column,col9Column,
                col10Column,col11Column,col12Column,col13Column,col14Column,col15Column, col16Column,col17Column,col18Column,
                col19Column,col20Column,col21Column,col22Column,col23Column,col24Column,col25Column,col26Column,col27Column,
                col28Column,col29Column,col30Column);

        initCellFactory();

        initActions();
        FilteredList<TestDataVO> filteredData = new FilteredList<>(testBigDataModel.getTestDatas(), p -> true);
        filteredTableView.setItems(filteredData);
        FilteredTableView.configureForFiltering(filteredTableView, testBigDataModel.getTestDatas());
        query();
    }
    private void query() {
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    testBigDataModel.getTestDatas().clear();
                    ApplicationStore.showProgress();
                    rootPane.setDisable(true);
                    queryParams.put("page",page);
                    queryParams.put("limit",20);
                })
                .addSupplierInExecutor(
                        () -> Request.connector(TestDataFeign.class).getPageData(queryParams)
                )
                .addConsumerInPlatformThread(result -> {
                    List<TestData> datas = result.getDatas();
                    for (TestData testData : datas) {
                        testBigDataModel.getTestDatas().add(new TestDataVO(testData));
                    }
                    long total = result.getTotal();
                    pagination.setPageCount(new BigDecimal(total).intValue()/20);
                })
                .withFinal(() -> {
                    ApplicationStore.hideProgress();
                    rootPane.setDisable(false);
                    southFilterCol1 = new SouthFilter<>(col1Column, String.class);
                    editorColor = new SouthFilter<>(col9Column, String.class);
                    editorColor.getFilterEditor().setCellFactory(c -> new ListCell<String>() {
                        private final HBox box;
                        private final Circle circle;
                        {
                            circle = new Circle(10);
                            box = new HBox(circle);
                            box.setAlignment(Pos.CENTER);
                            setText(null);
                        }
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null && ! empty) {
                                circle.setFill(Color.web(item));
                                setGraphic(box);
                            } else {
                                setGraphic(null);
                            }
                        }

                    });

                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    private <T> void setupCellValueFactory(FilteredTableColumn<TestDataVO, T> column, Function<TestDataVO, ObservableValue<T>> mapper) {
        column.setCellValueFactory((FilteredTableColumn.CellDataFeatures<TestDataVO, T> param) -> mapper.apply(param.getValue()));
    }

    private void initActions(){
        search.setOnMouseClicked(e-> query());
        pagination.currentPageIndexProperty().addListener((observable,o,n)->{
            page=n.intValue()+1;
            query();
        });
        confirmSelectedRow.setOnMouseClicked(e->{
            TestDataVO selectedItem = filteredTableView.getSelectionModel().getSelectedItem();
            System.out.println(selectedItem.toString());
        });
    }

    private void initContextMenu(FilteredTableColumn... columns){
        for (FilteredTableColumn column : columns) {
            ContextMenu cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(column)));
            column.setContextMenu(cm);
        }
    }

    private void initCellFactory(){
        col9Column.setCellFactory(p -> new TableCell<TestDataVO, String>() {
            private final HBox box;
            private final Circle circle;
            {
                circle = new Circle(10);
                box = new HBox(circle);
                box.setAlignment(Pos.CENTER);
                setText(null);
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && ! empty) {
                    circle.setFill(Color.web(item));
                    setGraphic(box);
                } else {
                    setGraphic(null);
                }
            }

        });

        filteredTableView.setRowHeaderContextMenuFactory((i, data) -> {
            ContextMenu rowCM = ActionUtils.createContextMenu(Arrays.asList(new RowFixAction(filteredTableView, i), ActionUtils.ACTION_SEPARATOR));
            if (data != null) {
                final MenuItem menuItem = new MenuItem("删除  " + data.getCol1());
                menuItem.setOnAction(e -> {
                    if (i >= 0) {
                        final ObservableList<TestDataVO> items = filteredTableView.getItems();
                        if (items instanceof SortedList) {
                            int sourceIndex = ((SortedList<TestDataVO>) items).getSourceIndexFor(testBigDataModel.getTestDatas(), i);
                            testBigDataModel.getTestDatas().remove(sourceIndex);
                        } else {
                            testBigDataModel.getTestDatas().remove(i.intValue());
                        }
                    }
                });
                rowCM.getItems().add(menuItem);
            }
            final MenuItem menuItemAdd = new MenuItem("新增一条");
            menuItemAdd.setOnAction(e -> {
                testBigDataModel.getTestDatas().add(new TestDataVO());
            });
            rowCM.getItems().add(menuItemAdd);
            return rowCM;
        });
    }

    private void filterAction(FilteredTableColumn... columns) {
        for (FilteredTableColumn column : columns) {
            PopupFilter<TestDataVO, String> popupCol1Filter = new PopupStringFilter<>(column);
            column.setOnFilterAction(e -> popupCol1Filter.showPopup());
        }
    }

    private void southNodeFilterAction() {
        col1Column.setOnFilterAction(e -> {
            if (col1Column.getPredicate() != null) {
                southFilterCol1.getFilterEditor().cancelFilter();
            }
        });
        col1Column.setOnFilterAction(e -> {
            if (col1Column.getPredicate() != null) {
                editorColor.getFilterEditor().cancelFilter();
            }
        });
    }

    public void setupFilter(boolean southFilter) {
        if (southFilter) {
            southNodeFilterAction();
            col1Column.setSouthNode(southFilterCol1);
            col9Column.setSouthNode(editorColor);
        } else {
            filterAction();
            col1Column.setSouthNode(null);
            col9Column.setSouthNode(null);
        }
    }
}
