package com.tcoding.client.test;


import com.jfoenix.controls.*;
import com.tcoding.client.components.customer.CustomerExport;
import com.tcoding.client.components.customer.CustomerPrint;
import com.tcoding.client.components.customer.CustomerSearch;
import com.tcoding.client.components.customer.CustomerTableContextMenu;
import com.tcoding.client.components.extend.SnackbarExtend;
import com.tcoding.client.editor.CustomerBoxEditorModel;
import com.tcoding.client.gui.feature.FeatureResourceConsumer;
import com.tcoding.client.model.CustomerSearchModel;
import com.tcoding.client.model.TestBigDataModel;
import com.tcoding.client.request.Request;
import com.tcoding.client.request.feign.admin.TestDataFeign;
import com.tcoding.client.test.model.TestDataVO;
import com.tcoding.client.utils.Pinyin4jUtil;
import com.tcoding.client.utils.SvgGraphicUtil;
import com.tcoding.core.entity.TestData;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Map;

/**
 * @description:
 * @className: OnlineUserController
 * @author: liwen
 * @date: 2021/1/1 13:24
 */
@ViewController(value = "/fxml/test/big_data.fxml", title = "大数据量测试")
public class TestBigDataController {
    @FXMLViewFlowContext
    private ViewFlowContext flowContext;
    @ActionHandler
    private FlowActionHandler actionHandler;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox contentPane;
    @FXML
    private TextField userNameTextField;
    @FXML
    private TextField ipTextfiled;
    @FXML
    private TableView<TestData> tableView;
    @FXML
    private TableColumn<TestData,String> col1Column;
    @FXML
    private JFXSpinner spinner;
    @FXML
    @ActionTrigger("rest")
    private Button restButton;
    @FXML
    @ActionTrigger("search")
    private Button searchButton;
    @FXML
    private Button addButton;

    @FXML
    private HBox gridBar;

    @FXML
    private TableColumn<TestData, Label> numberColumn;

    @FXML
    private Label dataCount;

    @FXML
    private Label loadingDuration;

    @Inject
    private TestBigDataModel testBigDataModel;

    @Inject
    private CustomerSearchModel customerSearchModel;

    private SnackbarExtend snackbar;

    /**
     * 高级查询
     */
    private CustomerSearch<TestData> customerSearch;
    /**
     * 表格导出
     */
    private CustomerExport<TestData> customerExport;
    /**
     * 表格打印
     */
    private CustomerPrint<TestData> customerPrint;

    /**
     * 右键菜单
     */
    private CustomerTableContextMenu<TestData> contextMenu;

    @Inject
    private FeatureResourceConsumer featureResourceConsumer;

    /**
     * 组标记
     */
    private ObservableList<CustomerBoxEditorModel> groupTagItems;


    @PostConstruct
    private void init() {
        if (groupTagItems == null) {
            ObservableList<CustomerBoxEditorModel> innerList = FXCollections.observableArrayList();
            groupTagItems = new SimpleListProperty<>(innerList);
        }
        //初始化sql操作
        groupTagItems.addAll(new ArrayList<CustomerBoxEditorModel>() {{
            add(new CustomerBoxEditorModel("(", "("));
            add(new CustomerBoxEditorModel(" ", " "));
        }});

        featureResourceConsumer.consumeResource(this);
        customerSearch = new CustomerSearch<>();
        tableView.setUserData("TestBigDataController");
        customerSearch.initComponent(customerSearchModel, flowContext, gridBar, rootPane, tableView, event -> {
            Map<String, Object> paramMap = event.getParamMap();
            System.out.println("获取到高级查询参数：" + paramMap.toString());
            Object data = paramMap.get("data");
            query();
        });
        customerExport = new CustomerExport<TestData>() {
            @Override
            public void startExport() {
                System.out.println("开始导出...."+System.currentTimeMillis());
            }
            @Override
            public void endExport() {
                spinner.setVisible(false);
                System.out.println("结束导出....."+System.currentTimeMillis());
                snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("数据导出完成")));
            }
        };
        customerExport.initComponent(gridBar,tableView,"testBigData");

        customerPrint = new CustomerPrint<>();
        customerPrint.initComponent(gridBar,tableView,flowContext);
        snackbar = new SnackbarExtend(rootPane,"SUCCESS");
        snackbar.setPrefWidth(500);
        contextMenu = new CustomerTableContextMenu<>();
        contextMenu.initComponent(tableView,
                event -> {
                    Map<String, Object> paramMap = event.getParamMap();
                    Object data = paramMap.get("data");
                    System.out.println("需要删除的数据：" + data.toString());
                    // TODO: 2022/8/9 删除事件
                },event -> {
                    Map<String, Object> paramMap = event.getParamMap();
                    Object data = paramMap.get("data");
                    System.out.println("需要修改的数据：" + data.toString());
                    openEditDialog(false);
                },snackbar);

        dataCount.setText("加载数据量：0");
        loadingDuration.setText("请求耗时：0ms");

        searchButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("search", 12, Color.WHITE));
        restButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("refresh", 12, Color.GRAY));
        spinner.setVisible(false);

        numberColumn.setCellFactory((col) -> new TableCell<TestData, Label>() {
            @Override
            public void updateItem(Label item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);

                if (!empty) {
                    int rowIndex = this.getIndex() + 1;
                    Label label = new Label("" + rowIndex);
                    this.setGraphic(label);
                }
            }
        });

//        col1Column.setCellFactory(param -> new TcodingTableTextEditingCell());

        FilteredList<TestData> filteredData = new FilteredList<>(testBigDataModel.getTestDatas(), p -> true);
        tableView.setItems(filteredData);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        userNameTextField.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol1()).contains(val);
            });
        });
        ipTextfiled.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol2()).contains(val);
            });
        });

        testBigDataModel.selectedIndexProperty().bind(tableView.getSelectionModel().selectedIndexProperty());
        tableView.getStyleClass().addAll("cf-table-view", "cf-scroll-bar-style");

        query();

        addButton.setOnAction(e-> openEditDialog(true));
    }

    private void query() {
        long start = System.currentTimeMillis();
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    testBigDataModel.getTestDatas().clear();
                    spinner.setVisible(true);
                    contentPane.setDisable(true);
                })
                .addSupplierInExecutor(
                        () -> Request.connector(TestDataFeign.class).getAllData()
                )
                .addConsumerInPlatformThread(result -> {

                    testBigDataModel.getTestDatas().addAll(result);
                    dataCount.setText("加载数据量：" + result.size());
                    long duration = System.currentTimeMillis() - start;
                    loadingDuration.setText("请求耗时：" + duration + "ms");

                })
                .withFinal(() -> {
                    spinner.setVisible(false);
                    contentPane.setDisable(false);

                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    @ActionMethod("search")
    private void search() {
        query();
    }

    @ActionMethod("rest")
    private void rest() throws FlowException, VetoException {
        userNameTextField.setText("");
        ipTextfiled.setText("");

    }

    /**
     * 打开修改窗口
     * @param isAdd 是否是新增
     */
    private void openEditDialog(boolean isAdd){

        Flow flow = new Flow(TestBigDataFormController.class);
        JFXDialog dialog =new JFXDialog();
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label("编辑表单"));
        TestData testData =isAdd?new TestData():testBigDataModel.getTestDatas().get(testBigDataModel.getSelectedIndex());
        TestDataVO dataVO = new TestDataVO(testData);
        FlowHandler handler1 = flow.createHandler(flowContext);
        flow.withGlobalAction("submit", (flowHandler, actionId) -> {
            TestData newData = dataVO.convertToData();
            saveData(newData);
            dialog.close();
        });
        flow.withGlobalAction("cancel", (flowHandler, actionId) -> {
            dialog.close();
        });
        try {
            handler1.registerInFlowContext("data",dataVO);
            layout.setBody(handler1.start());
            featureResourceConsumer.consumeResource(handler1.getCurrentViewContext().getController());
        } catch (FlowException flowException) {
            flowException.printStackTrace();
        }
        layout.setPrefWidth(800);
        dialog.setContent(layout);
        dialog.setOverlayClose(false);
        dialog.setDialogContainer(rootPane);
        dialog.show();
    }

    /**
     * 保存数据
     * @param testData
     */
    private void saveData(TestData testData){
        System.out.println(testData);
    }

}

