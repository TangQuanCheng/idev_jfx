package com.tcoding.client.gui.contextmenu;

import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.utils.SvgGraphicUtil;
import com.tcoding.client.websocket.CustomEventManager;
import com.tcoding.client.websocket.GlobalContextMenuActionEvent;
import com.tcoding.core.enums.ContextMenuActionEnum;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.paint.Color;

import java.util.ResourceBundle;

/**
 * @author 81072
 */
@SuppressWarnings("restriction")
public class GlobalMenu extends ContextMenu {
    /** * 私有构造函数 */
    private GlobalMenu(Object n) {

        ResourceBundle resourceBundle = ApplicationStore.getResourceBundle();

        MenuItem independentlyOpen = new MenuItem(resourceBundle.getString("independentlyOpen"));
        MenuItem closeCurrent = new MenuItem(resourceBundle.getString("closeCurrent"));
        MenuItem closeRight = new MenuItem(resourceBundle.getString("closeRight"));
        MenuItem closeAll = new MenuItem(resourceBundle.getString("closeAll"));

        independentlyOpen.setGraphic(SvgGraphicUtil.buildSvgGraphic("bookmark",12, Color.GRAY));
        closeCurrent.setGraphic(SvgGraphicUtil.buildSvgGraphic("power-off",12, Color.GRAY));
        closeRight.setGraphic(SvgGraphicUtil.buildSvgGraphic("filter",12, Color.GRAY));
        closeAll.setGraphic(SvgGraphicUtil.buildSvgGraphic("display",12, Color.GRAY));

        getItems().add(independentlyOpen);
        getItems().add(closeCurrent);
        getItems().add(closeRight);
        getItems().add(closeAll);
        independentlyOpen.setOnAction(action-> CustomEventManager.publish(new GlobalContextMenuActionEvent(ContextMenuActionEnum.OPEN,n)));
        closeCurrent.setOnAction(action-> CustomEventManager.publish(new GlobalContextMenuActionEvent(ContextMenuActionEnum.CLOSE_CURRENT,n)));
        closeRight.setOnAction(action-> CustomEventManager.publish(new GlobalContextMenuActionEvent(ContextMenuActionEnum.CLOSE_RIGHT,n)));
        closeAll.setOnAction(action-> CustomEventManager.publish(new GlobalContextMenuActionEvent(ContextMenuActionEnum.CLOSE_ALL,n)));


    }

    /** * 获取实例 * @return GlobalMenu */
    public static GlobalMenu getInstance(Object n) {
        GlobalMenu globalMenu = new GlobalMenu(n);
        //设置class
        globalMenu.getStyleClass().add("cf-context-menu");
        return globalMenu;
    }
}
