package com.tcoding.client.gui.sidemenu;

import javafx.scene.control.Tab;

import java.util.LinkedHashMap;

/**
 * @author 唐全成
 * @Date: 2022/9/6 16:14
 * @description
 **/
public class MenuConstant {
    public static final LinkedHashMap<String, Tab> tabsMap = new LinkedHashMap<>();
    public static final LinkedHashMap<String, String> globalLinkMap = new LinkedHashMap<>();

}
