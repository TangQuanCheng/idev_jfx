package com.tcoding.client.gui;

import com.tcoding.client.store.ApplicationStore;

import java.util.ResourceBundle;

/**
 * @author 唐全成
 * @Date: 2022/7/6 14:22
 * @description
 **/
public class BaseController {

    private ResourceBundle baseBundle;

    public void reloadBundle(){
        baseBundle = ResourceBundle.getBundle("local/language");
        ApplicationStore.setResourceBundle(baseBundle);
    }


    public ResourceBundle getBundle() {
        if(baseBundle==null){
            baseBundle = ResourceBundle.getBundle("local/language");
            ApplicationStore.setResourceBundle(baseBundle);
            return baseBundle;
        }
        return baseBundle;
    }

    public void setBundle(ResourceBundle bundle) {
        this.baseBundle = bundle;
    }
}
