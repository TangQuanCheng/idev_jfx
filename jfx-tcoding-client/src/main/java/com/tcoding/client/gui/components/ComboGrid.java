package com.tcoding.client.gui.components;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

/**
 * @author 唐全成
 * @Date: 2022/7/11 15:38
 * @description
 **/
public class ComboGrid<T> extends ComboBox<T> {


    /**
     * Creates a default ComboBox instance with the provided items list and
     * a default {@link #selectionModelProperty() selection model}.
     *
     * @param items
     */
    public ComboGrid(ObservableList<T> items) {
        super(items);
    }


}
