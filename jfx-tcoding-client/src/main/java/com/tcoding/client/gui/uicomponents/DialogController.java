package com.tcoding.client.gui.uicomponents;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialog.DialogTransition;
import com.jfoenix.controls.JFXDialogLayout;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.tcoding.client.gui.main.LoginController;

import javax.annotation.PostConstruct;

@ViewController(value = "/fxml/ui/Dialog.fxml", title = "Material Design Example")
public class DialogController {

    @FXMLViewFlowContext
    private ViewFlowContext context;
    @FXML
    private JFXButton centerButton;
    @FXML
    private JFXButton topButton;
    @FXML
    private JFXButton rightButton;
    @FXML
    private JFXButton bottomButton;
    @FXML
    private JFXButton leftButton;
    @FXML
    private JFXButton acceptButton;
    @FXML
    private JFXButton alertButton;
    @FXML
    private StackPane root;
    @FXML
    private JFXDialog dialog;

    private StackPane dialogContainer;
    /**
     * init fxml when loaded.
     */
    @PostConstruct
    public void init() {
        root.getChildren().remove(dialog);

        centerButton.setOnAction(action -> {
            dialog.setTransitionType(DialogTransition.CENTER);

            Flow flow = new Flow(LoginController.class);

            GridPane grid = new GridPane();
            grid.setMaxWidth(1000);
            grid.setMaxHeight(500);
            try {
                grid.add(flow.start(),1,1);
                grid.add(acceptButton,1,2);
            } catch (FlowException e) {
                e.printStackTrace();
            }
            ObservableList<ColumnConstraints> columnConstraints = grid.getColumnConstraints();
            dialog.setContent( grid);
            dialog.show(root);
        });

        topButton.setOnAction(action -> {
            dialog.setTransitionType(DialogTransition.TOP);
            dialog.show(root);
        });

        rightButton.setOnAction(action -> {
            dialog.setTransitionType(DialogTransition.RIGHT);
            dialog.show(root);
        });

        bottomButton.setOnAction(action -> {
            dialog.setTransitionType(DialogTransition.BOTTOM);
            dialog.show(root);
        });

        leftButton.setOnAction(action -> {
            dialog.setTransitionType(DialogTransition.LEFT);
            dialog.show(root);
        });

        acceptButton.setOnAction(action -> {
            GridPane content = (GridPane)dialog.getContent();
            dialog.close();
        });

        alertButton.setOnAction(action -> {
            JFXAlert alert = new JFXAlert((Stage) alertButton.getScene().getWindow());
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setOverlayClose(false);
            JFXDialogLayout layout = new JFXDialogLayout();
            layout.setHeading(new Label("Modal Dialog using JFXAlert"));
            layout.setBody(new Label("Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
                                     + " sed do eiusmod tempor incididunt ut labore et dolore magna"
                                     + " aliqua. Utenim ad minim veniam, quis nostrud exercitation"
                                     + " ullamco laboris nisi ut aliquip ex ea commodo consequat."));
            JFXButton closeButton = new JFXButton("ACCEPT");
            closeButton.getStyleClass().add("dialog-accept");
            closeButton.setOnAction(event -> alert.hideWithAnimation());
            layout.setActions(closeButton);
            alert.setContent(layout);
            alert.show();
        });
    }

}
