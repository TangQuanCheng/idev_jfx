package com.tcoding.core.enums;

/**
 * @author 唐全成
 * @Date: 2022/6/13 13:16
 * @description
 **/
public enum MessageTypeEnum {
    /**
     * 文字
     */
    TEXT(0),
    /**
     * 图片
     */
    IMAGE(1),
    /**
     * 连接
     */
    LINK(2),
    /**
     * 图文
     */
    IMAGE_TEXT(3);

    private int code;

    MessageTypeEnum(int code) {
        this.code = code;
    }
}
