package com.tcoding.core.enums;

/**
 * @author 唐全成
 * @Date: 2022/6/13 13:16
 * @description
 **/
public enum DraftTypeEnum {
    /**
     * 文章
     */
    ARTICLE(0),
    /**
     * 音乐
     */
    MUSIC(1),
    /**
     * 电影
     */
    FILM(2),
    /**
     * 图片
     */
    IMAGE(3);

    private int code;

    DraftTypeEnum(int code) {
        this.code = code;
    }
}
