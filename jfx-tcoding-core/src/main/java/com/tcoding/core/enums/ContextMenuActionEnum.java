package com.tcoding.core.enums;

/**
 * @author 唐全成
 * @Date: 2022/7/19 15:05
 * @description
 **/
public enum ContextMenuActionEnum {

    /**
     * 独立窗口
     */
    OPEN,
    /**
     * 关闭右侧
     */
    CLOSE_RIGHT,
    /**
     * 关闭全部
     */
    CLOSE_ALL,
    /**
     * 关闭当前
     */
    CLOSE_CURRENT


}
