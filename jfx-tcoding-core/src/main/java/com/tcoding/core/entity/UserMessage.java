package com.tcoding.core.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 唐全成
 * @Date: 2022/6/13 13:15
 * @description
 **/
@Data
public class UserMessage {

    private Integer id;

    private Integer messageId;

    private Integer userId;

    private Integer readingStatus;

    private LocalDateTime readingTime;

}
