package com.tcoding.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 唐全成
 * @Date: 2022/8/8 10:25
 * @description 高级查询条目
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerSearchItem implements Serializable {

    private Integer id;

    private Integer programmeId;

    private Integer itemIndex;

    private String groupTag;

    private String itemName;

    private String action;

    private String value;

    private String logic;

}
