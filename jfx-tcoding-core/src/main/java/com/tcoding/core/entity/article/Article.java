package com.tcoding.core.entity.article;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 唐全成
 * @Date: 2022/7/11 14:35
 * @description
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Article extends  Draft{

    private String content;

}
