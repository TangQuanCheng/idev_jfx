package com.tcoding.core.entity.article;

import lombok.Data;
import com.tcoding.core.enums.DraftTypeEnum;

import java.time.LocalDateTime;

/**
 * @author 唐全成
 * @Date: 2022/7/11 14:37
 * @description
 **/
@Data
public class Draft {

    private Integer id;

    private String title;

    private String publisher;

    private LocalDateTime createTime;

    private DraftTypeEnum draftType;

}
