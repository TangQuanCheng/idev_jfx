package com.tcoding.core.entity;

import lombok.Data;
import com.tcoding.core.enums.MessageTypeEnum;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 唐全成
 * @Date: 2022/6/13 13:14
 * @description
 **/
@Data
public class Message  implements Serializable {

    private Integer id;

    private String title;

    private String content;

    private LocalDateTime createTime;

    private String publisher;

    private MessageTypeEnum messageType;

}
