package com.tcoding.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 唐全成
 * @Date: 2022/7/21 13:56
 * @description
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TestData implements Serializable {

    private Integer id;
    private String col1;
    private String col2;
    private String col3;
    private String col4;
    private String col5;
    private String col6;
    private String col7;
    private String col8;
    private String col9;
    private String col10;
    private String col11;
    private String col12;
    private String col13;
    private String col14;
    private String col15;
    private String col16;
    private String col17;
    private String col18;
    private String col19;
    private String col20;
    private String col21;
    private String col22;
    private String col23;
    private String col24;
    private String col25;
    private String col26;
    private String col27;
    private String col28;
    private String col29;
    private String col30;
}
