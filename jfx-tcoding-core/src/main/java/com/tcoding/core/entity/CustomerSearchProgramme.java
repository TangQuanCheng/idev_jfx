package com.tcoding.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 唐全成
 * @Date: 2022/8/8 10:24
 * @description 高级查询方案
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerSearchProgramme implements Serializable {

    private Integer id;

    private Integer parentId;

    private String title;

    private Integer userId;
    /**
     * 作用功能名称
     */
    private String actionRealm;

    private Date createTime;

    /**
     * 树的层级
     */
    private Integer level;

}
