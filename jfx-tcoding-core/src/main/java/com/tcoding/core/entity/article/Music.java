package com.tcoding.core.entity.article;

import lombok.Data;

/**
 * @author 唐全成
 * @Date: 2022/7/11 14:41
 * @description
 **/
@Data
public class Music extends Draft{

    private String filePath;

    private Long size;

}
