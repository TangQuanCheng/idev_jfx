package com.tcoding.client.service.feign;

import com.tcoding.client.store.ApplicationStore;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class AppRequestInterceptor implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate template) {

        template.header("Authorization", ApplicationStore.getToken());
    }
}
