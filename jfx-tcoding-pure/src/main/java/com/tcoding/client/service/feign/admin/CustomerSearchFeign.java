package com.tcoding.client.service.feign.admin;

import com.tcoding.client.service.feign.FeignAPI;
import com.tcoding.core.entity.CustomerSearchItem;
import com.tcoding.core.entity.CustomerSearchProgramme;
import feign.Param;
import feign.RequestLine;

import java.util.List;

/**
 * @author 唐全成
 * @Date: 2022/8/8 11:33
 * @description
 **/
public interface CustomerSearchFeign extends FeignAPI {

    @RequestLine("POST /customerSearchProgramme/list/{realm}")
    List<CustomerSearchProgramme> getCustomerSearchProgrammeList(@Param("realm") String realmName);

    @RequestLine("POST /customerSearchProgramme/save")
    Integer addCustomerSearchProgramme(CustomerSearchProgramme customerSearchProgramme);

    @RequestLine("POST /customerSearchProgramme/update")
    Integer updateCustomerSearchProgramme(CustomerSearchProgramme customerSearchProgramme);

    @RequestLine("GET /customerSearchProgramme/delete/{id}")
    Integer deleteCustomerSearchProgramme(@Param("id") Integer id);

    @RequestLine("POST /customerSearchItem/list/{id}")
    List<CustomerSearchItem> queryItemByProgrammeId(@Param("id") Integer id);

    @RequestLine("POST /customerSearchItem/save")
    Integer addCustomerSearchItem(List<CustomerSearchItem> customerSearchItem);

    @RequestLine("POST /customerSearchItem/update")
    Integer updateCustomerSearchItem(CustomerSearchItem customerSearchItem);


}
