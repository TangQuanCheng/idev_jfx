package com.tcoding.client.service.feign.admin;

import com.tcoding.client.service.feign.FeignAPI;
import com.tcoding.core.entity.log.GateLog;
import com.tcoding.core.msg.ObjectRestResponse;
import com.tcoding.core.msg.TableResultResponse;
import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

/**
 * @description:
 * @className: TestFeign
 * @author: liwen
 * @date: 2020/4/1 17:31
 */
@Headers("Content-Type: application/json")
public interface LogFeign extends FeignAPI {
    @RequestLine("GET /log/page")
    TableResultResponse<GateLog> getPageList(@QueryMap Map<String, Object> map);

    @RequestLine("DELETE /log/{id}")
    ObjectRestResponse<Integer> delete(@Param("id") Integer id);


}
