package com.tcoding.client.event;

/**
 * @author 唐全成
 * @Date: 2022/6/13 11:36
 * @description
 **/
public class SocketDataChangeEvent extends CustomEvent {

    private String title;

    public SocketDataChangeEvent(String title) {
        this.title = title;
    }

    public SocketDataChangeEvent() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
