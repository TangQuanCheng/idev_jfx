package com.tcoding.client.event;

import com.tcoding.core.enums.ContextMenuActionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 唐全成
 * @Date: 2022/6/13 11:36
 * @description
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GlobalContextMenuActionEvent extends CustomEvent {

    private ContextMenuActionEnum action;

    private Object value;
}
