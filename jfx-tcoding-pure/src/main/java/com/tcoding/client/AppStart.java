package com.tcoding.client;

import com.jfoenix.svg.SVGGlyphLoader;
import com.netflix.config.ConfigurationManager;
import com.netflix.config.util.ConfigurationUtils;
import com.tcoding.client.config.AppConfig;
import com.tcoding.client.controller.main.LoginController;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.utils.SvgUtil;
import com.tcoding.client.websocket.WSClient;
import io.datafx.controller.ViewConfiguration;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * @author 81072
 */
public class AppStart extends Application {

    @FXMLViewFlowContext
    private ViewFlowContext flowContext;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception {
        ConfigurationManager.loadPropertiesFromResources("config.properties");
        Properties properties = ConfigurationUtils.getPropertiesFromFile(AppStart.class.getResource("/config.properties"),new HashSet<>(4));
        AppConfig.setProperties(properties);
    }

    @Override
    public void start(Stage stage) throws Exception {
        new Thread(() -> {
            try {
                SVGGlyphLoader.loadGlyphsFont(AppStart.class.getResourceAsStream("/fonts/icomoon.svg"),
                    "icomoon.svg");
                SvgUtil.initSvgs();
            } catch (IOException ioExc) {
                ioExc.printStackTrace();
            }
        }).start();
        openLoginWindow(flowContext,stage);
    }

    public static void openLoginWindow(ViewFlowContext flowContext,Stage stage) throws FlowException {
        ViewConfiguration configuration = new ViewConfiguration();
        ResourceBundle bundle = ResourceBundle.getBundle("local/language");
        ApplicationStore.setResourceBundle(bundle);
        configuration.setResources(bundle);
        Flow flow = new Flow(LoginController.class);
        flowContext = new ViewFlowContext();
        flowContext.register("LoginStage", stage);
        FlowHandler handler = new FlowHandler(flow, new ViewFlowContext(), configuration);
        double width = 600;
        double height = 800;
        StackPane node = handler.start(new AnimatedFlowContainer(Duration.millis(150), ContainerAnimations.FADE));
        Scene scene = new Scene(node, width, height);
        initShortKey(scene);
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(AppStart.class.getResource("/css/theme/jfoenix-main-dark.css").toExternalForm());
        stage.setScene(scene);
        stage.getIcons().add(new Image(AppStart.class.getResourceAsStream("/images/bg1.jpg")));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        WSClient.getInstance().close();

    }
    public static void initShortKey(Scene scene){
        KeyCombination ctrl_c = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN);
        scene.getAccelerators().put(ctrl_c, () -> {
            System.out.println("快捷键CTRL + C");
            System.out.println(Thread.currentThread().getName());
        });
    }
}
