package com.tcoding.client.extend.combogrid;

import com.tcoding.client.extend.jfoenix.combogrid.ComboGridConfig;
import com.tcoding.client.extend.jfoenix.combogrid.ComboGridTableColumn;
import com.tcoding.client.extend.jfoenix.combogrid.JFXComboGrid;

import java.util.*;

/**
 * @author 唐全成
 * @Date: 2022/9/21 15:06
 * @description
 **/
public class ComboGridFactory {

    public static void initShipSelect(JFXComboGrid jfxComboGrid,Double width){

        LinkedList<ComboGridTableColumn> columns = new LinkedList<ComboGridTableColumn>(){{
            add(new ComboGridTableColumn("船名","shipName",200.0));
            add(new ComboGridTableColumn("船号","shipCode",220.0));
            add(new ComboGridTableColumn("船型","shipType",220.0));
            add(new ComboGridTableColumn("船代","shipRant",220.0));
            add(new ComboGridTableColumn("航次","voyage",220.0));
        }};
        ComboGridConfig comboGridConfig = new ComboGridConfig();
        comboGridConfig.setPlaceholder("选船");
        comboGridConfig.setColumns(columns);
        comboGridConfig.setTableTextColumn("shipName");
        comboGridConfig.setTableValueColumn("shipCode");
        comboGridConfig.setWidth(width);
        List<Map<String,Object>> col10ComboGridList = new ArrayList<Map<String,Object>>(){{
            add(new HashMap<String,Object>(4){{
                put("shipName","公主");
                put("shipCode","princess");
                put("shipType","台风");
                put("shipRant","张三");
                put("voyage","2022090301");
            }});
            add(new HashMap<String,Object>(4){{
                put("shipName","王子");
                put("shipCode","prince");
                put("shipType","台风");
                put("shipRant","李四");
                put("voyage","2022090304");
            }});
        }};
        comboGridConfig.setItems(col10ComboGridList);

        jfxComboGrid.setConfig(comboGridConfig);
    }


}
