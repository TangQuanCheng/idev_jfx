package com.tcoding.client.extend.jfoenix.combogrid;

import lombok.Data;

@Data
public class ComboGridTableColumn{

    private String text;

    private String prop;

    private Double width;

    public ComboGridTableColumn(String text, String prop, Double width) {
        this.text = text;
        this.prop = prop;
        this.width = width;
    }
}
