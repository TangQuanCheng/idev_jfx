package com.tcoding.client.extend.jfoenix;

import com.jfoenix.controls.JFXSnackbar;
import javafx.scene.layout.Pane;

/**
 * @author 唐全成
 * @Date: 2022/8/4 9:01
 * @description
 **/
public class SnackbarExtend extends JFXSnackbar {

    public SnackbarExtend(Pane snackbarContainer, String type) {
        super(snackbarContainer);
        switch (type){
            case "SUCCESS":
                this.getStyleClass().add("jfx-snackbar-success");
                break;
            case "WARNING":
                this.getStyleClass().add("jfx-snackbar-warning");
                break;
            case "DANGER":
                this.getStyleClass().add("jfx-snackbar-danger");
                break;
            case "INFO":
                this.getStyleClass().add("jfx-snackbar-info");
                break;
            default:
                this.getStyleClass().add("jfx-snackbar-default");

        }
    }
}
