package com.tcoding.client.store;

import com.jfoenix.controls.JFXProgressBar;
import com.tcoding.client.bean.MenuVoCell;
import com.tcoding.client.extend.jfoenix.JFXDecorator;
import com.tcoding.core.vo.GroupVO;
import com.tcoding.core.vo.MenuVO;
import com.tcoding.core.vo.PermissionInfo;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @className: User
 * @author: liwen
 * @date: 2020/8/2 10:17
 */

public class ApplicationStore {

    public static String ICON_FONT_KEY = "icomoon.svg";
    private static SimpleBooleanProperty style = new SimpleBooleanProperty();
    private static SimpleIntegerProperty status = new SimpleIntegerProperty();
    private static SimpleStringProperty code = new SimpleStringProperty();
    private static SimpleStringProperty token = new SimpleStringProperty();
    private static SimpleStringProperty name = new SimpleStringProperty();
    private static SimpleStringProperty avatar = new SimpleStringProperty();
    private static SimpleStringProperty introduction = new SimpleStringProperty();
    private static SimpleStringProperty theme = new SimpleStringProperty();
    private static ListProperty<GroupVO> roles;
    private static ListProperty<PermissionInfo> menus;
    private static ListProperty<MenuVO> permissionMenus;
    private static ListProperty<MenuVO> allMenu;
    private static ListProperty<MenuVoCell> menuVoCells;
    private static ListProperty<PermissionInfo> elements;
    private static MapProperty<String, String> featureMap;
    private static ResourceBundle resourceBundle;
    private static JFXProgressBar progressBar;
    private static JFXDecorator decorator;
    private static ListProperty<String> messageList;


    public static ListProperty<String> getMessageList() {

        if(messageList==null){
            ObservableList<String> innerList = FXCollections.observableArrayList();
            setMessageList(new SimpleListProperty<>(innerList));
        }

        return messageList;
    }

    public static void setMessageList(ListProperty<String> messageList) {
        ApplicationStore.messageList = messageList;
    }

    public static void addMessage(String s){
        if(ApplicationStore.messageList==null){
            ObservableList<String> innerList = FXCollections.observableArrayList();
            setMessageList(new SimpleListProperty<>(innerList));
        }
        ApplicationStore.messageList.add(s);
    }

    public static JFXDecorator getDecorator() {
        return decorator;
    }

    public static void setDecorator(JFXDecorator decorator) {
        ApplicationStore.decorator = decorator;
    }

    public static final ExecutorService EXECUTOR_SERVICE = new ThreadPoolExecutor(5, 20,
            60, TimeUnit.SECONDS, new LinkedBlockingDeque<>());


    public static JFXProgressBar getProgressBar() {
        return progressBar;
    }

    public static void showProgress(){
        if(progressBar!=null&&!progressBar.isVisible()){
            progressBar.setVisible(true);
        }
    }

    public static void hideProgress(){
        if(progressBar!=null&&progressBar.isVisible()){
            progressBar.setVisible(false);
        }
    }

    public static void setProgressBar(JFXProgressBar progressBar) {
        ApplicationStore.progressBar = progressBar;
    }

    private static SimpleStringProperty bellNumber = new SimpleStringProperty();

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public static void setResourceBundle(ResourceBundle resourceBundle) {
        ApplicationStore.resourceBundle = resourceBundle;
    }

    public static String getTheme() {
        return theme.get();
    }

    public static SimpleStringProperty themeProperty() {
        return theme;
    }

    public static void setTheme(String theme) {
        ApplicationStore.theme.set(theme);
    }

    public static String getBellNumber() {
        return bellNumber==null?"0":bellNumber.get();
    }

    public static SimpleStringProperty bellNumberProperty() {
        return bellNumber;
    }

    public static void setBellNumber(String bellNumber) {
        ApplicationStore.bellNumber.set(bellNumber);
    }

    public static int getStatus() {
        return status.get();
    }

    public static SimpleIntegerProperty statusProperty() {
        return status;
    }

    public static void setStatus(int status) {
        ApplicationStore.status.set(status);
    }

    public static String getCode() {
        return code.get();
    }

    public static SimpleStringProperty codeProperty() {
        return code;
    }

    public static void setCode(String code) {
        ApplicationStore.code.set(code);
    }

    public static String getToken() {
        return token.get();
    }

    public static SimpleStringProperty tokenProperty() {
        return token;
    }

    public static void setToken(String token) {
        ApplicationStore.token.set(token);
    }

    public static String getName() {
        return name.get();
    }

    public static SimpleStringProperty nameProperty() {
        return name;
    }

    public static void setName(String name) {
        ApplicationStore.name.set(name);
    }

    public static String getAvatar() {
        return avatar.get();
    }

    public static SimpleStringProperty avatarProperty() {
        return avatar;
    }

    public static void setAvatar(String avatar) {
        ApplicationStore.avatar.set(avatar);
    }

    public static String getIntroduction() {
        return introduction.get();
    }

    public static SimpleStringProperty introductionProperty() {
        return introduction;
    }

    public static void setIntroduction(String introduction) {
        ApplicationStore.introduction.set(introduction);
    }


    public static ListProperty<GroupVO> getRoles() {
        if (roles == null) {
            ObservableList<GroupVO> innerList = FXCollections.observableArrayList();
            roles = new SimpleListProperty<>(innerList);
        }
        return roles;
    }


    public static ListProperty<PermissionInfo> getMenus() {
        if (menus == null) {
            ObservableList<PermissionInfo> innerList = FXCollections.observableArrayList();
            menus = new SimpleListProperty<>(innerList);
        }
        return menus;
    }


    public static ListProperty<MenuVO> getAllMenu() {
        if (allMenu == null) {
            ObservableList<MenuVO> innerList = FXCollections.observableArrayList();
            allMenu = new SimpleListProperty<>(innerList);
        }
        return allMenu;
    }

    public static ListProperty<MenuVoCell> getMenuVoCells() {
        if (menuVoCells == null) {
            ObservableList<MenuVoCell> innerList = FXCollections.observableArrayList();
            menuVoCells = new SimpleListProperty<>(innerList);
        }

        return menuVoCells;
    }


    public static ListProperty<PermissionInfo> getElements() {
        if (elements == null) {
            ObservableList<PermissionInfo> innerList = FXCollections.observableArrayList();
            elements = new SimpleListProperty<>(innerList);
        }
        return elements;
    }

    public static MapProperty<String, String> getFeatureMap() {
        if (featureMap == null) {
            ObservableMap<String, String> map = FXCollections.observableMap(new HashMap<>());
            featureMap = new SimpleMapProperty<>(map);
        }
        return featureMap;
    }

    public static ListProperty<MenuVO> getPermissionMenus() {
        if (permissionMenus == null) {
            ObservableList<MenuVO> innerList = FXCollections.observableArrayList();
            permissionMenus = new SimpleListProperty<>(innerList);
        }
        return permissionMenus;
    }

    public static boolean isStyle() {
        return style.get();
    }

    public static SimpleBooleanProperty styleProperty() {
        return style;
    }

    public static void setStyle(boolean style) {
        ApplicationStore.style.set(style);
    }

    public static void clearPermissionInfo() {
        setName("");
        getAllMenu().clear();
        getMenuVoCells().clear();
        getMenus().clear();
        ApplicationStore.getElements().clear();
        getPermissionMenus().clear();
        getRoles().clear();
        getFeatureMap().clear();
    }


}


