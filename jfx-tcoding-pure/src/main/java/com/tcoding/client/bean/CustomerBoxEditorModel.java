package com.tcoding.client.bean;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableObjectValue;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author 唐全成
 * @Date: 2022/8/5 10:09
 * @description
 **/
public class CustomerBoxEditorModel implements ObservableObjectValue {

    private String label;

    private Object value;

    private String styleClass;

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public CustomerBoxEditorModel() {
    }

    public CustomerBoxEditorModel(String label, Object value) {
        this.label = label;
        this.value = value;
    }

    public CustomerBoxEditorModel(String label, Object value, String styleClass) {
        this.label = label;
        this.value = value;
        this.styleClass = styleClass;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if(o instanceof String){
            return StringUtils.equals(getValue().toString(),o.toString());
        }

        if (!(o instanceof CustomerBoxEditorModel)){
            return false;
        }
        CustomerBoxEditorModel model = (CustomerBoxEditorModel) o;
        return Objects.equals(getValue(), model.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

    /**
     * Adds a {@link ChangeListener} which will be notified whenever the value
     * of the {@code ObservableValue} changes. If the same listener is added
     * more than once, then it will be notified more than once. That is, no
     * check is made to ensure uniqueness.
     * <p>
     * Note that the same actual {@code ChangeListener} instance may be safely
     * registered for different {@code ObservableValues}.
     * <p>
     * The {@code ObservableValue} stores a strong reference to the listener
     * which will prevent the listener from being garbage collected and may
     * result in a memory leak. It is recommended to either unregister a
     * listener by calling {@link #removeListener(ChangeListener)
     * removeListener} after use or to use an instance of
     * {@link WeakChangeListener} avoid this situation.
     *
     * @param listener The listener to register
     * @throws NullPointerException if the listener is null
     * @see #removeListener(ChangeListener)
     */
    @Override
    public void addListener(ChangeListener listener) {

    }

    /**
     * Removes the given listener from the list of listeners, that are notified
     * whenever the value of the {@code ObservableValue} changes.
     * <p>
     * If the given listener has not been previously registered (i.e. it was
     * never added) then this method call is a no-op. If it had been previously
     * added then it will be removed. If it had been added more than once, then
     * only the first occurrence will be removed.
     *
     * @param listener The listener to remove
     * @throws NullPointerException if the listener is null
     * @see #addListener(ChangeListener)
     */
    @Override
    public void removeListener(ChangeListener listener) {

    }

    @Override
    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return label;
    }


    /**
     * Returns the current value of this {@code ObservableObjectValue<T>}.
     *
     * @return The current value
     */
    @Override
    public Object get() {
        return value;
    }

    /**
     * Adds an {@link InvalidationListener} which will be notified whenever the
     * {@code Observable} becomes invalid. If the same
     * listener is added more than once, then it will be notified more than
     * once. That is, no check is made to ensure uniqueness.
     * <p>
     * Note that the same actual {@code InvalidationListener} instance may be
     * safely registered for different {@code Observables}.
     * <p>
     * The {@code Observable} stores a strong reference to the listener
     * which will prevent the listener from being garbage collected and may
     * result in a memory leak. It is recommended to either unregister a
     * listener by calling {@link #removeListener(InvalidationListener)
     * removeListener} after use or to use an instance of
     * {@link WeakInvalidationListener} avoid this situation.
     *
     * @param listener The listener to register
     * @throws NullPointerException if the listener is null
     * @see #removeListener(InvalidationListener)
     */
    @Override
    public void addListener(InvalidationListener listener) {

    }

    /**
     * Removes the given listener from the list of listeners, that are notified
     * whenever the value of the {@code Observable} becomes invalid.
     * <p>
     * If the given listener has not been previously registered (i.e. it was
     * never added) then this method call is a no-op. If it had been previously
     * added then it will be removed. If it had been added more than once, then
     * only the first occurrence will be removed.
     *
     * @param listener The listener to remove
     * @throws NullPointerException if the listener is null
     * @see #addListener(InvalidationListener)
     */
    @Override
    public void removeListener(InvalidationListener listener) {

    }
}
