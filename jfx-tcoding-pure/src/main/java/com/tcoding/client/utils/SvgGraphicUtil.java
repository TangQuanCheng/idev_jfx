package com.tcoding.client.utils;

import com.jfoenix.svg.SVGGlyph;
import com.jfoenix.svg.SVGGlyphLoader;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;

/**
 * @author 唐全成
 * @Date: 2022/7/8 10:56
 * @description
 **/
@Slf4j
public class SvgGraphicUtil {

    /**
     * 构建svg图标
     * @param name
     * @param size
     * @param color
     * @return
     */
    public static SVGGlyph buildSvgGraphic(String name ,int size ,Color color){
        SVGGlyph icoMoonGlyph = null;
        try {
            icoMoonGlyph = SVGGlyphLoader.getIcoMoonGlyph("icomoon.svg."+name);
            icoMoonGlyph.setSize(size);
            icoMoonGlyph.setFill(color==null?Color.WHITE:color);
            icoMoonGlyph.getStyleClass().add("svg-font-icon");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return icoMoonGlyph;
    }

    public static SVGGlyph loadSingleGraphic(String url){
        URL url1= null;
        try {
            url1 = new URL(url);
            return   SVGGlyphLoader.loadGlyph(url1);
        } catch (IOException e) {
            log.error("加载svg文件失败，{}",e.getMessage(),e);
            return null;
        }

    }
}
