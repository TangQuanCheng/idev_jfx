package com.tcoding.client.components.print;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.svg.SVGGlyph;
import com.tcoding.client.utils.SvgGraphicUtil;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * @author 唐全成
 * @Date: 2022/8/9 10:24
 * @description 打印
 **/
public class CustomerPrint<T> {

    public void initComponent(Pane buttonPane, TableView<T> tableView, ViewFlowContext context){
        JFXButton button = new JFXButton();
        button.setText("打印");
        button.getStyleClass().add("cf-danger-but");
        SVGGlyph svgGlyph = SvgGraphicUtil.buildSvgGraphic("print", 12, Color.WHITE);
        button.setGraphic(svgGlyph);
        buttonPane.getChildren().add(button);
        final Stage stage = (Stage) context.getRegisteredObject("Stage");
        button.setOnAction(e-> print(tableView,stage));
    }
    public void print(final Node node, Stage stage) {

        PrinterJob printerJob = PrinterJob.createPrinterJob();
        if (printerJob.showPrintDialog(stage) && printerJob.printPage(node)) {
            printerJob.endJob();
            System.out.println("printed");
        }
    }

}
