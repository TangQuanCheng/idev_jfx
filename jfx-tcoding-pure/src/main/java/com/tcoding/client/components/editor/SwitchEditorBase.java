/*
 * Copyright (c) 2016 JFoenix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.tcoding.client.components.editor;

import com.jfoenix.controls.JFXToggleButton;
import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import com.tcoding.client.event.CustomerEvent;
import com.tcoding.client.extend.jfoenix.EnduringEditableTreeTableCell;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.control.TreeTableRow;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * <h1>Text field cell editor</h1>
 * this an example of the cell editor, it creates a JFXTextField node to
 * allow the user to edit the cell value
 * <p>
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2016-03-09
 */
public abstract class SwitchEditorBase<T> implements EditorNodeBuilder<T> {

    protected JFXToggleButton toggleButton;

    protected EventHandler<CustomerEvent<T>> handler;

    @Override
    public void startEdit() {

    }

    @Override
    public void cancelEdit() {

    }

    @Override
    public void updateItem(T item, boolean empty) {

    }

    @Override
    public Region createNode(T value, EventHandler<KeyEvent> keyEventsHandler, ChangeListener<Boolean> focusChangeListener) {
        toggleButton = new JFXToggleButton();
        toggleButton.setOnKeyPressed(keyEventsHandler);
        toggleButton.focusedProperty().addListener(focusChangeListener);
        toggleButton.setSize(5);
        toggleButton.setOnAction(event -> {
            EnduringEditableTreeTableCell enduringEditableTreeTableCell =(EnduringEditableTreeTableCell) ((JFXToggleButton)event.getTarget()).getParent();
            TreeTableRow treeTableRow = enduringEditableTreeTableCell.getTreeTableRow();
            handler.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                put("data", toggleButton.selectedProperty().getValue());
                put("row",treeTableRow.getIndex());
            }}));
        });

        return toggleButton;
    }

    @Override
    public void setValue(T value) {

        if(value==null){
            toggleButton.setSelected(false);
            return;
        }

        if(value instanceof String || value instanceof Integer){
            if(StringUtils.isBlank(value.toString())||StringUtils.equals(value.toString(),"0")){
                toggleButton.setSelected(false);
                return;
            }else {
                toggleButton.setSelected(true);
            }
            return;
        }

        if(value instanceof Boolean){
            if((Boolean)value){
                toggleButton.setSelected(true);
            }else {
                toggleButton.setSelected(false);
            }

        }
        toggleButton.setSelected(false);
    }

    @Override
    public void validateValue() throws Exception {

    }

    @Override
    public void nullEditorNode() {
//        checkBox = null;
    }

}
