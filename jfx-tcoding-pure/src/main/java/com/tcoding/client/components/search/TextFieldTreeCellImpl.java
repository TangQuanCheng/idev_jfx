package com.tcoding.client.components.search;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * 树形组件的编辑文本框
 */
public abstract class TextFieldTreeCellImpl extends TreeCell<Label> {


    /**
     * 文本域对象的定义
     */
    private TextField textField;

    public TextFieldTreeCellImpl() {
    }

    /**
     * 开始编辑时机的时候的回调函数
     */
    @Override
    public void startEdit() {
        //调用父类的开始编辑模板方法
        super.startEdit();
        if (textField == null) {
            //创建文本域对象
            createTextField();
        }
        textField.setText(getString());
        //设置文本内容
        setText(null);
        //设置图形外观为前文定义的textField对象
        setGraphic(textField);
        //选中文本域对象使其获得焦点
        textField.selectAll();
    }

    /**
     * 重写继承自TreeCell类的模板方法
     */
    @Override
    public void cancelEdit() {
        //取消编辑内容文本
        super.cancelEdit();
        Label item = getItem();
        //设置文本内容为所选条目
        setText(item.getText());
        //设置取消的所选图形外观对象为树形结构所选的图形外观组件
        setGraphic(getItem().getGraphic());
    }

    /**
     * 更新选项（重写继承自TreeCell类的模板方法）
     * @param item
     * @param empty
     */
    @Override
    public void updateItem(Label item, boolean empty) {
        //更新树形结构选项，调用父类的模板方法
        super.updateItem(item, empty);
        if (empty) {
            //设置文本元素为空
            setText(null);
            //设置图形元素为空
            setGraphic(null);
        } else {
            //如果正在编辑
            if (isEditing()) {
                if (textField != null) {
                    //将前文定义的文本域内容设置为从编辑的cell组件中获取的文本内容
                    textField.setText(getString());
                }
                //设置文本内容为null
                setText(null);
                //设置图形外观为前文定义的textField类型元素对象
                setGraphic(textField);
            } else {
                //设置文本内容
                setText(getString());
                //设置外观图形组件为所选树形结构中的图形外观元素
                setGraphic(getItem().getGraphic());
            }
        }
    }

    /**
     * 创建文本域对象
     */
    private void createTextField() {
        //创建文本域，初始化文本域对象的文本内容为从cell组件中取文本值
        textField = new TextField(getString());
        //针对本文本域对象的键盘释放时间处理回调函数的编写
        textField.setOnKeyReleased((KeyEvent t) -> {
            //判断当前敲击的键如果是回车键
            if (t.getCode() == KeyCode.ENTER) {
                //则调用继承自父类TreeCell类型的提交编辑改变内容方法
                commitEdit(new Label(textField.getText(),getItem().getGraphic()));
                this.getEditingText(textField.getText());
                //判断当前键盘事件中获取当前使用键盘的代码是否是ESC键
            } else if (t.getCode() == KeyCode.ESCAPE) {
                //调用取消编辑方法
                cancelEdit();
            }
        });
    }

    public abstract void getEditingText(String text);

    /**
     * 获取文本选项域对象的内容,利用继承自Cell父类的getItem()选项方法获取
     * @return
     */
    private String getString() {//
        return getItem() == null ? "" : getItem().getText();
    }

}
