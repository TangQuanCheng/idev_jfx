/*
 * Copyright (c) 2016 JFoenix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.tcoding.client.components.editor;

import com.jfoenix.controls.JFXColorPicker;
import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import com.tcoding.client.event.CustomerEvent;
import com.tcoding.client.extend.jfoenix.EnduringEditableTreeTableCell;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.control.TreeTableRow;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

import java.util.HashMap;

/**
 * <h1>Text field cell editor</h1>
 * this an example of the cell editor, it creates a JFXTextField node to
 * allow the user to edit the cell value
 * <p>
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2016-03-09
 */
public abstract class ColorPickerEditorBase<T> implements EditorNodeBuilder<T> {

    protected JFXColorPicker colorPicker;

    protected EventHandler<CustomerEvent<T>> handler;


    @Override
    public void startEdit() {

    }

    @Override
    public void cancelEdit() {

    }

    @Override
    public void updateItem(T item, boolean empty) {

    }

    @Override
    public Region createNode(T value, EventHandler<KeyEvent> keyEventsHandler, ChangeListener<Boolean> focusChangeListener) {
        colorPicker =new JFXColorPicker();
        colorPicker.setOnKeyPressed(keyEventsHandler);
        colorPicker.focusedProperty().addListener(focusChangeListener);

        colorPicker.setOnAction(event -> {
            EnduringEditableTreeTableCell enduringEditableTreeTableCell =(EnduringEditableTreeTableCell) ((JFXColorPicker)event.getTarget()).getParent();
            TreeTableRow treeTableRow = enduringEditableTreeTableCell.getTreeTableRow();
            handler.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                String fullColor = colorPicker.getValue().toString();
                String substring = fullColor.substring(2);
                String substring1 = substring.substring(0, 6);
                put("data", "#"+substring1.toUpperCase());
                put("row",treeTableRow.getIndex());
            }}));
        });

        return colorPicker;
    }

    @Override
    public void setValue(T value) {

        colorPicker.setValue(Color.valueOf(value.toString()));

    }

    @Override
    public void validateValue() throws Exception {

    }

    @Override
    public void nullEditorNode() {
//        checkBox = null;
    }

}
