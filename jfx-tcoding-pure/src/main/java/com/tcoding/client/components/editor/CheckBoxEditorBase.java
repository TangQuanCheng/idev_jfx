/*
 * Copyright (c) 2016 JFoenix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.tcoding.client.components.editor;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import com.tcoding.client.event.CustomerEvent;
import com.tcoding.client.extend.jfoenix.EnduringEditableTreeTableCell;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.control.TreeTablePosition;
import javafx.scene.control.TreeTableRow;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * <h1>Text field cell editor</h1>
 * this an example of the cell editor, it creates a JFXTextField node to
 * allow the user to edit the cell value
 * <p>
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2016-03-09
 */
public abstract class CheckBoxEditorBase<T> implements EditorNodeBuilder<T> {

    protected JFXCheckBox checkBox;

    protected EventHandler<CustomerEvent<T>> handler;

    protected TreeTablePosition treeTablePosition;

    @Override
    public void startEdit() {

    }

    @Override
    public void cancelEdit() {

    }

    @Override
    public void updateItem(T item, boolean empty) {

    }

    @Override
    public Region createNode(T value, EventHandler<KeyEvent> keyEventsHandler, ChangeListener<Boolean> focusChangeListener) {
        checkBox = value == null ? new JFXCheckBox() : new JFXCheckBox();
        checkBox.setOnKeyPressed(keyEventsHandler);
        checkBox.focusedProperty().addListener(focusChangeListener);

        checkBox.setOnAction(event -> {
            EnduringEditableTreeTableCell enduringEditableTreeTableCell =(EnduringEditableTreeTableCell) ((JFXCheckBox)event.getTarget()).getParent();
            TreeTableRow treeTableRow = enduringEditableTreeTableCell.getTreeTableRow();
            handler.handle(new CustomerEvent<>(event, new HashMap<String, Object>(4) {{
                put("data", checkBox.selectedProperty().getValue());
                put("row",treeTableRow.getIndex());
            }}));
        });

        return checkBox;
    }

    @Override
    public void setValue(T value) {

        if(value==null){
            checkBox.setSelected(false);
            return;
        }

        if(value instanceof String || value instanceof Integer){
            if(StringUtils.isBlank(value.toString())||StringUtils.equals(value.toString(),"0")){
                checkBox.setSelected(false);
                return;
            }else {
                checkBox.setSelected(true);
            }
            return;
        }

        if(value instanceof Boolean){
            if((Boolean)value){
                checkBox.setSelected(true);
            }else {
                checkBox.setSelected(false);
            }

        }
        checkBox.setSelected(false);
    }

    @Override
    public void validateValue() throws Exception {

    }

    @Override
    public void nullEditorNode() {
//        checkBox = null;
    }

}
