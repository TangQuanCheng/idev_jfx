/*
 * Copyright (c) 2016 JFoenix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.tcoding.client.components.editor;

import com.jfoenix.controls.cells.editors.base.EditorNodeBuilder;
import com.tcoding.client.bean.CustomerBoxEditorModel;
import com.tcoding.client.extend.jfoenix.combogrid.ComboGridConfig;
import com.tcoding.client.extend.jfoenix.combogrid.JFXComboGrid;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;

/**
 * <h1>Text field cell editor</h1>
 * this an example of the cell editor, it creates a JFXTextField node to
 * allow the user to edit the cell value
 * <p>
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2016-03-09
 */
public abstract class ComboGridEditorBase<T> implements EditorNodeBuilder<T> {

    protected JFXComboGrid comboGrid;
    protected ComboGridConfig config;

    @Override
    public void startEdit() {

    }

    @Override
    public void cancelEdit() {

    }

    @Override
    public void updateItem(T item, boolean empty) {
        Platform.runLater(() -> {
            comboGrid.getComboTextField().requestFocus();
        });
    }

    @Override
    public Region createNode(T value, EventHandler<KeyEvent> keyEventsHandler, ChangeListener<Boolean> focusChangeListener) {

        if(comboGrid==null){
            comboGrid = new JFXComboGrid(config);
        }

        comboGrid.getComboTextField().setOnKeyPressed(keyEventsHandler);
        comboGrid.getComboTextField().focusedProperty().addListener(focusChangeListener);

        comboGrid.getItems().clear();
        comboGrid.getItems().addAll(config.getItems());

        return comboGrid;
    }

    @Override
    public void setValue(T value) {
        SimpleStringProperty v = new SimpleStringProperty(value.toString());
        if(comboGrid!=null){
            if(value instanceof  String){
                comboGrid.setValue(value.toString());
            }else {
                comboGrid.setValue(((CustomerBoxEditorModel)value).getValue().toString());
            }

        }
    }

    @Override
    public void validateValue() throws Exception {
        if (comboGrid.getValue()==null) {
            throw new Exception("Invalid value");
        }
    }

    @Override
    public void nullEditorNode() {
        comboGrid = null;
    }
}
