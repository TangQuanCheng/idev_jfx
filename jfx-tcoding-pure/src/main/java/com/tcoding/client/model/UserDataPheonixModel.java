package com.tcoding.client.model;

import com.tcoding.client.controller.privilege.user.vo.UserPhoenixVO;
import io.datafx.controller.injection.scopes.ViewScoped;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@ViewScoped
public class UserDataPheonixModel {
    private int counter = 0;
    private ListProperty<UserPhoenixVO> users;

    private IntegerProperty pageCount;
    private IntegerProperty selectedPersonIndex;


    public ListProperty<UserPhoenixVO> getUsers() {
        if (users == null) {
            ObservableList<UserPhoenixVO> innerList = FXCollections.observableArrayList();
            users = new SimpleListProperty<>(innerList);
        }
        return users;
    }

    public ObservableList<UserPhoenixVO> getObservableUsers() {
        if (users == null) {
            ObservableList<UserPhoenixVO> innerList = FXCollections.observableArrayList();
            return innerList;
        }
        return users;
    }


    public int getSelectedPersonIndex() {
        return selectedPersonIndexProperty().get();
    }

    public void setSelectedPersonIndex(int selectedPersonIndex) {
        this.selectedPersonIndex.set(selectedPersonIndex);
    }

    public IntegerProperty selectedPersonIndexProperty() {
        if (selectedPersonIndex == null) {
            selectedPersonIndex = new SimpleIntegerProperty();
        }
        return selectedPersonIndex;
    }

    public int getPageCount() {
        return pageCount.get();
    }

    public IntegerProperty pageCountProperty() {
        if (pageCount == null) {
            pageCount = new SimpleIntegerProperty();
        }
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount.set(pageCount);
    }
}
