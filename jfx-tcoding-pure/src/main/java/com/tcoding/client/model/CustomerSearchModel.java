package com.tcoding.client.model;

import com.tcoding.client.bean.CustomerBoxEditorModel;
import com.tcoding.client.bean.SearchItem;
import io.datafx.controller.injection.scopes.ViewScoped;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * 自定义高级查询
 */
@ViewScoped
public class CustomerSearchModel {
    private static ListProperty<CustomerBoxEditorModel> searchItems;
    private IntegerProperty selectedIndex;
    private ObservableList<CustomerBoxEditorModel> observableSearchItems;

    private static ListProperty<SearchItem> searchDatas;


    public ListProperty<CustomerBoxEditorModel> getSearchItems() {
        if (searchItems == null) {
            ObservableList<CustomerBoxEditorModel> innerList = FXCollections.observableArrayList();
            searchItems = new SimpleListProperty<>(innerList);
        }
        return searchItems;
    }

    public ListProperty<SearchItem> getSearchDatas() {
        if (searchDatas == null) {
            ObservableList<SearchItem> innerList = FXCollections.observableArrayList();
            searchDatas = new SimpleListProperty<>(innerList);
        }
        return searchDatas;
    }

    public ObservableList<CustomerBoxEditorModel> getObservableSearchItems() {

        if(observableSearchItems==null){
            observableSearchItems = FXCollections.observableArrayList();
        }
        return observableSearchItems;
    }

    public static ListProperty<SearchItem> searchDatasProperty() {
        return searchDatas;
    }

    public static void setSearchDatas(ObservableList<SearchItem> searchDatas) {
        CustomerSearchModel.searchDatas.set(searchDatas);
    }

    public int getSelectedIndex() {
        return selectedIndexProperty().get();
    }

    public IntegerProperty selectedIndexProperty() {
        if (selectedIndex == null) {
            selectedIndex = new SimpleIntegerProperty();
        }
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        selectedIndexProperty().set(selectedIndex);
    }
}
