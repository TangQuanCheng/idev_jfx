package com.tcoding.client.model;

import com.tcoding.client.controller.test.model.TestDataVO;
import io.datafx.controller.injection.scopes.ViewScoped;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@ViewScoped
public class TestBigDataVOModel {
    private ListProperty<TestDataVO> testDatas;
    private IntegerProperty selectedIndex;


    public ListProperty<TestDataVO> getTestDatas() {
        if (testDatas == null) {
            ObservableList<TestDataVO> innerList = FXCollections.observableArrayList();
            testDatas = new SimpleListProperty<>(innerList);
        }
        return testDatas;
    }


    public int getSelectedIndex() {
        return selectedIndexProperty().get();
    }

    public IntegerProperty selectedIndexProperty() {
        if (selectedIndex == null) {
            selectedIndex = new SimpleIntegerProperty();
        }
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        selectedIndexProperty().set(selectedIndex);
    }
}
