package com.tcoding.client.controller.test.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.tcoding.client.bean.CustomerBoxEditorModel;
import com.tcoding.client.controller.test.combo.ConstantComboModels;
import com.tcoding.core.entity.TestData;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

/**
 * @author 唐全成
 * @Date: 2022/8/11 11:12
 * @description
 **/
public class TestDataVO extends RecursiveTreeObject<TestDataVO> {

    private SimpleIntegerProperty id=new SimpleIntegerProperty();
    private SimpleStringProperty col1=new SimpleStringProperty();
    private SimpleStringProperty col2=new SimpleStringProperty();
    private SimpleIntegerProperty col3=new SimpleIntegerProperty();
    private SimpleStringProperty col4=new SimpleStringProperty();
    private SimpleStringProperty col5=new SimpleStringProperty();
    private SimpleStringProperty col6=new SimpleStringProperty();
    private SimpleStringProperty col7=new SimpleStringProperty();
    private SimpleStringProperty col8=new SimpleStringProperty();
    private SimpleStringProperty col9=new SimpleStringProperty();
    private SimpleStringProperty col10=new SimpleStringProperty();
    private SimpleStringProperty col11=new SimpleStringProperty();
    private SimpleStringProperty col12=new SimpleStringProperty();
    private SimpleStringProperty col13=new SimpleStringProperty();
    private SimpleStringProperty col14=new SimpleStringProperty();
    private SimpleStringProperty col15=new SimpleStringProperty();
    private SimpleStringProperty col16=new SimpleStringProperty();
    private SimpleStringProperty col17=new SimpleStringProperty();
    private SimpleStringProperty col18=new SimpleStringProperty();
    private SimpleStringProperty col19=new SimpleStringProperty();
    private SimpleStringProperty col20=new SimpleStringProperty();
    private SimpleStringProperty col21=new SimpleStringProperty();
    private SimpleStringProperty col22=new SimpleStringProperty();
    private SimpleStringProperty col23=new SimpleStringProperty();
    private SimpleStringProperty col24=new SimpleStringProperty();
    private SimpleStringProperty col25=new SimpleStringProperty();
    private SimpleStringProperty col26=new SimpleStringProperty();
    private SimpleStringProperty col27=new SimpleStringProperty();
    private SimpleStringProperty col28=new SimpleStringProperty();
    private SimpleStringProperty col29=new SimpleStringProperty();
    private SimpleStringProperty col30=new SimpleStringProperty();


    public TestDataVO(int id, String col1, String col2, Integer col3, String col4, String col5,
                      String col6, String col7, String col8, String col9, String col10, String col11, String col12,
                      String col13, String col14, String col15, String col16, String col17, String col18, String col19,
                      String col20, String col21, String col22, String col23, String col24, String col25,
                      String col26, String col27, String col28, String col29, String col30) {
        this.id = new SimpleIntegerProperty(id);
        this.col1 = new SimpleStringProperty(col1);
        this.col2 = new SimpleStringProperty(col2);
        this.col3 = new SimpleIntegerProperty(col3);
        this.col4 = new SimpleStringProperty(col4);
        this.col5 = new SimpleStringProperty(col5);
        this.col6 = new SimpleStringProperty(col6);
        this.col7 = new SimpleStringProperty(col7);
        this.col8 = new SimpleStringProperty(col8);
        this.col9 = new SimpleStringProperty(col9);
        this.col10 =new SimpleStringProperty(col10);
        this.col11 =new SimpleStringProperty(col11);
        this.col12 =new SimpleStringProperty(col12);
        this.col13 =new SimpleStringProperty(col13);
        this.col14 =new SimpleStringProperty(col14);
        this.col15 =new SimpleStringProperty(col15);
        this.col16 =new SimpleStringProperty(col16);
        this.col17 =new SimpleStringProperty(col17);
        this.col18 =new SimpleStringProperty(col18);
        this.col19 =new SimpleStringProperty(col19);
        this.col20 =new SimpleStringProperty(col20);
        this.col21 =new SimpleStringProperty(col21);
        this.col22 =new SimpleStringProperty(col22);
        this.col23 =new SimpleStringProperty(col23);
        this.col24 =new SimpleStringProperty(col24);
        this.col25 =new SimpleStringProperty(col25);
        this.col26 =new SimpleStringProperty(col26);
        this.col27 =new SimpleStringProperty(col27);
        this.col28 =new SimpleStringProperty(col28);
        this.col29 =new SimpleStringProperty(col29);
        this.col30 =new SimpleStringProperty(col30);
    }

    public TestDataVO(TestData data){
        String col3 = data.getCol3();


        this.id = data.getId()==null?new SimpleIntegerProperty(): new SimpleIntegerProperty(data.getId());
        this.col1 = new SimpleStringProperty(data.getCol1());
        this.col2 = new SimpleStringProperty(data.getCol2());
        this.col3 = new SimpleIntegerProperty(Integer.parseInt(col3));
        this.col4 = new SimpleStringProperty(data.getCol4());
        this.col5 = new SimpleStringProperty(data.getCol5());
        this.col6 = new SimpleStringProperty(data.getCol6());
        this.col7 = new SimpleStringProperty(data.getCol7());
        this.col8 = new SimpleStringProperty(data.getCol8());
        this.col9 = new SimpleStringProperty(data.getCol9());
        this.col10 =new SimpleStringProperty(data.getCol10());
        this.col11 =new SimpleStringProperty(data.getCol11());
        this.col12 =new SimpleStringProperty(data.getCol12());
        this.col13 =new SimpleStringProperty(data.getCol13());
        this.col14 =new SimpleStringProperty(data.getCol14());
        this.col15 =new SimpleStringProperty(data.getCol15());
        this.col16 =new SimpleStringProperty(data.getCol16());
        this.col17 =new SimpleStringProperty(data.getCol17());
        this.col18 =new SimpleStringProperty(data.getCol18());
        this.col19 =new SimpleStringProperty(data.getCol19());
        this.col20 =new SimpleStringProperty(data.getCol20());
        this.col21 =new SimpleStringProperty(data.getCol21());
        this.col22 =new SimpleStringProperty(data.getCol22());
        this.col23 =new SimpleStringProperty(data.getCol23());
        this.col24 =new SimpleStringProperty(data.getCol24());
        this.col25 =new SimpleStringProperty(data.getCol25());
        this.col26 =new SimpleStringProperty(data.getCol26());
        this.col27 =new SimpleStringProperty(data.getCol27());
        this.col28 =new SimpleStringProperty(data.getCol28());
        this.col29 =new SimpleStringProperty(data.getCol29());
        this.col30 =new SimpleStringProperty(data.getCol30());
    }

    public TestData convertToData(){
        return TestData.builder()
                .id(id.get())
                .col1(col1.get())
                .col2(col2.get())
                .col3(String.valueOf(col3.get()))
                .col4(col4.get())
                .col5(col5.get())
                .col6(col6.get())
                .col7(col7.get())
                .col8(col8.get())
                .col9(col9.get())
                .col10(col10.get())
                .col11(col11.get())
                .col12(col12.get())
                .col13(col13.get())
                .col14(col14.get())
                .col15(col15.get())
                .col16(col16.get())
                .col17(col17.get())
                .col18(col18.get())
                .col19(col19.get())
                .col20(col20.get())
                .col21(col21.get())
                .col22(col22.get())
                .col23(col23.get())
                .col24(col24.get())
                .col25(col25.get())
                .col26(col26.get())
                .col27(col27.get())
                .col28(col28.get())
                .col29(col29.get())
                .col30(col30.get())
                .build();

    }

    public TestDataVO() {
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getCol1() {
        return col1.get();
    }

    public SimpleStringProperty col1Property() {
        return col1;
    }

    public void setCol1(String col1) {
        this.col1.set(col1);
    }

    public String getCol2() {
        return col2.get();
    }

    public SimpleStringProperty col2Property() {
        return col2;
    }

    public void setCol2(String col2) {
        this.col2.set(col2);
    }

    public int getCol3() {
        return col3.get();
    }

    public SimpleIntegerProperty col3Property() {
        return col3;
    }

    public SimpleStringProperty col3StringProperty() {
        return new SimpleStringProperty(String.valueOf(col3.get()));
    }


    public void setCol3(int col3) {
        this.col3.set(col3);
    }

    public String getCol4() {
        return col4.get();
    }

    public SimpleStringProperty col4Property() {
        return col4;
    }

    public CustomerBoxEditorModel col4ComboboxModelProperty() {
        return ConstantComboModels.getComboModelByValue(col4.get(),ConstantComboModels.LIFE_TYPES);
    }

    public void setCol4(String col4) {
        this.col4.set(col4);
    }

    public String getCol5() {
        return col5.get();
    }

    public SimpleStringProperty col5Property() {
        return col5;
    }
    public SimpleObjectProperty<LocalDate> col5LocalDateProperty() {
        String s = col5.get();

        if(col5==null){
            return new SimpleObjectProperty<>();
        }else {
            try {
                LocalDate parse = LocalDate.parse(s);
                return new SimpleObjectProperty<>(parse);
            }catch (Exception e){
                System.out.println("格式转换异常：--");
                return new SimpleObjectProperty<>();
            }
        }
    }

    public void setCol5(String col5) {
        this.col5.set(col5);
    }
    public void setLocalDateCol5(LocalDate col5) {
        this.col5.set(col5==null?"":col5.toString());
    }
    public String getCol6() {
        return col6.get();
    }

    public SimpleObjectProperty<LocalTime> col6LocalTimeProperty() {
        String s = col6.get();

        if(col6==null){
            return new SimpleObjectProperty<>();
        }else {
            try {
                LocalTime parse = LocalTime.parse(s);
                return new SimpleObjectProperty<>(parse);
            }catch (Exception e){
                System.out.println("格式转换异常：--");
                return new SimpleObjectProperty<>();
            }
        }
    }


    public SimpleStringProperty col6Property() {
        return col6;
    }

    public void setLocalDateCol6(LocalTime col6) {
        this.col6.set(col6==null?"":col6.toString());
    }
    public void setCol6(String col6) {
        this.col6.set(col6);
    }

    public String getCol7() {
        return col7.get();
    }

    public SimpleStringProperty col7Property() {
        return col7;
    }


    public SimpleBooleanProperty col7BooleanProperty() {
        if(StringUtils.isBlank(col7.get())){
            return new SimpleBooleanProperty(false);
        }
        if(StringUtils.equals(col7.get(),"1")){
            return new SimpleBooleanProperty(true);
        }
        return new SimpleBooleanProperty(false);
    }

    public SimpleBooleanProperty col3BooleanProperty() {
        if(col3==null||col3.get()==0){
            return new SimpleBooleanProperty(false);
        }
        if(col3.get()==1){
            return new SimpleBooleanProperty(true);
        }
        return new SimpleBooleanProperty(false);
    }

    public void setCol7(String col7) {
        this.col7.set(col7);
    }

    public String getCol8() {
        return col8.get();
    }

    public SimpleStringProperty col8Property() {
        return col8;
    }

    public void setCol8(String col8) {
        this.col8.set(col8);
    }

    public String getCol9() {
        return col9.get();
    }

    public SimpleStringProperty col9Property() {
        return col9;
    }

    public void setCol9(String col9) {
        this.col9.set(col9);
    }

    public String getCol10() {
        return col10.get();
    }

    public CustomerBoxEditorModel col10ComboboxModelProperty(List<Map<String,Object>> list, String prop, String text) {
        return ConstantComboModels.getComboGridModelByValue(col10.get(),list,prop,text);
    }


    public SimpleStringProperty col10Property() {
        return col10;
    }

    public void setCol10(String col10) {
        this.col10.set(col10);
    }

    public String getCol11() {
        return col11.get();
    }

    public SimpleStringProperty col11Property() {
        return col11;
    }

    public void setCol11(String col11) {
        this.col11.set(col11);
    }

    public String getCol12() {
        return col12.get();
    }

    public SimpleStringProperty col12Property() {
        return col12;
    }

    public void setCol12(String col12) {
        this.col12.set(col12);
    }

    public String getCol13() {
        return col13.get();
    }

    public SimpleStringProperty col13Property() {
        return col13;
    }

    public void setCol13(String col13) {
        this.col13.set(col13);
    }

    public String getCol14() {
        return col14.get();
    }

    public SimpleStringProperty col14Property() {
        return col14;
    }

    public void setCol14(String col14) {
        this.col14.set(col14);
    }

    public String getCol15() {
        return col15.get();
    }

    public SimpleStringProperty col15Property() {
        return col15;
    }

    public void setCol15(String col15) {
        this.col15.set(col15);
    }

    public String getCol16() {
        return col16.get();
    }

    public SimpleStringProperty col16Property() {
        return col16;
    }

    public void setCol16(String col16) {
        this.col16.set(col16);
    }

    public String getCol17() {
        return col17.get();
    }

    public SimpleStringProperty col17Property() {
        return col17;
    }

    public void setCol17(String col17) {
        this.col17.set(col17);
    }

    public String getCol18() {
        return col18.get();
    }

    public SimpleStringProperty col18Property() {
        return col18;
    }

    public void setCol18(String col18) {
        this.col18.set(col18);
    }

    public String getCol19() {
        return col19.get();
    }

    public SimpleStringProperty col19Property() {
        return col19;
    }

    public void setCol19(String col19) {
        this.col19.set(col19);
    }

    public String getCol20() {
        return col20.get();
    }

    public SimpleStringProperty col20Property() {
        return col20;
    }

    public void setCol20(String col20) {
        this.col20.set(col20);
    }

    public String getCol21() {
        return col21.get();
    }

    public SimpleStringProperty col21Property() {
        return col21;
    }

    public void setCol21(String col21) {
        this.col21.set(col21);
    }

    public String getCol22() {
        return col22.get();
    }

    public SimpleStringProperty col22Property() {
        return col22;
    }

    public void setCol22(String col22) {
        this.col22.set(col22);
    }

    public String getCol23() {
        return col23.get();
    }

    public SimpleStringProperty col23Property() {
        return col23;
    }

    public void setCol23(String col23) {
        this.col23.set(col23);
    }

    public String getCol24() {
        return col24.get();
    }

    public SimpleStringProperty col24Property() {
        return col24;
    }

    public void setCol24(String col24) {
        this.col24.set(col24);
    }

    public String getCol25() {
        return col25.get();
    }

    public SimpleStringProperty col25Property() {
        return col25;
    }

    public void setCol25(String col25) {
        this.col25.set(col25);
    }

    public String getCol26() {
        return col26.get();
    }

    public SimpleStringProperty col26Property() {
        return col26;
    }

    public void setCol26(String col26) {
        this.col26.set(col26);
    }

    public String getCol27() {
        return col27.get();
    }

    public SimpleStringProperty col27Property() {
        return col27;
    }

    public void setCol27(String col27) {
        this.col27.set(col27);
    }

    public String getCol28() {
        return col28.get();
    }

    public SimpleStringProperty col28Property() {
        return col28;
    }

    public void setCol28(String col28) {
        this.col28.set(col28);
    }

    public String getCol29() {
        return col29.get();
    }

    public SimpleStringProperty col29Property() {
        return col29;
    }

    public void setCol29(String col29) {
        this.col29.set(col29);
    }

    public String getCol30() {
        return col30.get();
    }

    public SimpleStringProperty col30Property() {
        return col30;
    }

    public void setCol30(String col30) {
        this.col30.set(col30);
    }

    @Override
    public String toString() {
        return "TestDataVO{" +
                "id=" + id +
                ", col1=" + col1 +
                ", col2=" + col2 +
                ", col3=" + col3 +
                ", col4=" + col4 +
                ", col5=" + col5 +
                ", col6=" + col6 +
                ", col7=" + col7 +
                ", col8=" + col8 +
                ", col9=" + col9 +
                ", col10=" + col10 +
                ", col11=" + col11 +
                ", col12=" + col12 +
                ", col13=" + col13 +
                ", col14=" + col14 +
                ", col15=" + col15 +
                ", col16=" + col16 +
                ", col17=" + col17 +
                ", col18=" + col18 +
                ", col19=" + col19 +
                ", col20=" + col20 +
                ", col21=" + col21 +
                ", col22=" + col22 +
                ", col23=" + col23 +
                ", col24=" + col24 +
                ", col25=" + col25 +
                ", col26=" + col26 +
                ", col27=" + col27 +
                ", col28=" + col28 +
                ", col29=" + col29 +
                ", col30=" + col30 +
                '}';
    }
}
