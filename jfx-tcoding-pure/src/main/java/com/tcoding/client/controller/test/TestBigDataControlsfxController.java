package com.tcoding.client.controller.test;

import com.jfoenix.controls.JFXRippler;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXToggleButton;
import com.tcoding.client.bean.CustomerBoxEditorModel;
import com.tcoding.client.controller.test.model.TestDataVO;
import com.tcoding.client.extend.combogrid.ComboGridFactory;
import com.tcoding.client.extend.controlsfx.DatePickerTableCell;
import com.tcoding.client.extend.controlsfx.DateTimePickerTableCell;
import com.tcoding.client.extend.controlsfx.SwitchTableCell;
import com.tcoding.client.extend.controlsfx.TextField2TableCell;
import com.tcoding.client.extend.jfoenix.combogrid.ComboGridConfig;
import com.tcoding.client.extend.jfoenix.combogrid.ComboGridTableColumn;
import com.tcoding.client.extend.jfoenix.combogrid.JFXComboGrid;
import com.tcoding.client.extend.render.FilterTableExtend;
import com.tcoding.client.model.TestBigDataVOModel;
import com.tcoding.client.service.Request;
import com.tcoding.client.service.feign.admin.TestDataFeign;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.core.entity.TestData;
import io.datafx.controller.ViewController;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.tableview2.FilteredTableColumn;
import org.controlsfx.control.tableview2.FilteredTableView;
import org.controlsfx.control.tableview2.actions.ColumnFixAction;
import org.controlsfx.control.tableview2.actions.RowFixAction;
import org.controlsfx.control.tableview2.cell.ComboBox2TableCell;
import org.controlsfx.control.tableview2.filter.filtereditor.SouthFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupStringFilter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;

/**
 * @author 唐全成
 * @Date: 2022/9/13 14:04
 * @description
 **/
@ViewController(value = "/fxml/test/big_data_controlsfx.fxml", title = "大数据量表单测试")
public class TestBigDataControlsfxController {

    @FXML
    private StackPane rootPane;
    @FXML
    private JFXSpinner spinner;
    @FXML
    private JFXComboGrid testComboGrid;
    @FXML
    private Pagination pagination;
    @FXML
    private JFXRippler confirmSelectedRow;
    @FXML
    private FilteredTableView<TestDataVO> filteredTableView;
    @FXML
    private FilteredTableColumn<TestDataVO,String> col1Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col2Column;
    @FXML
    private FilteredTableColumn<TestDataVO, Boolean> col3Column;
    @FXML
    private FilteredTableColumn<TestDataVO, CustomerBoxEditorModel> col4Column;
    @FXML
    private FilteredTableColumn<TestDataVO, LocalDate> col5Column;
    @FXML
    private FilteredTableColumn<TestDataVO, LocalTime> col6Column;
    @FXML
    private FilteredTableColumn<TestDataVO, Boolean> col7Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col8Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col9Column;
    @FXML
    private FilteredTableColumn<TestDataVO, CustomerBoxEditorModel> col10Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col11Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col12Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col13Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col14Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col15Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col16Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col17Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col18Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col19Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col20Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col21Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col22Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col23Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col24Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col25Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col26Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col27Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col28Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col29Column;
    @FXML
    private FilteredTableColumn<TestDataVO, String> col30Column;

    @FXML
    private AnchorPane anchorPane;
    private Integer page = 1;
    private Integer total=0;
    private Map<String,Object> queryParams = new HashMap<>();
    private SouthFilter<TestDataVO, String> editorColor;
    @Inject
    private TestBigDataVOModel testBigDataModel;
    @FXML
    private JFXToggleButton customSearch;
    @FXML
    private JFXToggleButton showRowIndex;
    @FXML
    private JFXRippler search;
    @FXML
    private FilterTableExtend<TestDataVO> filterTableExtend;

    @PostConstruct
    public void init(){
        filterTableExtend = new FilterTableExtend<>();
        //初始化下拉表格
        ComboGridFactory.initShipSelect(testComboGrid,200.0);
        //初始化列值绑定
        initCellValueFactory();
        //初始化单元格自定义展示
        initCellFactory();
        //初始化点击动作
        initActions();
        //注册表格扩展
        filterTableExtend.register(filteredTableView,testBigDataModel.getTestDatas(), e-> testBigDataModel.getTestDatas().add(new TestDataVO()));
        //查询数据
        query();
    }

    /**
     * 查询数据
     */
    private void query() {
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    testBigDataModel.getTestDatas().clear();
                    ApplicationStore.showProgress();
                    rootPane.setDisable(true);
                    queryParams.put("page",page);
                    queryParams.put("limit",20);
                })
                .addSupplierInExecutor(
                        () -> Request.connector(TestDataFeign.class).getPageData(queryParams)
                )
                .addConsumerInPlatformThread(result -> {
                    List<TestData> datas = result.getDatas();
                    for (TestData testData : datas) {
                        testBigDataModel.getTestDatas().add(new TestDataVO(testData));
                    }
                    long total = result.getTotal();
                    pagination.setPageCount(new BigDecimal(total).intValue()/20);
                })
                .withFinal(() -> {
                    ApplicationStore.hideProgress();
                    rootPane.setDisable(false);
                    //注册自定义下拉过滤
                    initCustomerSouthFilter();
                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    /**
     * 初始化点击动作
     */
    private void initActions(){
        search.setOnMouseClicked(e-> query());
        pagination.currentPageIndexProperty().addListener((observable,o,n)->{
            page=n.intValue()+1;
            query();
        });
        confirmSelectedRow.setOnMouseClicked(e->{
            TestDataVO selectedItem = filteredTableView.getSelectionModel().getSelectedItem();
            System.out.println(selectedItem.toString());
        });

        showRowIndex.selectedProperty().addListener((obs,o,n)-> filteredTableView.setRowHeaderVisible(n));
        customSearch.selectedProperty().addListener((obs,o,n)-> filterTableExtend.setupFilter(n));
    }

    /**
     * 初始化自定义下拉搜索
     */
    private void initCustomerSouthFilter(){
        editorColor = new SouthFilter<>(col9Column, String.class);
        editorColor.getFilterEditor().setCellFactory(c -> new ListCell<String>() {
            private final HBox box;
            private final Circle circle;
            {
                circle = new Circle(10);
                box = new HBox(circle);
                box.setAlignment(Pos.CENTER);
                setText(null);
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && ! empty) {
                    circle.setFill(Color.web(item));
                    setGraphic(box);
                } else {
                    setGraphic(null);
                }
            }

        });
        filterTableExtend.registerCustomerSouthFilter(col9Column,editorColor);
    }

    /**
     * 初始化cellValueFactory
     */
    private void initCellValueFactory(){
        setupCellValueFactory(col1Column,TestDataVO::col1Property );
        setupCellValueFactory(col2Column,TestDataVO::col2Property );
        setupCellValueFactory(col3Column,TestDataVO::col3BooleanProperty );
        setupCellValueFactory(col4Column,TestDataVO::col4ComboboxModelProperty);
        setupCellValueFactory(col5Column,TestDataVO::col5LocalDateProperty );
        setupCellValueFactory(col6Column,TestDataVO::col6LocalTimeProperty );
        setupCellValueFactory(col7Column,TestDataVO::col7BooleanProperty );
        setupCellValueFactory(col8Column,TestDataVO::col8Property);
        setupCellValueFactory(col9Column,TestDataVO::col9Property);
        setupCellValueFactory(col11Column,TestDataVO::col11Property );
        setupCellValueFactory(col12Column,TestDataVO::col12Property );
        setupCellValueFactory(col13Column,TestDataVO::col13Property );
        setupCellValueFactory(col14Column,TestDataVO::col14Property );
        setupCellValueFactory(col15Column,TestDataVO::col15Property );
        setupCellValueFactory(col16Column,TestDataVO::col16Property );
        setupCellValueFactory(col17Column,TestDataVO::col17Property );
        setupCellValueFactory(col18Column,TestDataVO::col18Property );
        setupCellValueFactory(col19Column,TestDataVO::col19Property );
        setupCellValueFactory(col20Column,TestDataVO::col20Property );
        setupCellValueFactory(col21Column,TestDataVO::col21Property );
        setupCellValueFactory(col22Column,TestDataVO::col22Property );
        setupCellValueFactory(col23Column,TestDataVO::col23Property );
        setupCellValueFactory(col24Column,TestDataVO::col24Property );
        setupCellValueFactory(col25Column,TestDataVO::col25Property );
        setupCellValueFactory(col26Column,TestDataVO::col26Property );
        setupCellValueFactory(col27Column,TestDataVO::col27Property );
        setupCellValueFactory(col28Column,TestDataVO::col28Property );
        setupCellValueFactory(col29Column,TestDataVO::col29Property );
        setupCellValueFactory(col30Column,TestDataVO::col30Property );
    }
    private <T> void setupCellValueFactory(FilteredTableColumn<TestDataVO, T> column, Function<TestDataVO, ObservableValue<T>> mapper) {
        column.setCellValueFactory((FilteredTableColumn.CellDataFeatures<TestDataVO, T> param) -> mapper.apply(param.getValue()));
    }
    /**
     * 初始化单元格自定义展示
     */
    private void initCellFactory(){
        col1Column.setCellFactory(TextField2TableCell.forTableColumn());
        col15Column.setCellFactory(TextField2TableCell.forTableColumn());
        col16Column.setCellFactory(TextField2TableCell.forTableColumn());
        col17Column.setCellFactory(TextField2TableCell.forTableColumn());

        col2Column.setCellFactory(ComboBox2TableCell.forTableColumn("Name 1", "Name 2", "Name 3", "Name 4"));

        col3Column.setCellFactory(SwitchTableCell.forTableColumn(event -> {
            Map<String, Object> paramMap = event.getParamMap();
            Boolean data = (Boolean)paramMap.get("data");
            Integer row = (Integer) paramMap.get("row");
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol3(data?1:0);
        }));

        col7Column.setCellFactory(CheckBoxTableCell.forTableColumn(col7Column));

        col9Column.setCellFactory(p -> new TableCell<TestDataVO, String>() {
            private final HBox box;
            private final Circle circle;
            {
                circle = new Circle(10);
                box = new HBox(circle);
                box.setAlignment(Pos.CENTER);
                setText(null);
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && ! empty) {
                    circle.setFill(Color.web(item));
                    setGraphic(box);
                } else {
                    setGraphic(null);
                }
            }
        });

        col13Column.setCellFactory(DatePickerTableCell.forTableColumn());
        col13Column.setOnEditCommit(e->{
            int row = e.getTablePosition().getRow();
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol13(e.getNewValue());
        });

        col14Column.setCellFactory(DateTimePickerTableCell.forTableColumn());
        col14Column.setOnEditCommit(e->{
            int row = e.getTablePosition().getRow();
            TestDataVO testDataVO = testBigDataModel.getTestDatas().get(row);
            testDataVO.setCol14(e.getNewValue());
        });
    }
}
