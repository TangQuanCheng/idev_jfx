package com.tcoding.client.controller.test;

import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSpinner;
import com.tcoding.client.controller.feature.FeatureResourceConsumer;
import com.tcoding.client.model.CustomerSearchModel;
import com.tcoding.client.model.TestBigDataModel;
import com.tcoding.client.service.Request;
import com.tcoding.client.service.feign.admin.TestDataFeign;
import com.tcoding.client.utils.Pinyin4jUtil;
import com.tcoding.client.utils.SvgGraphicUtil;
import com.tcoding.core.entity.TestData;
import com.tcoding.core.entity.log.SysLoginInfor;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.core.concurrent.ProcessChain;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author 唐全成
 * @Date: 2022/8/4 10:43
 * @description
 **/
@ViewController(value = "/fxml/test/scroll_sync.fxml", title = "同步滚动测试")
public class TestScrollSync {
    @FXMLViewFlowContext
    private ViewFlowContext flowContext;
    @ActionHandler
    private FlowActionHandler actionHandler;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox contentPane;
    @FXML
    private TextField userNameTextField;
    @FXML
    private TextField ipTextfiled;
    @FXML
    private ScrollPane scrollPane1;
    @FXML
    private ScrollPane scrollPane2;

    @FXML
    private TableView<TestData> tableView;
    @FXML
    private TableView<TestData> tableView2;
    @FXML
    private JFXSpinner spinner;
    @FXML
    @ActionTrigger("rest")
    private Button restButton;
    @FXML
    @ActionTrigger("search")
    private Button searchButton;
    @FXML
    private HBox gridBar;
    @FXML
    private SplitPane splitPane;
    @FXML
    private TableColumn<SysLoginInfor, String> numberColumn;
    @FXML
    private TableColumn<TestData, String> col1Column;
    @FXML
    private TableColumn<TestData, String> col2Column;
    @FXML
    private TableColumn<TestData, String> col3Column;
    @FXML
    private TableColumn<TestData, String> col4Column;
    @FXML
    private TableColumn<TestData, String> col5Column;
    @FXML
    private TableColumn<TestData, String> col6Column;
    @FXML
    private TableColumn<TestData, String> col7Column;
    @FXML
    private TableColumn<TestData, String> col8Column;
    @FXML
    private TableColumn<TestData, String> col9Column;
    @FXML
    private TableColumn<TestData, String> col10Column;
    @FXML
    private TableColumn<TestData, String> col11Column;
    @FXML
    private TableColumn<TestData, String> col12Column;
    @FXML
    private TableColumn<TestData, String> col13Column;
    @FXML
    private TableColumn<TestData, String> col14Column;
    @FXML
    private TableColumn<TestData, String> col15Column;
    @FXML
    private TableColumn<TestData, String> col16Column;
    @FXML
    private TableColumn<TestData, String> col17Column;
    @FXML
    private TableColumn<TestData, String> col18Column;
    @FXML
    private TableColumn<TestData, String> col19Column;
    @FXML
    private TableColumn<TestData, String> col20Column;
    @FXML
    private TableColumn<TestData, String> col21Column;
    @FXML
    private TableColumn<TestData, String> col22Column;
    @FXML
    private TableColumn<TestData, String> col23Column;
    @FXML
    private TableColumn<TestData, String> col24Column;
    @FXML
    private TableColumn<TestData, String> col25Column;
    @FXML
    private TableColumn<TestData, String> col26Column;
    @FXML
    private TableColumn<TestData, String> col27Column;
    @FXML
    private TableColumn<TestData, String> col28Column;
    @FXML
    private TableColumn<TestData, String> col29Column;
    @FXML
    private TableColumn<TestData, String> col30Column;


    @FXML
    private TableColumn<SysLoginInfor, String> numberColumn2;
    @FXML
    private TableColumn<TestData, String> col1Column2;
    @FXML
    private TableColumn<TestData, String> col2Column2;
    @FXML
    private TableColumn<TestData, String> col3Column2;
    @FXML
    private TableColumn<TestData, String> col4Column2;
    @FXML
    private TableColumn<TestData, String> col5Column2;
    @FXML
    private TableColumn<TestData, String> col6Column2;
    @FXML
    private TableColumn<TestData, String> col7Column2;
    @FXML
    private TableColumn<TestData, String> col8Column2;
    @FXML
    private TableColumn<TestData, String> col9Column2;
    @FXML
    private TableColumn<TestData, String> col10Column2;
    @FXML
    private TableColumn<TestData, String> col11Column2;
    @FXML
    private TableColumn<TestData, String> col12Column2;
    @FXML
    private TableColumn<TestData, String> col13Column2;
    @FXML
    private TableColumn<TestData, String> col14Column2;
    @FXML
    private TableColumn<TestData, String> col15Column2;
    @FXML
    private TableColumn<TestData, String> col16Column2;
    @FXML
    private TableColumn<TestData, String> col17Column2;
    @FXML
    private TableColumn<TestData, String> col18Column2;
    @FXML
    private TableColumn<TestData, String> col19Column2;
    @FXML
    private TableColumn<TestData, String> col20Column2;
    @FXML
    private TableColumn<TestData, String> col21Column2;
    @FXML
    private TableColumn<TestData, String> col22Column2;
    @FXML
    private TableColumn<TestData, String> col23Column2;
    @FXML
    private TableColumn<TestData, String> col24Column2;
    @FXML
    private TableColumn<TestData, String> col25Column2;
    @FXML
    private TableColumn<TestData, String> col26Column2;
    @FXML
    private TableColumn<TestData, String> col27Column2;
    @FXML
    private TableColumn<TestData, String> col28Column2;
    @FXML
    private TableColumn<TestData, String> col29Column2;
    @FXML
    private TableColumn<TestData, String> col30Column2;

    private ScrollBar scroll;

    @FXML
    private Label dataCount;

    @FXML
    private Label loadingDuration;

    @Inject
    private TestBigDataModel testBigDataModel;

    @Inject
    private CustomerSearchModel customerSearchModel;

    private JFXSnackbar snackbar;

    @Inject
    private FeatureResourceConsumer featureResourceConsumer;


    @PostConstruct
    private void init() {

        featureResourceConsumer.consumeResource(this);

        searchButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("search", 12, Color.WHITE));
        restButton.setGraphic(SvgGraphicUtil.buildSvgGraphic("refresh", 12, Color.GRAY));
        spinner.setVisible(false);
        snackbar = new JFXSnackbar(rootPane);
        snackbar.setPrefWidth(500);
        numberColumn.setCellFactory((col) -> new TableCell<SysLoginInfor, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);

                if (!empty) {
                    int rowIndex = this.getIndex() + 1;
                    this.setText(String.valueOf(rowIndex));
                }
            }
        });
        col1Column.setCellValueFactory(new PropertyValueFactory<>("col1"));
        col2Column.setCellValueFactory(new PropertyValueFactory<>("col2"));
        col3Column.setCellValueFactory(new PropertyValueFactory<>("col3"));
        col4Column.setCellValueFactory(new PropertyValueFactory<>("col4"));
        col5Column.setCellValueFactory(new PropertyValueFactory<>("col5"));
        col6Column.setCellValueFactory(new PropertyValueFactory<>("col6"));
        col7Column.setCellValueFactory(new PropertyValueFactory<>("col7"));
        col8Column.setCellValueFactory(new PropertyValueFactory<>("col8"));
        col9Column.setCellValueFactory(new PropertyValueFactory<>("col9"));
        col10Column.setCellValueFactory(new PropertyValueFactory<>("col10"));
        col11Column.setCellValueFactory(new PropertyValueFactory<>("col11"));
        col12Column.setCellValueFactory(new PropertyValueFactory<>("col12"));
        col13Column.setCellValueFactory(new PropertyValueFactory<>("col13"));
        col14Column.setCellValueFactory(new PropertyValueFactory<>("col14"));
        col15Column.setCellValueFactory(new PropertyValueFactory<>("col15"));
        col16Column.setCellValueFactory(new PropertyValueFactory<>("col16"));
        col17Column.setCellValueFactory(new PropertyValueFactory<>("col17"));
        col18Column.setCellValueFactory(new PropertyValueFactory<>("col18"));
        col19Column.setCellValueFactory(new PropertyValueFactory<>("col19"));
        col20Column.setCellValueFactory(new PropertyValueFactory<>("col20"));
        col21Column.setCellValueFactory(new PropertyValueFactory<>("col21"));
        col22Column.setCellValueFactory(new PropertyValueFactory<>("col22"));
        col23Column.setCellValueFactory(new PropertyValueFactory<>("col23"));
        col24Column.setCellValueFactory(new PropertyValueFactory<>("col24"));
        col25Column.setCellValueFactory(new PropertyValueFactory<>("col25"));
        col26Column.setCellValueFactory(new PropertyValueFactory<>("col26"));
        col27Column.setCellValueFactory(new PropertyValueFactory<>("col27"));
        col28Column.setCellValueFactory(new PropertyValueFactory<>("col28"));
        col29Column.setCellValueFactory(new PropertyValueFactory<>("col29"));
        col30Column.setCellValueFactory(new PropertyValueFactory<>("col30"));


        numberColumn2.setCellFactory((col) -> new TableCell<SysLoginInfor, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);

                if (!empty) {
                    int rowIndex = this.getIndex() + 1;
                    this.setText(String.valueOf(rowIndex));
                }
            }
        });
        col1Column2.setCellValueFactory(new PropertyValueFactory<>("col1"));
        col2Column2.setCellValueFactory(new PropertyValueFactory<>("col2"));
        col3Column2.setCellValueFactory(new PropertyValueFactory<>("col3"));
        col4Column2.setCellValueFactory(new PropertyValueFactory<>("col4"));
        col5Column2.setCellValueFactory(new PropertyValueFactory<>("col5"));
        col6Column2.setCellValueFactory(new PropertyValueFactory<>("col6"));
        col7Column2.setCellValueFactory(new PropertyValueFactory<>("col7"));
        col8Column2.setCellValueFactory(new PropertyValueFactory<>("col8"));
        col9Column2.setCellValueFactory(new PropertyValueFactory<>("col9"));
        col10Column2.setCellValueFactory(new PropertyValueFactory<>("col10"));
        col11Column2.setCellValueFactory(new PropertyValueFactory<>("col11"));
        col12Column2.setCellValueFactory(new PropertyValueFactory<>("col12"));
        col13Column2.setCellValueFactory(new PropertyValueFactory<>("col13"));
        col14Column2.setCellValueFactory(new PropertyValueFactory<>("col14"));
        col15Column2.setCellValueFactory(new PropertyValueFactory<>("col15"));
        col16Column2.setCellValueFactory(new PropertyValueFactory<>("col16"));
        col17Column2.setCellValueFactory(new PropertyValueFactory<>("col17"));
        col18Column2.setCellValueFactory(new PropertyValueFactory<>("col18"));
        col19Column2.setCellValueFactory(new PropertyValueFactory<>("col19"));
        col20Column2.setCellValueFactory(new PropertyValueFactory<>("col20"));
        col21Column2.setCellValueFactory(new PropertyValueFactory<>("col21"));
        col22Column2.setCellValueFactory(new PropertyValueFactory<>("col22"));
        col23Column2.setCellValueFactory(new PropertyValueFactory<>("col23"));
        col24Column2.setCellValueFactory(new PropertyValueFactory<>("col24"));
        col25Column2.setCellValueFactory(new PropertyValueFactory<>("col25"));
        col26Column2.setCellValueFactory(new PropertyValueFactory<>("col26"));
        col27Column2.setCellValueFactory(new PropertyValueFactory<>("col27"));
        col28Column2.setCellValueFactory(new PropertyValueFactory<>("col28"));
        col29Column2.setCellValueFactory(new PropertyValueFactory<>("col29"));
        col30Column2.setCellValueFactory(new PropertyValueFactory<>("col30"));

        FilteredList<TestData> filteredData = new FilteredList<>(testBigDataModel.getTestDatas(), p -> true);
        tableView.setItems(filteredData);
        tableView2.setItems(filteredData);
        userNameTextField.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol1()).contains(val);
            });
        });
        ipTextfiled.textProperty().addListener((o, oldVal, newVal) -> {
            filteredData.setPredicate(elementProp -> {
                if (newVal == null || newVal.isEmpty()) {
                    return true;
                }
                String val = Pinyin4jUtil.toPinYinLowercase(newVal);
                return Pinyin4jUtil.toPinYinLowercase(elementProp.getCol2()).contains(val);
            });
        });

        testBigDataModel.selectedIndexProperty().bind(tableView.getSelectionModel().selectedIndexProperty());

        scrollPane1.hvalueProperty().bindBidirectional(scrollPane2.hvalueProperty());
        query();


    }

    private void query() {
        long start = System.currentTimeMillis();
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    testBigDataModel.getTestDatas().clear();
                    spinner.setVisible(true);
                    contentPane.setDisable(true);
                })
                .addSupplierInExecutor(
                        () -> Request.connector(TestDataFeign.class).getAllData()
                )
                .addConsumerInPlatformThread(result -> {

                    testBigDataModel.getTestDatas().addAll(result);
                    dataCount.setText("加载数据量：" + result.size());
                    long duration = System.currentTimeMillis() - start;
                    loadingDuration.setText("请求耗时：" + duration + "ms");

                })
                .withFinal(() -> {
                    spinner.setVisible(false);
                    contentPane.setDisable(false);

                })
                .onException(e -> e.printStackTrace())
                .run();
    }

    @ActionMethod("search")
    private void search() {
        query();
    }

    @ActionMethod("rest")
    private void rest() {
        userNameTextField.setText("");
        ipTextfiled.setText("");
    }
}
