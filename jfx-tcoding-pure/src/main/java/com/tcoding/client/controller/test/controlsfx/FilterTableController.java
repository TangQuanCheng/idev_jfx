package com.tcoding.client.controller.test.controlsfx;

import io.datafx.controller.ViewController;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.table.TableFilter;

import javax.annotation.PostConstruct;

/**
 * @author 唐全成
 * @Date: 2022/9/9 10:42
 * @description
 **/
@ViewController(value = "/fxml/controlsfx/filterTable.fxml", title = "大数据量测试")
public class FilterTableController {
    @FXML
    private StackPane rootPane;
    private TableFilter<Person> tableFilter;
    private TableColumn<Person, String> firstNameCol;
    private TableColumn<Person, String> lastNameCol;
    private TableColumn<Person, String> emailCol;
    private SplitPane splitPane;

    @PostConstruct
    public void init(){
        splitPane=new SplitPane();

        TableView<Person> tableView = new TableView<>();

        firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        emailCol = new TableColumn<>("Email");
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));

        tableView.getColumns().addAll(firstNameCol, lastNameCol, emailCol);

        tableView.getItems().add(new Person("Jacob", "Smith", "jacob.smith@example.com"));
        tableView.getItems().add(new Person("Isabella", "Johnson", "isabella.johnson@example.com"));
        tableView.getItems().add(new Person("Ethan", "Williams", "ethan.williams@example.com"));
        tableView.getItems().add(new Person("Emma", "Jones", "emma.jones@example.com"));
        tableView.getItems().add(new Person("Michael", "Brown", "michael.brown@example.com"));
        tableView.getItems().add(new Person("Isabella", "Smith", "isabella.smith@example.com"));

        // apply filter
        tableFilter = TableFilter.forTableView(tableView).apply();
        splitPane.getItems().add(tableView);
        splitPane.getItems().add(getControlPanel());
        rootPane.getChildren().add(splitPane);
    }

    public Node getControlPanel() {
        VBox controlPane = new VBox(10);

        String sFirstname = "Isabella";
        Button buttonFilterFirstname = new Button("Filter First Name '" + sFirstname + "'");
        buttonFilterFirstname.setOnAction(e -> {
            tableFilter.unSelectAllValues(firstNameCol);
            tableFilter.selectValue(firstNameCol, sFirstname);
            tableFilter.executeFilter();
        });
        Button buttonFilterFirstnameReset = new Button("\u21BA");
        buttonFilterFirstnameReset.setOnAction(e -> {
            tableFilter.selectAllValues(firstNameCol);
            tableFilter.executeFilter();
        });

        String sLastname = "Smith";
        Button buttonFilterLastname = new Button("Filter Last Name '" + sLastname + "'");
        buttonFilterLastname.setOnAction(e -> {
            tableFilter.unSelectAllValues(lastNameCol);
            tableFilter.selectValue(lastNameCol, sLastname);
            tableFilter.executeFilter();
        });
        Button buttonFilterLastnamReset = new Button("\u21BA");
        buttonFilterLastnamReset.setOnAction(e -> {
            tableFilter.selectAllValues(lastNameCol);
            tableFilter.executeFilter();
        });

        String sEmail = "michael.brown@example.com";
        Button buttonFilterEmail = new Button("Filter Email '" + sEmail + "'");
        buttonFilterEmail.setOnAction(e -> {
            tableFilter.unSelectAllValues(emailCol);
            tableFilter.selectValue(emailCol, sEmail);
            tableFilter.executeFilter();
        });
        Button buttonFilterEmailReset = new Button("\u21BA");
        buttonFilterEmailReset.setOnAction(e -> {
            tableFilter.selectAllValues(emailCol);
            tableFilter.executeFilter();
        });

        Button buttonResetAll = new Button("Reset All Filters \u21BA");
        buttonResetAll.setOnAction(e -> {
            tableFilter.resetFilter();
        });

        CheckBox cbRegularExpSearchStrategy = new CheckBox("Use regular expressions");
        cbRegularExpSearchStrategy.selectedProperty().addListener((ov, old_val, new_val) -> {
            if(cbRegularExpSearchStrategy.isSelected()) {
                tableFilter.setSearchStrategy((input,target) -> {
                    try {
                        return target.matches(input);
                    } catch (Exception e) {
                        return false;
                    }
                });
            } else {
                // fallback to normal
                tableFilter.setSearchStrategy((inputString, subjectString) -> subjectString.toLowerCase().contains(inputString.toLowerCase()));
            }
        });

        controlPane.getChildren().add(new Label("Programmatic Filtering:"));
        controlPane.getChildren().add(new HBox(10, buttonFilterFirstname, buttonFilterFirstnameReset));
        controlPane.getChildren().add(new HBox(10,buttonFilterLastname, buttonFilterLastnamReset));
        controlPane.getChildren().add(new HBox(10,buttonFilterEmail, buttonFilterEmailReset));
        buttonResetAll.setPrefWidth(Double.MAX_VALUE);
        controlPane.getChildren().add(buttonResetAll);
        controlPane.getChildren().add(new Label("Custom Search Strategies (e.g., '.*o.*n' for 'Last Name')"));
        controlPane.getChildren().add(cbRegularExpSearchStrategy);

        return controlPane;
    }

    public static class Person {

        private final SimpleStringProperty firstName;
        private final SimpleStringProperty lastName;
        private final SimpleStringProperty email;

        public Person(String firstName, String lastName, String email) {
            this.firstName = new SimpleStringProperty(firstName);
            this.lastName = new SimpleStringProperty(lastName);
            this.email = new SimpleStringProperty(email);
        }

        public String getFirstName() {
            return firstName.get();
        }

        public void setFirstName(String firstName) {
            this.firstName.set(firstName);
        }

        public String getLastName() {
            return lastName.get();
        }

        public void setLastName(String lastName) {
            this.lastName.set(lastName);
        }

        public String getEmail() {
            return email.get();
        }

        public void setEmail(String fName) {
            email.set(fName);
        }
    }
}
