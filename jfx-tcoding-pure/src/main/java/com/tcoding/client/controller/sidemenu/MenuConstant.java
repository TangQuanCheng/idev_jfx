package com.tcoding.client.controller.sidemenu;

import javafx.scene.control.Tab;

import java.util.LinkedHashMap;

/**
 * @author 唐全成
 * @Date: 2022/9/6 16:14
 * @description
 **/
public class MenuConstant {
    public static final LinkedHashMap<String, Tab> TABS_MAP = new LinkedHashMap<>();
    public static final LinkedHashMap<String, String> GLOBAL_LINK_MAP = new LinkedHashMap<>();

}
