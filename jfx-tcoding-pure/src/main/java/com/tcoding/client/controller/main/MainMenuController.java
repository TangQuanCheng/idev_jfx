package com.tcoding.client.controller.main;

import com.jfoenix.controls.JFXListView;
import com.tcoding.client.store.ApplicationStore;
import com.tcoding.client.websocket.WSClient;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.context.FlowActionHandler;
import javafx.fxml.FXML;

/**
 * @author 唐全成
 * @Date: 2022/6/9 16:07
 * @description
 **/
@ViewController("/fxml/ui/popup/MainPopup.fxml")
public class MainMenuController {
    @FXML
    private JFXListView<?> toolbarPopupList;

    private FlowActionHandler actionHandler;

    public MainMenuController(FlowActionHandler actionHandler) {
        this.actionHandler = actionHandler;
    }

    public MainMenuController() {
    }

    @FXML
    private void submit() {
        if (toolbarPopupList.getSelectionModel().getSelectedIndex() == 1) {

            try {
                actionHandler.navigate(LoginController.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ApplicationStore.setName("");
            ApplicationStore.getAllMenu().clear();
            ApplicationStore.getMenus().clear();
            ApplicationStore.getElements().clear();
            ApplicationStore.getPermissionMenus().clear();
            ApplicationStore.getRoles().clear();
            WSClient.getInstance().close();
        }
    }

}
