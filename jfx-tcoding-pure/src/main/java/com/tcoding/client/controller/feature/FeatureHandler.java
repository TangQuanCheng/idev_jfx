package com.tcoding.client.controller.feature;

import com.tcoding.client.store.ApplicationStore;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;

import java.util.Map;

public class FeatureHandler {

    private static FeatureHandler instance;

    private Map<String, String> permissionInfoMap;

    private FeatureHandler() {
    }

    public static synchronized FeatureHandler getInstance() {
        if (instance == null) {
            instance = new FeatureHandler();
        }
        return instance;
    }




    public void hideByFeature(Node node, String featureName) {
        node.visibleProperty().bind(new SimpleBooleanProperty(ApplicationStore.getFeatureMap().get(featureName)!=null));
        node.managedProperty().bind(node.visibleProperty());
    }

    public void disableByFeature(Node node, String featureName) {
        node.disableProperty().bind(new SimpleBooleanProperty(ApplicationStore.getFeatureMap().get(featureName)!=null));
    }

}
