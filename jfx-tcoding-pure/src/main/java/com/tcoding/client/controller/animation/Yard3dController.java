package com.tcoding.client.controller.animation;

import io.datafx.controller.ViewController;
import javafx.fxml.FXML;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;

import javax.annotation.PostConstruct;

/**
 * @author 唐全成
 * @Date: 2022/6/7 15:06
 * @description
 **/
@ViewController(value = "/fxml/3D/yard.fxml")
public class Yard3dController {

    private double anchorX, anchorY, anchorAngleX, anchorAngleY;

    @FXML
    private Group rootPane;

    @PostConstruct
    public void init() throws Exception{


        Box zis = new Box(1, 1, 200);
        zis.setMaterial(new PhongMaterial(Color.MEDIUMSEAGREEN));
        zis.setTranslateZ(100);
        buildBackgroud();
        buildBoxes();
        rootPane.getChildren().add(zis);

        Camera camera = new PerspectiveCamera(true);
        camera.setFarClip(5000);
        Scene scene = new Scene(rootPane, 1280, 720, true);
        scene.setFill(Color.BLACK);
        scene.setCamera(camera);

        initMouseControl();
    }


    private void buildBoxes(){

        for(int i = -5;i<5;i++){
            for(int j = -5;j<5;j++){
                Box box = new Box(100, 50, 50);
                box.setMaterial(new PhongMaterial(Color.RED));
                box.setTranslateX(120*i);
                box.setTranslateY(60*j);
                rootPane.getChildren().add(box);
            }
        }
    }


    private void buildBackgroud(){

        for(int i = -20;i<20;i++){
            Line linex = new Line(-500,i*50,500,i*50);
            linex.setFill(Color.GREEN);
            linex.setStrokeWidth(0.2);
            linex.getStrokeDashArray().add(3d);
            rootPane.getChildren().add(linex);

            Line liney = new Line(i*50,-500,i*50,500);
            liney.setFill(Color.RED);
            liney.setStrokeWidth(0.2);
            liney.getStrokeDashArray().add(3d);
            rootPane.getChildren().add(liney);
        }

    }



    private void initMouseControl() {
        Rotate xRotate = new Rotate(0, Rotate.X_AXIS);
        Rotate yRotate = new Rotate(0, Rotate.Y_AXIS);
        rootPane.getTransforms().addAll(xRotate, yRotate);

        rootPane.setOnMousePressed(event -> {
            anchorX = event.getSceneX();
            anchorY = event.getSceneY();
            anchorAngleX = xRotate.getAngle();
            anchorAngleY = yRotate.getAngle();
        });

        rootPane.setOnMouseDragged(event -> {
            xRotate.setAngle(anchorAngleX - (anchorY - event.getSceneY()));
            double ay= anchorAngleY + (anchorX - event.getSceneX());
            yRotate.setAngle(Math.abs(ay)>15?(ay>0?15:-15):ay);
        });

//        group.setOnScroll(event -> group.translateZProperty().set(group.getTranslateZ() + event.getDeltaY()));
    }
}
