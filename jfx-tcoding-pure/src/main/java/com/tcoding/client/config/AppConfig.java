package com.tcoding.client.config;

import io.datafx.controller.injection.scopes.ViewScoped;

import java.util.Properties;

/**
 * @author 唐全成
 * @Date: 2022/8/3 14:29
 * @description
 **/
@ViewScoped
public class AppConfig {

    public static Properties properties;

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperties(Properties properties) {
        AppConfig.properties = properties;
    }
}
